package daq.rc;

import java.util.ArrayList;
import java.util.List;

import daq.rc.RCException.BadCommandException;
import daq.rc.internal.Controllable;
import daq.rc.internal.FSM_STATE;
import daq.rc.internal.ResynchCmd;
import daq.rc.internal.StringVector;
import daq.rc.internal.SubTransitionCmd;
import daq.rc.internal.TransitionCmd;
import daq.rc.internal.UserCmd;


/**
 * This is a proxy class used to completely hide the generated {@link Controllable} class to the users.
 * <p>
 * This {@link ControllableProxy} wraps all the {@link JControllable} methods catching thrown exceptions and translating them to
 * {@link RuntimeException} exceptions. That allows the C++ JNI layer to properly handle the exception.
 */
class ControllableProxy extends Controllable {
    private final JControllable ctrl;

    ControllableProxy(final JControllable ctrl) {
        this.ctrl = ctrl;
    }

    @Override
    public void publish() {
        try {
            this.ctrl.publish();
        }
        catch(final ers.Issue ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    @Override
    public void publishFullStats() {
        try {
            this.ctrl.publishFullStats();
        }
        catch(final ers.Issue ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    @Override
    public void user(final UserCmd usrCmd) {
        try {
            final daq.rc.Command.UserCmd uc = (daq.rc.Command.UserCmd) daq.rc.Command.buildCommand(usrCmd.serialize());
            this.ctrl.user(uc);
        }
        catch(final ers.Issue | BadCommandException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    @Override
    public void enable(final StringVector components) {
        final List<String> cmpt = new ArrayList<>();
        for(int idx = 0; idx < components.size(); ++idx) {
            cmpt.add(components.get(idx));
        }

        try {
            this.ctrl.enable(cmpt);
        }
        catch(final ers.Issue ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    @Override
    public void disable(final StringVector components) {
        final List<String> cmpt = new ArrayList<>();
        for(int idx = 0; idx < components.size(); ++idx) {
            cmpt.add(components.get(idx));
        }

        try {
            this.ctrl.disable(cmpt);
        }
        catch(final ers.Issue ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    @Override
    public void onExit(final FSM_STATE state) {
        this.ctrl.onExit(ApplicationState.valueOf(state.name()));
    }

    @Override
    public void configure(final TransitionCmd cmd) {
        try {
            final daq.rc.Command.TransitionCmd trCmd = (daq.rc.Command.TransitionCmd) daq.rc.Command.buildCommand(cmd.serialize());
            this.ctrl.configure(trCmd);
        }
        catch(final ers.Issue | BadCommandException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    @Override
    public void connect(final TransitionCmd cmd) {
        try {
            final daq.rc.Command.TransitionCmd trCmd = (daq.rc.Command.TransitionCmd) daq.rc.Command.buildCommand(cmd.serialize());
            this.ctrl.connect(trCmd);
        }
        catch(final ers.Issue | BadCommandException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }

    }

    @Override
    public void prepareForRun(final TransitionCmd cmd) {
        try {
            final daq.rc.Command.TransitionCmd trCmd = (daq.rc.Command.TransitionCmd) daq.rc.Command.buildCommand(cmd.serialize());
            this.ctrl.prepareForRun(trCmd);
        }
        catch(final ers.Issue | BadCommandException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }

    }

    @Override
    public void stopROIB(final TransitionCmd cmd) {
        try {
            final daq.rc.Command.TransitionCmd trCmd = (daq.rc.Command.TransitionCmd) daq.rc.Command.buildCommand(cmd.serialize());
            this.ctrl.stopROIB(trCmd);
        }
        catch(final ers.Issue | BadCommandException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }

    }

    @Override
    public void stopDC(final TransitionCmd cmd) {
        try {
            final daq.rc.Command.TransitionCmd trCmd = (daq.rc.Command.TransitionCmd) daq.rc.Command.buildCommand(cmd.serialize());
            this.ctrl.stopDC(trCmd);
        }
        catch(final ers.Issue | BadCommandException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }

    }

    @Override
    public void stopHLT(final TransitionCmd cmd) {
        try {
            final daq.rc.Command.TransitionCmd trCmd = (daq.rc.Command.TransitionCmd) daq.rc.Command.buildCommand(cmd.serialize());
            this.ctrl.stopHLT(trCmd);
        }
        catch(final ers.Issue | BadCommandException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }

    }

    @Override
    public void stopRecording(final TransitionCmd cmd) {
        try {
            final daq.rc.Command.TransitionCmd trCmd = (daq.rc.Command.TransitionCmd) daq.rc.Command.buildCommand(cmd.serialize());
            this.ctrl.stopRecording(trCmd);
        }
        catch(final ers.Issue | BadCommandException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }

    }

    @Override
    public void stopGathering(final TransitionCmd cmd) {
        try {
            final daq.rc.Command.TransitionCmd trCmd = (daq.rc.Command.TransitionCmd) daq.rc.Command.buildCommand(cmd.serialize());
            this.ctrl.stopGathering(trCmd);
        }
        catch(final ers.Issue | BadCommandException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }

    }

    @Override
    public void stopArchiving(final TransitionCmd cmd) {
        try {
            final daq.rc.Command.TransitionCmd trCmd = (daq.rc.Command.TransitionCmd) daq.rc.Command.buildCommand(cmd.serialize());
            this.ctrl.stopArchiving(trCmd);
        }
        catch(final ers.Issue | BadCommandException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }

    }

    @Override
    public void disconnect(final TransitionCmd cmd) {
        try {
            final daq.rc.Command.TransitionCmd trCmd = (daq.rc.Command.TransitionCmd) daq.rc.Command.buildCommand(cmd.serialize());
            this.ctrl.disconnect(trCmd);
        }
        catch(final ers.Issue | BadCommandException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }

    }

    @Override
    public void unconfigure(final TransitionCmd cmd) {
        try {
            final daq.rc.Command.TransitionCmd trCmd = (daq.rc.Command.TransitionCmd) daq.rc.Command.buildCommand(cmd.serialize());
            this.ctrl.unconfigure(trCmd);
        }
        catch(final ers.Issue | BadCommandException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }

    }

    @Override
    public void subTransition(final SubTransitionCmd cmd) {
        try {
            final daq.rc.Command.SubTransitionCmd trCmd = (daq.rc.Command.SubTransitionCmd) daq.rc.Command.buildCommand(cmd.serialize());
            this.ctrl.subTransition(trCmd);
        }
        catch(final ers.Issue | BadCommandException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }

    }

    @Override
    public void resynch(final ResynchCmd cmd) {
        try {
            final daq.rc.Command.ResynchCmd trCmd = (daq.rc.Command.ResynchCmd) daq.rc.Command.buildCommand(cmd.serialize());
            this.ctrl.resynch(trCmd);
        }
        catch(final ers.Issue | BadCommandException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }
}
