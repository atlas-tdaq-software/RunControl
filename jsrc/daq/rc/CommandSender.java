package daq.rc;

import java.net.InetAddress;
import java.util.Date;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.xml.transform.TransformerException;
import javax.xml.ws.Holder;

import org.omg.CORBA.StringHolder;
import org.omg.CORBA.SystemException;

import TM.Test.Scope;
import daq.rc.Command.ApplicationStatusCmd;
import daq.rc.Command.ChangeFullStatsIntervalCmd;
import daq.rc.Command.ChangePublishIntervalCmd;
import daq.rc.Command.ChangeStatusCmd;
import daq.rc.Command.DynRestartCmd;
import daq.rc.Command.ExitCmd;
import daq.rc.Command.FSMCommands;
import daq.rc.Command.IgnoreErrorCmd;
import daq.rc.Command.PublishCmd;
import daq.rc.Command.PublishFullStatsCmd;
import daq.rc.Command.RCCommands;
import daq.rc.Command.RestartAppsCmd;
import daq.rc.Command.ResynchCmd;
import daq.rc.Command.StartAppsCmd;
import daq.rc.Command.StopAppsCmd;
import daq.rc.Command.SubTransitionCmd;
import daq.rc.Command.TestAppCmd;
import daq.rc.Command.TransitionCmd;
import daq.rc.Command.UserBroadCastCmd;
import daq.rc.Command.UserCmd;
import daq.rc.RCException.ApplicationBusy;
import daq.rc.RCException.ApplicationInError;
import daq.rc.RCException.BadCommandException;
import daq.rc.RCException.BadUserCredentials;
import daq.rc.RCException.CORBAException;
import daq.rc.RCException.CommandNotAllowed;
import daq.rc.RCException.IPCLookupException;
import daq.rc.RCException.Unexpected;
import rc.CannotExecute;
import rc.FailureReason;

import daq.tokens.JWToken;
import daq.tokens.AcquireTokenException;


/**
 * Class allowing to send commands to Run Control applications.
 * <p>
 * An instance of this class can be safely accessed concurrently from different threads.<br>
 * Commands can be sent both to segment's controllers and leaf applications (some of them may not be supported by one category of
 * applications).
 * <p>
 * NOTE: all the commands are asynchronous unless differently specified.
 * <p>
 * The two methods {@link #executeCommand(String, Command, CommandNotifier)} and
 * {@link #executeCommand(String, TransitionCmd, CommandNotifier)} can be used to receive a notification when the command's execution is
 * completed. Note that this mechanism is not available for the {@link RCCommands#EXIT}, {@link RCCommands#DYN_RESTART} and
 * {@link RCCommands#IGNORE_ERROR} commands.
 * <p>
 * The other two methods {@link #executeCommand(String, Command, long)} and {@link #executeCommand(String, TransitionCmd, long)} can be used
 * to wait for the sent command to be executed.
 */
public class CommandSender {
    private final static String UNSAFE_USER_NAME = System.getProperty("user.name", "unknown");
    private final static String HOST_NAME = CommandSender.getLocalHostName();
    private final static boolean IS_TOKEN_ENABLED = JWToken.enabled();

    private final ipc.Partition ipcPartition;
    private final String senderId;

    /**
     * Constructor
     * 
     * @param partitionName The name of the partition the application belongs to
     * @param senderId Simple "friendly" identifier for the application sending the command
     */
    public CommandSender(final String partitionName, final String senderId) {
        this(new ipc.Partition(partitionName), senderId);
    }

    /**
     * Constructor
     * 
     * @param partition The IPC partition the application belongs to
     * @param senderId Simple "friendly" identifier for the application sending the command
     */
    public CommandSender(final ipc.Partition partition, final String senderId) {
        this.ipcPartition = partition;
        this.senderId = senderId;   
    }
    
    /**
     * It returns the full qualified name of the local host, or the "unknown" string in case of errors
     * 
     * @return The full qualified name of the local host, or the "unknown" string in case of errors
     */
    private static String getLocalHostName() {
        try {
            return InetAddress.getLocalHost().getCanonicalHostName();
        }
        catch(final Exception ex) {
            return "unknown";
        }
    }
    
    /**
     * It returns the sender context associated to any sent command
     * 
     * @return The sender context associated to any sent command
     * @throws BadUserCredentials If the "daq token" mechanism is enabled and the token cannot be acquired
     */
    private rc.SenderContext getSenderContext(String recipient) throws BadUserCredentials {
        try {
            return new rc.SenderContext(CommandSender.IS_TOKEN_ENABLED ? JWToken.acquire(JWToken.MODE.REUSE, "rc://" + ipcPartition.getName() +"/" + recipient) : CommandSender.UNSAFE_USER_NAME, 
                                        CommandSender.HOST_NAME, 
                                        this.senderId);
        }
        catch(final AcquireTokenException ex) {
            throw new BadUserCredentials("Cannot acquire user token: " + ex, ex);
        }
    }

    /**
     * It sends a command (not an FSM transition command) to the application requesting to be notified when the command is done.
     * 
     * @param recipient The name of the application that should execute the command (i.e., the name the application uses to publish itself
     *            in IPC)
     * @param cmd The command to send
     * @param notifier The object to be notified when the command execution is completed
     * @throws IPCLookupException The application is not found in the IPC naming service
     * @throws CORBAException The application cannot be properly contacted
     * @throws ApplicationBusy The command could not be executed because the application was busy with some other command
     * @throws CommandNotAllowed The execution of the command is not allowed (e.g., because of Access Management restrictions)
     * @throws Unexpected The command could not be executed because of some unexpected error
     * @throws ApplicationInError The command could not be executed because the application was in an error state
     * @throws BadCommandException The command could not be serialized
     * @throws BadUserCredentials Credentials for the user sending the command cannot be acquired
     * @throws IllegalArgumentException The <em>cmd</em> is not valid
     * @see RCCommands
     */
    public void executeCommand(final String recipient, final Command cmd, final CommandNotifier notifier)
        throws CORBAException,
            ApplicationBusy,
            CommandNotAllowed,
            Unexpected,
            ApplicationInError,
            IPCLookupException, 
            BadCommandException,
            BadUserCredentials,
            IllegalArgumentException
    {
        if(cmd.supportsNotification() == false) {
            throw new IllegalArgumentException("The command \"" + cmd + "\" does not support asynchronous notification on completion");
        }

        cmd.commandSender(notifier.reference());

        this.executeCommand(recipient, cmd);
    }

    /**
     * It asks the application to perform an FSM transition with the request to be notified when the transition is done
     * 
     * @param recipient The name of the application that should execute the command (i.e., the name the application uses to publish itself
     *            in IPC)
     * @param trCmd The command representing the FSM transition to be done
     * @param notifier The object to be notified when the command execution is completed
     * @throws IPCLookupException The application is not found in the IPC naming service
     * @throws CORBAException The application cannot be properly contacted
     * @throws ApplicationBusy The command could not be executed because the application was busy with some other command
     * @throws CommandNotAllowed The execution of the command is not allowed (e.g., because of Access Management restrictions)
     * @throws Unexpected The command could not be executed because of some unexpected error
     * @throws ApplicationInError The command could not be executed because the application was in an error state
     * @throws BadCommandException The command could not be serialized and sent
     * @throws BadUserCredentials Credentials for the user sending the command cannot be acquired
     * @throws IllegalArgumentException The <em>trCmd</em> is not valid
     * @see FSMCommands
     * @see RCCommands#MAKE_TRANSITION
     */
    public void executeCommand(final String recipient, final TransitionCmd trCmd, final CommandNotifier notifier)
        throws CORBAException,
            ApplicationBusy,
            CommandNotAllowed,
            Unexpected,
            ApplicationInError,
            IPCLookupException,
            BadCommandException, 
            BadUserCredentials, 
            IllegalArgumentException
    {
        if(trCmd.supportsNotification() == false) {
            throw new IllegalArgumentException("The command \"" + trCmd + "\" does not support asynchronous notification on completion");
        }

        trCmd.commandSender(notifier.reference());

        this.executeCommand(recipient, trCmd);
    }

    /**
     * It asks the application to perform an FSM transition and waits for its completion
     * 
     * @param recipient recipient The name of the application that should execute the command (i.e., the name the application uses to
     *            publish itself in IPC)
     * @param trCmd The command representing the FSM transition to be done
     * @param timeout The time (in milliseconds) to wait for the command to be completed. A value of zero means to wait for ever.
     * @return <em>true</em> if the command has been completed within the specified timeout
     * @throws CORBAException The application cannot be properly contacted
     * @throws ApplicationBusy The command could not be executed because the application was busy with some other command
     * @throws CommandNotAllowed The execution of the command is not allowed (e.g., because of Access Management restrictions)
     * @throws Unexpected The command could not be executed because of some unexpected error
     * @throws ApplicationInError The command could not be executed because the application was in an error state
     * @throws IPCLookupException The application is not found in the IPC naming service
     * @throws InterruptedException The thread has been interrupted while waiting
     * @throws BadCommandException The command could not be serialized and sent
     * @throws BadUserCredentials Credentials for the user sending the command cannot be acquired
     * @throws IllegalArgumentException The <em>trCmd</em> is not valid or <em>timeout</em> is less than zero
     */
    public boolean executeCommand(final String recipient, final TransitionCmd trCmd, final long timeout)
        throws CORBAException,
            ApplicationBusy,
            CommandNotAllowed,
            Unexpected,
            ApplicationInError,
            IPCLookupException,
            InterruptedException,
            BadCommandException, 
            BadUserCredentials, 
            IllegalArgumentException
    {
        if(timeout < 0) {
            throw new IllegalArgumentException("The timeout value cannot be less than zero");
        }

        final Lock lk = new ReentrantLock();
        final Condition cond = lk.newCondition();
        final Holder<Boolean> done = new Holder<Boolean>(Boolean.FALSE);

        final CommandNotifier not = new CommandNotifier() {
            @Override
            protected void commandExecuted(final Command cmd) {
                lk.lock();
                try {
                    done.value = Boolean.TRUE;
                    cond.signal();
                }
                finally {
                    lk.unlock();
                }
            }
        };

        boolean timeoutElapsed = false;
        final Date now = new Date();

        try (final CommandNotifier c = not) {
            this.executeCommand(recipient, trCmd, c);

            lk.lock();
            try {
                if(timeout == 0) {
                    while(done.value == Boolean.FALSE) {
                        cond.await();
                    }
                } else {
                    final Date until = new Date(now.getTime() + timeout);
                    while(done.value == Boolean.FALSE) {
                        final boolean elapsed = !cond.awaitUntil(until);
                        if(elapsed == true) {
                            timeoutElapsed = true;
                            break;
                        }
                    }
                }

            }
            finally {
                lk.unlock();
            }
        }

        return !timeoutElapsed;
    }

    /**
     * It asks the application to execute a command (not an FSM transition) and waits for the command to be executed
     * 
     * @param recipient recipient The name of the application that should execute the command (i.e., the name the application uses to
     *            publish itself in IPC)
     * @param command The command to be executed
     * @param timeout The time (in milliseconds) to wait for the command to be completed. A value of zero means to wait for ever.
     * @return <em>true</em> if the command has been completed within the specified timeout
     * @throws CORBAException The application cannot be properly contacted
     * @throws ApplicationBusy The command could not be executed because the application was busy with some other command
     * @throws CommandNotAllowed The execution of the command is not allowed (e.g., because of Access Management restrictions)
     * @throws Unexpected The command could not be executed because of some unexpected error
     * @throws ApplicationInError The command could not be executed because the application was in an error state
     * @throws IPCLookupException The application is not found in the IPC naming service
     * @throws InterruptedException The thread has been interrupted while waiting
     * @throws BadCommandException The command could not be serialized and sent
     * @throws BadUserCredentials Credentials for the user sending the command cannot be acquired
     * @throws IllegalArgumentException The <em>command</em> is not valid or <em>timeout</em> is less than zero
     */
    public boolean executeCommand(final String recipient, final Command command, final long timeout)
        throws CORBAException,
            ApplicationBusy,
            CommandNotAllowed,
            Unexpected,
            ApplicationInError,
            IPCLookupException,
            InterruptedException,
            BadCommandException, 
            BadUserCredentials, 
            IllegalArgumentException
    {
        if(timeout < 0) {
            throw new IllegalArgumentException("The timeout value cannot be less than zero");
        }

        final Lock lk = new ReentrantLock();
        final Condition cond = lk.newCondition();
        final Holder<Boolean> done = new Holder<Boolean>(Boolean.FALSE);

        final CommandNotifier not = new CommandNotifier() {
            @Override
            protected void commandExecuted(final Command cmd) {
                lk.lock();
                try {
                    done.value = Boolean.TRUE;
                    cond.signal();
                }
                finally {
                    lk.unlock();
                }
            }
        };

        boolean timeoutElapsed = false;
        final Date now = new Date();

        try (final CommandNotifier c = not) {
            this.executeCommand(recipient, command, c);

            lk.lock();
            try {
                if(timeout == 0) {
                    while(done.value == Boolean.FALSE) {
                        cond.await();
                    }
                } else {
                    final Date until = new Date(now.getTime() + timeout);
                    while(done.value == Boolean.FALSE) {
                        final boolean elapsed = !cond.awaitUntil(until);
                        if(elapsed == true) {
                            timeoutElapsed = true;
                            break;
                        }
                    }
                }

            }
            finally {
                lk.unlock();
            }
        }

        return !timeoutElapsed;
    }

    /**
     * It sends a command (not an FSM transition command) to the application
     * 
     * @param recipient The name of the application that should execute the command (i.e., the name the application uses to publish itself
     *            in IPC)
     * @param cmd The command to send
     * @throws IPCLookupException The application is not found in the IPC naming service
     * @throws CORBAException The application cannot be properly contacted
     * @throws ApplicationBusy The command could not be executed because the application was busy with some other command
     * @throws CommandNotAllowed The execution of the command is not allowed (e.g., because of Access Management restrictions)
     * @throws Unexpected The command could not be executed because of some unexpected error
     * @throws ApplicationInError The command could not be executed because the application was in an error state
     * @throws BadCommandException The <em>cmd</em> cannot be serialized
     * @throws BadUserCredentials Credentials for the user sending the command cannot be acquired
     * @throws IllegalArgumentException The <em>cmd</em> is not valid
     * @see RCCommands
     */
    public void executeCommand(final String recipient, final Command cmd)
        throws IPCLookupException,
            CORBAException,
            ApplicationBusy,
            CommandNotAllowed,
            Unexpected,
            ApplicationInError, 
            BadCommandException,
            BadUserCredentials,
            IllegalArgumentException
    {
        if(cmd.isAlreadySent() == true) {
            throw new IllegalArgumentException("The same command cannot be sent more than once");
        }

        if(TransitionCmd.class.isInstance(cmd) == true) {
            throw new IllegalArgumentException("Invalid command: it must not be a transition command");
        }

        try {
            this.ipcReference(recipient).executeCommand(this.getSenderContext(recipient), cmd.serialize());
            cmd.setAlreadySent();
        }
        catch(final CannotExecute ex) {
            switch(ex.reason.value()) {
                case FailureReason._BUSY:
                    throw new ApplicationBusy(cmd.getCommand().toString(), recipient, ex);
                case FailureReason._NOT_ALLOWED:
                    throw new CommandNotAllowed(cmd.getCommand().toString(), recipient, ex);
                case FailureReason._IN_ERROR:
                    throw new ApplicationInError(cmd.getCommand().toString(), recipient, ex);
                case FailureReason._UNEXPECTED:
                    throw new Unexpected(cmd.getCommand().toString(), recipient, ex);
                default:
                    throw new Unexpected(cmd.getCommand().toString(), recipient, ex);
            }
        }
        catch(final SystemException ex) {
            throw new CORBAException(cmd.getCommand().toString(), recipient, ex);
        }
        catch(final TransformerException ex) {
            throw new BadCommandException(cmd.toString(), "The provided command cannot be serialized", ex);
        }
    }

    /**
     * It asks the application to perform an FSM transition
     * 
     * @param recipient The name of the application that should execute the command (i.e., the name the application uses to publish itself
     *            in IPC)
     * @param trCmd The command representing the FSM transition to be done
     * @throws IPCLookupException The application is not found in the IPC naming service
     * @throws CORBAException The application cannot be properly contacted
     * @throws ApplicationBusy The command could not be executed because the application was busy with some other command
     * @throws CommandNotAllowed The execution of the command is not allowed (e.g., because of Access Management restrictions)
     * @throws Unexpected The command could not be executed because of some unexpected error
     * @throws ApplicationInError The command could not be executed because the application was in an error state
     * @throws BadCommandException The <em>trCmd</em> cannot be serialized
     * @throws BadUserCredentials Credentials for the user sending the command cannot be acquired
     * @throws IllegalArgumentException The <em>trCmd</em> is not valid
     * @see FSMCommands
     * @see RCCommands#MAKE_TRANSITION
     */
    public void executeCommand(final String recipient, final TransitionCmd trCmd)
        throws IPCLookupException,
            CORBAException,
            ApplicationBusy,
            CommandNotAllowed,
            Unexpected,
            ApplicationInError,
            BadCommandException, 
            BadUserCredentials,
            IllegalArgumentException
    {
        if(trCmd.isAlreadySent() == true) {
            throw new IllegalArgumentException("The same command cannot be sent more than once");
        }

        try {
            this.ipcReference(recipient).makeTransition(this.getSenderContext(recipient), trCmd.serialize());
            trCmd.setAlreadySent();
        }
        catch(final CannotExecute ex) {
            switch(ex.reason.value()) {
                case FailureReason._BUSY:
                    throw new ApplicationBusy(trCmd.getCommand().toString(), recipient, ex);
                case FailureReason._NOT_ALLOWED:
                    throw new CommandNotAllowed(trCmd.getCommand().toString(), recipient, ex);
                case FailureReason._IN_ERROR:
                    throw new ApplicationInError(trCmd.getCommand().toString(), recipient, ex);
                case FailureReason._UNEXPECTED:
                    throw new Unexpected(trCmd.getCommand().toString(), recipient, ex);
                default:
                    throw new Unexpected(trCmd.getCommand().toString(), recipient, ex);
            }
        }
        catch(final SystemException ex) {
            throw new CORBAException(trCmd.getFSMCommand().toString(), recipient, ex);
        }
        catch(final TransformerException ex) {
            throw new BadCommandException(trCmd.toString(), "The provided transition command cannot be serialized", ex);
        }
    }

    /**
     * It asks the application to perform an FSM transition
     * <p>
     * <em>command</em> cannot be {@link Command.FSMCommands#USERBROADCAST}, {@link Command.FSMCommands#RESYNCH} or
     * {@link Command.FSMCommands#SUB_TRANSITION}. Use {@link #userBroadcast(String, String, String[])},
     * {@link #resynch(String, int, String[])} or {@link #subTransition(String, String, Command.FSMCommands)} in those cases.
     * 
     * @param recipient The name of the application that should execute the command (i.e., the name the application uses to publish itself
     *            in IPC)
     * @param command The FSM transition to be executed
     * @throws IPCLookupException The application is not found in the IPC naming service
     * @throws BadCommandException Some issue occurred building the command to be sent to the remote application
     * @throws CORBAException The application cannot be properly contacted
     * @throws ApplicationBusy The command could not be executed because the application was busy with some other command
     * @throws CommandNotAllowed The execution of the command is not allowed (e.g., because of Access Management restrictions)
     * @throws Unexpected The command could not be executed because of some unexpected error
     * @throws ApplicationInError The command could not be executed because the application was in an error state
     * @throws BadUserCredentials Credentials for the user sending the command cannot be acquired
     * @throws IllegalArgumentException <em>command</em> is not valid
     * @see RCCommands#MAKE_TRANSITION
     */
    public void makeTransition(final String recipient, final FSMCommands command)
        throws IPCLookupException,
            BadCommandException,
            CORBAException,
            ApplicationBusy,
            CommandNotAllowed,
            Unexpected,
            ApplicationInError, 
            BadUserCredentials, 
            IllegalArgumentException
    {
        this.executeCommand(recipient, new TransitionCmd(command));
    }

    /**
     * It asks a segment's controller to broadcast an user defined command to all of its child applications
     * <p>
     * When this command is sent to a leaf application, it just executes the user command.
     * 
     * @param recipient The name of the application that should execute the command (i.e., the name the application uses to publish itself
     *            in IPC)
     * @param cmdName The name of the user defined command
     * @param args The arguments of the user defined command
     * @throws IPCLookupException The application is not found in the IPC naming service
     * @throws BadCommandException Some issue occurred building the command to be sent to the remote application
     * @throws CORBAException The application cannot be properly contacted
     * @throws ApplicationBusy The command could not be executed because the application was busy with some other command
     * @throws CommandNotAllowed The execution of the command is not allowed (e.g., because of Access Management restrictions)
     * @throws Unexpected The command could not be executed because of some unexpected error
     * @throws ApplicationInError The command could not be executed because the application was in an error state
     * @throws BadUserCredentials Credentials for the user sending the command cannot be acquired
     * @throws IllegalArgumentException <em>cmdName</em> is empty or null or <em>args</em> is null
     * @see RCCommands#MAKE_TRANSITION
     * @see FSMCommands#USERBROADCAST
     * @see RCCommands#USER
     * @see UserBroadCastCmd
     */
    public void userBroadcast(final String recipient, final String cmdName, final String[] args)
        throws IPCLookupException,
            BadCommandException,
            CORBAException,
            ApplicationBusy,
            CommandNotAllowed,
            Unexpected,
            ApplicationInError, 
            BadUserCredentials, 
            IllegalArgumentException
    {
        this.executeCommand(recipient, new UserBroadCastCmd(new UserCmd(cmdName, args)));
    }

    /**
     * It asks a segment's controller to broadcast a re-synchronization command to all of its child applications
     * <p>
     * When this command is sent to a leaf application, it just executes the re-synchronization.
     * 
     * @param recipient The name of the application that should execute the command (i.e., the name the application uses to publish itself
     *            in IPC)
     * @param ecrCounts The ECR counts
     * @param extendedL1ID The extended L1 ID
     * @param modules The name of the modules to be re-synchronized
     * @throws IPCLookupException The application is not found in the IPC naming service
     * @throws BadCommandException Some issue occurred building the command to be sent to the remote application
     * @throws CORBAException The application cannot be properly contacted
     * @throws ApplicationBusy The command could not be executed because the application was busy with some other command
     * @throws CommandNotAllowed The execution of the command is not allowed (e.g., because of Access Management restrictions)
     * @throws Unexpected The command could not be executed because of some unexpected error
     * @throws ApplicationInError The command could not be executed because the application was in an error state
     * @throws IllegalArgumentException <em>modules</em> is null
     * @throws BadUserCredentials Credentials for the user sending the command cannot be acquired
     * @see RCCommands#MAKE_TRANSITION
     * @see FSMCommands#RESYNCH
     * @see ResynchCmd
     */
    public void resynch(final String recipient, final long ecrCounts, final long extendedL1ID, final String[] modules)
        throws IPCLookupException,
            BadCommandException,
            CORBAException,
            ApplicationBusy,
            CommandNotAllowed,
            Unexpected,
            ApplicationInError,
            BadUserCredentials,
            IllegalArgumentException
    {
        this.executeCommand(recipient, new ResynchCmd(ecrCounts, extendedL1ID, modules));
    }
    
    /**
     * It asks an application to perform a sub-transition
     * 
     * @param recipient The name of the application that should execute the command (i.e., the name the application uses to publish itself
     *            in IPC)
     * @param subTrName The name of the sub-transition (as defined in the OKS database)
     * @param mainTr The name of the main transition the sub-transition refers to
     * @throws IPCLookupException The application is not found in the IPC naming service
     * @throws BadCommandException Some issue occurred building the command to be sent to the remote application
     * @throws CORBAException The application cannot be properly contacted
     * @throws ApplicationBusy The command could not be executed because the application was busy with some other command
     * @throws CommandNotAllowed The execution of the command is not allowed (e.g., because of Access Management restrictions)
     * @throws Unexpected The command could not be executed because of some unexpected error
     * @throws ApplicationInError The command could not be executed because the application was in an error state
     * @throws IllegalArgumentException <em>subTrName</em> is empty or null or <em>mainTR</em> is null
     * @throws BadUserCredentials Credentials for the user sending the command cannot be acquired
     * @see RCCommands#MAKE_TRANSITION
     * @see FSMCommands#SUB_TRANSITION
     * @see SubTransitionCmd
     */
    public void subTransition(final String recipient, final String subTrName, final FSMCommands mainTr)
        throws CORBAException,
            ApplicationBusy,
            CommandNotAllowed,
            Unexpected,
            IPCLookupException,
            BadCommandException,
            ApplicationInError,
            BadUserCredentials,
            IllegalArgumentException
    {
        this.executeCommand(recipient, new SubTransitionCmd(subTrName, mainTr));
    }

    /**
     * It asks a segment's controller to enter an error state.
     * <p>
     * NOTE: this command is synchronous.<br>
     * NOTE: reserved to the Expert System in order to manager the error state of controllers.<br>
     * This command has no effect if sent to a leaf application.
     * 
     * @param recipient The name of the segment's controller (i.e., the name the application uses to publish itself in IPC)
     * @param errorItems List of elements causing the controller to be in error (if empty the error state will be cleared)
     * 
     * @throws CORBAException The controller cannot be properly contacted
     * @throws IPCLookupException The controller is not found in the IPC naming service
     * @throws BadUserCredentials Credentials for the user sending the command cannot be acquired
     * @throws CommandNotAllowed User is not allowed to execute the command
     * @throws ApplicationBusy The command could not be executed because the application was busy with some other command
     * @throws Unexpected The command could not be executed because of some unexpected error
     * @throws ApplicationInError The command could not be executed because the application was in an error state
     */
    public void setError(final String recipient, final List<? extends ErrorElement> errorItems)
        throws CORBAException,
            IPCLookupException,
            BadUserCredentials,
            CommandNotAllowed,
            ApplicationBusy,
            ApplicationInError,
            Unexpected
    {
        try {
            final int items = errorItems.size();
            final rc.ErrorItem[] ei = new rc.ErrorItem[items];
            for(int i = 0; i < items; ++i) {
                final ErrorElement e = errorItems.get(i);
                ei[i] = new rc.ErrorItem(e.getApplicationName(), e.getErrorDescription(), e.getErrorType());
            }

            this.ipcReference(recipient).setError(this.getSenderContext(recipient), ei);
        }
        catch(final CannotExecute ex) {
            switch(ex.reason.value()) {
                case FailureReason._BUSY:
                    throw new ApplicationBusy("SET_ERROR", recipient, ex);
                case FailureReason._NOT_ALLOWED:
                    throw new CommandNotAllowed("SET_ERROR", recipient, ex);
                case FailureReason._IN_ERROR:
                    throw new ApplicationInError("SET_ERROR", recipient, ex);
                case FailureReason._UNEXPECTED:
                    throw new Unexpected("SET_ERROR", recipient, ex);
                default:
                    throw new Unexpected("SET_ERROR", recipient, ex);
            }
        }
        catch(final SystemException ex) {
            throw new CORBAException("SET_ERROR", recipient, ex);
        }
    }

    /**
     * It asks a segment's controller to ignore (i.e., put out of membership) any application causing its error state
     * <p>
     * This command has no effect if sent to a leaf application.
     * 
     * @param recipient The name of the segment's controller (i.e., the name the application uses to publish itself in IPC)
     * @throws IPCLookupException The controller is not found in the IPC naming service
     * @throws BadCommandException Some issue occurred building the command to be sent to the remote controller
     * @throws CORBAException The controller cannot be properly contacted
     * @throws CommandNotAllowed The execution of the command is not allowed (e.g., because of Access Management restrictions)
     * @throws Unexpected The command could not be executed because of some unexpected error
     * @throws BadUserCredentials Credentials for the user sending the command cannot be acquired
     * @see RCCommands#IGNORE_ERROR
     * @see IgnoreErrorCmd
     */
    public void ignoreError(final String recipient)
        throws IPCLookupException,
            BadCommandException,
            CORBAException,
            CommandNotAllowed,
            Unexpected, 
            BadUserCredentials
    {
        try {
            this.executeCommand(recipient, new IgnoreErrorCmd());
        }
        catch(ApplicationInError | ApplicationBusy ex) {
            // This should never happen
            throw new Unexpected(RCCommands.IGNORE_ERROR.name(), recipient, (CannotExecute) ex.getCause());
        }
    }

    /**
     * It asks an application to execute an user-defined command
     * <p>
     * NOTE: this command has no effect if sent to a segment's controller
     * 
     * @param recipient The name of the application that should execute the command (i.e., the name the application uses to publish itself
     *            in IPC)
     * @param cmdName The name of the user-defined command
     * @param args The arguments of the user-defined command
     * @throws IPCLookupException The application is not found in the IPC naming service
     * @throws BadCommandException Some issue occurred building the command to be sent to the remote application
     * @throws CORBAException The application cannot be properly contacted
     * @throws CommandNotAllowed The execution of the command is not allowed (e.g., because of Access Management restrictions)
     * @throws Unexpected The command could not be executed because of some unexpected error
     * @throws IllegalArgumentException <em>cmdName</em> is empty or null or <em>args</em> is null
     * @throws BadUserCredentials Credentials for the user sending the command cannot be acquired
     * @see RCCommands#USER
     * @see UserCmd
     */
    public void userCommand(final String recipient, final String cmdName, final String[] args)
        throws IPCLookupException,
            BadCommandException,
            CORBAException,
            CommandNotAllowed,
            Unexpected, 
            BadUserCredentials, 
            IllegalArgumentException
    {
        try {
            this.executeCommand(recipient, new UserCmd(cmdName, args));
        }
        catch(final ApplicationInError | ApplicationBusy ex) {
            // This should never happen
            throw new Unexpected(RCCommands.USER.name(), recipient, (CannotExecute) ex.getCause());
        }
    }

    /**
     * It asks a segment's controller to change the membership of any child application
     * <p>
     * NOTE: This command is synchronous.<br>
     * NOTE: Use {@link #changeStatus(String, boolean, String[])} to ask leaf applications to enable or disable specific components.
     * 
     * @param recipient The name of the controller that should execute the command (i.e., the name the application uses to publish itself in
     *            IPC)
     * @param membership <em>false</em> if applications should be put out of membership
     * @param applications List of applications whose membership status should be changed
     * @throws IPCLookupException The controller is not found in the IPC naming service
     * @throws BadCommandException Some issue occurred building the command to be sent to the remote controller
     * @throws CORBAException The controller cannot be properly contacted
     * @throws CommandNotAllowed The execution of the command is not allowed (e.g., because of Access Management restrictions)
     * @throws Unexpected The command could not be executed because of some unexpected error
     * @throws IllegalArgumentException 
     * @throws BadUserCredentials Credentials for the user sending the command cannot be acquired
     * @see RCCommands#ENABLE
     * @see RCCommands#DISABLE
     * @see ChangeStatusCmd
     */
    public void changeMembership(final String recipient, final boolean membership, final String[] applications)
        throws IPCLookupException,
            BadCommandException,
            CORBAException,
            CommandNotAllowed,
            Unexpected, 
            BadUserCredentials, 
            IllegalArgumentException
    {
        this.changeStatus(recipient, membership, applications);
    }

    /**
     * It asks an application to enable/disable some components
     * <p>
     * NOTE: If sent to a segment's controller, this call has the same effect as {@link #changeMembership(String, boolean, String[])}
     * 
     * @param recipient The name of the application that should execute the command (i.e., the name the application uses to publish itself
     *            in IPC)
     * @param enable <em>false</em> if components should be disable
     * @param components List of components to be enabled or disabled
     * @throws IPCLookupException The controller is not found in the IPC naming service
     * @throws BadCommandException Some issue occurred building the command to be sent to the remote controller
     * @throws CORBAException The controller cannot be properly contacted
     * @throws CommandNotAllowed The execution of the command is not allowed (e.g., because of Access Management restrictions)
     * @throws Unexpected The command could not be executed because of some unexpected error
     * @throws IllegalArgumentException <em>components</em> is null
     * @throws BadUserCredentials Credentials for the user sending the command cannot be acquired
     * @see RCCommands#ENABLE
     * @see RCCommands#DISABLE
     * @see ChangeStatusCmd
     */
    public void changeStatus(final String recipient, final boolean enable, final String[] components)
        throws IPCLookupException,
            BadCommandException,
            CORBAException,
            CommandNotAllowed,
            Unexpected, 
            BadUserCredentials, 
            IllegalArgumentException
    {
        try {
            this.executeCommand(recipient, new ChangeStatusCmd(enable, components));
        }
        catch(final ApplicationInError | ApplicationBusy ex) {
            // This should never happen
            throw new Unexpected(enable == true ? RCCommands.ENABLE.name() : RCCommands.DISABLE.name(),
                                 recipient,
                                 (CannotExecute) ex.getCause());
        }
    }

    /**
     * It asks a segment's controller to start some child applications
     * <p>
     * This command has no effect if sent to a leaf application
     * 
     * @param recipient The name of the controller that should execute the command (i.e., the name the application uses to publish itself in
     *            IPC)
     * @param applications The name of the applications that should be started
     * @throws IPCLookupException The controller is not found in the IPC naming service
     * @throws BadCommandException Some issue occurred building the command to be sent to the remote controller
     * @throws CORBAException The controller cannot be properly contacted
     * @throws ApplicationBusy The command could not be executed because the controller was busy with some other command
     * @throws CommandNotAllowed The execution of the command is not allowed (e.g., because of Access Management restrictions)
     * @throws Unexpected The command could not be executed because of some unexpected error
     * @throws IllegalArgumentException <em>applications</em> is null
     * @throws BadUserCredentials Credentials for the user sending the command cannot be acquired
     * @see RCCommands#STARTAPP
     * @see StartAppsCmd
     */
    public void startApplications(final String recipient, final String[] applications)
        throws IPCLookupException,
            BadCommandException,
            CORBAException,
            ApplicationBusy,
            CommandNotAllowed,
            Unexpected, 
            BadUserCredentials, 
            IllegalArgumentException
    {
        try {
            this.executeCommand(recipient, new StartAppsCmd(applications));
        }
        catch(final ApplicationInError ex) {
            throw new Unexpected(RCCommands.STARTAPP.name(), recipient, ex.getCause());
        }
    }

    /**
     * It asks a segment's controller to stop some child applications
     * <p>
     * This command has no effect if sent to a leaf application
     * 
     * @param recipient The name of the controller that should execute the command (i.e., the name the application uses to publish itself in
     *            IPC)
     * @param applications The name of the applications that should be stopped
     * @throws IPCLookupException The controller is not found in the IPC naming service
     * @throws BadCommandException Some issue occurred building the command to be sent to the remote controller
     * @throws CORBAException The controller cannot be properly contacted
     * @throws ApplicationBusy The command could not be executed because the controller was busy with some other command
     * @throws CommandNotAllowed The execution of the command is not allowed (e.g., because of Access Management restrictions)
     * @throws Unexpected The command could not be executed because of some unexpected error
     * @throws IllegalArgumentException <em>applications</em> is null
     * @throws BadUserCredentials Credentials for the user sending the command cannot be acquired
     * @see RCCommands#STOPAPP
     * @see StopAppsCmd
     */
    public void stopApplications(final String recipient, final String[] applications)
        throws IPCLookupException,
            BadCommandException,
            CORBAException,
            ApplicationBusy,
            CommandNotAllowed,
            Unexpected, 
            BadUserCredentials, 
            IllegalArgumentException
    {
        try {
            this.executeCommand(recipient, new StopAppsCmd(applications));
        }
        catch(final ApplicationInError ex) {
            throw new Unexpected(RCCommands.STOPAPP.name(), recipient, ex.getCause());
        }
    }

    /**
     * It asks a segment's controller to restart some child applications
     * <p>
     * This command has no effect if sent to a leaf application
     * 
     * @param recipient The name of the controller that should execute the command (i.e., the name the application uses to publish itself in
     *            IPC)
     * @param applications The name of the applications that should be restarted
     * @throws IPCLookupException The controller is not found in the IPC naming service
     * @throws BadCommandException Some issue occurred building the command to be sent to the remote controller
     * @throws CORBAException The controller cannot be properly contacted
     * @throws ApplicationBusy The command could not be executed because the controller was busy with some other command
     * @throws CommandNotAllowed The execution of the command is not allowed (e.g., because of Access Management restrictions)
     * @throws Unexpected The command could not be executed because of some unexpected error
     * @throws IllegalArgumentException <em>applications</em> is null
     * @throws BadUserCredentials Credentials for the user sending the command cannot be acquired
     * @see RCCommands#RESTARTAPP
     * @see RestartAppsCmd
     */
    public void restartApplications(final String recipient, final String[] applications)
        throws IPCLookupException,
            BadCommandException,
            CORBAException,
            ApplicationBusy,
            CommandNotAllowed,
            Unexpected,
            BadUserCredentials, 
            IllegalArgumentException
    {
        try {
            this.executeCommand(recipient, new RestartAppsCmd(applications));
        }
        catch(final ApplicationInError ex) {
            throw new Unexpected(RCCommands.RESTARTAPP.name(), recipient, ex.getCause());
        }
    }

    /**
     * It asks a segment's controller to dynamically restart some child segments
     * <p>
     * This command has no effect if sent to a leaf application
     * 
     * @param recipient The name of the controller that should execute the command (i.e., the name the application uses to publish itself in
     *            IPC)
     * @param segments The name of the segments that should be dynamically restarted
     * @throws IPCLookupException The controller is not found in the IPC naming service
     * @throws BadCommandException Some issue occurred building the command to be sent to the remote controller
     * @throws CORBAException The controller cannot be properly contacted
     * @throws ApplicationBusy The command could not be executed because the controller was busy with some other command
     * @throws CommandNotAllowed The execution of the command is not allowed (e.g., because of Access Management restrictions)
     * @throws Unexpected The command could not be executed because of some unexpected error
     * @throws ApplicationInError The command could not be executed because the controller was in an error state
     * @throws IllegalArgumentException If <em>segments</em> is null
     * @throws BadUserCredentials Credentials for the user sending the command cannot be acquired
     * @see RCCommands#DYN_RESTART
     * @see DynRestartCmd
     */
    public void dynamicRestart(final String recipient, final String[] segments)
        throws IPCLookupException,
            BadCommandException,
            CORBAException,
            ApplicationBusy,
            CommandNotAllowed,
            Unexpected,
            ApplicationInError, 
            BadUserCredentials, 
            IllegalArgumentException
    {
        this.executeCommand(recipient, new DynRestartCmd(segments));
    }

    /**
     * It asks a segment's controller to test the functionality of some child applications. Test level is set to 1 and the test scope is set
     * to {@link TestManager.Test.Scope#Any}.
     * <p>
     * This command has no effect if sent to a leaf application
     * 
     * @param recipient The name of the controller that should execute the command (i.e., the name the application uses to publish itself in
     *            IPC)
     * @param applications The name of the applications that should be tested
     * @throws IPCLookupException The controller is not found in the IPC naming service
     * @throws BadCommandException Some issue occurred building the command to be sent to the remote controller
     * @throws CORBAException The controller cannot be properly contacted
     * @throws ApplicationBusy The command could not be executed because the controller was busy with some other command
     * @throws CommandNotAllowed The execution of the command is not allowed (e.g., because of Access Management restrictions)
     * @throws Unexpected The command could not be executed because of some unexpected error
     * @throws IllegalArgumentException <em>applications</em> is null
     * @throws BadUserCredentials Credentials for the user sending the command cannot be acquired
     * @see RCCommands#TESTAPP
     * @see TestAppCmd
     */
    public void testApp(final String recipient, final String[] applications)
        throws IPCLookupException,
            BadCommandException,
            CORBAException,
            ApplicationBusy,
            CommandNotAllowed,
            Unexpected, 
            BadUserCredentials, 
            IllegalArgumentException
    {
        try {
            this.executeCommand(recipient, new TestAppCmd(applications));
        }
        catch(final ApplicationInError ex) {
            throw new Unexpected(RCCommands.TESTAPP.name(), recipient, ex.getCause());
        }
    }

    /**
     * It asks a segment's controller to test the functionality of some child applications.
     * <p>
     * This command has no effect if sent to a leaf application
     * 
     * @param recipient The name of the controller that should execute the command (i.e., the name the application uses to publish itself in
     *            IPC)
     * @param applications The name of the applications that should be tested
     * @param scope The test's scope
     * @param level The test's level
     * @throws IPCLookupException The controller is not found in the IPC naming service
     * @throws BadCommandException Some issue occurred building the command to be sent to the remote controller
     * @throws CORBAException The controller cannot be properly contacted
     * @throws ApplicationBusy The command could not be executed because the controller was busy with some other command
     * @throws CommandNotAllowed The execution of the command is not allowed (e.g., because of Access Management restrictions)
     * @throws Unexpected The command could not be executed because of some unexpected error
     * @throws IllegalArgumentException <em>applications</em> or <em>scope</em> is null
     * @throws BadUserCredentials Credentials for the user sending the command cannot be acquired
     * @see RCCommands#TESTAPP
     * @see TestAppCmd
     */
    public void testApp(final String recipient, final String[] applications, final Scope scope, final int level)
        throws IPCLookupException,
            BadCommandException,
            CORBAException,
            ApplicationBusy,
            CommandNotAllowed,
            Unexpected, 
            BadUserCredentials, 
            IllegalArgumentException
    {
        try {
            this.executeCommand(recipient, new TestAppCmd(applications, scope, level));
        }
        catch(final ApplicationInError ex) {
            throw new Unexpected(RCCommands.TESTAPP.name(), recipient, ex.getCause());
        }
    }

    /**
     * It asks an application to publish its status information
     * 
     * @param recipient The name of the application that should execute the command (i.e., the name the application uses to publish itself
     *            in IPC)
     * @throws IPCLookupException The application is not found in the IPC naming service
     * @throws BadCommandException Some issue occurred building the command to be sent to the remote application
     * @throws CORBAException The application cannot be properly contacted
     * @throws ApplicationBusy The command could not be executed because the application was busy with some other command
     * @throws CommandNotAllowed The execution of the command is not allowed (e.g., because of Access Management restrictions)
     * @throws Unexpected The command could not be executed because of some unexpected error
     * @throws BadUserCredentials Credentials for the user sending the command cannot be acquired
     * @see RCCommands#PUBLISH
     * @see PublishCmd
     */
    public void publish(final String recipient)
        throws IPCLookupException,
            BadCommandException,
            CORBAException,
            ApplicationBusy,
            CommandNotAllowed,
            Unexpected, 
            BadUserCredentials
    {
        try {
            this.executeCommand(recipient, new PublishCmd());
        }
        catch(final ApplicationInError ex) {
            throw new Unexpected(RCCommands.PUBLISH.name(), recipient, ex.getCause());
        }
    }

    /**
     * It asks an application to change the rate it publishes its status information
     * <p>
     * NOTE: the change in publishing rate is not permanent and the default value (i.e., the one defined in the configuration database) will
     * be restored at the next {@link FSMCommands#CONFIGURE} transition.
     * 
     * @param recipient The name of the application that should execute the command (i.e., the name the application uses to publish itself
     *            in IPC)
     * @param seconds The new publishing interval in seconds
     * @throws IPCLookupException The application is not found in the IPC naming service
     * @throws BadCommandException Some issue occurred building the command to be sent to the remote application
     * @throws CORBAException The application cannot be properly contacted
     * @throws ApplicationBusy The command could not be executed because the application was busy with some other command
     * @throws CommandNotAllowed The execution of the command is not allowed (e.g., because of Access Management restrictions)
     * @throws Unexpected The command could not be executed because of some unexpected error
     * @throws BadUserCredentials Credentials for the user sending the command cannot be acquired
     * @throws IllegalArgumentException <em>seconds</em> is less than zero
     * @see RCCommands#CHANGE_PROBE_INTERVAL
     * @see ChangePublishIntervalCmd
     */
    public void changePublishInterval(final String recipient, final int seconds)
        throws IPCLookupException,
            BadCommandException,
            CORBAException,
            ApplicationBusy,
            CommandNotAllowed,
            Unexpected, 
            BadUserCredentials,
            IllegalArgumentException
    {
        try {
            this.executeCommand(recipient, new ChangePublishIntervalCmd(seconds));
        }
        catch(final ApplicationInError ex) {
            throw new Unexpected(RCCommands.CHANGE_PROBE_INTERVAL.name(), recipient, ex.getCause());
        }
    }

    /**
     * It asks an application to publish its statistics information
     * 
     * @param recipient The name of the application that should execute the command (i.e., the name the application uses to publish itself
     *            in IPC)
     * @throws IPCLookupException The application is not found in the IPC naming service
     * @throws BadCommandException Some issue occurred building the command to be sent to the remote application
     * @throws CORBAException The application cannot be properly contacted
     * @throws ApplicationBusy The command could not be executed because the application was busy with some other command
     * @throws CommandNotAllowed The execution of the command is not allowed (e.g., because of Access Management restrictions)
     * @throws Unexpected The command could not be executed because of some unexpected error
     * @throws BadUserCredentials Credentials for the user sending the command cannot be acquired
     * @see RCCommands#PUBLISHSTATS
     */
    public void publishFullStats(final String recipient)
        throws IPCLookupException,
            BadCommandException,
            CORBAException,
            ApplicationBusy,
            CommandNotAllowed,
            Unexpected, 
            BadUserCredentials
    {
        try {
            this.executeCommand(recipient, new PublishFullStatsCmd());
        }
        catch(final ApplicationInError ex) {
            throw new Unexpected(RCCommands.PUBLISHSTATS.name(), recipient, ex.getCause());
        }
    }

    /**
     * It asks an application to change the rate it publishes its statistics information.
     * <p>
     * NOTE: the change in publishing rate is not permanent and the default value (i.e., the one defined in the configuration database) will
     * be restored at the next {@link FSMCommands#CONFIGURE} transition.
     * 
     * @param recipient The name of the application that should execute the command (i.e., the name the application uses to publish itself
     *            in IPC)
     * @param seconds The new publishing interval in seconds
     * @throws IPCLookupException The application is not found in the IPC naming service
     * @throws BadCommandException Some issue occurred building the command to be sent to the remote application
     * @throws CORBAException The application cannot be properly contacted
     * @throws ApplicationBusy The command could not be executed because the application was busy with some other command
     * @throws CommandNotAllowed The execution of the command is not allowed (e.g., because of Access Management restrictions)
     * @throws Unexpected The command could not be executed because of some unexpected error
     * @throws IllegalArgumentException <em>seconds</em> is less than zero
     * @throws BadUserCredentials Credentials for the user sending the command cannot be acquired
     * @see RCCommands#CHANGE_FULLSTATS_INTERVAL
     * @see ChangeFullStatsIntervalCmd
     */
    public void changeFullStatsInterval(final String recipient, final int seconds)
        throws IPCLookupException,
            BadCommandException,
            CORBAException,
            ApplicationBusy,
            CommandNotAllowed,
            Unexpected, 
            BadUserCredentials, 
            IllegalArgumentException
    {
        try {
            this.executeCommand(recipient, new ChangeFullStatsIntervalCmd(seconds));
        }
        catch(final ApplicationInError ex) {
            throw new Unexpected(RCCommands.CHANGE_FULLSTATS_INTERVAL.name(), recipient, ex.getCause());
        }
    }

    /**
     * It asks a segmet's controller to gracefully exit
     * 
     * @param recipient The name of the controller that should execute the command (i.e., the name the application uses to publish itself in
     *            IPC)
     * @throws IPCLookupException The controller is not found in the IPC naming service
     * @throws BadCommandException Some issue occurred building the command to be sent to the remote controller
     * @throws CORBAException The controller cannot be properly contacted
     * @throws ApplicationBusy The command could not be executed because the controller was busy with some other command
     * @throws CommandNotAllowed The execution of the command is not allowed (e.g., because of Access Management restrictions)
     * @throws Unexpected The command could not be executed because of some unexpected error
     * @throws BadUserCredentials Credentials for the user sending the command cannot be acquired
     * @see RCCommands#EXIT
     */
    public void exit(final String recipient)
        throws IPCLookupException,
            BadCommandException,
            CORBAException,
            ApplicationBusy,
            CommandNotAllowed,
            Unexpected, 
            BadUserCredentials
    {
        try {
            this.executeCommand(recipient, new ExitCmd());
        }
        catch(final ApplicationInError ex) {
            throw new Unexpected(RCCommands.CHANGE_FULLSTATS_INTERVAL.name(), recipient, ex.getCause());
        }
    }

    /**
     * It gets the ERS debug level of a remote application.
     * <p>
     * NOTE: this command is synchronous.
     * 
     * @param recipient The name of the application that should execute the command (i.e., the name the application uses to publish itself
     *            in IPC)
     * @return The ERS debug level
     * @throws IPCLookupException The application is not found in the IPC naming service
     * @throws CORBAException The application cannot be properly contacted
     */
    public short getDebugLevel(final String recipient) throws IPCLookupException, CORBAException {
        try {
            return this.ipcReference(recipient).debug_level();
        }
        catch(final SystemException ex) {
            throw new CORBAException("GET_DEBUG_LEVEL", recipient, ex);
        }
    }

    /**
     * It "pings" the remote application.
     * <p>
     * It can be used as a simple health check.
     * 
     * @param recipient The name of the remote application
     * @throws IPCLookupException The application is not found in the IPC naming service
     * @throws CORBAException The application cannot be properly contacted
     */
    public void ping(final String recipient) throws IPCLookupException, CORBAException {
        try {
            this.ipcReference(recipient).ping();
        }
        catch(final SystemException ex) {
            throw new CORBAException("PING", recipient, ex);
        }
    }
    
    /**
     * It sets the ERS debug level of a remote application.
     * <p>
     * NOTE: this command is synchronous.
     * 
     * @param recipient The name of the application that should execute the command (i.e., the name the application uses to publish itself
     *            in IPC)
     * @param level The new ERS debug level
     * @throws IPCLookupException The application is not found in the IPC naming service
     * @throws CORBAException The application cannot be properly contacted
     */
    public void setDebugLevel(final String recipient, final short level) throws IPCLookupException, CORBAException {
        try {
            this.ipcReference(recipient).debug_level(level);
        }
        catch(final SystemException ex) {
            throw new CORBAException("SET_DEBUG_LEVEL", recipient, ex);
        }
    }

    /**
     * It asks a Run Control application about its current status
     * 
     * @param name The name of the Run Control application
     * @return The status of the Run Control application
     * @throws IPCLookupException The application is not found in the IPC naming service
     * @throws CORBAException The application cannot be properly contacted
     * @throws BadCommandException Failure in building the command to be sent to the application
     */
    public RCApplicationStatus getApplicationStatus(final String name) throws IPCLookupException, CORBAException, BadCommandException {
        try {
            final StringHolder str = new StringHolder();
            this.ipcReference(name).status(str);

            return new RCApplicationStatus(new ApplicationStatusCmd(str.value));
        }
        catch(final SystemException ex) {
            throw new CORBAException(RCCommands.UPDATE_CHILD_STATUS.toString(), name, ex);
        }
    }

    private rc.commander ipcReference(final String recipient) throws IPCLookupException {
        try {
            return this.ipcPartition.lookup(rc.commander.class, recipient);
        }
        catch(final ipc.InvalidObjectException ex) {
            throw new IPCLookupException(recipient, ex);
        }
        catch(final ipc.InvalidPartitionException ex) {
            throw new IPCLookupException(recipient, ex);
        }
    }
}
