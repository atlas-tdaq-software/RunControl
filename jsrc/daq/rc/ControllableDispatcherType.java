package daq.rc;

/**
 * Enumeration for the different ways of executing actions from multiple {@link JControllable} instances by the {@link JItemCtrl}.
 */
public enum ControllableDispatcherType {
    /**
     * Actions from each {@link JControllable} are serially executed following the order {@link JControllable} instances are passed to the
     * {@link JItemCtrl}
     */
    DEFAULT,
    /**
     * Actions from each {@link JControllable} are serially executed following the order {@link JControllable} instances are passed to the
     * {@link JItemCtrl} moving to the RUNNING state, and in the inverse order moving to the NONE state
     */
    INVERSE,
    /**
     * Actions from each {@link JControllable} are executed in a parallel concurrent way (no guarantee on the order)
     */
    PARALLEL
}
