package daq.rc;

/**
 * Enumeration for all the possible FSM states a Run Control application may be in.
 * <p>
 * The {@link ApplicationState#ABSENT} state is the default state (i.e., the state the application is before the FSM is initialized).<br>
 * The {@link ApplicationState#NONE} state is the FSM initial state for the Root Controller.<br>
 * The {@link ApplicationState#INITIAL} state is the FSM initial state for leaf applications and child controllers.
 */
public enum ApplicationState {
    /**
     * Default state (i.e., the state the application is before the FSM is initialized)
     */
    ABSENT,
    /**
     * FSM initial state for the Root Controller
     */
    NONE,
    /**
     * FSM initial state for leaf applications and child controllers
     */
    INITIAL, 
    CONFIGURED, 
    CONNECTED, 
    GTHSTOPPED, 
    SFOSTOPPED, 
    HLTSTOPPED, 
    DCSTOPPED, 
    ROIBSTOPPED, 
    RUNNING;
}
