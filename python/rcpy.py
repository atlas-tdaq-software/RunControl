"""This module was written to encapsulate librc_PyModules call"""
import librc_PyModules as L
from ipc import IPCPartition	#It is required for CommandSender.MakeTransition

Controllable = L.Controllable

FSM_STATE = L.FSM_STATE
FSM_COMMAND = L.FSM_COMMAND
RC_COMMAND = L.RC_COMMAND

TransitionCmd = L.TransitionCmd
SubTransitionCmd = L.SubTransitionCmd
ResynchCmd = L.ResynchCmd
UserBroadcastCmd = L.UserBroadcastCmd
UserCmd =L.UserCmd
ChangeStatusCmd = L.ChangeStatusCmd
TestAppCmd = L.TestAppCmd
is_start_t = L.is_start_t
is_stop_t = L.is_stop_t
is_restart_t = L.is_restart_t
StartStopAppCmd = L.StartStopAppCmd
ErrorElement = L.ErrorElement
CommandSender= L.CommandSender
