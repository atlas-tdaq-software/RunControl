/*
 * DVSController.cxx
 *
 *  Created on: Nov 28, 2013
 *      Author: avolio
 */
#include "RunControl/Controller/DVSController.h"
#include "RunControl/Controller/ApplicationList.h"
#include "RunControl/Controller/Application.h"
#include "RunControl/Controller/InformationPublisher.h"
#include "RunControl/Controller/ApplicationSupervisorInfo.h"
#include "RunControl/Common/Exceptions.h"

#include <ExpertSystemInterface/ExpertSystemInterface.h>
#include <rc/TestInfo.h>
#include <TM/TestResult.h>
#include <dvs/action.h>
#include <dvs/dvscomponent.h>
#include <dvs/exceptions.h>
#include <ers/Issue.h>
#include <ers/LocalContext.h>
#include <ers/ers.h>

#include <map>
#include <functional>
#include <sstream>
#include <mutex>
#include <set>
#include <utility>


namespace daq { namespace rc {

DVSController::DVSController(const std::shared_ptr<ApplicationList>& appList,
                             const std::shared_ptr<InformationPublisher>& infoPub) :
        m_app_sync(), m_app_list(appList), m_info_pub(infoPub), m_dvs_manager(new daq::dvs::Manager()), m_top_component(nullptr)
{
    m_dvs_manager->setTestsAutoExec(true);

    m_is_cleaner = std::shared_ptr<void>(nullptr, [appList, infoPub](void*)
    {
        for(const auto& app_ : appList->getAllApplications()) {
            infoPub->removeTestInfo(app_->getName());
        }
    });
}

DVSController::~DVSController() {
    stopAllTests();
}

void DVSController::load(::Configuration& config, const std::string& partitionName, const std::string& segmentName) {
    try {
        std::lock_guard<std::shared_mutex> lk(m_mutex);
        m_top_component.reset();
        m_top_component = m_dvs_manager->loadConfiguration(config, partitionName, segmentName, false);
    }
    catch(daq::dvs::CannotLoadDatabase& ex) {
        throw daq::rc::ConfigurationIssue(ERS_HERE, "the DVS core could not load the database", ex);
    }

    initISInfo(m_app_list->getAllApplications());
}

void DVSController::reload(const Application::ApplicationSet& addedApps, const Application::ApplicationSet& removedApps)
{
    try {
        std::lock_guard<std::shared_mutex> lk(m_mutex);
        m_top_component = m_dvs_manager->reloadConfiguration();

        // Initialize the IS information for new application
        initISInfo(addedApps);

        // Remove the IS information for no more available applications
        for(const auto& app_ : removedApps) {
            m_info_pub->removeTestInfo(app_->getName());
        }
    }
    catch(daq::dvs::CannotLoadDatabase& ex) {
        throw daq::rc::ConfigurationIssue(ERS_HERE, "the DVS core could not re-load the database", ex);
    }
}

std::future<daq::dvs::Result> DVSController::testApplication(const std::shared_ptr<Application>& app,
                                                             const daq::dvs::TestScope& scope,
                                                             const daq::dvs::TestLevel& level,
                                                             bool flushCache)
{
    std::future<daq::dvs::Result> futRet;

    // This will not allow a reload to be executed in the while
    // We need to lock the full method otherwise there is the risk to pass
    // to "testApplication" some component that is no more valid because after the reload
    std::shared_lock<std::shared_mutex> lk(m_mutex);

    // Reset the test status before launching a new test
    resetTestStatus(app);

    const std::string& appName = app->getName();

    try {
        daq::dvs::Component* c = m_dvs_manager->findComponent(appName);
        if(c != nullptr) {
            ERS_DEBUG(0, "Application " << appName << " found in DVS tree: " << c->key());

            if(flushCache == true) {
                m_dvs_manager->resetTestResult(c);
            }

            daq::dvs::CallbackV&& cb = std::bind(&DVSController::testCallback, this, std::placeholders::_1);

            std::set<std::string> hosts = app->getBackupHosts();
            hosts.insert(app->getHost());
            hosts.erase(app->getApplicationSVInfo().getRunningHost());

            futRet = m_dvs_manager->testApplication(c, hosts, std::move(cb), scope, level);
        } else {
            ERS_DEBUG(0, "Application " << appName << " not found in DVS tree");
            // The application is not known to the DVS system: it means that no tests are associated to it
            // In this case the test status is set to UNSUPPORTED
            {
                std::lock_guard<std::mutex> lk(*(m_app_sync.get(appName)));

                app->setTestStatus(daq::tmgr::TestResult::TmUnsupported);

                TestInfo ti;
                ti.objectId = appName;
                ti.testStatus = DVSController::toString(daq::tmgr::TestResult::TmUnsupported);
                ti.testLog = std::string("No tests are defined for this application");

                m_info_pub->publish(appName, daq::es::rc::TestInfo {daq::tmgr::TestResult::TmUnsupported, {}}, ti, {});
            }

            // Return a proper future
            std::promise<daq::dvs::Result> p;

            daq::dvs::Result r;
            r.globalResult = daq::tmgr::TestResult::TmUnsupported;
            r.componentId = appName;
            r.componentResult.result = daq::tmgr::TestResult::TmUnsupported;

            p.set_value(r);
            futRet = p.get_future();
        }
    }
    catch(ers::Issue& ex) {
        ers::error(ex);

        // The application cannot be tested: set the test status to UNTESTED
        // The lock makes sure that the update is not executed concurrently with some call-back
        {
            std::lock_guard<std::mutex> lk(*(m_app_sync.get(appName)));

            app->setTestStatus(daq::tmgr::TestResult::TmUntested);

            TestInfo ti;
            ti.objectId = appName;
            ti.testStatus = DVSController::toString(daq::tmgr::TestResult::TmUntested);
            ti.testLog = std::string("Tests could not be executed: ") + ex.message();

            m_info_pub->publish(appName, daq::es::rc::TestInfo {daq::tmgr::TestResult::TmUntested, {}}, ti, {});
        }

        // Return a proper future
        std::promise<daq::dvs::Result> p;

        daq::dvs::Result r;
        r.globalResult = daq::tmgr::TestResult::TmUntested;
        r.componentId = appName;
        r.componentResult.result = daq::tmgr::TestResult::TmUntested;

        p.set_value(r);
        futRet = p.get_future();
    }

    return futRet;
}

void DVSController::testCallback(std::vector<daq::dvs::Result> testResults) {
    if(testResults.empty() == false) {
        const std::string& appName = testResults[0].componentId;
        const auto& app_ = m_app_list->getApplication(appName);
        if(app_.get() != nullptr) {
            // Do not process call-backs for the same application concurrently
            std::lock_guard<std::mutex> lk(*(m_app_sync.get(appName)));

            daq::tmgr::TestResult globalResult = testResults[0].globalResult;
            const std::string& globalResultStr = DVSController::toString(globalResult);

            // Global result for the application
            TestInfo globalTestInfo;
            globalTestInfo.objectId = appName;
            globalTestInfo.testStatus = globalResultStr;
            globalTestInfo.testLog = std::string("Test status is ") + globalResultStr;

            // Test results for all the other tested components
            std::vector<TestInfo> componentsTestInfo;
            componentsTestInfo.reserve(testResults.size());

            std::vector<std::shared_ptr<daq::es::rc::ComponentTestInfo>> components;
            for(const daq::dvs::Result& res : testResults) {
                // Fill the info for CHIP
                auto&& ci = std::make_shared<daq::es::rc::ComponentTestInfo>();
                ci->m_component_name = res.componentId;
                ci->m_test_result = res.componentResult.result;
                ci->m_actions.insert(ci->m_actions.end(), res.actions.begin(), res.actions.end());
                components.push_back(std::move(ci));

                // Fill the info for IS
                const std::string& testLog = getLog(res.componentResult);

                TestInfo ti;
                ti.objectId = res.componentId;
                ti.testStatus = DVSController::toString(res.componentResult.result);
                ti.testLog = (testLog.empty() == true) ? "No log available" : testLog;

                componentsTestInfo.push_back(std::move(ti));
            }

            app_->setTestStatus(globalResult);
            m_info_pub->publish(appName,
                                daq::es::rc::TestInfo {globalResult, std::move(components)},
                                globalTestInfo,
                                componentsTestInfo);
        } else {
            ers::warning(daq::rc::TestIssue(ERS_HERE,
                                            appName,
                                            "the controller received a test result for it but the application is unknown"));
        }
    } else {
        ERS_LOG("Got an empty test result");
    }
}

void DVSController::resetTestStatus(const std::shared_ptr<Application>& app) {
    const std::string& appName = app->getName();

    // The lock makes sure that the update is not executed concurrently with some call-back
    std::lock_guard<std::mutex> lk(*(m_app_sync.get(appName)));

    const auto& result = app->setTestStatus(daq::tmgr::TestResult::TmUndef);
    if(result.second == true) {
        TestInfo ti;
        ti.objectId = appName;
        ti.testStatus = DVSController::toString(daq::tmgr::TestResult::TmUndef);
        ti.testLog = "No log available";

        m_info_pub->publish(appName,
                            daq::es::rc::TestInfo {daq::tmgr::TestResult::TmUndef, {}},
                            ti,
                            {},
                            InformationPublisher::Destinations::IS_SERVER);
    }
}

void DVSController::stopTest(const std::string& appName) {
    // This is not a shared lock because we do not want any test to be started if we are stopping them
    std::lock_guard<std::shared_mutex> lk(m_mutex);

    daq::dvs::Component* c = m_dvs_manager->findComponent(appName);
    if(c != nullptr) {
        try {
            m_dvs_manager->stopTest(c);
        }
        catch(daq::dvs::NoTestHandle& ex) {
            ers::debug(ex, 0);
        }
    }
}

void DVSController::stopAllTests() {
    const auto& allApps = m_app_list->getAllApplications();

    // This is not a shared lock because we do not want any test to be started if we are stopping them
    std::lock_guard<std::shared_mutex> lk(m_mutex);

    // Check validity because this method may be executed in the destructor
    // (and in principle when exceptions are thrown from the initialization list)
    if((m_top_component.get() != nullptr) && (m_dvs_manager.get() != nullptr)) {
        for(const auto& app_ : allApps) {
            const auto& appName = app_->getName();
            daq::dvs::Component* c = m_dvs_manager->findComponent(appName);
            if(c != nullptr) {
                try {
                    m_dvs_manager->stopTest(c);
                }
                catch(daq::dvs::NoTestHandle& ex) {
                    ers::debug(ex, 0);
                }
            }
        }
    }
}

void DVSController::initISInfo(const Application::ApplicationSet& apps) {
    // Put default values for all the applications in IS
    for(const auto& app_ : apps) {
        const std::string& appName = app_->getName();

        TestInfo ti;
        ti.objectId = appName;
        ti.testStatus = DVSController::toString(daq::tmgr::TestResult::TmUndef);
        ti.testLog = "No log available";

        m_info_pub->publish(appName,
                            daq::es::rc::TestInfo {daq::tmgr::TestResult::TmUndef, {}},
                            ti,
                            {},
                            InformationPublisher::Destinations::IS_SERVER);
    }
}

std::string DVSController::getLog(const daq::tm::ComponentResult& res) {
    std::string testLog;

    testLog += std::string("*** Test global result is ") + DVSController::toString(res.result) + " ("
            + std::to_string(res.result) + ") ***\n";

    {
        // Collect issues
        std::string allIssues;
        for(const auto& r : res.test_results) {
            std::string issues;
            for(const auto& i : r.issues) {
                std::ostringstream os;
                os << *i << "\n";
                issues += os.str();
            }

            if(issues.empty() == false) {
                issues.insert(0, "Test \"" + r.test_id + "\":\n");
                issues += "\n";
                allIssues += issues;
            }
        }

        if(allIssues.empty() == false) {
            allIssues.insert(0, "\n*** Errors occurred launching some tests:\n\n");
            allIssues += "\n";
            testLog += allIssues;
        }
    }

    for(const auto& r : res.test_results) {
        testLog += "\n*** " + r.test_id + " (return code " + std::to_string(r.return_code) + ") ***\n";

        if(r.diagnosis.empty() == false) {
            testLog += "\n*** Diagnosis ***\n" + r.diagnosis + "\n";
        } else {
            testLog += "\n*** Diagnosis ***\nNone\n";
        }

        testLog += "- Standard OUT:\n";
        if(r.stdout.empty() == false) {
            testLog += r.stdout + "\n";
        } else {
            testLog += "Empty\n";
        }

        testLog += "- Standard ERR:\n";
        if(r.stderr.empty() == false) {
            testLog += r.stderr + "\n";
        } else {
            testLog += "Empty\n";
        }
    }

    return testLog;
}

std::string DVSController::toString(daq::tmgr::TestResult rc) {
    static std::map<daq::tmgr::TestResult, std::string> map = {{daq::tmgr::TmPass, "PASSED"},
                                                               {daq::tmgr::TmUndef, "UNDEFINED"},
                                                               {daq::tmgr::TmFail, "FAILED"},
                                                               {daq::tmgr::TmUnresolved, "UNRESOLVED"},
                                                               {daq::tmgr::TmUntested, "UNTESTED"},
                                                               {daq::tmgr::TmUnsupported, "UNSUPPORTED"}};

    const auto& it = map.find(rc);
    if(it != map.end()) {
        return it->second;
    }

    ers::error(daq::rc::Exception(ERS_HERE, std::string("Unknown test result: " + std::to_string(rc))));

    return std::string("UNDEFINED");
}

}}

