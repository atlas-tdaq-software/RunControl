/*
 * ApplicationList.cxx
 *
 *  Created on: Nov 5, 2012
 *      Author: avolio
 */

#include "RunControl/Controller/ApplicationList.h"
#include "RunControl/Controller/InfrastructureApplication.h"
#include "RunControl/Controller/RunControlApplication.h"
#include "RunControl/Controller/ControllerApplication.h"
#include "RunControl/Controller/OSInfrastructureApplication.h"
#include "RunControl/Common/RunControlCommands.h"
#include "RunControl/Common/Exceptions.h"

#include <config/Configuration.h>
#include <config/DalObject.h>
#include <config/Errors.h>
#include <config/map.h>
#include <dal/Partition.h>
#include <dal/Segment.h>
#include <dal/OnlineSegment.h>
#include <dal/BaseApplication.h>
#include <dal/RunControlApplicationBase.h>
#include <dal/util.h>
#include <ers/Issue.h>
#include <ers/LocalContext.h>
#include <ers/ers.h>

#include <boost/tuple/tuple.hpp>
#include <boost/algorithm/string.hpp>

#include <set>
#include <vector>
#include <utility>
#include <mutex>
#include <sstream>
#include <type_traits>


namespace daq { namespace rc {

ApplicationList::ApplicationList() : m_app_list(), m_mutex() {
}

ApplicationList::ApplicationList(ApplicationList&& other) {
	std::lock_guard<std::shared_mutex> lk_other(other.m_mutex);
	m_app_list.swap(other.m_app_list);
}

ApplicationList::ApplicationList(const ApplicationList& other) {
	std::shared_lock<std::shared_mutex> lk_other(other.m_mutex);
	m_app_list = other.m_app_list;
}

ApplicationList& ApplicationList::operator=(ApplicationList other) {
	{
		// Lock only this mutex; no need to lock also "other" because it is a copy
		std::lock_guard<std::shared_mutex> lk(m_mutex);
		m_app_list.swap(other.m_app_list);
	}

	return *this;
}

ApplicationList::~ApplicationList() noexcept {
}

void ApplicationList::load(Configuration& db, const daq::core::Partition& part, const std::string& thisSegmentName) {
	try {
		const daq::core::OnlineSegment* onlSeg = part.get_OnlineInfrastructure();
		if(onlSeg != nullptr) {
			const bool isRoot = (onlSeg->UID() == thisSegmentName);

			///////////////////////////////////////////////////////////////////
	        // First get all the applications and child segments from Config //
			///////////////////////////////////////////////////////////////////

            // Get the Segment for the Online segment (needed to build the Applications)
			const daq::core::Segment* rootSeg = part.get_segment(onlSeg->UID());

			// Here we get this segment
			const daq::core::Segment* thisSeg = part.get_segment(thisSegmentName);

            // Collect here all the applications
			std::vector<const daq::core::BaseApplication*> allApps = rootSeg->get_all_applications();

            /////////////////////////////////////////////////////////////////////////
            // Now let's start to organize applications in well defined categories //
            /////////////////////////////////////////////////////////////////////////

            // Write lock
            std::lock_guard<std::shared_mutex> lk(m_mutex);

            m_app_list.clear();

	        // From nested segments take the controllers and the infrastructure applications
	        for(const auto ns : thisSeg->get_nested_segments()) {
	            // The segment's controller
	            if(ns->is_disabled() == false) {
                    const daq::core::BaseApplication* ctrl = ns->get_controller();

                    std::shared_ptr<Application> ctrlApp(new ControllerApplication(db, part, *ctrl, isRoot));
                    ctrlApp->unfoldStartDependencies(*ctrl, allApps);
                    ctrlApp->unfoldShutdownDependencies(*ctrl, allApps);

                    ERS_DEBUG(2, "Adding child controller: " << ctrl->UID());

                    m_app_list.insert(ctrlApp);

                    // The infrastructure applications
                    for(const auto infr : ns->get_infrastructure()) {
                        std::shared_ptr<Application> infrApp(new InfrastructureApplication(db, part, *infr, isRoot));
                        infrApp->unfoldStartDependencies(*infr, allApps);
                        infrApp->unfoldShutdownDependencies(*infr, allApps);

                        ERS_DEBUG(2, "\tAdding infrastructure application: " << infr->UID());

                        m_app_list.insert(infrApp);
                    }
	            }
	        }

	        // Here are the Online Segment infrastructure applications
	        if(isRoot == true) {
	            for(const auto os : thisSeg->get_infrastructure()) {
	                std::shared_ptr<Application> osApp(new OSInfrastructureApplication(db, part, *os));
	                osApp->unfoldStartDependencies(*os, allApps);
	                osApp->unfoldShutdownDependencies(*os, allApps);

	                ERS_DEBUG(2, "Adding Online Segment infrastructure application: " << os->UID());

	                m_app_list.insert(osApp);
	            }
	        }

	        // And here are the Run Control and simple applications
	        for(const auto app_ : thisSeg->get_applications()) {
	            if(db.cast<daq::core::RunControlApplicationBase>(app_) != nullptr) {
	                // This is a Run Control application
	                std::shared_ptr<Application> rcApp(new RunControlApplication(db, part, *app_, isRoot));
	                rcApp->unfoldStartDependencies(*app_, allApps);
	                rcApp->unfoldShutdownDependencies(*app_, allApps);

                    ERS_DEBUG(2, "Adding Run Control application: " << app_->UID());

	                m_app_list.insert(rcApp);
	            } else {
	                // This is a standard application
	                std::shared_ptr<Application> stdApp(new Application(db, part, *app_, isRoot));
	                stdApp->unfoldStartDependencies(*app_, allApps);
	                stdApp->unfoldShutdownDependencies(*app_, allApps);

                    ERS_DEBUG(2, "Adding standard application: " << app_->UID());

	                m_app_list.insert(stdApp);
	            }
	        }
		} else {
		    const std::string msg = "Cannot build the list of applications to control";
		    const std::string reason = "cannot find the partition online segment";
		    throw daq::rc::ConfigurationIssue(ERS_HERE, msg + ": " + reason, reason);
		}
	}
	catch(daq::config::Exception& ex) {
		const std::string msg = "Cannot build the list of applications to control";
		const std::string& reason = ex.message();
	    throw daq::rc::ConfigurationIssue(ERS_HERE, msg + ": " + reason, reason, ex);
	}
	catch(daq::core::AlgorithmError& ex) {
        const std::string msg = "Cannot build the list of applications to control";
        const std::string& reason = ex.message();
        throw daq::rc::ConfigurationIssue(ERS_HERE, msg + ": " + reason, reason, ex);
	}
}

bool ApplicationList::isEmpty() const {
    std::shared_lock<std::shared_mutex> lk(m_mutex);

	return m_app_list.empty();
}

std::shared_ptr<Application> ApplicationList::getApplication(const std::string& applicationName) const {
	std::shared_lock<std::shared_mutex> lk(m_mutex);

	const auto& map = m_app_list.get<name_key>();
	const auto& it = map.find(applicationName);
	if(it != map.end()) {
		return (*it);
	} else {
		return nullptr;
	}
}

Application::ApplicationSet ApplicationList::getApplications(const std::regex& expression) const {
    Application::ApplicationSet apps;

    {
        std::shared_lock<std::shared_mutex> lk(m_mutex);

        const auto& start = m_app_list.begin();
        const auto& end = m_app_list.end();
        for(auto it = start; it != end; ++it) {
            if(std::regex_match((*it)->getName(), expression) == true) {
                apps.insert(*it);
            }
        }
    }

    return apps;
}

std::map<std::string, std::shared_ptr<Application>>
ApplicationList::getApplications(const std::set<std::string>& applications) const {
    static const auto TOKEN_SIZE = RunControlCommands::REGEX_TOKEN.size();
    const auto isRegExEnabled = [] (const std::string& name) { return boost::algorithm::ends_with(name, RunControlCommands::REGEX_TOKEN); };

    std::map<std::string, std::shared_ptr<Application>> apps;

    for(const std::string& n : applications) {
        std::shared_ptr<Application>&& app = getApplication(n);
        if((app.get() != nullptr) || (isRegExEnabled(n) == false)) {
            apps.emplace(std::make_pair(n, std::move(app)));
        } else {
            // Try a regex
            const std::string regEx(n.substr(0, n.size() - TOKEN_SIZE));
            try {
                Application::ApplicationSet&& regExApps = getApplications(std::regex(regEx));
                if(regExApps.empty() == true) {
                    apps.emplace(std::make_pair(n, nullptr));
                    ERS_LOG("\"" << regEx << "\" tested as a regular expression but it did not match the name of any application");
                } else {
                    for(auto&& ap : regExApps) {
                        apps.emplace(std::make_pair(ap->getName(), std::move(ap)));
                    }
                }
            }
            catch(std::regex_error& ex) {
                apps.emplace(std::make_pair(regEx, nullptr));
                ERS_LOG("\"" << n << "\" tested as a regular expression but it is not valid: " << ex.what());
            }
        }
    }

    return apps;
}

Application::ApplicationSet ApplicationList::getApplications(ApplicationType appType) const {
    Application::ApplicationSet apps;

	{
		std::shared_lock<std::shared_mutex> lk(m_mutex);

		const auto& multi_map = m_app_list.get<type_start_key>();
		auto&& pair = multi_map.equal_range(appType);
		while(pair.first != pair.second) {
			apps.insert(*(pair.first++));
		}
	}

	return apps;
}

Application::ApplicationSet ApplicationList::getApplications(const std::string& when, bool atStart) const {
    Application::ApplicationSet apps;

	{
		std::shared_lock<std::shared_mutex> lk(m_mutex);

		if(atStart == true) {
			const auto& multi_map = m_app_list.get<start_key>();
			auto&& pair = multi_map.equal_range(when);
			while(pair.first != pair.second) {
				apps.insert(*(pair.first++));
			}
		} else {
			const auto& multi_map = m_app_list.get<stop_type_key>();
			auto&& pair = multi_map.equal_range(when);
			while(pair.first != pair.second) {
				apps.insert(*(pair.first++));
			}
		}
	}

	return apps;
}

Application::ApplicationSet ApplicationList::getApplications(ApplicationType appType, const std::string& when, bool atStart) const {
    Application::ApplicationSet apps;

	{
		std::shared_lock<std::shared_mutex> lk(m_mutex);

		if(atStart == true) {
			const auto& multi_map = m_app_list.get<type_start_key>();
			auto&& pair = multi_map.equal_range(boost::make_tuple(appType, when));
			while(pair.first != pair.second) {
				apps.insert(*(pair.first++));
			}
		} else {
			const auto& multi_map = m_app_list.get<stop_type_key>();
			auto&& pair = multi_map.equal_range(boost::make_tuple(when, appType));
			while(pair.first != pair.second) {
				apps.insert(*(pair.first++));
			}
		}
	}

	return apps;
}

Application::ApplicationSet ApplicationList::getAllApplications() const {
	std::shared_lock<std::shared_mutex> lk(m_mutex);
	return Application::ApplicationSet(m_app_list.begin(), m_app_list.end());
}

void ApplicationList::swap(ApplicationList& other) {
	// Mutexes are locked in the free function
	::swap(*this, other);
}

}}

void swap(daq::rc::ApplicationList& lhs, daq::rc::ApplicationList& rhs) {
    std::unique_lock<std::shared_mutex> lk_l(lhs.m_mutex, std::defer_lock);
    std::unique_lock<std::shared_mutex> lk_r(rhs.m_mutex, std::defer_lock);
    std::lock(lk_l, lk_r);

    lhs.m_app_list.swap(rhs.m_app_list);
}
