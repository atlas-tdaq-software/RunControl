/*
 * OnlineSegmentApplication.cxx
 *
 *  Created on: Nov 2, 2012
 *      Author: avolio
 */

#include "RunControl/Controller/OSInfrastructureApplication.h"
#include "RunControl/FSM/FSMCommands.h"


namespace daq { namespace rc {

OSInfrastructureApplication::OSInfrastructureApplication(Configuration& db,
                                                         const daq::core::Partition& p,
                                                         const daq::core::BaseApplication& appConfig)
    : InfrastructureApplication(db, p, appConfig, true)
{
    InfrastructureApplication::setStartAt(FSMCommands::INIT_FSM_CMD);
    InfrastructureApplication::setStopAt(FSMCommands::STOP_FSM_CMD);
}

OSInfrastructureApplication::~OSInfrastructureApplication() noexcept {
}

ApplicationType OSInfrastructureApplication::type() const noexcept {
	return ApplicationType::ONLINE_SEGMENT_INFRASTRUCTURE;
}

}}
