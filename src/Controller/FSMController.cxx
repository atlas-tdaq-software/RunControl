/*
 * FSMController.cxx
 *
 *  Created on: Aug 31, 2012
 *      Author: avolio
 */

#include "RunControl/Controller/FSMController.h"
#include "RunControl/Common/RunControlCommands.h"
#include "RunControl/Common/Exceptions.h"
#include "RunControl/FSM/FSMCommands.h"
#include "RunControl/FSM/FSMStates.h"

#include <ers/LocalContext.h>

#include <exception>
#include <string>
#include <vector>


namespace daq { namespace rc {

FSMController::FSMController(const std::shared_ptr<RunControlTransitionActions>& rc, const std::shared_ptr<UserRoutines>& ur) :
		m_rc_actions(rc), m_user_functions(ur), m_fsm()
{
}

FSMController::~FSMController() {
}

void FSMController::start(bool isRestart) {
    m_fsm->start(isRestart);
}

void FSMController::interruptTransition(bool shouldInterrupt) {
    m_fsm->interruptTransition(shouldInterrupt);
}

bool FSMController::makeTransition(const std::shared_ptr<TransitionCmd>& cmd) {
	return m_fsm->makeTransition(cmd);
}

void FSMController::goToState(FSM_STATE state, bool restart, const Algorithms::SubtransitionMap& st) {
    if(m_fsm->isStateValid(state) == false) {
        throw daq::rc::InvalidState(ERS_HERE, FSMStates::stateToString(state));
    }

    FSM_STATE currState = std::get<0>(status());
    if(currState != state) {
        const auto& trans = m_fsm->transitions(currState, state);
        if(trans.empty() == false) {
            for(FSM_COMMAND cmd : trans) {
                std::shared_ptr<TransitionCmd> trCmd(new TransitionCmd(cmd, restart));
                trCmd->subTransitions(st);

                makeTransition(trCmd);

                const FSM_STATE newState = std::get<0>(status());
                if(currState != newState) {
                    currState = newState;
                } else {
                    // If the state is not changed in the transition, then it
                    // means that the transition has been interrupted
                    throw daq::rc::TransitionInterrupted(ERS_HERE, trCmd->fsmCommand());
                }
            }
        } else {
            throw daq::rc::FSMPathError(ERS_HERE,
                                        FSMStates::stateToString(currState),
                                        FSMStates::stateToString(state),
                                        "no path returned by the FSM");
        }
    }
}

bool FSMController::isBusy() const {
	return m_fsm->busy();
}

bool FSMController::isCommandValid(FSM_COMMAND cmd) const {
    return m_fsm->isCommandValid(cmd);
}

std::tuple<FSM_STATE, FSM_STATE, FSM_COMMAND> FSMController::status() const {
    return m_fsm->status();
}

FSMController::CallbackToken FSMController::registerOnFSMBusyCallback(const FSMDefs::OnFSMBusyType& cb) {
    return m_busy_signal.connect(cb);
}

FSMController::CallbackToken FSMController::registerOnTransitioningCallback(const FSMDefs::OnTransitioningSlotType& cb) {
    return m_transitioning_signal.connect(cb);
}

FSMController::CallbackToken FSMController::registerOnTransitionDoneCallback(const FSMDefs::OnTransitionDoneSlotType& cb) {
    return m_transition_done_signal.connect(cb);
}

FSMController::CallbackToken FSMController::registerOnNoTransitionCallback(const FSMDefs::OnNoTransitionSlotType& cb) {
    return m_no_transition_signal.connect(cb);
}

FSMController::CallbackToken FSMController::registerOnTransitionExceptionCallback(const FSMDefs::OnTransitionExceptionSlotType& cb) {
    return m_transition_ex_signal.connect(cb);
}

void FSMController::removeCallback(const CallbackToken& token) {
    token.disconnect();
}

void FSMController::busy(bool isBusy) {
    m_busy_signal(isBusy);
}

void FSMController::transitioning(const TransitionCmd& trCmd) {
    m_transitioning_signal(trCmd);
}

void FSMController::transitionDone(const TransitionCmd& trCmd) {
    m_transition_done_signal(trCmd);
}

void FSMController::noTransition(const TransitionCmd& trCmd) {
    m_no_transition_signal(trCmd);
}

void FSMController::transitionException(const TransitionCmd& trCmd, std::exception& ex) {
    m_transition_ex_signal(trCmd, ex);
}

}}
