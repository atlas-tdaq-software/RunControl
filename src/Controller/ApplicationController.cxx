/*
 * ApplicationController.cxx
 *
 *  Created on: Nov 12, 2012
 *      Author: avolio
 */

#include "RunControl/Controller/ApplicationController.h"
#include "RunControl/Controller/ApplicationList.h"
#include "RunControl/Controller/ApplicationSupervisorInfo.h"
#include "RunControl/Controller/ApplicationAlgorithms.h"
#include "RunControl/Controller/InformationPublisher.h"
#include "RunControl/Controller/DVSController.h"
#include "RunControl/FSM/FSMStates.h"
#include "RunControl/FSM/FSMCommands.h"
#include "RunControl/Common/Exceptions.h"
#include "RunControl/Common/OnlineServices.h"
#include "RunControl/Common/ThreadPool.h"
#include "RunControl/Common/RunControlCommands.h"

#include <ProcessManager/Singleton.h>
#include <ProcessManager/ProcessDescription.h>
#include <ProcessManager/Process.h>
#include <ProcessManager/Handle.h>
#include <ProcessManager/Exceptions.h>
#include <ProcessManager/defs.h>

#include <config/Configuration.h>
#include <config/Errors.h>
#include <config/map.h>
#include <dal/Computer.h>
#include <TestManager/Test.h>
#include <ers/ers.h>
#include <ers/Issue.h>
#include <ers/LocalContext.h>

#include <boost/bind/bind.hpp>
#include <boost/bind/placeholders.hpp>

#include <algorithm>
#include <thread>
#include <future>
#include <chrono>
#include <random>
#include <iterator>
#include <map>
#include <ostream>
#include <unordered_map>

#include <unistd.h>


namespace daq { namespace rc {

const std::unique_ptr<ThreadPool> ApplicationController::m_pmg_cbk_tp(new ThreadPool(1));

class StopAppTask : public FutureTask<bool> {
    public:
        StopAppTask(ApplicationController& appCtrl, const std::shared_ptr<Application>& app, bool checkDeps, bool interruptible)
            : FutureTask<bool>(), m_app_ctrl(appCtrl), m_app(app), m_check_deps(checkDeps), m_interruptible(interruptible)
        {
        }

        const std::shared_ptr<Application>& application() {
            return m_app;
        }

    protected:
        // If the process is not stopped because in 'REQUESTED' then wait and retry
        bool run() override {
            bool signalSent = false;

   	     	const auto& sp = m_app_ctrl.get().applicationSyncPair(m_app);
          	std::unique_lock<std::recursive_mutex> lock(sp->first);

            checkInterrupted();

            while(((signalSent = m_app_ctrl.get().stopApp(m_app, m_check_deps)) == false) && (m_app->getApplicationSVInfo().getStatus() == APPLICATION_STATUS::REQUESTED)) {
                sp->second.wait_for(lock, std::chrono::milliseconds(100));
                checkInterrupted();
          	}

            return signalSent;
        }

        void checkInterrupted() {
            if(m_interruptible == true) {
                Interruptible::interrupted();
                m_app_ctrl.get().interrupted();
            }
        }

    private:
        const std::reference_wrapper<ApplicationController> m_app_ctrl;
        const std::shared_ptr<Application> m_app;
        const bool m_check_deps;
        const bool m_interruptible;
};

struct AppCtrlDeleter {
    void operator()(ApplicationController* ctrl) const {
        delete ctrl;
    }
};

// Note the usage of the weak pointer: this allows a safe execution of this callback function even if it comes very late (i.e., because
// of some huge timeout or just because an application may exit in the while). In case the ApplicationController has no other references,
// this function will just do nothing. Otherwise it will acquire a shared reference to the ApplicationController itself and it will be
// safely executed. This is a by far better solution than using the "void*" standard argument of a typical PMG call-back.
void ApplicationController::pmgCallback(const std::weak_ptr<ApplicationController>& w_ac, std::shared_ptr<daq::pmg::Process> p, void* /* do not use this */) {
    const std::string& processName = p->name();
    const std::string& handle = p->handle().toString();
    unsigned int procID = p->handle().id();
    const PMGProcessStatusInfo& processStatus = p->status();
    const bool processExited = p->exited();

    try {
        const auto& ac = w_ac.lock();
        if(ac.get() != nullptr) {
            ApplicationController::m_pmg_cbk_tp->submit<void>([ac, processName, handle, procID, processStatus, processExited] ()
            {
                if(ac.get() != nullptr) {
                    std::shared_ptr<Application> app = ac->m_app_list->getApplication(processName);
                    if(app.get() != nullptr) {
                        ERS_DEBUG(2, "Received PMG call-back for application " << processName << " with state \"" << daq::pmg::p_state_strings[processStatus.state] << "\"");

                        APPLICATION_STATUS newStatus = APPLICATION_STATUS::NOTAV; // Just to initialize it

                        // Here this lock is very important: it synchronizes the application call-back with any attempt to start or stop the application itself
                        const auto& sp = ac->applicationSyncPair(app);
                        std::unique_lock<std::recursive_mutex> lock(sp->first);

                        // The PMG document says that when a call-back is received, the process can be only in the following states:
                        // FAILED, SIGNALED, EXITED, SYNCERROR, RUNNING
                        const ApplicationSVInfo& appSVInfo = app->getApplicationSVInfo();
                        newStatus = appSVInfo.getStatus();

                        // Why this? To handle the following sequence of event:
                        // - Application UP;
                        // - The PMG (or the machine where it is running) dies;
                        // - The application is started on a back-up host;
                        // - The PMG is back again and the initial process is no more there (or just exits after a while);
                        // - This call-back is called but it does not refer to the current process anymore.
                        // P.S.: Always process the call-back for RUNNING: it may be that we get a TIMEOUT when trying to start it
                        // (and the handle is not updated with the new one) but the process is actually started (it just took more
                        // than expected).
                        if((handle == appSVInfo.getHandle()) || (processStatus.state == PMGProcessState_RUNNING)) {
                            switch(processStatus.state) {
                                case PMGProcessState_RUNNING: {
                                    const APPLICATION_STATUS statusNow = appSVInfo.getStatus();
                                    if((statusNow == APPLICATION_STATUS::TERMINATING) && (app->getInitTimeout() != 0)) {
                                        // The running call-back is coming but the status is TERMINATING and the process init timeout
                                        // is different than zero: it means that the process is being killed while still in REQUESTED
                                        // In this case the PMG, for consistency, always sends the RUNNING call-back followed by the
                                        // EXITED one
                                        ERS_LOG("Received a RUNNING call-back for application " << processName << " but is it actually being stopped");
                                    } else if((statusNow != APPLICATION_STATUS::REQUESTED) && (procID == appSVInfo.getLastID())) {
                                        // The running call-back is coming in a state different than REQUESTED. It may be an
                                        // out-of-order call-back but in this case the IDs of the applications must be the same
                                        ERS_LOG("Out of order PMG call-back for application " << processName);
                                    } else {
                                        ERS_DEBUG(1, "Application " << processName << " running on host " << processStatus.start_info.host);
                                        newStatus = APPLICATION_STATUS::UP;

                                        // Reset the test status: this has to be done before the new application
                                        // status is set for the application, otherwise a bad result may be used
                                        // for the test check during the transitions (e.g., the test status is
                                        // still successful before the test itself is started)
                                        ac->m_dvs_ctrl->resetTestStatus(app);
                                    }

                                    break;
                                }
                                case PMGProcessState_EXITED: {
                                    if((appSVInfo.getStatus() != APPLICATION_STATUS::TERMINATING) &&
                                            ((app->isSpontaneousExitAllowed() == false) || (processStatus.info.exit_value != 0)))
                                    {
                                        ers::warning(daq::rc::ApplicationExited(ERS_HERE, processName, std::string(processStatus.start_info.host), std::string(processStatus.start_info.streams.out_path),
                                                                                std::string(processStatus.start_info.streams.err_path), processStatus.info.exit_value));
                                    }

                                    newStatus = APPLICATION_STATUS::EXITED;

                                    break;
                                }
                                case PMGProcessState_SIGNALED: {
                                    if(appSVInfo.getStatus() == APPLICATION_STATUS::TERMINATING) {
                                        ers::warning(daq::rc::ApplicationSignaled(ERS_HERE, processName, std::string(processStatus.start_info.host), std::string(processStatus.start_info.streams.out_path),
                                                                                     std::string(processStatus.start_info.streams.err_path), processStatus.info.signal_value, true));
                                    } else {
                                        ers::warning(daq::rc::ApplicationSignaled(ERS_HERE, processName, std::string(processStatus.start_info.host), std::string(processStatus.start_info.streams.out_path),
                                                                                     std::string(processStatus.start_info.streams.err_path), processStatus.info.signal_value, false));
                                    }

                                    newStatus = APPLICATION_STATUS::EXITED;

                                    break;
                                }
                                case PMGProcessState_SYNCERROR: {
                                    const std::string errMsg = "the application could not be started within the init timeout";
                                    ers::warning(daq::rc::ApplicationGenericIssue(ERS_HERE, processName, errMsg));

                                    newStatus = APPLICATION_STATUS::FAILED;

                                    break;
                                }
                                case PMGProcessState_FAILED: {
                                    const std::string& errMsg = std::string("the application failed to start with reason \"") + std::string(processStatus.failure_str_hr) + "\"";
                                    ers::warning(daq::rc::ApplicationGenericIssue(ERS_HERE, processName, errMsg));

                                    newStatus = APPLICATION_STATUS::FAILED;

                                    break;
                                }
                                default: {
                                    // We should never never be here
                                    ers::fatal(daq::rc::BadPMGCallback(ERS_HERE, processName, daq::pmg::p_state_strings[processStatus.state]));
                                    break;
                                }
                            }

                            // The last ID is updated so that we have the possibility to understand whether a RUNNING call-back is received out-of-order
                            app->setExitCode(processStatus.info.exit_value);
                            app->setExitSignal(processStatus.info.signal_value);
                            app->setHandle(handle);
                            app->setLastID(procID);
                            app->setRunningHost(std::string(processStatus.start_info.host));
                            app->setStatus(newStatus);

                            // First publish the new application's status
                            const std::string& an = app->getName();
                            const ApplicationSVInfo& appSVInfo_new = app->getApplicationSVInfo();
                            ac->m_info_publisher->publish(an, appSVInfo_new);

                            // Then remove the info if we are exiting
                            if((processExited == true) && (ac->m_exiting == true)) {
                                ac->m_info_publisher->removeProcessInfo(an);
                            }

                            // Inform whoever is waiting about the change in the application status
                            // Keep this at the end, so that we are sure that all the operations done in
                            // this call-back are executed before all the 'waits' are "resurrected"
                            sp->second.notify_all();

                            lock.unlock();

                            if(processExited == true) {
                                ac->signalApplicationExited(app, ac->m_exiting.load());

                                // If the application failed to start, then schedule a precondition test
                                if(newStatus == APPLICATION_STATUS::FAILED) {
                                    ac->m_dvs_ctrl->testApplication(app, daq::tm::Test::Scope::Precondition, 1);
                                }
                            } else {
                                // Schedule test for this process: remember to reset the test status before
                                // This is now done when the process status is analyzed (few lines before...)
                                ac->m_dvs_ctrl->testApplication(app, daq::tm::Test::Scope::Functional, 1);
                            }
                        } else {
                            ers::warning(daq::rc::ApplicationGenericIssue(ERS_HERE,
                                                                          processName,
                                                                          "PMG call-back not processed because referring to an old instance of the process (this may happen when a host or the PMG itself dies)"));
                        }
                    } else {
                        ers::warning(daq::rc::ApplicationGenericIssue(ERS_HERE,
                                                                      processName,
                                                                      "cannot process a PMG call-back because the application is not in the controller's list of known applications"));
                    }
                }
            });
        }
    }
    catch(daq::rc::ThreadPoolPaused& ex) {
        ers::warning(ex);
    }

    // Unlink to allow the PMG garbage collection on the Process object
    if(processExited == true) {
        p->unlink();
    }
}

std::shared_ptr<ApplicationController> ApplicationController::create(const std::shared_ptr<ApplicationList>& appList,
                                                                     const std::shared_ptr<DVSController>& dvsCtrl,
                                                                     const std::shared_ptr<InformationPublisher>& infoPublisher)
{
    return std::shared_ptr<ApplicationController>(new ApplicationController(appList, dvsCtrl, infoPublisher), AppCtrlDeleter());
}

// throws daq::rc::PMGSystemError, daq::rc::ThreadPoolError
ApplicationController::ApplicationController(const std::shared_ptr<ApplicationList>& appList,
                                             const std::shared_ptr<DVSController>& dvsCtrl,
                                             const std::shared_ptr<InformationPublisher>& infoPublisher)
    :   std::enable_shared_from_this<ApplicationController>(), m_app_sync(), m_exiting(false),
        m_interrupted(false), m_app_list(appList), m_info_publisher(infoPublisher), m_dvs_ctrl(dvsCtrl),
        m_lookup_tp(new ThreadPool(5)), m_start_stop_tp(new ThreadPool(5))
{
	try {
		daq::pmg::Singleton::init();
	}
	catch(daq::pmg::Already_Init& ex) {
	    ers::log(ex);
	}
	catch(daq::pmg::IPC_Port_Error& ex) {
	    ers::warning(ex);
	}
	catch(daq::pmg::Exception& ex) {
	    throw daq::rc::PMGSystemError(ERS_HERE, ex);
	}
}

ApplicationController::~ApplicationController() noexcept {
}

bool ApplicationController::resetAppStatus(const std::shared_ptr<Application>& app) {
    bool done = false;

    // Changing the application status: lock to not be executed concurrently
    // with call-backs from the PMG
    const auto& sp = applicationSyncPair(app);
    std::lock_guard<std::recursive_mutex> lock(sp->first);

    // Reset the status to ABSENT only if in EXITED or FAILED
    const APPLICATION_STATUS st = app->getApplicationSVInfo().getStatus();
    if((st == APPLICATION_STATUS::EXITED) || (st == APPLICATION_STATUS::FAILED)) {
        const auto& result = app->setStatus(APPLICATION_STATUS::ABSENT);
        if(result.second == true) {
            m_info_publisher->publish(app->getName(), result.first);
            done = true;
        }
    }

    return done;
}

Application::ApplicationSet ApplicationController::startApps(const TransitionCmd& cmd) {
    std::string cmdStr;
    try {
        cmdStr = cmd.fsmCommand();
    }
    catch(daq::rc::CommandParameterNotFound& ex) {
        throw daq::rc::BadCommand(ERS_HERE, RunControlCommands::MAKE_TRANSITION_CMD,
                                  "cannot get the FSM command associated to the transition", ex);
    }

    // This list will contain all the apps that should be started during the transition
    // (regardless of their membership status)
    Application::ApplicationSet allApps;

    ERS_DEBUG(0, "Going to start all the relevant applications for transition command " << cmdStr);

    const std::vector<std::vector<ApplicationType>> appTypes = {{ApplicationType::ONLINE_SEGMENT_INFRASTRUCTURE}, {ApplicationType::INFRASTRUCTURE},
                                                                {ApplicationType::CHILD_CONTROLLER, ApplicationType::RUN_CONTROL, ApplicationType::STANDARD}};

    for(const auto& types : appTypes) {
        Application::ApplicationSet apps;

        for(ApplicationType type : types) {
            const auto& tmp = m_app_list->getApplications(type, cmdStr, true);
            apps.insert(tmp.begin(), tmp.end());
        }

        auto&& ordered_apps = ApplicationAlgorithms::resolveDependencies(apps, true); // this may throw

        // Remember the reverse order!
        const auto& b = ordered_apps.rbegin();
        const auto& e = ordered_apps.rend();
        for(auto it = b; it != e; ++it) {
            interrupted();

            ERS_DEBUG(3, "Going to start applications at level " << it->first);

            // These are the applications to be started at this level
            auto& appsToStartNow = it->second;

            // Shuffle so that we reduce the possibility to start several processes at the same time on the same machine
            // This makes a HUGE difference starting a lot of applications
            std::shuffle(appsToStartNow.begin(), appsToStartNow.end(), std::mt19937(std::random_device()()));

            std::atomic<bool> shouldStop(false);

            std::vector<std::future<void>> startedTasks;
            for(auto itt = appsToStartNow.begin(); itt != appsToStartNow.end(); ++itt) {
                // All the apps are added to the list of apps that should be started during the transition
                allApps.insert(*itt);

                // An app is started only if its membership is IN
                if((*itt)->getApplicationSVInfo().isMembership() == true) {
                    const std::shared_ptr<Application>& app_ = (*itt);
                    std::future<void>&& fut = m_start_stop_tp->submit<void>([this, app_, &shouldStop]()
                                              {
                                                    ERS_DEBUG(3, "Starting application " << app_->getName());

                                                    try {
                                                        if((isInterrupted() == true) || (shouldStop == true)) {
                                                            throw daq::rc::InterruptedException(ERS_HERE);
                                                        }

                                                        // Why do we check the dependencies even if we are performing an ordered start?
                                                        // Mainly because a failing applications could be put OUT of membership, the
                                                        // transition at that point could continue but any dependent application should
                                                        // not be able to start
                                                        bool signalSent = startApp(app_, true);
                                                        if(signalSent == false) {
                                                            if(app_->getApplicationSVInfo().getStatus() == APPLICATION_STATUS::TERMINATING) {
                                                                ers::error(daq::rc::ApplicationStartingFailure(ERS_HERE, app_->getName(),
                                                                                                               "the application is still being terminated"));
                                                            } else {
                                                                ers::log(daq::rc::ApplicationGenericIssue(ERS_HERE, app_->getName(),
                                                                                                          "PMG not asked to start the application because it is already alive"));
                                                            }
                                                        }
                                                    }
                                                    catch(daq::rc::ApplicationStartingFailure& ex) {
                                                        // No need to inform the ES here, the status of the application is set to FAILED in 'startApp'
                                                        ers::error(ex);
                                                    }
                                              });

                    startedTasks.push_back(std::move(fut));
                }
            }

            // Wait for the PMG being asked to start the processes: in this way we are sure
            // that the application's status is properly set to REQUESTED before the 'waitForCondition'
            for(auto& task : startedTasks) {
                try {
                    task.get();

                    interrupted();
                }
                catch(daq::rc::InterruptedException& ex) {
                    // This will tell the tasks to stop
                    // Continue the loop anyway in order to be sure that all the tasks are gone
                    if(shouldStop == false) {
                        shouldStop = true;

                        ers::log(ex);
                    }
                }
            }

            if((shouldStop == true) || (isInterrupted() == true)) {
                throw daq::rc::InterruptedException(ERS_HERE);
            }

            // Here wait for apps to be up
            ApplicationController::waitForCondition_i(std::move(appsToStartNow), std::bind(&ApplicationAlgorithms::isStartCompleted, std::placeholders::_1, true, true));

            ERS_DEBUG(3, "All the applications at level " << it->first << " have been started");
        }
    }

    ERS_DEBUG(0, "Started all the needed applications for transition command " << cmdStr);

    return allApps;
}

ApplicationController::OnApplicationExitSlotToken ApplicationController::registerOnApplicationExitCallback(const OnApplicationExitSlotType& cb) {
    return m_app_exit_sig.connect(cb);
}

void ApplicationController::removeOnApplicationExitCallback(const OnApplicationExitSlotToken& token) {
    // This synchronizes with the signal notification
    std::lock_guard<std::shared_mutex> lk(m_sig_mutex);
    token.disconnect();
}

void ApplicationController::stopApps(const TransitionCmd& cmd) {
    const std::string& cmdStr = cmd.fsmCommand();
    std::string fsmSource;
    try {
        fsmSource = cmd.fsmSourceState();
    }
    catch(daq::rc::CommandParameterNotFound& ex) {
        throw daq::rc::BadCommand(ERS_HERE, RunControlCommands::MAKE_TRANSITION_CMD,
                                  "cannot get the FSM source state of the transition", ex);
    }

    ERS_DEBUG(0, "Going to kill the relevant applications for transition command " << cmdStr);

    const std::vector<std::vector<ApplicationType>> appTypes = {{ApplicationType::STANDARD, ApplicationType::RUN_CONTROL, ApplicationType::CHILD_CONTROLLER},
                                                                {ApplicationType::INFRASTRUCTURE}, {ApplicationType::ONLINE_SEGMENT_INFRASTRUCTURE}};

    for(const auto& types : appTypes) {
        Application::ApplicationSet apps_to_stop;

        for(ApplicationType type : types) {
            const auto& tmp = m_app_list->getApplications(type, cmdStr, false);
            apps_to_stop.insert(tmp.begin(), tmp.end());
        }

        // Some specific actions for the SHUTDOWN command
        if(cmdStr == FSMCommands::SHUTDOWN_CMD) {
            const unsigned int currState = static_cast<unsigned int>(FSMStates::stringToState(fsmSource));
            if(currState >= static_cast<unsigned int>(FSM_STATE::GTHSTOPPED)) {
                // Kill applications to be stopped at EOR if the EOR transition will not be executed
                for(ApplicationType type : types) {
                    const auto& additionalApps = m_app_list->getApplications(type, FSMCommands::EOR_CMD, false);
                    apps_to_stop.insert(additionalApps.begin(), additionalApps.end());
                }
            } else if(currState <= static_cast<unsigned int>(FSM_STATE::CONNECTED)) {
                // Kill applications to be stopped at SOR if the SOR transition will not be executed
                for(ApplicationType type : types) {
                    const auto& additionalApps = m_app_list->getApplications(type, FSMCommands::SOR_CMD, false);
                    apps_to_stop.insert(additionalApps.begin(), additionalApps.end());
                }
            }

            if(currState >= static_cast<unsigned int>(FSM_STATE::CONFIGURED)) {
                // Kill applications to be stopped at UNCONFIGURE if the CONFIGURE transition will not be executed
                for(ApplicationType type : types) {
                    const auto& additionalApps = m_app_list->getApplications(type, FSMCommands::UNCONFIGURE_CMD, false);
                    apps_to_stop.insert(additionalApps.begin(), additionalApps.end());
                }
            }
        }

        auto&& ordered_apps = ApplicationAlgorithms::resolveDependencies(apps_to_stop, false); // this may throw
        const auto& b = ordered_apps.rbegin();
        const auto& e = ordered_apps.rend();
        for(auto it = b; it != e; ++it) {
            interrupted();

            ERS_DEBUG(3, "Stopping applications at level " << it->first);

            std::vector<std::shared_ptr<StopAppTask>> allTasks;

            auto& appsToStopNow = it->second;
            std::shuffle(appsToStopNow.begin(), appsToStopNow.end(), std::mt19937(std::random_device()()));

            for(const std::shared_ptr<Application>& app_ : appsToStopNow) {
                // Here we do not check for dependencies because we are already performing an ordered stop
                // and, additionally, the application membership is not taken into account when deciding
                // whether an application has been stopped or not
                std::shared_ptr<StopAppTask> stopTask(new StopAppTask(*this, app_, false, true));
                m_start_stop_tp->execute<bool>(stopTask);
                allTasks.push_back(std::move(stopTask));
            }

            // Get the result of all the tasks: needed for error and interruption checking
            bool alreadyInterrupted = false;
            for(const auto& tsk : allTasks) {
                try {
                    const bool result = tsk->get();
                    if(result == false) {
                        const std::string& msg = std::string("application not stopped because already exited or exiting (its status is ") +
                                                 ApplicationStates::statusToString(tsk->application()->getApplicationSVInfo().getStatus()) + ")";
                        ers::log(daq::rc::ApplicationGenericIssue(ERS_HERE, tsk->application()->getName(), msg));
                    }

                    interrupted();
                }
                catch(daq::rc::ApplicationStopFailure& ex) {
                    ers::error(ex);
                }
                catch(daq::rc::CancellationException& ex) {
                    // We are here if the 'get()' in the loop is called after an interruption
                    ers::log(ex);
                }
                catch(daq::rc::InterruptedException& ex) {
                    if(alreadyInterrupted == false) {
                        // Cancel all the other tasks
                        for(const auto& tsk : allTasks) {
                            tsk->cancel(true);
                        }

                        alreadyInterrupted = true;

                        ers::log(ex);
                    }
                }
            }

            if((alreadyInterrupted == true) || (isInterrupted() == true)) {
                throw daq::rc::InterruptedException(ERS_HERE);
            }

            // Here wait for apps to not be UP anymore
            // Is this correct? We keep waiting for the application to exit even if it is out of membership
            // (that is not generally an issue, apart the case in which the application cannot be killed for any reason)
            // On the other side there is not much more to do: we are in a transition and that transition cannot
            // be considered as done if the processes that should be terminated are not...
            ApplicationController::waitForCondition_i(std::move(appsToStopNow),
                                                      std::bind(&ApplicationAlgorithms::isStopCompleted,
                                                                std::placeholders::_1,
                                                                false));

            ERS_DEBUG(3, "All applications at level " << it->first << " have been killed");
        }
    }

    ERS_DEBUG(0, "Killed all the applications for transition command " << cmdStr);
}

Application::ApplicationSet ApplicationController::startApps(const StartStopAppCmd& cmd) {
    if(cmd.isStart() == false) {
        throw daq::rc::BadCommand(ERS_HERE, cmd.name(), "the received command does not correspond to an application start");
    }

    // Retrieve the application objects associated to the names
    std::map<ApplicationType, Application::ApplicationSet> appsToBeStarted;

    try {
        const auto& allApps = m_app_list->getApplications(cmd.applications());
        const std::string& currState = cmd.currentState();
        const std::string& destState = cmd.destinationState();

        for(const auto& entry : allApps) {
            if(entry.second.get() != nullptr) {
                if(ApplicationAlgorithms::canBeStarted(*(entry.second.get()), currState, destState, false) == false) {
                    std::string errMsg = std::string("the application is configured to not be up in state ") +
                                         ((currState == destState) ? currState : (destState + " (that is the destination state of the current transition")) +
                                         ". Maybe you meant to kill it!";

                    ers::warning(daq::rc::ApplicationStartingFailure(ERS_HERE, entry.first, errMsg));
                } else {
                    appsToBeStarted[entry.second->type()].insert(entry.second);
                }
            } else {
                ers::warning(daq::rc::ApplicationStartingFailure(ERS_HERE, entry.first, "no application with this name exists"));
            }
        }
    }
    catch(daq::rc::CommandParameterNotFound& ex) {
        throw daq::rc::BadCommand(ERS_HERE, cmd.name(), "the received command is missing some mandatory parameters", ex);
    }

    // Here will go all the started apps
    Application::ApplicationSet allStartedApps;

    // Start applications taking into account implicit dependencies
    const std::vector<std::vector<ApplicationType>> appTypes = {{ApplicationType::ONLINE_SEGMENT_INFRASTRUCTURE}, {ApplicationType::INFRASTRUCTURE},
                                                                {ApplicationType::CHILD_CONTROLLER, ApplicationType::RUN_CONTROL, ApplicationType::STANDARD}};

    for(const auto& types : appTypes) {
        // Resolve any start dependency between applications to be started
        Application::ApplicationSet appsNow;

        for(ApplicationType type : types) {
            const auto& found = appsToBeStarted.find(type);
            if(found != appsToBeStarted.end()) {
                appsNow.insert(found->second.begin(), found->second.end());
            }
        }

        auto&& orderedApps = ApplicationAlgorithms::resolveDependencies(appsNow, true); // this may throw

        // We can start in parallel all the applications that have the same key in the map
        // Be careful, applications with highest keys have to be started before the others
        const auto& b = orderedApps.rbegin();
        const auto& e = orderedApps.rend();
        for(auto it = b; it != e; ++it) {
            interrupted();

            std::vector<std::future<std::shared_ptr<Application>>> startResults;

            ERS_DEBUG(3, "Starting applications at level " << it->first);

            auto& appsToStartNow = it->second;

            std::atomic<bool> shouldStop(false);

            // Randomize (look at the same comment when starting applications for transitions)
            std::shuffle(appsToStartNow.begin(), appsToStartNow.end(), std::mt19937(std::random_device()()));
            for(const std::shared_ptr<Application>& app_ : appsToStartNow) {
                auto&& result = m_start_stop_tp->submit<std::shared_ptr<Application>>([this, app_, &shouldStop]() -> std::shared_ptr<Application>
                                                                                      {
                                                                                        if((isInterrupted() == true) || (shouldStop == true)) {
                                                                                            throw daq::rc::InterruptedException(ERS_HERE);
                                                                                        }

                                                                                        ERS_DEBUG(3, "Going to start application " << app_->getName());

                                                                                        // Here dependencies with respect to other applications (possibly not
                                                                                        // even included in the list of applications to start now) are checked
                                                                                        const bool signalSent = startApp(app_, true); // This throws
                                                                                        if(signalSent == false) {
                                                                                            if(app_->getApplicationSVInfo().getStatus() == APPLICATION_STATUS::TERMINATING) {
                                                                                                throw daq::rc::ApplicationStartingFailure(ERS_HERE, app_->getName(),
                                                                                                                                          "application still being terminated");
                                                                                            } else {
                                                                                                ers::log(daq::rc::ApplicationGenericIssue(ERS_HERE, app_->getName(),
                                                                                                                                          "application not started because already alive"));
                                                                                            }
                                                                                        }

                                                                                        return app_;
                                                                                      });
                startResults.push_back(std::move(result));
            }

            // Put here the applications we want to wait for (i.e., they are started)
            std::vector<std::shared_ptr<Application>> appsToWaitFor;

            // Wait for the tasks to be completed and get the result
            for(auto& res : startResults) {
                try {
                    const std::shared_ptr<Application>& startedApp = res.get();

                    // We wait only for applications that have been actually started
                    appsToWaitFor.push_back(startedApp);
                    allStartedApps.insert(startedApp);

                    // This helps in detecting the interrupted state earlier
                    interrupted();
                }
                catch(daq::rc::ApplicationStartingFailure& ex) {
                    ers::error(ex);
                }
                catch(daq::rc::InterruptedException& ex) {
                    // Just go on in order to be sure that all the tasks are completed
                    // Setting the flag here will cause the tasks to be stopped
                    if(shouldStop == false) {
                        shouldStop = true;

                        ers::log(ex);
                    }
                }
            }

            if((shouldStop == true) || (isInterrupted() == true)) {
                throw daq::rc::InterruptedException(ERS_HERE);
            }

            // Now loop over all the applications that should be started and wait for them to be up
            // The membership is not taken into account here: it could be that we are starting an already 'out' application

            // TODO: actually an improvement may be possible: if all the applications do not have start dependencies amongst themselves (i.e., orderedApps.size() == 1),
            // then the membership *can be safely* taken into account (and we do not hang on some not responding PMG).
            // As a further optimization this can be generalized to the last loop over orderedApps (i.e., it = e - 1)
            ApplicationController::waitForCondition_i(std::move(appsToWaitFor), std::bind(&ApplicationAlgorithms::isStartCompleted, std::placeholders::_1, false, false));

            ERS_DEBUG(3, "All applications at level " << it->first << " have been started");
        }
    }

    return allStartedApps;
}

Application::ApplicationSet ApplicationController::stopApps(const StartStopAppCmd& cmd) {
    if((cmd.isStart() == true) || (cmd.isRestart() == true)) {
        throw daq::rc::BadCommand(ERS_HERE, cmd.name(), "the received command does not correspond to an application stop");
    }

    const auto& allApps = m_app_list->getApplications(cmd.applications());

    // Retrieve the application objects associated to the names
    std::map<ApplicationType, Application::ApplicationSet> appsToBeStopped;
    for(const auto& entry : allApps) {
        if(entry.second.get() != nullptr) {
            appsToBeStopped[entry.second->type()].insert(entry.second);
        } else {
            ers::warning(daq::rc::ApplicationStopFailure(ERS_HERE, entry.first, "no application with this name exists"));
        }
    }

    // Here go all the stopped apps
    Application::ApplicationSet allStoppedApps;

    // Take into account implicit dependencies
    const std::vector<std::vector<ApplicationType>> appTypes = {{ApplicationType::STANDARD, ApplicationType::RUN_CONTROL, ApplicationType::CHILD_CONTROLLER},
                                                                {ApplicationType::INFRASTRUCTURE}, {ApplicationType::ONLINE_SEGMENT_INFRASTRUCTURE}};

    for(const auto& types : appTypes) {
        // Resolve any stop dependency between applications to be stopped
        Application::ApplicationSet appsNow;

        for(ApplicationType type : types) {
            const auto& found = appsToBeStopped.find(type);
            if(found != appsToBeStopped.end()) {
                appsNow.insert(found->second.begin(), found->second.end());
            }
        }

        // Resolve any start dependency between applications to be started
        auto&& orderedApps = ApplicationAlgorithms::resolveDependencies(appsNow, false); // this may throw

        // We can start in parallel all the applications that have the same key in the map
        // Be careful, applications with highest keys have to be started before the others
        const auto& b = orderedApps.rbegin();
        const auto& e = orderedApps.rend();
        for(auto it = b; it != e; ++it) {
            interrupted();

            ERS_DEBUG(3, "Stopping applications at level " << it->first);

            std::vector<std::shared_ptr<StopAppTask>> stopResults;

            auto& appsToStopNow = it->second;
            std::shuffle(appsToStopNow.begin(), appsToStopNow.end(), std::mt19937(std::random_device()()));

            for(const std::shared_ptr<Application>& app_ : appsToStopNow) {
                // Here the dependencies are checked because the application may have some stop dependency
                // from some other application not included in the this list of applications to stop
                std::shared_ptr<StopAppTask> stopTask(new StopAppTask(*this, app_, true, true));
                m_start_stop_tp->execute<bool>(stopTask);

                stopResults.push_back(std::move(stopTask));
            }

            // Put here the applications we want to wait for (i.e., they are stopped)
            std::vector<std::shared_ptr<Application>> appsToWaitFor;

            // Wait for the tasks to be completed and get the result
            bool alreadyInterrupted = false;
            for(auto& res : stopResults) {
                try {
                    const bool signalSent = res->get();
                    if(signalSent == false) {
                        const std::string& msg = std::string("application not stopped because already exited or exiting (its status is ") +
                                                 ApplicationStates::statusToString(res->application()->getApplicationSVInfo().getStatus()) + ")";
                        ers::log(daq::rc::ApplicationGenericIssue(ERS_HERE, res->application()->getName(), msg));
                    }

                    // As soon as the signal is not sent because the application is already exited
                    // or going to exit, it is fine to wait for it
                    appsToWaitFor.push_back(res->application());
                    allStoppedApps.insert(res->application());

                    interrupted();
                }
                catch(daq::rc::ApplicationStopFailure& ex) {
                    ers::error(ex);
                }
                catch(daq::rc::CancellationException& ex) {
                    // We are here, then it means that the tasks have been cancelled
                    ers::log(ex);
                }
                catch(daq::rc::InterruptedException& ex) {
                    if(alreadyInterrupted == false) {
                        for(auto& res : stopResults) {
                            res->cancel(true);
                        }

                        alreadyInterrupted = true;

                        ers::log(ex);
                    }
                }
            }

            if((alreadyInterrupted == true) || (isInterrupted() == true)) {
                throw daq::rc::InterruptedException(ERS_HERE);
            }

            // Now loop over all the applications that should be stopped and wait for them to exit
            // Do not take into account the application membership, we may actually have been requested to stop an application that is OUT
            ApplicationController::waitForCondition_i(std::move(appsToWaitFor),
                                                      std::bind(&ApplicationAlgorithms::isStopCompleted,
                                                                std::placeholders::_1,
                                                                false));

            ERS_DEBUG(3, "All applications at level " << it->first << " have been stopped");
        }
    }

    return allStoppedApps;
}

Application::ApplicationSet ApplicationController::restartApps(const StartStopAppCmd& cmd) {
    if(cmd.isRestart() == false) {
        throw daq::rc::BadCommand(ERS_HERE, cmd.name(), "the received command does not correspond to an application restart");
    }

    // Retrieve the application objects associated to the names
    std::map<ApplicationType, Application::ApplicationSet> appsToBeRestarted;

    try {
        const auto& allApps = m_app_list->getApplications(cmd.applications());

        for(const auto& entry : allApps) {
            if(entry.second.get() != nullptr) {
                const std::string& currState = cmd.currentState();
                const std::string& destState = cmd.destinationState();

                if(ApplicationAlgorithms::canBeStarted(*(entry.second.get()), currState, destState, true) == false) {
                    std::string errMsg = std::string("the application is configured to not be restarted or to be up in state ") +
                                         ((currState == destState) ? currState : (destState + " (that is the destination state of the current transition)")) +
                                         ". Maybe you meant to kill it!";

                    ers::warning(daq::rc::ApplicationRestartingFailure(ERS_HERE, entry.first, errMsg));
                } else {
                    appsToBeRestarted[entry.second->type()].insert(entry.second);
                }
            } else {
                ers::warning(daq::rc::ApplicationRestartingFailure(ERS_HERE, entry.first, "no application with this name exists"));
            }
        }
    }
    catch(daq::rc::CommandParameterNotFound& ex) {
        throw daq::rc::BadCommand(ERS_HERE, cmd.name(), "the received command is missing some mandatory parameters", ex);
    }

    // All restarted apps go here
    Application::ApplicationSet allRestartedApps;

    // Start applications taking into account implicit dependencies
    const std::vector<std::vector<ApplicationType>> appTypes = {{ApplicationType::ONLINE_SEGMENT_INFRASTRUCTURE}, {ApplicationType::INFRASTRUCTURE},
                                                                {ApplicationType::CHILD_CONTROLLER, ApplicationType::RUN_CONTROL, ApplicationType::STANDARD}};

    for(const auto& types : appTypes) {
        // Resolve dependencies between applications to be restarted
        Application::ApplicationSet appsNow;

        for(ApplicationType type : types) {
            const auto& found = appsToBeRestarted.find(type);
            if(found != appsToBeRestarted.end()) {
                appsNow.insert(found->second.begin(), found->second.end());
            }
        }

        auto&& orderedApps = ApplicationAlgorithms::resolveDependencies(appsNow, true); // this may throw

        // We can start in parallel all the applications that have the same key in the map
        // Be careful, applications with highest keys have to be started before the others
        const auto& b = orderedApps.rbegin();
        const auto& e = orderedApps.rend();
        for(auto it = b; it != e; ++it) {
            interrupted();

            std::vector<std::future<std::shared_ptr<Application>>> restartResults;

            ERS_DEBUG(3, "Restarting applications at level " << it->first);

            auto& appsToStartNow = it->second;
            std::shuffle(appsToStartNow.begin(), appsToStartNow.end(), std::mt19937(std::random_device()()));

            std::atomic<bool> shouldStop(false);

            for(const std::shared_ptr<Application>& app_ : appsToStartNow) {
                auto&& result = m_start_stop_tp->submit<std::shared_ptr<Application>>([this, app_, &shouldStop]() -> std::shared_ptr<Application>
                                                                                      {
                                                                                        ERS_DEBUG(3, "Going to restart application " << app_->getName());

                                                                                        // Check the interrupted state here
                                                                                        // Even if the interrupted state is checked by 'restartApp' as well,
                                                                                        // an additional check here helps starting the restart at all
                                                                                        if((isInterrupted() == true) || (shouldStop == true)) {
                                                                                            throw daq::rc::InterruptedException(ERS_HERE);
                                                                                        }

                                                                                        // This throws
                                                                                        restartApp(app_);

                                                                                        return app_;
                                                                                      });
                restartResults.push_back(std::move(result));
            }

            // Put here the applications we want to wait for (i.e., they are started)
            std::vector<std::shared_ptr<Application>> appsToWaitFor;

            // Wait for the tasks to be completed and get the result
            for(auto& res : restartResults) {
                try {
                    const std::shared_ptr<Application>& restartedApp = res.get();

                    // We wait only for applications that have been actually restarted
                    appsToWaitFor.push_back(restartedApp);
                    allRestartedApps.insert(restartedApp);

                    interrupted();
                }
                catch(daq::rc::ApplicationRestartingFailure &ex) {
                    ers::error(ex);
                }
                catch(daq::rc::InterruptedException& ex) {
                    // Just continue the loop in order to be sure that the tasks are properly terminated
                    // The interrupted state is checked again afterwards
                    if(shouldStop == false) {
                        shouldStop = true;

                        ers::log(ex);
                    }
                }
            }

            if((shouldStop == true) || (isInterrupted() == true)) {
                throw daq::rc::InterruptedException(ERS_HERE);
            }

            // Now loop over all the applications that should be started and wait for them to be up
            // Do not take into account the application membership: the application to be restarted may actually already be out
            ApplicationController::waitForCondition_i(std::move(appsToWaitFor), std::bind(&ApplicationAlgorithms::isStartCompleted, std::placeholders::_1, false, false));

            ERS_DEBUG(3, "All applications at level " << it->first << " have been restarted");
        }
    }

    return allRestartedApps;
}

void ApplicationController::restartApp(const std::shared_ptr<Application>& app) {
    const std::string& appName = app->getName();

    try {
        const auto& sp = applicationSyncPair(app);
        auto& m = sp->first;
        auto& cv = sp->second;

        // Check the interrupted state before and after locking the mutex
        interrupted();

        // This is a recursive mutex
        // The mutex is locked a second time in stopApp but then is unlocked in "wait_until"
        // When the wait call returns, the mutex is acquired again and then startApp locks again by its own
        // This is the best attempt to make  stop/start atomic:
        // - during 'stopApp' the state is set to TERMINATING
        // - when the lock is released in 'wait_until', any attempt to start the application (from an external command) is not accepted because of the TERMINATING state
        // - the same is valid for any additional 'stopApp' (process is stopped only if UP or REQUESTED)
        // - when the call-back is executed and the condition variable is notified, this thread acquires again the lock (so no start/stop is possible)
        // - the 'startApp' method is executed (it acquires again the recursive lock)
        std::unique_lock<std::recursive_mutex> lock(m);

        interrupted();

        // If we check stop dependencies also during the restart, then probably we end up with having nothing really restartable
        // TODO: we may consider to use the 'StopAppTask' here instead of the simple 'stopApp'
        bool stopSignalSent = stopApp(app, false);
        if(stopSignalSent == true) {
            const auto that_time = std::chrono::steady_clock::now() + std::chrono::seconds(app->getExitTimeout() + 10);
            while(app->getApplicationSVInfo().getStatus() == APPLICATION_STATUS::TERMINATING) {
                // The call to 'stopApp' causes the status of the application to change to TERMINATING
                // After the PMG has been asked to stop the process, the coming call-back should change the status to EXITED
                // Here we just check for a change in the application status (safest)
                // In order to avoid hanging here forever, we wait at most for the application exit timeout (plus a small tolerance)

                // TODO: I still have the impression that waiting here may cause some headache in some conditions...
                auto status = cv.wait_until(lock, that_time);
                if(status == std::cv_status::timeout) {
                    // Waited too much, break the loop and consider the stop as not sent
                    stopSignalSent = false;
                    ers::warning(daq::rc::ApplicationGenericIssue(ERS_HERE, appName, "during the restart procedure, the timeout elapsed waiting for the application to exit"));
                    break;
                }
            }
        }

        interrupted();

        const bool startSignalSent = startApp(app, true);

        // If the application is not stopped and not started, then it is in the REQUESTED or TERMINATING state
        if((stopSignalSent == false) && (startSignalSent == false)) {
            throw daq::rc::ApplicationRestartingFailure(ERS_HERE, appName, "the application is still being started or already terminated");
        }
    }
    catch(daq::rc::ApplicationStopFailure& ex) {
        throw daq::rc::ApplicationRestartingFailure(ERS_HERE, appName, "errors occurred trying to stop it", ex);
    }
    catch(daq::rc::ApplicationStartingFailure& ex) {
        throw daq::rc::ApplicationRestartingFailure(ERS_HERE, appName, "errors occurred trying to start it", ex);
    }
}

bool ApplicationController::stopApp(const std::shared_ptr<Application>& app, bool checkDeps) {
    const std::string& applicationName = app->getName();

    const auto& sp = applicationSyncPair(app);
    std::lock_guard<std::recursive_mutex> lock(sp->first);

    bool signalSent = false;

    try {
        // Note: do not take and cache the handle here, a call to 'tryToConnect' may change its value
        ApplicationSVInfo appSVInfo = app->getApplicationSVInfo();
        const bool forceLookup = appSVInfo.isForceLookup();
        if(forceLookup == true) {
            app->setForceLookup(false);
        }

        // We try to stop the process if the application is UP or REQUESTED (this may be needed for apps with a long init timeout)
        // If it is not UP or REQUESTED, or the process is not known (i.e., its status is NOTAV) then we try to reconnect
        // If the reconnection succeeds (i.e., the process is running) then we can kill it
        // Note that 'tryToConnet' re-connects to the application even if it is in REQUESTED from a PMG point of view
        // Note that the PMG may deny to stop the application if in REQUESTED (it may throw daq::pmg::Process_Already_Exited)
        // Look at the 'pmgCallback' in order to see how the stop of an application in REQUESTED is dealt with (in this case the
        // PMG will send anyway the RUNNING notification first and then the EXITED one)
        const APPLICATION_STATUS currAppStatus = appSVInfo.getStatus();
        if(((currAppStatus == APPLICATION_STATUS::UP) || ((currAppStatus == APPLICATION_STATUS::REQUESTED) && (app->getInitTimeout() != 0))) ||
           (((currAppStatus == APPLICATION_STATUS::NOTAV) || (forceLookup == true)) && (tryToConnect(app).first == true)))
        {
            // Evaluate again the application SVInfo (a call to "tryToConnet" may have changed the app status)
            appSVInfo = app->getApplicationSVInfo();

            std::shared_ptr<daq::pmg::Process> p;
            try {
                if(checkDeps == true) {
                    checkDependencies(app, false);
                }

                // Here the handle cannot be empty because the application is UP or it has been possible to connect to it
                p = ApplicationAlgorithms::getPmgProcess(daq::pmg::Handle(appSVInfo.getHandle()));
                if((p.get() != nullptr) && (p->exited() == false)) {
                    p->kill_soft(app->getExitTimeout());
                    signalSent = true;

                    app->setStatus(APPLICATION_STATUS::TERMINATING);
                } else {
                    // Null pointer (the process does not exist anymore) or already exited
                    app->setStatus(APPLICATION_STATUS::EXITED);
                }

                m_info_publisher->publish(applicationName, app->getApplicationSVInfo());
            }
            catch(daq::rc::DependenciesFailure& ex) {
                throw daq::rc::ApplicationStopFailure(ERS_HERE, applicationName, "failure in dependencies", ex);
            }
            catch(daq::rc::PMGError& ex) {
                if(ex.get_pmg_exists() == false) {
                    app->setForceLookup(true);
                    app->setStatus(APPLICATION_STATUS::ABSENT);
                    m_info_publisher->publish(applicationName, app->getApplicationSVInfo());

                    const std::string& msg = std::string("it looks like the PMG is not available on host\"") + ex.get_host_name() +
                                             "\"; assuming that the application is not running there";
                    ers::warning(daq::rc::ApplicationStopFailure(ERS_HERE, applicationName, msg, ex));
                } else {
                    throw daq::rc::ApplicationStopFailure(ERS_HERE, applicationName, ex.message(), ex);
                }
            }
            catch(daq::pmg::No_PMG_Server& ex) {
                app->setForceLookup(true);
                app->setStatus(APPLICATION_STATUS::ABSENT);
                m_info_publisher->publish(applicationName, app->getApplicationSVInfo());

                const std::string& msg = std::string("it looks like the PMG is not available on host\"") + ex.get_host() +
                                         "\"; assuming that the application is not running there";
                ers::warning(daq::rc::ApplicationStopFailure(ERS_HERE, applicationName, msg, ex));
            }
            catch(daq::pmg::Process_Already_Exited& ex) {
                // We can say that the process is really exited only if it is not in the REQUESTED state
                if(p->exited() == true) {
                    app->setStatus(APPLICATION_STATUS::EXITED);
                    m_info_publisher->publish(applicationName, app->getApplicationSVInfo());
                }

                // Here 'signalSent' is still false as it should be
           }
           catch(daq::pmg::Exception& ex) {
               throw daq::rc::ApplicationStopFailure(ERS_HERE, applicationName, ex.message(), ex);
           }
        }
    }
    catch(daq::rc::ApplicationReconnectionFailure& ex) {
        throw daq::rc::ApplicationStopFailure(ERS_HERE, applicationName, "the application status was not known and errors occurred retrieving that information from the PMG", ex);
    }

    return signalSent;
}

bool ApplicationController::startApp(const std::shared_ptr<Application>& app, bool checkDeps) {
    // Here we lock for *this* application
    // This is the safest thing in order to avoid concurrent start/stop for the same application
    // and at the same time allows to work on multiple application concurrently
    // The same is valid for the method stopping the application
    const auto& sp = applicationSyncPair(app);
    std::lock_guard<std::recursive_mutex> lock(sp->first);

    ERS_DEBUG(0, "Asked to start application " << app->getName());

    // Here we take note whether the PMG is asked or not
    bool signalSent = false;

    try {
        const ApplicationSVInfo& currentAppSVInfo = app->getApplicationSVInfo();
        const APPLICATION_STATUS currentAppStatus = currentAppSVInfo.getStatus();
        const bool notRunning = ((currentAppStatus == APPLICATION_STATUS::EXITED) || (currentAppStatus == APPLICATION_STATUS::FAILED) || (currentAppStatus == APPLICATION_STATUS::ABSENT));
        const bool forceLookup = currentAppSVInfo.isForceLookup();
        if(forceLookup == true) {
            app->setForceLookup(false);
        }

        // Here we store the result of a possible call to 'tryToConnect'
        std::pair<bool, std::set<std::string>> ttc;

        // If the application is not running we just start it
        // If the process status is not known, then the PMG is asked in 'tryToConnect'
        // If the connection to an already running application is not established (i.e., there is no running application) then the process is started
        if(((notRunning == true) && (forceLookup == false)) ||
           (((currentAppStatus == APPLICATION_STATUS::NOTAV) || (forceLookup == true)) && ((ttc = tryToConnect(app)).first == false)))
        {
            // Check dependencies
            if(checkDeps == true) {
                checkDependencies(app, true);
            }

            // Launch the process (i.e., actually ask the PMG)
            launch(app, ttc.second);

            signalSent = true;
        }
    }
    catch(daq::rc::ApplicationReconnectionFailure& ex) {
        app->setStatus(APPLICATION_STATUS::FAILED);
        m_info_publisher->publish(app->getName(), app->getApplicationSVInfo());

        // Schedule a precondition test in order to try to understand the reason of the failure
        m_dvs_ctrl->testApplication(app, daq::tm::Test::Scope::Precondition, 1);

        const std::string& errMsg = std::string("unable to determine whether a running instance of the application already exists ") +
                "(the application is not started in order to avoid having multiple running instances of it)";
        throw daq::rc::ApplicationStartingFailure(ERS_HERE, app->getName(), errMsg, ex);
    }
    catch(daq::rc::DependenciesFailure& ex) {
        app->setStatus(APPLICATION_STATUS::FAILED);
        m_info_publisher->publish(app->getName(), app->getApplicationSVInfo());

        // Schedule a precondition test in order to try to understand the reason of the failure
        m_dvs_ctrl->testApplication(app, daq::tm::Test::Scope::Precondition, 1);

        throw daq::rc::ApplicationStartingFailure(ERS_HERE, app->getName(), "start dependencies not fulfilled", ex);
    }
    catch(daq::rc::ApplicationLaunchFailure& ex) {
        app->setStatus(APPLICATION_STATUS::FAILED);
        m_info_publisher->publish(app->getName(), app->getApplicationSVInfo());

        // Schedule a precondition test in order to try to understand the reason of the failure
        m_dvs_ctrl->testApplication(app, daq::tm::Test::Scope::Precondition, 1);

        throw daq::rc::ApplicationStartingFailure(ERS_HERE, app->getName(), "the ProcessManager server(s) experienced some problems", ex);
    }

    return signalSent;
}

std::pair<bool, std::set<std::string>> ApplicationController::tryToConnect(const std::shared_ptr<Application>& app) {
    const std::string& applicationName = app->getName();

    bool applicationReconnected = false;

    // If we are here then we need to ask the PMG about the status of the process
    // and we have to assume our handle is not valid
    // This means we need to ask the PMGs on all the hosts where the process could be running

    ERS_DEBUG(0, "Looking for already running instances of application " << applicationName);

    std::set<std::string> badHosts;
    try {
        const auto& irs = isRunningSomewhere(app); // This throws
        const std::vector<std::shared_ptr<daq::pmg::Handle>>& allHandles = irs.first;
        badHosts = irs.second;

        // If we are here then we have been able to contact (at least some) PMGs
        const unsigned int foundHandles = allHandles.size();
        if(foundHandles == 0) {
            // We have not found any process, we can try to start a fresh one
            ERS_DEBUG(2, "No running instances of process " << applicationName << " have been found, setting its status to ABSENT");

            app->setStatus(APPLICATION_STATUS::ABSENT);
            m_info_publisher->publish(applicationName, app->getApplicationSVInfo());
        } else if(foundHandles == 1) {
            // We have found only one process running, save the information we need
            ERS_DEBUG(2, "Only one instance of the process " << applicationName << " has been found");

            // Get the Process pointer: if this call fail we cannot assume anything about the status of the process
            const daq::pmg::Handle* procHandle = allHandles[0].get();

            // Save the value of the handle and of the running host
            app->setRunningHost(procHandle->server());
            app->setHandle(procHandle->toString());

            try {
                std::shared_ptr<daq::pmg::Process> pmgProc = ApplicationAlgorithms::getPmgProcess(*procHandle);

                // We need to check the pointer because between the moment in which we got the handle the the call to get the
                // process pointer, it could be that the process exited
                if(pmgProc.get() != nullptr) {
                    // Look at the process status and link to it if needed
                    const PMGProcessStatusInfo& procInfo = pmgProc->status();
                    switch(procInfo.state) {
                        case PMGProcessState_NOTAV:
                        case PMGProcessState_REQUESTED:
                        case PMGProcessState_LAUNCHING:
                        case PMGProcessState_CREATED: {
                            // The application has already been scheduled to be started
                            ERS_DEBUG(2, "The found instance of the process " << applicationName << " is already scheduled to be started");

                            app->setStatus(APPLICATION_STATUS::REQUESTED);
                            if(pmgProc->is_linked() == false) {

                                pmgProc->link(boost::bind(ApplicationController::pmgCallback,
                                                          std::weak_ptr<ApplicationController>(shared_from_this()),
                                                          boost::placeholders::_1,
                                                          boost::placeholders::_2), nullptr);
                            }

                            applicationReconnected = true;

                            break;
                        }
                        case PMGProcessState_RUNNING: {
                            // The application is already running
                            ERS_DEBUG(2, "The found instance of the process " << applicationName << " is already up and running");

                            app->setStatus(APPLICATION_STATUS::UP);
                            if(pmgProc->is_linked() == false) {
                                pmgProc->link(boost::bind(ApplicationController::pmgCallback,
                                                          std::weak_ptr<ApplicationController>(shared_from_this()),
                                                          boost::placeholders::_1,
                                                          boost::placeholders::_2), nullptr);
                            }

                            applicationReconnected = true;

                            // Ask the DVS controller to perform a test on the reconnected application
                            m_dvs_ctrl->testApplication(app, daq::tm::Test::Scope::Functional, 1);

                            break;
                        }
                        case PMGProcessState_SIGNALED:
                        case PMGProcessState_EXITED: {
                            ERS_DEBUG(2, "The found instance of the process " << applicationName << " is not up, setting its status to EXITED");

                            app->setStatus(APPLICATION_STATUS::EXITED);
                            break;
                        }
                        case PMGProcessState_SYNCERROR:
                        case PMGProcessState_FAILED: {
                            ERS_DEBUG(2, "The found instance of the process " << applicationName << " is not up, setting its status to FAILED");

                            app->setStatus(APPLICATION_STATUS::FAILED);
                            break;
                        }
                    }
                } else {
                    // Process pointer is null, the process is gone (set the state to ABSENT because we cannot say if it failed or just exited)
                    ERS_DEBUG(2, "The found instance of the process " << applicationName << " is not up, setting its status to ABSENT");

                    app->setStatus(APPLICATION_STATUS::ABSENT);
                }
            }
            catch(daq::rc::PMGError& ex) {
                if(ex.get_pmg_exists() == true) {
                    const std::string& errMsg = std::string("found an application instance running on host \"") +
                                                ex.get_host_name() + "\" but it has not been possible to determine its status.";
                    throw daq::rc::ApplicationReconnectionFailure(ERS_HERE, applicationName, errMsg, ex);
                } else {
                    app->setForceLookup(true);
                    app->setStatus(APPLICATION_STATUS::ABSENT);

                    const std::string& msg = std::string("it looks like the PMG is not available on host \"") + ex.get_host_name() +
                                             "\"; assuming that the applications is not running there";
                    ers::warning(daq::rc::ApplicationReconnectionFailure(ERS_HERE, applicationName, msg, ex));
                }
            }

            m_info_publisher->publish(applicationName, app->getApplicationSVInfo());
        } else {
            // More than one running instance found: kill all of them
            ERS_LOG("More than one running instance of process " << applicationName << " has been found, killing all of them!");

            ApplicationAlgorithms::killLeftOver(allHandles); // This throws

            ERS_LOG("Left over instance of process " << applicationName << " have been killed, now setting its status to ABSENT");

            app->setStatus(APPLICATION_STATUS::ABSENT);
            m_info_publisher->publish(applicationName, app->getApplicationSVInfo());
        }
    }
    catch(daq::rc::ApplicationLookupFailure& ex) {
        const std::string errMsg = "the lookup procedure with the PMGs could not be performed correctly";
        throw daq::rc::ApplicationReconnectionFailure(ERS_HERE, applicationName, errMsg, ex);
    }
    catch(daq::rc::LeftoverKillingFailure& ex) {
        const std::string errMsg = "some left-over running process could not be killed";
        throw daq::rc::ApplicationReconnectionFailure(ERS_HERE, applicationName, errMsg, ex);
    }

    return std::make_pair(applicationReconnected, badHosts);
}

std::pair<std::vector<std::shared_ptr<daq::pmg::Handle>>, std::set<std::string>> ApplicationController::isRunningSomewhere(const std::shared_ptr<Application>& app) const {
    // Here we collect any host having troubles with the PMG
    std::set<std::string> badHosts;

    const std::string& applicationName = app->getName();
    const std::string& partitionName = app->getPartition();

    // Lookup on all the hosts the application may be running on
    const std::string& mainHost = app->getHost();
    const std::set<std::string>& backupHosts = app->getBackupHosts();

    std::set<std::string> allHosts = {mainHost};
    allHosts.insert(backupHosts.begin(), backupHosts.end());

    unsigned int numOfDisabledHosts = 0;
    std::vector<std::future<std::unique_ptr<daq::pmg::Handle>>> lookupResults;
    for(const std::string& h : allHosts) {
        // If the host is disabled in the database, do not take it into account
        try {
            const daq::core::Computer* dalHost = OnlineServices::instance().getConfiguration().get<daq::core::Computer>(h);
            if((dalHost != nullptr) && (dalHost->get_State() == true)) {
                auto&& result = m_lookup_tp->submit<std::unique_ptr<daq::pmg::Handle>>([applicationName, h, partitionName]() -> std::unique_ptr<daq::pmg::Handle>
                                             {
                                                return std::unique_ptr<daq::pmg::Handle>(ApplicationAlgorithms::getPmgHandle(applicationName, h, partitionName));
                                             });
                lookupResults.push_back(std::move(result));
            } else {
                ers::log(daq::rc::HostDisabled(ERS_HERE, h));

                numOfDisabledHosts++;

                // Add the disabled host to the list of bad hosts
                badHosts.insert(h);
            }
        }
        catch(daq::config::Generic& ex) {
            ers::error(ex);

            // If we cannot get info about the host from config, treat it as disabled
            numOfDisabledHosts++;
            badHosts.insert(h);
        }
    }

    // Here we collect all the processes we have found
    std::vector<std::shared_ptr<daq::pmg::Handle>> allHandles;

    // Here loop over the async tasks waiting for their result
    unsigned int numOfFailures = 0;
    for(auto& result : lookupResults) {
        try {
            std::shared_ptr<daq::pmg::Handle> pmgHandle(result.get());
            if(pmgHandle.get() != nullptr) {
                ERS_DEBUG(1, "Found already running instance with handle " << pmgHandle->toString());

                allHandles.push_back(pmgHandle);
            }
        }
        catch(daq::rc::PMGError& ex) {
            if(ex.get_pmg_exists() == true) {
                numOfFailures++;

                const std::string& hostName = ex.get_host_name();
                badHosts.insert(hostName);

                const std::string& errMsg = std::string("error asking the PMG running on host ") + hostName + " (" + ex.get_message() + ")";
                ers::warning(daq::rc::ApplicationLookupFailure(ERS_HERE, applicationName, errMsg, ex));
            } else {
                // If the PMG is not running, then assume that the application is not running there
                app->setForceLookup(true);

                const std::string& msg = std::string("it looks like the PMG is not available on host \"") + ex.get_host_name() +
                                         "\"; assuming that the application is not running there";
                ers::warning(daq::rc::ApplicationLookupFailure(ERS_HERE, applicationName, msg, ex));
            }
        }
    }

    // If all the PMGs could not be contacted, then stop here
    if((numOfFailures != 0) && (numOfFailures == (allHosts.size() - numOfDisabledHosts))) {
        throw daq::rc::ApplicationLookupFailure(ERS_HERE, applicationName, "failed to contact all the PMGs both for the main and the back-up hosts");
    }

    return std::make_pair(allHandles, badHosts);
}

void ApplicationController::launch(const std::shared_ptr<Application>& app, const std::set<std::string>& badHosts) {
    // Get all the hosts where the application could be started

    // First add the back-up hosts and shuffle them, so that they are used
    std::vector<std::string> allHosts(app->getBackupHosts().begin(), app->getBackupHosts().end());
    std::shuffle(allHosts.begin(), allHosts.end(), std::mt19937(std::random_device()()));

    // Now add the primary host to the end
    allHosts.push_back(app->getHost());

    // Now remove the hosts marked as bad (please, note that the algo keeps the relative order of elements, so the primary host is always the last one - if not removed)
    const auto new_end = std::remove_if(allHosts.begin(), allHosts.end(), [&badHosts](const std::string& hostName_) { return (badHosts.find(hostName_) != badHosts.end()); });
    allHosts.erase(new_end, allHosts.end());

    // Now try to start the process (loop on all the hosts available)
    // Note that the loop is done in reverse order, so that the primary host is asked first

    std::vector<std::pair<std::string, std::unique_ptr<ers::Issue>>> failures;

    bool started = false;
    const auto& b = allHosts.rbegin();
    const auto& e = allHosts.rend();
    for(auto it = b; it != e; ++it) {
        try {
            // Do not try to start the application on a host switched off
            const daq::core::Computer* dalHost = OnlineServices::instance().getConfiguration().get<daq::core::Computer>(*it);
            if((dalHost != nullptr) && (dalHost->get_State() == true)) {
                ERS_DEBUG(2, "Trying to start application " << app->getName() << " on host " << *it);

                daq::pmg::ProcessDescription pdr(*it,
                                                 app->getName(),
                                                 app->getPartition(),
                                                 app->getBinPath(),
                                                 app->getWorkingDir(),
                                                 app->getApplicationSVInfo().isRestarting() == false ? app->getParameters() : app->getRestartParameters(),
                                                 app->getEnvironment(),
                                                 app->getLogPath(),
                                                 app->getInputDevice(),
                                                 app->getSwObject(),
                                                 !app->isLogging(),
                                                 app->getInitTimeout());

                std::shared_ptr<daq::pmg::Process> p = pdr.start(boost::bind(ApplicationController::pmgCallback,
                                                                             std::weak_ptr<ApplicationController>(shared_from_this()),
                                                                             boost::placeholders::_1,
                                                                             boost::placeholders::_2), nullptr);
                started = true;

                // Here we update only the handle, the last ID is updated once a call-back is received
                app->setHandle(p->handle().toString());
                app->setStatus(APPLICATION_STATUS::REQUESTED);

                m_info_publisher->publish(app->getName(), app->getApplicationSVInfo());

                break;
            } else {
                const std::string& errMsg = std::string("the host ") + *it + " cannot be used to start any process (a different back-up host, if any, will be used)";
                failures.push_back(std::make_pair(errMsg, std::unique_ptr<ers::Issue>(new daq::rc::HostDisabled(ERS_HERE, *it))));
            }
        }
        catch(daq::config::Generic& ex) {
            ers::log(ex);

            const std::string& errMsg = std::string("the status of host ") + *it + " could not be determined (a different back-up host, if any, will be used)";
            failures.push_back(std::make_pair(errMsg, std::unique_ptr<ers::Issue>(new daq::config::Generic(ex))));
        }
        catch(daq::pmg::Exception& ex) {
            ers::log(ex);

            const std::string& errMsg = std::string("the PMG running on host ") + *it + " could not start the process (a different back-up host, if any, will be used)";
            failures.push_back(std::make_pair(errMsg, std::unique_ptr<ers::Issue>(new daq::pmg::Exception(ex))));
        }
    }

    if(started == true) {
        const std::string& appName = app->getName();
        for(const auto& cause : failures) {
            ers::warning(daq::rc::ApplicationLaunchFailure(ERS_HERE, appName, cause.first, *(cause.second)));
        }
    } else {
        std::ostringstream errMsg;
        errMsg << "failed to start the process both on the primary and the back-up hosts:\n";

        for(const auto& cause : failures) {
            errMsg << "* " << *(cause.second) << "\n";
        }

        throw daq::rc::ApplicationLaunchFailure(ERS_HERE, app->getName(), errMsg.str());
    }
}

void ApplicationController::checkDependencies(const std::shared_ptr<const Application>& app, bool isStart) const {
    // Loop over the applications that should be already running
    // From the list of dependencies, remove the applications which are not in the application list (configuration issue)

    std::function<const std::set<std::string>& (const Application&)> getDependencies = ((isStart == true) ? &Application::getInitDep : &Application::getShutdownDep);
    std::function<bool (APPLICATION_STATUS)> isUP;
    if(isStart == true) {
        isUP = [] (APPLICATION_STATUS appStatus) -> bool { return appStatus == APPLICATION_STATUS::UP; };
    } else {
        isUP = [] (APPLICATION_STATUS appStatus) -> bool
                { return (appStatus == APPLICATION_STATUS::UP) || (appStatus == APPLICATION_STATUS::REQUESTED) || (appStatus == APPLICATION_STATUS::TERMINATING); };
    }

    for(const std::string& depAppName_ : getDependencies(*app)) {
        const std::shared_ptr<Application> depApp_ = m_app_list->getApplication(depAppName_);
        if(depApp_.get() != nullptr) {
            bool foundRunning = false;

            const APPLICATION_STATUS depAppStatus_ = depApp_->getApplicationSVInfo().getStatus();
            if(depAppStatus_ == APPLICATION_STATUS::NOTAV) {
                ERS_LOG(app->getName() << " depends from application " << depApp_->getName() << " whose state is not known, going to ask the PMGs");

                try {
                    const auto& irs = isRunningSomewhere(depApp_);
                    const std::vector<std::shared_ptr<daq::pmg::Handle>>& foundHandles = irs.first;

                    // With this check we just send a warning
                    // This is not the place in which eventually multiple running instances issues should be fixed
                    // The problem will be spotted in 'tryToReconnect' when the real look-up for this application will happen
                    if(foundHandles.size() > 1) {
                        const std::string errMsg = "found more than one PMG handle";
                        ers::warning(daq::rc::ApplicationGenericIssue(ERS_HERE, depAppName_, errMsg));
                    }

                    for(const std::shared_ptr<daq::pmg::Handle>& ph : foundHandles) {
                        // Why we do not make the process look-up parallel?
                        // Mainly because it is not really needed:
                        // 1 - the probability that we are here is low (under normal conditions the applications are started in a proper order)
                        // 2 - even in case of a restart after a crash, the status of the applications is collected in "tryToReconnect"
                        // 3 - after the call to 'isRunningSomewhere', the probability that the PMG does not respond is really low
                        // 4 - here we just need to find a running process: if we get the handle, the probability the the process is not running is low
                        // 5 - we do not look up for all the handles (one in any case...) but we stop when we find one corresponding to a running process
                        try {
                            std::shared_ptr<daq::pmg::Process> p = ApplicationAlgorithms::getPmgProcess(*(ph.get()));
                            if((p.get() != nullptr) && (p->exited() == false)) {
                                foundRunning = true;
                                break;
                            }
                        }
                        catch(daq::rc::PMGError& ex) {
                            ers::error(ex);
                        }
                    }
                }
                catch(daq::rc::ApplicationLookupFailure& ex) {
                    const std::string& errMsg = std::string("cannot determine the status of the dependent application: ") + depAppName_;
                    throw daq::rc::DependenciesFailure(ERS_HERE, true, app->getName(), errMsg, ex);
                }
            } else {
                foundRunning = isUP(depAppStatus_);
            }

            if(isStart == true) {
                // In case the application is not running, consider this a dependency failure only
                // if the the application is not allowed to exit spontaneously or it is but it
                // could not be started (i.e., it is in the FAILED status)
                if((foundRunning == false) &&
                   ((depApp_->isSpontaneousExitAllowed() == false) ||
                    (depApp_->getApplicationSVInfo().getStatus() == APPLICATION_STATUS::FAILED)))
                {
                    const std::string& errMsg = std::string("the dependent application \"") + depAppName_ + "\" is not running";
                    throw daq::rc::DependenciesFailure(ERS_HERE, isStart, app->getName(), errMsg);
                }
            } else if(foundRunning == true) {
                // Here 'isStart' is false
                const std::string& errMsg = std::string("the dependent application \"") + depAppName_ + "\" is still running";
                throw daq::rc::DependenciesFailure(ERS_HERE, isStart, app->getName(), errMsg);
            }
        } else {
            const std::string& msg = std::string("detected bad ") + (isStart == true ? "start" : "stop") +
                                     " dependency from application \"" + depAppName_ + "\". Please, check your configuration";
            ers::warning(daq::rc::ApplicationGenericIssue(ERS_HERE, app->getName(), msg));
        }
    }
}

void ApplicationController::waitForCondition_i(std::vector<std::shared_ptr<Application>> apps, const std::function<bool (const Application&)>& check) const {
    while(apps.empty() == false) {
        interrupted();

        const auto& ne = std::remove_if(apps.begin(), apps.end(), [&check](const std::shared_ptr<Application>& app_){ return check(*app_); });
        apps.erase(ne, apps.end());

        std::this_thread::sleep_for(std::chrono::microseconds(100));
    }
}

void ApplicationController::waitForCondition(std::vector<std::shared_ptr<Application>> apps, const std::function<bool (const Application&)>& check) const {
    while(apps.empty() == false) {
        const auto& ne = std::remove_if(apps.begin(), apps.end(), [&check](const std::shared_ptr<Application>& app_){ return check(*app_); });
        apps.erase(ne, apps.end());

        std::this_thread::sleep_for(std::chrono::microseconds(100));
    }
}

std::shared_ptr<ApplicationController::SyncPair> ApplicationController::applicationSyncPair(const std::shared_ptr<const Application>& app) {
    const std::string& appName = app->getName();
    return m_app_sync.get(appName);
}

void ApplicationController::exiting() {
    // This is called when the controller exits
    // Here we really want to kill everything, almost blindly (just keeping into account the application dependencies)
    ERS_LOG("Starting an ordered stop of all the applications");

    m_exiting = true;

    // Use a bigger thread pool in order to be more protected against PMG timeouts
    std::unique_ptr<ThreadPool> n_tp;
    std::reference_wrapper<const std::unique_ptr<ThreadPool>> etp(m_start_stop_tp);
    try {
        n_tp.reset(new ThreadPool(15));
        etp = std::cref(n_tp);
    }
    catch(daq::rc::ThreadPoolError& ex) {
        ers::warning(ex);
    }

    const std::vector<std::vector<ApplicationType>> appTypes = {{ApplicationType::STANDARD, ApplicationType::RUN_CONTROL, ApplicationType::CHILD_CONTROLLER},
                                                                {ApplicationType::INFRASTRUCTURE}, {ApplicationType::ONLINE_SEGMENT_INFRASTRUCTURE}};

    for(const auto& types : appTypes) {
        Application::ApplicationSet apps;

        for(ApplicationType type : types) {
            ERS_DEBUG(0, "Going to kill applications of type " << static_cast<unsigned int>(type));

            const auto& tmp = m_app_list->getApplications(type);
            apps.insert(tmp.begin(), tmp.end());
        }

        auto&& ordered_apps = ApplicationAlgorithms::resolveDependencies(apps, false); // this throws
        const auto& b = ordered_apps.rbegin();
        const auto& e = ordered_apps.rend();

        for(auto it = b; it != e; ++it) {
            ERS_DEBUG(3, "Going to stop applications at level " << it->first);

            // The random shuffle helps
            auto& toStopNow = it->second;
            std::shuffle(toStopNow.begin(), toStopNow.end(), std::mt19937(std::random_device()()));

            // Here we ask the PMGs to kill the processes
            std::vector<std::shared_ptr<StopAppTask>> killTasks;
            for(const std::shared_ptr<Application>& app_ : toStopNow) {
                // We are exiting, do not double check the stop dependencies
                // Moreover we should try to kill as many applications as possible
                std::shared_ptr<StopAppTask> stopTask(new StopAppTask(*this, app_, false, false));
                etp.get()->execute<bool>(stopTask);

                killTasks.push_back(std::move(stopTask));
            }

            // Here we wait for the commands to the PMG to be sent
            std::vector<std::pair<std::shared_ptr<Application>, bool>> appsToWaitFor;
            for(const auto& task : killTasks) {
                try {
                    const bool sigSent = task->get();
                    appsToWaitFor.push_back(std::make_pair(task->application(), sigSent));
                }
                catch(daq::rc::ApplicationStopFailure& ex) {
                    ers::error(ex);
                }
            }

            // And here we wait for the applications to be really exited
            // This is also needed in order to synchronize with the PMG call-backs and
            // then be sure that they have been fully executed when this method returns
            for(const auto& pair :appsToWaitFor) {
                {
                    // Do not just look at the application's status, but also sync with the call-back!
                    const auto& sp = applicationSyncPair(pair.first);
                    std::unique_lock<std::recursive_mutex> lock(sp->first);

                    while(ApplicationAlgorithms::isStopCompleted(*(pair.first), false) == false) {
                        sp->second.wait(lock);
                    }
                }

                if(pair.second == false) {
                    // If the PMG has not been asked to kill the process, then it means the process was already stopped
                    // Remove the information here because the PMG call-back will not be executed at all
                    m_info_publisher->removeProcessInfo(pair.first->getName());
                }
            }

            ERS_DEBUG(3, "All applications at level " << it->first << " have been stopped");
        }

#ifndef ERS_NO_DEBUG
        for(ApplicationType type : types) {
            ERS_DEBUG(0, "Applications of type " << static_cast<unsigned int>(type) << " are no more running");
        }
#endif
    }

    ERS_DEBUG(0, "All the applications have been stopped");
}

void ApplicationController::signalApplicationExited(const std::shared_ptr<Application>& app, bool exiting) {
    // This synchronizes with the removal of the notification
    std::shared_lock<std::shared_mutex> lk(m_sig_mutex);
    m_app_exit_sig(app, exiting);
}

void ApplicationController::interrupt(bool shouldInterrupt) {
    m_interrupted.store(shouldInterrupt);
}

void ApplicationController::interrupted() const {
    if(m_interrupted == true) {
        throw daq::rc::InterruptedException(ERS_HERE);
    }
}

bool ApplicationController::isInterrupted() const {
    return m_interrupted.load();
}

}}
