#include "RunControl/Controller/Root/ActiveTime.h"
#include "RunControl/Common/ScheduledThreadPool.h"
#include "RunControl/Common/OnlineServices.h"
#include "RunControl/Common/Constants.h"

#include <ers/ers.h>
#include <is/exceptions.h>
#include <rc/RunParamsNamed.h>
#include <owl/time.h>

#include <functional>
#include <mutex>

#include <sys/types.h>

namespace daq { namespace rc {

const std::string ActiveTime::RUN_INFO_SERVER = "RunParams.RunInfo";

ActiveTime::ActiveTime()
    :   m_start_time(),
        m_runinfo(OnlineServices::instance().getIPCPartition(), ActiveTime::RUN_INFO_SERVER),
        m_timer(new ScheduledThreadPool(1))
{
    m_timer->pauseExecution(true);
}

ActiveTime::~ActiveTime() {
}

void ActiveTime::init() {
    m_timer->submit(std::bind(&ActiveTime::update, this), "", 0, 10);
}

bool ActiveTime::update() {
    m_runinfo.activeTime = static_cast<unsigned int>(get_activeTime());

    try {
        m_runinfo.checkin();
    }
    catch(daq::is::Exception& ex) {
        ers::warning(ex);
    }

    return true;
}

unsigned long ActiveTime::get_activeTime() const {
    std::shared_lock<std::shared_mutex> lk(m_mutex);
    return chrono::duration_cast<chrono::seconds>(clock_t::now() - m_start_time).count();
}

void ActiveTime::start(bool continuing) {
    std::lock_guard<std::shared_mutex> lk(m_mutex);

    m_start_time = clock_t::now();
    m_runinfo.activeTime = 0;

    if(continuing == true) {
        try {
            RunParamsNamed rp(OnlineServices::instance().getIPCPartition(), Constants::RUN_PARAMS_IS_SERVER + ".RunParams");
            rp.checkout();

            const std::time_t diff = OWLTime().c_time() - rp.timeSOR.c_time();
            m_start_time -= chrono::seconds(diff);

            m_runinfo.activeTime = chrono::duration_cast<chrono::seconds>(clock_t::now() - m_start_time).count();
        }
        catch(daq::is::Exception& ex) {
            ers::warning(ex);
        }
    }

    try {
        m_runinfo.checkin();
    }
    catch(daq::is::Exception& ex) {
        ers::warning(ex);
    }

    m_timer->resumeExecution();
}

void ActiveTime::stop() {
    m_timer->pauseExecution(true);

    try {
        m_runinfo.activeTime = get_activeTime();
        m_runinfo.checkin();
    }
    catch(daq::is::Exception& ex) {
        ers::warning(ex);
    }
}

}}
