/*
 * ApplicationSelfInfo.cxx
 *
 *  Created on: Dec 14, 2012
 *      Author: avolio
 */

#include "RunControl/Controller/ApplicationSelfInfo.h"
#include "RunControl/FSM/FSMStates.h"

#include <ers/IssueFactory.h>

#include <chrono>


namespace daq { namespace rc {

ApplicationSelfInfo::ApplicationSelfInfo(FSM_STATE state,
                                         bool isTransitioning,
                                         const std::vector<std::string>& errorReasons,
                                         const std::vector<std::string>& badChildren,
                                         const std::shared_ptr<const TransitionCmd>& lastTrCmd,
                                         const std::shared_ptr<const TransitionCmd>& currTrCmd,
                                         unsigned int lastTrTime,
                                         const std::shared_ptr<const RunControlBasicCommand>& lastCmd,
                                         bool isBusy,
                                         bool fullyInizialized,
                                         infotime_t infoTimeTag,
                                         long long wallTime) :
        m_fsm_state(state), m_transitioning(isTransitioning), m_error_reasons(errorReasons), m_bad_children(badChildren),
        m_internal_error(!errorReasons.empty()), m_last_tr_cmd(lastTrCmd), m_current_tr_cmd(currTrCmd), m_tr_time(lastTrTime),
        m_last_cmd(lastCmd), m_busy(isBusy), m_fully_initialized(fullyInizialized), m_time_tag(infoTimeTag), m_wall_time(wallTime)
{
}

ApplicationSelfInfo::ApplicationSelfInfo() :
        m_fsm_state(FSM_STATE::ABSENT), m_transitioning(false), m_error_reasons(), m_internal_error(false),
        m_last_tr_cmd(), m_current_tr_cmd(), m_tr_time(0), m_last_cmd(), m_busy(false), m_fully_initialized(false), m_time_tag(0),
        m_wall_time(std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now().time_since_epoch()).count())
{
}

ApplicationSelfInfo::~ApplicationSelfInfo() {
}

FSM_STATE ApplicationSelfInfo::getFSMState() const {
    return m_fsm_state;
}

bool ApplicationSelfInfo::isTransitioning() const {
    return m_transitioning;
}

bool ApplicationSelfInfo::isInternalError() const {
    return m_internal_error;
}

std::vector<std::string> ApplicationSelfInfo::getErrorReasons() const {
    return m_error_reasons;
}

std::vector<std::string> ApplicationSelfInfo::getBadChildren() const {
    return m_bad_children;
}

std::shared_ptr<const TransitionCmd> ApplicationSelfInfo::getLastTransitionCommand() const {
    return m_last_tr_cmd;
}

std::shared_ptr<const TransitionCmd> ApplicationSelfInfo::getCurrentTransitionCommand() const {
    return m_current_tr_cmd;
}

infotime_t ApplicationSelfInfo::getLastTransitionTime() const {
    return m_tr_time;
}

std::shared_ptr<const RunControlBasicCommand> ApplicationSelfInfo::getLastCommand() const {
    return m_last_cmd;
}

bool ApplicationSelfInfo::isBusy() const {
    return m_busy;
}

bool ApplicationSelfInfo::isFullyInitialized() const {
    return m_fully_initialized;
}

infotime_t ApplicationSelfInfo::getInfoTimeTag() const {
    return m_time_tag;
}

long long ApplicationSelfInfo::getInfoWallTime() const {
    return m_wall_time;
}

void ApplicationSelfInfo::setFSMState(FSM_STATE state) {
    m_fsm_state = state;
    updateTimeTags();
}

void ApplicationSelfInfo::setTransitioning(bool isTransitioning) {
    m_transitioning = isTransitioning;
    updateTimeTags();
}

void ApplicationSelfInfo::setInternalError(const std::vector<std::string>& errorReasons,
                                           const std::vector<std::string>& badChildren)
{
    m_internal_error = !errorReasons.empty();
    m_error_reasons = errorReasons;
    m_bad_children = badChildren;
    updateTimeTags();
}

void ApplicationSelfInfo::setLastTransitionCommand(const std::shared_ptr<const TransitionCmd>& cmd) {
    m_last_tr_cmd = cmd;
    updateTimeTags();
}

void ApplicationSelfInfo::setCurrentTransitionCommand(const std::shared_ptr<const TransitionCmd>& cmd) {
    m_current_tr_cmd = cmd;
    updateTimeTags();
}

void ApplicationSelfInfo::setLastTransitionTime(infotime_t time) {
    m_tr_time = time;
    updateTimeTags();
}

void ApplicationSelfInfo::setLastCommand(const std::shared_ptr<const RunControlBasicCommand>& cmd) {
    m_last_cmd = cmd;
    updateTimeTags();
}

void ApplicationSelfInfo::setBusy(bool isBusy) {
    m_busy = isBusy;
    updateTimeTags();
}

void ApplicationSelfInfo::setFullyInitialized(bool done) {
    m_fully_initialized = done;
    updateTimeTags();
}

void ApplicationSelfInfo::updateTimeTags() {
    m_time_tag = clock_t::now().time_since_epoch().count();
    m_wall_time = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
}

}}
