/*
 * FSM.cxx
 *
 *  Created on: Sep 7, 2012
 *      Author: avolio
 */
// It is important that this header is the first one!
// It defines some boot MPL and FUSION macros that
// must not be overwritten (it would fail to compile)
#include "RunControl/FSM/Details/FSMInitial.h"
#include "RunControl/FSM/FSM.h"
#include "RunControl/FSM/FSMStates.h"
#include "RunControl/FSM/FSMCommands.h"
#include "RunControl/FSM/Details/FSMInternals.h"
#include "RunControl/FSM/Details/FSMGraph.h"
#include "RunControl/Common/RunControlTransitionActions.h"
#include "RunControl/Common/RunControlCommands.h"
#include "RunControl/Common/Exceptions.h"

#include <boost/scope_exit.hpp>

#include <functional>
#include <ostream>
#include <istream>
#include <exception>
#include <utility>

namespace daq { namespace rc {

template<FSM_STATE InitialState>
FSM<InitialState>::FSM(const std::shared_ptr<RunControlTransitionActions>& ctrl, const std::shared_ptr<UserRoutines>& ur) :
		FSMActions(), m_fsm_mutex(), m_state_mutex(), m_caught_exception(false), m_busy(false),
		m_current_state(FSM_STATE::ABSENT), m_destination_state(FSM_STATE::ABSENT), m_last_command(FSM_COMMAND::INIT_FSM),
		m_rc_actions(ctrl), m_fsm(new typename FSM_Initial<InitialState>::FSM())
{
    fillCommandMap();

	std::shared_ptr<typename FSM_Initial<InitialState>::FSM> pFSM = std::static_pointer_cast<typename FSM_Initial<InitialState>::FSM>(m_fsm);
    namespace ph = std::placeholders;

    // The size of the message queue determines the maximum number of events that can be
    // created during a transition (look at CONFIG, STOP, SUB-TRANSITIONS)
    pFSM->get_message_queue().set_capacity(50);

    // The size of the deferred queue limits the number of events that can be
    // deferred. We want only one RELOAD_DB event at a time to be deferred
    pFSM->get_deferred_queue().set_capacity(1);

	pFSM->registerTransitionDoneCallback(std::bind(&FSM<InitialState>::transitionDone, this, ph::_1));
	pFSM->template get_state<FSM_ACTIVE&>().registerTransitionDoneCallback(std::bind(&FSM<InitialState>::transitionDone, this, ph::_1));

    pFSM->registerTransitioningCallback(std::bind(&FSM<InitialState>::transitioning, this, ph::_1));
    pFSM->template get_state<FSM_ACTIVE&>().registerTransitioningCallback(std::bind(&FSM<InitialState>::transitioning, this, ph::_1));

    pFSM->registerNoTransitionCallback(std::bind(&FSM<InitialState>::noTransition, this, ph::_1));
    pFSM->template get_state<FSM_ACTIVE&>().registerNoTransitionCallback(std::bind(&FSM<InitialState>::noTransition, this, ph::_1));

    pFSM->registerTransitionExceptionCallback(std::bind(&FSM<InitialState>::transitionException, this, ph::_1, ph::_2));
    pFSM->template get_state<FSM_ACTIVE&>().registerTransitionExceptionCallback(std::bind(&FSM<InitialState>::transitionException, this, ph::_1, ph::_2));

	pFSM->controller(m_rc_actions);
	pFSM->template get_state<FSM_ACTIVE&>().controller(m_rc_actions);

	pFSM->userRoutines(ur);
	pFSM->template get_state<FSM_ACTIVE&>().userRoutines(ur);
}

template<FSM_STATE InitialState>
FSM<InitialState>::FSM(const std::shared_ptr<RunControlTransitionActions>& ctrl)
    : FSM<InitialState>(ctrl, std::shared_ptr<UserRoutines>(new UserRoutines()))
{
}

template<FSM_STATE InitialState>
FSM<InitialState>::~FSM() {
}

template<FSM_STATE InitialState>
void FSM<InitialState>::registerOnFSMBusyCallback(const FSMDefs::OnFSMBusyType& cb) {
    m_busy_sig.connect(cb);
}

template<FSM_STATE InitialState>
void FSM<InitialState>::registerOnTransitioningCallback(const FSMDefs::OnTransitioningSlotType& cb) {
    m_tr_sig.connect(cb);
}

template<FSM_STATE InitialState>
void FSM<InitialState>::registerOnTransitionDoneCallback(const FSMDefs::OnTransitionDoneSlotType& cb) {
    m_tr_done_sig.connect(cb);
}

template<FSM_STATE InitialState>
void FSM<InitialState>::registerOnNoTransitionCallback(const FSMDefs::OnNoTransitionSlotType& cb) {
    m_no_tr_sig.connect(cb);
}

template<FSM_STATE InitialState>
void FSM<InitialState>::registerOnTransitionExceptionCallback(const FSMDefs::OnTransitionExceptionSlotType& cb) {
    m_tr_ex_sig.connect(cb);
}

template<FSM_STATE InitialState>
void FSM<InitialState>::start(bool isRestart) {
    std::lock_guard<std::mutex> lk(m_fsm_mutex);

    m_busy = true;
    m_busy_sig(true);

    BOOST_SCOPE_EXIT_TPL( (&m_busy)(&m_busy_sig) )
    {
        m_busy = false;
        m_busy_sig(false);
    } BOOST_SCOPE_EXIT_END

	std::static_pointer_cast<typename FSM_Initial<InitialState>::FSM>(m_fsm)->start(FSMEvent<FSM_COMMAND::INIT_FSM>(isRestart));
}

template<FSM_STATE InitialState>
void FSM<InitialState>::interruptTransition(bool shouldInterrupt) {
    m_rc_actions->interrupt(shouldInterrupt);
}

template<FSM_STATE InitialState>
bool FSM<InitialState>::busy() const {
    return m_busy;
}

template<FSM_STATE InitialState>
std::tuple<FSM_STATE, FSM_STATE, FSM_COMMAND> FSM<InitialState>::status() const {
    std::shared_lock<std::shared_mutex> lk(m_state_mutex);
    return std::make_tuple(m_current_state, m_destination_state, m_last_command);
}

template<FSM_STATE InitialState>
void FSM<InitialState>::transitioning(const TransitionCmd& trCmd) {
    {
        std::lock_guard<std::shared_mutex> lk(m_state_mutex);

        m_current_state = FSMStates::stringToState(trCmd.fsmSourceState());
        m_destination_state = FSMStates::stringToState(trCmd.fsmDestinationState());
        m_last_command = FSMCommands::stringToCommand(trCmd.fsmCommand());
    }

    m_tr_sig(trCmd);
}

template<FSM_STATE InitialState>
void FSM<InitialState>::transitionDone(const TransitionCmd& trCmd) {
    {
        std::lock_guard<std::shared_mutex> lk(m_state_mutex);

        m_current_state = FSMStates::stringToState(trCmd.fsmDestinationState());
    }

    m_tr_done_sig(trCmd);
}

template<FSM_STATE InitialState>
void FSM<InitialState>::noTransition(const TransitionCmd& trCmd) {
    // It looks like the FSM calls "noTransition" when an exception is caught
    // In the FSM's logic the transition cannot be performed
    // The check here avoids in this case to notify subscribers with "noTransition"
    if(m_caught_exception == false) {
        m_no_tr_sig(trCmd);
    } else {
        m_caught_exception = false;
    }
}

template<FSM_STATE InitialState>
void FSM<InitialState>::transitionException(const TransitionCmd& trCmd, std::exception& ex) {
    m_caught_exception = true;

    m_tr_ex_sig(trCmd, ex);
}

template<FSM_STATE InitialState>
bool FSM<InitialState>::isCommandValid(FSM_COMMAND cmd) const {
    const auto& allCommands = commands();
    return (allCommands.find(cmd) != allCommands.end());
}

template<FSM_STATE InitialState>
const std::set<FSM_COMMAND>& FSM<InitialState>::commands() const {
    std::shared_ptr<typename FSM_Initial<InitialState>::FSM> pFSM = std::static_pointer_cast<typename FSM_Initial<InitialState>::FSM>(m_fsm);
    return pFSM->allowedCommands();
}

template<FSM_STATE InitialState>
bool FSM<InitialState>::isStateValid(FSM_STATE state) const {
    const auto& allStates = states();
    return (allStates.find(state) != allStates.end());
}

template<FSM_STATE InitialState>
const std::set<FSM_STATE>& FSM<InitialState>::states() const {
    std::shared_ptr<typename FSM_Initial<InitialState>::FSM> pFSM = std::static_pointer_cast<typename FSM_Initial<InitialState>::FSM>(m_fsm);
    return pFSM->allowedStates();
}

template<FSM_STATE InitialState>
bool FSM<InitialState>::makeTransition(const std::shared_ptr<TransitionCmd>& trCmd) {
    // How to deal with a command that is not valid for the current FSM (different initial states):
    // 1. If a client needs to know whether the command is valid or not, then it should call 'isCommandValid' before
    //    calling this method (this allows to conditionally execute some code *before* the transition may start);
    // 2. If a client may accept having a "delayed" answer directly from the FSM (i.e., via one of the supported
    //    call-backs), then an unique and direct call to this method should be sufficient.
    //
    // The explanation for #2 is the following:
    // Here a check in the command map is done (remember that the map contains all the available FSM commands/events).
    // The command map includes a larger set of commands with respect to the ones that may be accepted by the current FSM;
    // that means at the end may be finally the FSM to not execute any transition

    const std::string& cmdName = trCmd->fsmCommand();
    const FSM_COMMAND cmd = FSMCommands::stringToCommand(cmdName); // this may throw daq::rc::BadCommand

    // Look for the event to send to the FSM
    // The check on the map is not really needed because the map should contain all the valid events
    decltype(m_fsm_cmd_map.cbegin()) it;
    if((it = m_fsm_cmd_map.find(cmd)) == m_fsm_cmd_map.end()) {
        // If we are here, then the command must not correspond at any valid FSM event
        // Of course, the contrary is not true, the command may be in the map but it
        // does not correspond to a valid FSM event
        ERS_ASSERT(isCommandValid(cmd) == false);

        throw daq::rc::TransitionNotAllowed(ERS_HERE, cmdName, "it does not correspond to a valid FSM event");
    }

	std::unique_lock<std::mutex> lk(m_fsm_mutex, std::defer_lock);
	const bool free = lk.try_lock();
	if(free == true) {
	    m_busy = true;
	    m_busy_sig(true);

	    // This gives the guarantee that the busy flag is reset once the job is done
	    BOOST_SCOPE_EXIT_TPL( (&m_busy)(&m_busy_sig) )
	    {
	        m_busy = false;
	        m_busy_sig(false);
	    } BOOST_SCOPE_EXIT_END

	    // This may throw daq::rc::EventNotProcessed
	    (it->second)(trCmd);
	}

	return free;
}

template<FSM_STATE InitialState>
std::vector<FSM_COMMAND> FSM<InitialState>::transitions(FSM_STATE src, FSM_STATE dst) const {
    static FSMGraph<InitialState> fsmGraph;

    return fsmGraph.from(src, dst);
}

template<FSM_STATE InitialState>
void FSM<InitialState>::fillCommandMap() {
    // Here we put all the valid FSM commands, regardless of the FSM initial state
    // Note that INIT_FSM and STOP_FSM are not added
    MAP_FSM_EVENT(INITIALIZE)
    MAP_FSM_EVENT(CONFIGURE)
    MAP_FSM_EVENT(CONFIG)
    MAP_FSM_EVENT(CONNECT)
    MAP_FSM_EVENT(START)
    MAP_FSM_EVENT(STOP)
    MAP_FSM_EVENT(STOPROIB)
    MAP_FSM_EVENT(STOPDC)
    MAP_FSM_EVENT(STOPHLT)
    MAP_FSM_EVENT(STOPRECORDING)
    MAP_FSM_EVENT(STOPGATHERING)
    MAP_FSM_EVENT(STOPARCHIVING)
    MAP_FSM_EVENT(DISCONNECT)
    MAP_FSM_EVENT(UNCONFIG)
    MAP_FSM_EVENT(UNCONFIGURE)
    MAP_FSM_EVENT(SHUTDOWN)
    MAP_FSM_EVENT(USERBROADCAST)
    MAP_FSM_EVENT(RELOAD_DB)
    MAP_FSM_EVENT(RESYNCH)
    MAP_FSM_EVENT(NEXT_TRANSITION)
    MAP_FSM_EVENT(SUB_TRANSITION)
}

IMPLEMENT_SEND_FSM_EVENT(INITIALIZE)
IMPLEMENT_SEND_FSM_EVENT(CONFIGURE)
IMPLEMENT_SEND_FSM_EVENT(CONFIG)
IMPLEMENT_SEND_FSM_EVENT(CONNECT)
IMPLEMENT_SEND_FSM_EVENT(START)
IMPLEMENT_SEND_FSM_EVENT(STOP)
IMPLEMENT_SEND_FSM_EVENT(STOPROIB)
IMPLEMENT_SEND_FSM_EVENT(STOPDC)
IMPLEMENT_SEND_FSM_EVENT(STOPHLT)
IMPLEMENT_SEND_FSM_EVENT(STOPRECORDING)
IMPLEMENT_SEND_FSM_EVENT(STOPGATHERING)
IMPLEMENT_SEND_FSM_EVENT(STOPARCHIVING)
IMPLEMENT_SEND_FSM_EVENT(DISCONNECT)
IMPLEMENT_SEND_FSM_EVENT(UNCONFIG)
IMPLEMENT_SEND_FSM_EVENT(UNCONFIGURE)
IMPLEMENT_SEND_FSM_EVENT(SHUTDOWN)
IMPLEMENT_SEND_FSM_EVENT(USERBROADCAST)
IMPLEMENT_SEND_FSM_EVENT(RELOAD_DB)
IMPLEMENT_SEND_FSM_EVENT(RESYNCH)
IMPLEMENT_SEND_FSM_EVENT(NEXT_TRANSITION)
IMPLEMENT_SEND_FSM_EVENT(SUB_TRANSITION)

}}
