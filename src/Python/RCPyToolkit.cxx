// Include this first in order to avoid compilation
// warning on slc6 about "_XOPEN_SOURCE redefined"
#include <boost/python.hpp>

#include "RunControl/Python/RCPyControllable.h"
#include "RunControl/Python/RCPyToolkit.h"
#include "RunControl/Common/Controllable.h"

#include <ers/ers.h>

#include <iostream>
#include <stdlib.h> 

#include <Python.h>

using namespace daq::rc;

/*******************************************************************************
                                PyToolkit       
*******************************************************************************/
//PyToolkit should be instanciated only once as singleton
boost::scoped_ptr<PyToolkit> PyToolkit::_inst(new PyToolkit());


PyToolkit::PyToolkit()
{
    Py_Initialize();
    PyEval_InitThreads();
    PyEval_SaveThread();

	ERS_DEBUG( 0, "PyToolkit instantiated" );
}
PyToolkit::~PyToolkit() 
{
    PyGILState_Ensure();
    Py_Finalize();

    ERS_DEBUG( 0, "PyToolkit destroyed" );
}
//------------------------------------------------------------------------------
PyToolkit& PyToolkit::Instance()
{
    return *PyToolkit::_inst.get();
}
//------------------------------------------------------------------------------
std::unique_ptr<Controllable> PyToolkit::createWrap(const std::string& modName,
                                                    const std::string& className)
{
    return std::unique_ptr<Controllable>(new pyControllable(modName,
                                                            className));
}
