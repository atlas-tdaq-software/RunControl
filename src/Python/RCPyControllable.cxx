#include "RunControl/Python/RCPyControllable.h"
#include "RunControl/Common/RunControlCommands.h"

#include <ers/ers.h>

#include <iostream>

namespace {
    class Locker {
        public:
            Locker() {
                m_gil_state = PyGILState_Ensure();
            }

            ~Locker() {
                PyGILState_Release(m_gil_state);
            }

        private:
            PyGILState_STATE m_gil_state;
    };
}

using namespace daq::rc;
/*******************************************************************************
                                RCPyControllable
*******************************************************************************/

//Helper macros to run particular function. 
//   Do not require semicolon and try-catch blocks
#define PROPERCALL(cmd, opts) Locker lk;                                                    \
                              try {                                                         \
                                    this->_objectHandle->cmd(opts);                         \
                              }                                                             \
                              catch(boost::python::error_already_set const &)               \
                              {                                                             \
                                  std::string perror_str = this->parse_python_exception();  \
                                  throw PythonInnerException(ERS_HERE, perror_str.c_str()); \
                              }
//------------------------------------------------------------------------------
pyControllable::pyControllable(const std::string& modName, 
                               const std::string& className) : Controllable()
{
    Locker lk;

    try
    {
        boost::python::object mainMod(boost::python::import(modName.c_str()));

        ERS_DEBUG( 0, "Module \""<<modName<<"\" loaded successful");

        boost::python::object globalDict(mainMod.attr("__dict__"));
        boost::python::object derivedClass(globalDict[className]);

        ERS_DEBUG( 0, "Class \""<<className<<"\" loaded successful");

        baseClass = new boost::python::object(derivedClass());
        _objectHandle = boost::python::extract<Controllable*>(*baseClass) BOOST_EXTRACT_WORKAROUND;

        ERS_DEBUG( 0, "Object of \""<<className<<"\" was created" );
    } 
    catch(const boost::python::error_already_set&)
    {
        std::string perror_str = this->parse_python_exception();
        throw PythonInnerException(ERS_HERE, perror_str.c_str());
    }   
}
//------------------------------------------------------------------------------
pyControllable::~pyControllable() noexcept
{
    Locker lk;

    delete baseClass;

    ERS_DEBUG( 0, "Object with was destroyed" );
}
//------------------------------------------------------------------------------
std::string pyControllable::parse_python_exception(){
    PyObject *type_ptr = NULL, *value_ptr = NULL, *traceback_ptr = NULL;
    PyErr_Fetch(&type_ptr, &value_ptr, &traceback_ptr);
    std::string ret("Unfetchable Python error");
    if(type_ptr != NULL){
        boost::python::handle<> h_type(type_ptr);
        boost::python::str type_pstr(h_type);
        boost::python::extract<std::string> e_type_pstr(type_pstr);
        if(e_type_pstr.check())
            ret = e_type_pstr();
        else
            ret = "Unknown exception type";
    }
    if(value_ptr != NULL){
        boost::python::handle<> h_val(value_ptr);
        boost::python::str a(h_val);
        boost::python::extract<std::string> returned(a);
        if(returned.check())
            ret +=  ": " + returned();
        else
            ret += std::string(": Unparseable Python error: ");
    }
    if(traceback_ptr != NULL){
        boost::python::handle<> h_tb(traceback_ptr);
        boost::python::object tb(boost::python::import("traceback"));
        boost::python::object fmt_tb(tb.attr("format_tb"));
        boost::python::object tb_list(fmt_tb(h_tb));
        boost::python::object tb_str(boost::python::str("\n").join(tb_list));
        boost::python::extract<std::string> returned(tb_str);
        if(returned.check())
            ret += ": " + returned();
        else
            ret += std::string(": Unparseable Python traceback");
    }
    return ret;
}
//------------------------------------------------------------------------------
void pyControllable::configure(const daq::rc::TransitionCmd& tCmd) 
{
    PROPERCALL(configure, tCmd)
}
//------------------------------------------------------------------------------
void pyControllable::connect(const daq::rc::TransitionCmd& tCmd) 
{
    PROPERCALL(connect, tCmd)
}
//------------------------------------------------------------------------------
void pyControllable::prepareForRun(const daq::rc::TransitionCmd& tCmd) 
{
    PROPERCALL(prepareForRun, tCmd)
}
//==============================================================================
//==============================================================================
//stopROIB
//stopDC
//stopHLT
//stopRecording
//stopGathering
//stopArchiving
//------------------------------------------------------------------------------
void pyControllable::stopROIB(const daq::rc::TransitionCmd& tCmd) 
{
    PROPERCALL(stopROIB, tCmd)
}
//------------------------------------------------------------------------------
void pyControllable::stopDC(const daq::rc::TransitionCmd& tCmd) 
{
    PROPERCALL(stopDC, tCmd)
}
//------------------------------------------------------------------------------
void pyControllable::stopHLT(const daq::rc::TransitionCmd& tCmd) 
{
    PROPERCALL(stopHLT, tCmd)
}
//------------------------------------------------------------------------------
void pyControllable::stopRecording(const daq::rc::TransitionCmd& tCmd) 
{
    PROPERCALL(stopRecording, tCmd)
}
//------------------------------------------------------------------------------
void pyControllable::stopGathering(const daq::rc::TransitionCmd& tCmd) 
{
    PROPERCALL(stopGathering, tCmd)
}
//------------------------------------------------------------------------------
void pyControllable::stopArchiving(const daq::rc::TransitionCmd& tCmd) 
{
    PROPERCALL(stopArchiving, tCmd)
}

//==============================================================================
//==============================================================================
//disconnect
//unconfigure
//subTransition
//resynch
//------------------------------------------------------------------------------
void pyControllable::disconnect(const daq::rc::TransitionCmd& tCmd) 
{
    PROPERCALL(disconnect, tCmd)
}
//------------------------------------------------------------------------------
void pyControllable::unconfigure(const daq::rc::TransitionCmd& tCmd) 
{
    PROPERCALL(unconfigure, tCmd)
}
//------------------------------------------------------------------------------
void pyControllable::subTransition(const daq::rc::SubTransitionCmd& stCmd) 
{
    PROPERCALL(subTransition, stCmd)
}
//------------------------------------------------------------------------------
void pyControllable::resynch(const daq::rc::ResynchCmd& rCmd) 
{
    PROPERCALL(resynch, rCmd)
}
//==============================================================================
//==============================================================================
//publish
//publishFullStatistics
//disable
//enable
//userCommand
//------------------------------------------------------------------------------
void pyControllable::publish() 
{
    Locker lk;

    try
    {
        this->_objectHandle->publish();
    }
    catch(boost::python::error_already_set const &)
    {
        std::string perror_str = this->parse_python_exception();
        throw PythonInnerException(ERS_HERE, perror_str.c_str());
    }
}
//------------------------------------------------------------------------------
void pyControllable::publishFullStats() 
{
    Locker lk;

    try
    {
        this->_objectHandle->publishFullStats();
    }
    catch(boost::python::error_already_set const &)
    {
        std::string perror_str = this->parse_python_exception();
        throw PythonInnerException(ERS_HERE, perror_str.c_str());
    }
}
//------------------------------------------------------------------------------
void pyControllable::user(const daq::rc::UserCmd& uCmd) 
{
    Locker lk;

    try
    {
        this->_objectHandle->user(uCmd);
    }
    catch(boost::python::error_already_set const &)
    {
        std::string perror_str = this->parse_python_exception();
        throw PythonInnerException(ERS_HERE, perror_str.c_str());
    }
}
//------------------------------------------------------------------------------
void pyControllable::disable(const std::vector<std::string>& strVec) 
{
    Locker lk;

    try
    {
        this->_objectHandle->disable(strVec);
    }
    catch(boost::python::error_already_set const &)
    {
        std::string perror_str = this->parse_python_exception();
        throw PythonInnerException(ERS_HERE, perror_str.c_str());
    }
}
//------------------------------------------------------------------------------
void pyControllable::enable(const std::vector<std::string>& strVec) 
{
    Locker lk;

    try
    {
        this->_objectHandle->enable(strVec);
    }
    catch(boost::python::error_already_set const &)
    {
        std::string perror_str = this->parse_python_exception();
        throw PythonInnerException(ERS_HERE, perror_str.c_str());
    }
}
//------------------------------------------------------------------------------
void pyControllable::onExit(daq::rc::FSM_STATE fsm_state) noexcept
{
    Locker lk;

    try
    {
        this->_objectHandle->onExit(fsm_state);
    }
    catch(boost::python::error_already_set const &)
    {
        std::string perror_str = this->parse_python_exception();
        ers::error(PythonInnerException(ERS_HERE, perror_str.c_str()));
    }
}
