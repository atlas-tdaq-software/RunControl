/*
 * ItemCtrl.cxx
 *
 *  Created on: Jun 10, 2013
 *      Author: avolio
 */

#include "RunControl/ItemCtrl/ItemCtrl.h"
#include "RunControl/ItemCtrl/ItemCtrlImpl.h"
#include "RunControl/Common/CmdLineParser.h"
#include "RunControl/Common/OnlineServices.h"
#include "RunControl/Common/Exceptions.h"

#include <ers/LocalContext.h>

#include <signal.h>
#include <stdlib.h>
#include <unistd.h>


namespace daq { namespace rc {

namespace {
    // Pointer types are atomic and can be accessed in signal handlers:
    // http://www.gnu.org/software/libc/manual/html_node/Atomic-Types.html
    ItemCtrl* ITEM_CTRL = 0;
}

struct SignaHandlerInstaller {
    SignaHandlerInstaller() {
        struct sigaction sig_act;
        ::sigemptyset(&sig_act.sa_mask);
        ::sigaddset(&sig_act.sa_mask, SIGINT);
        ::sigaddset(&sig_act.sa_mask, SIGTERM);
        sig_act.sa_flags = 0;
        sig_act.sa_handler = &SignaHandlerInstaller::signal_handler;

        ::sigaction(SIGTERM, &sig_act, 0);
        ::sigaction(SIGINT, &sig_act, 0);
    }

    static void signal_handler(int) {
        if(ITEM_CTRL != 0) {
            ITEM_CTRL->shutdown();
        } else {
            // Note: documentation says that calling 'exit' in a signal handler is not safe
            ::_exit(EXIT_SUCCESS);
        }
    }
};

namespace {
    SignaHandlerInstaller sigInstaller;
}

// throws daq::rc::ItemCtrlInitializationFailed
ItemCtrl::ItemCtrl(const std::string& name,
                   const std::string& parent,
                   const std::string& segment,
                   const std::string& partition,
                   const ControllableDispatcher::ControllableList& ctrlList,
                   bool isInteractive,
                   const std::shared_ptr<ControllableDispatcher>& dispatcher)
{
    if(name.empty() || parent.empty() || segment.empty() || partition.empty()) {
        throw daq::rc::ItemCtrlInitializationFailed(ERS_HERE, "the application, parent, segment and partition names cannot be empty");
    }

    for(const auto& ctrl : ctrlList) {
        if(ctrl.get() == nullptr) {
            throw daq::rc::ItemCtrlInitializationFailed(ERS_HERE, "found a null Controllable");
        }
    }

    if(dispatcher.get() == nullptr) {
        throw daq::rc::ItemCtrlInitializationFailed(ERS_HERE, "the command dispatcher cannot be null");
    }

    try {
        OnlineServices::initialize(partition, segment);
    }
    catch(daq::rc::OnlineServicesFailure& ex) {
        throw daq::rc::ItemCtrlInitializationFailed(ERS_HERE, ex.message(), ex);
    }

    m_impl.reset(new ItemCtrlImpl(name, parent, segment, partition, ctrlList, isInteractive, dispatcher));
}

// throws daq::rc::ItemCtrlInitializationFailed
ItemCtrl::ItemCtrl(const std::string& name,
                   const std::string& parent,
                   const std::string& segment,
                   const std::string& partition,
                   const std::shared_ptr<Controllable>& ctrl,
                   bool isInteractive,
                   const std::shared_ptr<ControllableDispatcher>& dispatcher)
    : ItemCtrl(name, parent, segment, partition, ControllableDispatcher::ControllableList{ctrl}, isInteractive, dispatcher)
{
}

// throws daq::rc::ItemCtrlInitializationFailed
ItemCtrl::ItemCtrl(const CmdLineParser& cmdLine,
                   const ControllableDispatcher::ControllableList& ctrlList,
                   const std::shared_ptr<ControllableDispatcher>& dispatcher)
    : ItemCtrl(cmdLine.applicationName(), cmdLine.parentName(), cmdLine.segmentName(),
               cmdLine.partitionName(), ctrlList, cmdLine.interactiveMode(), dispatcher)
{
}

// throws daq::rc::ItemCtrlInitializationFailed
ItemCtrl::ItemCtrl(const CmdLineParser& cmdLine,
                   const std::shared_ptr<Controllable>& ctrl,
                   const std::shared_ptr<ControllableDispatcher>& dispatcher)
    : ItemCtrl(cmdLine.applicationName(), cmdLine.parentName(), cmdLine.segmentName(), cmdLine.partitionName(),
               ctrl, cmdLine.interactiveMode(), dispatcher)
{
}

ItemCtrl::~ItemCtrl() {
}

// throws daq::rc::ItemCtrlInitializationFailed
void ItemCtrl::init() {
    m_impl->init();

    // For the signal handler
    ITEM_CTRL = this;
}

void ItemCtrl::run() {
    // This blocks and it will return when 'shutdown' is called
    m_impl->run();
}

void ItemCtrl::shutdown() {
    // Be careful to what you call here! This may be called in a signal handler and not everything is safe!!!
    // https://www.securecoding.cert.org/confluence/pages/viewpage.action?pageId=1420
    m_impl->shutdown();
}

}}


