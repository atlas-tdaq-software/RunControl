/*
 * ItemCtrlStatus.cxx
 *
 *  Created on: Jun 12, 2013
 *      Author: avolio
 */

#include "RunControl/ItemCtrl/ItemCtrlStatus.h"
#include "RunControl/FSM/FSMStates.h"

#include <chrono>


namespace daq { namespace rc {

ItemCtrlStatus::ItemCtrlStatus()
    : m_fsm_state(FSM_STATE::ABSENT), m_transitioning(false), m_executing_cmd(false), m_publishing(false),
      m_publishing_stats(false), m_last_tr_time(0), m_time_tag(0), m_wall_time(ItemCtrlStatus::wallTime()),
      m_fully_init(false)
{
}

ItemCtrlStatus::~ItemCtrlStatus() {
}

FSM_STATE ItemCtrlStatus::getCurrentState() const {
    return m_fsm_state;
}

bool ItemCtrlStatus::isTransitioning() const {
    return m_transitioning;
}

bool ItemCtrlStatus::isBusy() const {
    return (m_transitioning || m_executing_cmd || m_publishing || m_publishing_stats);
}

bool ItemCtrlStatus::isError() const {
    return !m_error_reasons.empty();
}

bool ItemCtrlStatus::isFullyInitialized() const {
    return m_fully_init;
}

std::vector<std::string> ItemCtrlStatus::getErrorReasons() const {
    return m_error_reasons;
}

infotime_t ItemCtrlStatus::getLastTransitionTime() const {
    return m_last_tr_time;
}

infotime_t ItemCtrlStatus::getInfoTimeTag() const {
    return m_time_tag;
}

long long ItemCtrlStatus::getInfoWallTime() const {
    return m_wall_time;
}

std::shared_ptr<TransitionCmd> ItemCtrlStatus::getLastTransitionCmd() const {
    return m_last_tr_cmd;
}

std::shared_ptr<TransitionCmd> ItemCtrlStatus::getCurrentTransitionCmd() const {
    return m_current_tr_cmd;
}

std::shared_ptr<RunControlBasicCommand> ItemCtrlStatus::getLastCmd() const {
    return m_last_cmd;
}

void ItemCtrlStatus::setCurrentState(FSM_STATE state) {
    m_fsm_state = state;
    updateTimeStamps();
}

void ItemCtrlStatus::setTransitioning(bool transitioning) {
    m_transitioning = transitioning;
    updateTimeStamps();
}

void ItemCtrlStatus::setExecutingCmd(bool executingCmd) {
    m_executing_cmd = executingCmd;
    updateTimeStamps();
}

void ItemCtrlStatus::setPublishing(bool publishing) {
    m_publishing = publishing;
    updateTimeStamps();
}

void ItemCtrlStatus::setPublishingStats(bool publishing) {
    m_publishing_stats = publishing;
    updateTimeStamps();
}

void ItemCtrlStatus::setErrorReasons(const std::vector<std::string>& errorReasons) {
    m_error_reasons = errorReasons;
    updateTimeStamps();
}

void ItemCtrlStatus::setLastTransitionTime(infotime_t time) {
    m_last_tr_time = time;
    updateTimeStamps();
}

void ItemCtrlStatus::setLastTransitionCmd(const std::shared_ptr<TransitionCmd>& trCmd) {
    m_last_tr_cmd = trCmd;
    updateTimeStamps();
}

void ItemCtrlStatus::setCurrentTransitionCmd(const std::shared_ptr<TransitionCmd>& trCmd) {
    m_current_tr_cmd = trCmd;
    updateTimeStamps();
}

void ItemCtrlStatus::setLastCmd(const std::shared_ptr<RunControlBasicCommand>& cmd) {
    m_last_cmd = cmd;
    updateTimeStamps();
}

void ItemCtrlStatus::setFullyInitialized(bool done) {
    m_fully_init = done;
}

long long ItemCtrlStatus::wallTime() {
    return std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
}

infotime_t ItemCtrlStatus::timeTag() {
    return clock_t::now().time_since_epoch().count();
}

void ItemCtrlStatus::updateTimeStamps() {
    m_time_tag = ItemCtrlStatus::timeTag();
    m_wall_time = ItemCtrlStatus::wallTime();
}

}}
