/*
 * CORBAController.cxx
 *
 *  Created on: Oct 1, 2012
 *      Author: avolio
 */

#include "RunControl/Common/CORBAReceiver.h"
#include "RunControl/Common/CommandedApplication.h"
#include "RunControl/Common/Exceptions.h"

#include <ipc/exceptions.h>
#include <ipc/partition.h>
#include <ipc/policy.h>
#include <ers/Issue.h>
#include <ers/LocalContext.h>
#include <ers/ers.h>

#include <daq_tokens/verify.h>

#include <sstream>
#include <ostream>

namespace daq { namespace rc {

namespace {
    std::string unfoldIssueMessage(const ers::Issue& issue) {
        std::ostringstream s;
        s << issue << std::ends;
        return s.str();
    }
}

class Synchronizer {
    public:
        Synchronizer(std::atomic<bool>& sync) : m_atomic(sync) {
            m_atomic.load(std::memory_order_acquire);
        }

        ~Synchronizer() {
            m_atomic.store(false, std::memory_order_release);
        }

    private:
        std::atomic<bool>& m_atomic;
};

struct CORBAReceiverDeleter {
	void operator()(CORBAReceiver* ctrl) const {
	    delete ctrl;
	}
};

CORBAReceiver::IPCAdapter::IPCAdapter(CommandedApplication& ca, std::atomic<bool>& sync, OWLSemaphore& sem)
    : IPCNamedObject<POA_rc::commander, ::ipc::multi_thread>(IPCPartition(ca.partition()), ca.id()),
      m_commanded_application(ca), m_sync(sync), m_sem(sem)
{
}

void CORBAReceiver::IPCAdapter::makeTransition(const ::rc::SenderContext& sc, const char* commandDescription) {
    Synchronizer sync(m_sync);

    try {
        const std::string& creds = verify_credentials((const char *) sc.userName);
        const CommandedApplication::SenderContext sctxt(creds.c_str(),
                                                        (const char*) sc.hostName,
                                                        (const char*) sc.applicationId);
        m_commanded_application.makeTransition(sctxt, std::string(commandDescription));
    }
    catch(daq::tokens::CannotVerifyToken& ex) {
        ers::log(ex);
        throw ::rc::CannotExecute(unfoldIssueMessage(ex).c_str(), ::rc::FailureReason::NOT_ALLOWED);
    }
    catch(daq::rc::Busy& ex) {
        ers::log(ex);
        throw ::rc::CannotExecute(ex.message().c_str(), ::rc::FailureReason::BUSY);
    }
    catch(daq::rc::NotAllowed& ex) {
        ers::log(ex);
        throw ::rc::CannotExecute(ex.message().c_str(), ::rc::FailureReason::NOT_ALLOWED);
    }
    catch(daq::rc::InErrorState& ex) {
        ers::log(ex);
        throw ::rc::CannotExecute(ex.message().c_str(), ::rc::FailureReason::IN_ERROR);
    }
    catch(daq::rc::Exception& ex) {
        ers::log(ex);
        throw ::rc::CannotExecute(ex.message().c_str(), ::rc::FailureReason::UNEXPECTED);
    }
}

void CORBAReceiver::IPCAdapter::executeCommand(const ::rc::SenderContext& sc, const char* commandDescription) {
    Synchronizer sync(m_sync);

    try {
        const std::string& creds = verify_credentials((const char *) sc.userName);
        const CommandedApplication::SenderContext sctxt(creds.c_str(),
                                                        (const char*) sc.hostName,
                                                        (const char*) sc.applicationId);
        m_commanded_application.executeCommand(sctxt, std::string(commandDescription));
    }
    catch(daq::tokens::CannotVerifyToken& ex) {
        ers::log(ex);
        throw ::rc::CannotExecute(unfoldIssueMessage(ex).c_str(), ::rc::FailureReason::NOT_ALLOWED);
    } 
    catch(daq::rc::Busy& ex) {
        ers::log(ex);
        throw ::rc::CannotExecute(ex.message().c_str(), ::rc::FailureReason::BUSY);
    }
    catch(daq::rc::NotAllowed& ex) {
        ers::log(ex);
        throw ::rc::CannotExecute(ex.message().c_str(), ::rc::FailureReason::NOT_ALLOWED);
    }
    catch(daq::rc::InErrorState& ex) {
        ers::log(ex);
        throw ::rc::CannotExecute(ex.message().c_str(), ::rc::FailureReason::IN_ERROR);
    }
    catch(daq::rc::Exception& ex) {
        ers::log(ex);
        throw ::rc::CannotExecute(ex.message().c_str(), ::rc::FailureReason::UNEXPECTED);
    }
}

void CORBAReceiver::IPCAdapter::status(::CORBA::String_out statusDescription) {
    Synchronizer sync(m_sync);
    statusDescription = CORBA::string_dup(m_commanded_application.status().c_str());
}

void CORBAReceiver::IPCAdapter::childStatus(const char* childName, ::CORBA::String_out statusDescription) {
    Synchronizer sync(m_sync);
    try {
        statusDescription = CORBA::string_dup(m_commanded_application.childStatus(childName).c_str());
    }
    catch(daq::rc::UnknownChild&) {
        throw ::rc::UnknownChild(childName);
    }
}

void CORBAReceiver::IPCAdapter::updateChild(const char* applicationDescription) {
    Synchronizer sync(m_sync);
    m_commanded_application.updateChild(std::string(applicationDescription));
}

void CORBAReceiver::IPCAdapter::setError(const ::rc::SenderContext& sc, const ::rc::ErrorItemList& errorItemList) {
    try {
        const std::string& creds = verify_credentials((const char *) sc.userName);

        CommandedApplication::ErrorItems ei;

        const unsigned int num = errorItemList.length();
        for(unsigned long i = 0; i < num; ++i) {
            const auto& item = errorItemList[i];
            ei.push_back({{std::string(item.applicationName),
                          std::string(item.errorDescription),
                          std::string(item.errorType)}});
        }

        const CommandedApplication::SenderContext sctxt(creds.c_str(),
                                                        (const char*) sc.hostName,
                                                        (const char*) sc.applicationId);
        m_commanded_application.setError(sctxt, ei);
    }
    catch(daq::tokens::CannotVerifyToken& ex) {
        ers::log(ex);
        throw ::rc::CannotExecute(unfoldIssueMessage(ex).c_str(), ::rc::FailureReason::NOT_ALLOWED);
    }
    catch(daq::rc::NotAllowed& ex) {
        ers::log(ex);
        throw ::rc::CannotExecute(ex.message().c_str(), ::rc::FailureReason::NOT_ALLOWED);
    }
}

void CORBAReceiver::IPCAdapter::shutdown() {
    m_sem.post();
}

std::string CORBAReceiver::IPCAdapter::verify_credentials(std::string_view creds) {
    // verify can throw CannotVerifyToken
    static const bool s_tokens_enabled = daq::tokens::enabled();
    return s_tokens_enabled ? daq::tokens::verify(creds, "rc://" + partition().name() + "/" + m_commanded_application.id()).get_subject() : std::string(creds);
}

std::shared_ptr<CORBAReceiver> CORBAReceiver::create(CommandedApplication& ca) {
	return std::shared_ptr<CORBAReceiver>(new CORBAReceiver(ca), CORBAReceiverDeleter());
}

CORBAReceiver::CORBAReceiver(CommandedApplication& ca) :
		CommandReceiver(), m_sem(0), m_sync(true), m_deactivated(false),
		m_corba_obj(new IPCAdapter(ca, m_sync, m_sem))
{
}

CORBAReceiver::~CORBAReceiver() noexcept {
    if(m_deactivated.load() == false) {
        deactivate();
    }
}

void CORBAReceiver::init() {
    try {
        m_corba_obj->publish();
    }
    catch(daq::ipc::Exception& ex) {
        throw daq::rc::OnlineServicesFailure(ERS_HERE, "could not publish in IPC", ex);
    }
}

void CORBAReceiver::start() {
    m_sync.store(false, std::memory_order_release);

    m_sem.wait();

    m_sync.load(std::memory_order_acquire);
}

void CORBAReceiver::shutdown() {
    // Be careful to what you call here! This may be called in a signal handler and not everything is safe!!!
    // https://www.securecoding.cert.org/confluence/pages/viewpage.action?pageId=1420
	m_sem.post();
}

void CORBAReceiver::deactivate() {
    try {
        m_corba_obj->withdraw();
    }
    catch(ers::Issue& ex) {
        ers::error(ex);
    }

    try {
        m_corba_obj->_destroy(true);
    }
    catch(ers::Issue& ex) {
        ers::error(ex);
    }

    m_deactivated = true;
}

}}
