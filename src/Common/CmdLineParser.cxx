/*
 * CmdLineParser.cxx
 *
 *  Created on: Jun 4, 2013
 *      Author: avolio
 */

#include "RunControl/Common/CmdLineParser.h"
#include "RunControl/Common/Exceptions.h"

#include <ers/LocalContext.h>

#include <boost/program_options.hpp>

#include <cstdlib>
#include <iostream>
#include <sstream>

namespace po = boost::program_options;

namespace daq { namespace rc {

// The following strings are used as keys in the property tree
const std::string CmdLineParser::APPLICATION_NAME = "application";
const std::string CmdLineParser::PARTITION_NAME = "partition";
const std::string CmdLineParser::PARENT_NAME = "parent";
const std::string CmdLineParser::SEGMENT_NAME = "segment";
const std::string CmdLineParser::INTERACTIVE_MODE = "interactive";
const std::string CmdLineParser::DATABASE_NAME = "tdaq_tb";

CmdLineParser::CmdLineParser(int argc, char** argv, bool haveHelpSwitch) : m_params() {
    std::string applicationName((std::getenv("TDAQ_APPLICATION_NAME") == nullptr) ? "" : std::getenv("TDAQ_APPLICATION_NAME"));
    std::string partitionName((std::getenv("TDAQ_PARTITION") == nullptr) ? "" : std::getenv("TDAQ_PARTITION"));
    std::string dbName((std::getenv("TDAQ_DB") == nullptr) ? "" : std::getenv("TDAQ_DB"));
    std::string segmentName;
    std::string parentName;
    bool interactive = false;

    try {
        po::options_description desc("Basic command line options for a run control application");

        desc.add_options()
        ("partition,p",   po::value<std::string>(&partitionName)->default_value(partitionName)    , "Name of the partition (TDAQ_PARTITION)")
        ("parent,P",      po::value<std::string>(&parentName)                                     , "Name of the parent controller")
        ("segment,s",     po::value<std::string>(&segmentName)                                    , "Name of the segment")
        ("name,n",        po::value<std::string>(&applicationName)->default_value(applicationName), "Name of the application (TDAQ_APPLICATION_NAME)")
        ("database,d",    po::value<std::string>(&dbName)->default_value(dbName)                  , "Name of the database (TDAQ_DB)")
        ("interactive,i"                                                                          , "Flag for running in interactive mode (default is false)");

        if(haveHelpSwitch == true) {
            desc.add_options()("help,h", "Print help message");
        }

        po::variables_map vm;
        po::store(po::command_line_parser(argc, argv).options(desc).allow_unregistered().run(), vm);
        po::notify(vm);

        std::ostringstream h;
        h << desc;
        m_description = h.str();

        if(vm.count("help")) {
            throw daq::rc::CmdLineHelp(ERS_HERE, m_description);
        }

        if(vm.count("interactive")) {
            interactive = true;

            if(!vm.count("parent")) parentName = "NOTAV";
            if(!vm.count("segment")) segmentName = "NOTAV";
            if(partitionName.empty() == true) partitionName = "NOTAV";
            if(applicationName.empty() == true) applicationName = "Generic RunControl Application";
            if(dbName.empty() == true) dbName = "NOTAV";

            std::cout << "***************************************\n";
            std::cout << "***** Running in interactive mode *****\n";
            std::cout << "***************************************\n";
            std::cout << "Application's name: " << applicationName << "\n";
            std::cout << "Application's parent name: " << parentName << "\n";
            std::cout << "Application's segment name: " << segmentName << "\n";
            std::cout << "Application's partition name: " << partitionName << "\n";
            std::cout << "Database implementation: " << dbName << "\n" << std::endl;
        }

        if(parentName.empty() == true) {
            throw daq::rc::CmdLineError(ERS_HERE, "The applications's parent name has not been defined");
        }

        if(segmentName.empty() == true) {
            throw daq::rc::CmdLineError(ERS_HERE, "The applications's segment name has not been defined");
        }

        if(partitionName.empty() == true) {
            throw daq::rc::CmdLineError(ERS_HERE, "The partition name is not defined");
        }
        ::setenv("TDAQ_PARTITION", partitionName.c_str(), 1);

        if(applicationName.empty() == true) {
            throw daq::rc::CmdLineError(ERS_HERE, "The application name is not defined");
        }
        ::setenv("TDAQ_APPLICATION_NAME", applicationName.c_str(), 1);

        if(dbName.empty() == true) {
            throw daq::rc::CmdLineError(ERS_HERE, "The database name (TDAQ_DB) is not defined");
        }
        ::setenv("TDAQ_DB", dbName.c_str(), 1);

        m_params.put<std::string>(CmdLineParser::APPLICATION_NAME, applicationName);
        m_params.put<std::string>(CmdLineParser::PARTITION_NAME, partitionName);
        m_params.put<std::string>(CmdLineParser::PARENT_NAME, parentName);
        m_params.put<std::string>(CmdLineParser::SEGMENT_NAME, segmentName);
        m_params.put<bool>(CmdLineParser::INTERACTIVE_MODE, interactive);
        m_params.put<std::string>(CmdLineParser::DATABASE_NAME, dbName);
    }
    catch(daq::rc::CmdLineError&) {
        throw;
    }
    catch(daq::rc::CmdLineHelp&) {
        throw;
    }
    catch(boost::program_options::error& ex) {
        throw daq::rc::CmdLineError(ERS_HERE, ex.what(), ex);
    }
}

std::string CmdLineParser::partitionName() const {
    return m_params.get<std::string>(CmdLineParser::PARTITION_NAME);
}

std::string CmdLineParser::parentName() const {
    return m_params.get<std::string>(CmdLineParser::PARENT_NAME);
}

std::string CmdLineParser::segmentName() const {
    return m_params.get<std::string>(CmdLineParser::SEGMENT_NAME);
}

std::string CmdLineParser::applicationName() const {
    return m_params.get<std::string>(CmdLineParser::APPLICATION_NAME);
}

std::string CmdLineParser::database() const {
    return m_params.get<std::string>(CmdLineParser::DATABASE_NAME);
}

bool CmdLineParser::interactiveMode() const {
    return m_params.get<bool>(CmdLineParser::INTERACTIVE_MODE);
}

const std::string& CmdLineParser::description() const {
    return m_description;
}

}}
