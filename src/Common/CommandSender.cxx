/*
 * CommandSender.cxx
 *
 *  Created on: Dec 12, 2012
 *      Author: avolio
 */

#include "RunControl/Common/CommandSender.h"
#include "RunControl/Common/Exceptions.h"
#include "RunControl/Common/RunControlBasicCommand.h"
#include "RunControl/Common/RunControlCommands.h"
#include "RunControl/Common/CommandNotifier.h"
#include "RunControl/FSM/FSMCommands.h"

#include <rc/rc.hh>

#include <ers/IssueFactory.h>
#include <ers/LocalContext.h>
#include <ipc/exceptions.h>
#include <system/Host.h>
#include <system/User.h>
#include <daq_tokens/acquire.h>

#include <mutex>
#include <condition_variable>
#include <chrono>
#include <type_traits>
#include <typeinfo>


namespace daq { namespace rc {

namespace {
    class InternalNotifier : public daq::rc::CommandNotifier {
        public:
            InternalNotifier() : daq::rc::CommandNotifier(), m_received(false) {
            }

            void wait() {
                std::unique_lock<std::mutex> lk(m_mutex);
                while(m_received == false) {
                    m_cv.wait(lk);
                }
            }

            // true: condition met
            // false: timeout elapsed
            bool wait(long timeout) {
                const auto& now = std::chrono::system_clock::now();

                std::unique_lock<std::mutex> lk(m_mutex);
                return m_cv.wait_until(lk, now + std::chrono::milliseconds(timeout), [this]() { return m_received == true; });
            }

        protected:
            void commandExecuted(const daq::rc::RunControlBasicCommand&) noexcept override {
                {
                    std::lock_guard<std::mutex> lk(m_mutex);
                    m_received = true;
                }

                m_cv.notify_one();
            }

        private:
            bool m_received;
            std::mutex m_mutex;
            std::condition_variable m_cv;
    };
}

CommandSender::ErrorElement::ErrorElement(const std::string& applicationName,
                                          const std::string& errorDescription,
                                          const std::string& errorType)
    : m_app_name(applicationName), m_err_descr(errorDescription), m_err_type(errorType)
{
}

const std::string& CommandSender::ErrorElement::applicationName() const {
    return m_app_name;
}

const std::string& CommandSender::ErrorElement::errorDescription() const {
    return m_err_descr;
}

const std::string& CommandSender::ErrorElement::errorType() const {
    return m_err_type;
}

CommandSender::CommandSender(const std::string& partition, const std::string& senderId)
    : m_partition(partition), m_user_name(System::User().name_safe()),
      m_host_name(System::LocalHost().full_name()), m_app_id(senderId)
{
}

CommandSender::CommandSender(const IPCPartition& partition, const std::string& senderId)
    : CommandSender(partition.name(), senderId)
{
}

CommandSender::~CommandSender() {
}

void CommandSender::makeTransition(const std::string& recipient, FSM_COMMAND cmd) const {
    makeTransition(recipient, TransitionCmd(cmd));
}

void CommandSender::makeTransition(const std::string& recipient, const TransitionCmd& trCommand) const {
    try {
        ::rc::commander_var server = m_partition.lookup< ::rc::commander>(recipient);

        ::rc::SenderContext sc;

        sc.userName = ::CORBA::string_dup(credentials(m_user_name, recipient).c_str());
        sc.hostName = ::CORBA::string_dup(m_host_name.c_str());
        sc.applicationId = ::CORBA::string_dup(m_app_id.c_str());

        server->makeTransition(sc, trCommand.serialize().c_str());
    }
    catch(daq::ipc::Exception& ex) {
        throw daq::rc::IPCLookup(ERS_HERE, recipient, ex);
    }
    catch(::rc::CannotExecute& ex) {
        ::rc::FailureReason fr = ex.reason;
        const std::string errMsg(ex.error_message);
        switch(fr) {
            case(::rc::BUSY): {
                throw daq::rc::Busy(ERS_HERE, trCommand.fsmCommand());
            }
            case(::rc::NOT_ALLOWED): {
                throw daq::rc::NotAllowed(ERS_HERE, trCommand.fsmCommand(), errMsg);
            }
            case(::rc::IN_ERROR): {
                throw daq::rc::InErrorState(ERS_HERE, trCommand.fsmCommand());
            }
            case(::rc::UNEXPECTED): {
                throw daq::rc::ExecutionFailed(ERS_HERE, trCommand.fsmCommand(), errMsg);
            }
            default: {
                throw daq::rc::ExecutionFailed(ERS_HERE, trCommand.fsmCommand(), errMsg);
            }
        }
    }
    catch(daq::rc::BadCommand& ex) {
        throw;
    }
    catch(CORBA::Exception& ex) {
        throw daq::rc::CorbaException(ERS_HERE, ex._name(), recipient);
    }
}

void CommandSender::setError(const std::string& recipient, const std::vector<CommandSender::ErrorElement>& errorReasons) const {
    try {
        ::rc::commander_var server = m_partition.lookup< ::rc::commander>(recipient);

        const unsigned int num = errorReasons.size();
        ::rc::ErrorItemList eil;
        eil.length(num);
        for(unsigned int i = 0; i < num; ++i) {
            const CommandSender::ErrorElement& val = errorReasons[i];

            ::rc::ErrorItem item;
            item.applicationName = CORBA::string_dup(val.applicationName().c_str());
            item.errorDescription = CORBA::string_dup(val.errorDescription().c_str());
            item.errorType = CORBA::string_dup(val.errorType().c_str());

            eil[i] = item;
        }

        ::rc::SenderContext sc;
        sc.userName = ::CORBA::string_dup(credentials(m_user_name, recipient).c_str());
        sc.hostName = ::CORBA::string_dup(m_host_name.c_str());
        sc.applicationId = ::CORBA::string_dup(m_app_id.c_str());

        server->setError(sc, eil);
    }
    catch(daq::ipc::Exception& ex) {
        throw daq::rc::IPCLookup(ERS_HERE, recipient, ex);
    }
    catch(CORBA::Exception& ex) {
        throw daq::rc::CorbaException(ERS_HERE, ex._name(), recipient);
    }
}

void CommandSender::userCommand(const std::string& recipient, const UserCmd& userCmd) const {
    try {
        execute(recipient, userCmd);
    }
    catch(daq::rc::InErrorState& ex) {
        // This should never happen: in case just re-throw it
        throw daq::rc::ExecutionFailed(ERS_HERE, RunControlCommands::USER_CMD, ex.message(), ex);
    }
    catch(daq::rc::Busy& ex) {
        // This should never happen: in case just re-throw it
        throw daq::rc::ExecutionFailed(ERS_HERE, RunControlCommands::USER_CMD, ex.message(), ex);
    }
}

void CommandSender::userCommand(const std::string& recipient, const std::string& cmdName, const std::vector<std::string>& args) const {
    try {
        execute(recipient, UserCmd(cmdName, args));
    }
    catch(daq::rc::InErrorState& ex) {
        // This should never happen: in case just re-throw it
        throw daq::rc::ExecutionFailed(ERS_HERE, RunControlCommands::USER_CMD, ex.message(), ex);
    }
    catch(daq::rc::Busy& ex) {
        // This should never happen: in case just re-throw it
        throw daq::rc::ExecutionFailed(ERS_HERE, RunControlCommands::USER_CMD, ex.message(), ex);
    }
}

void CommandSender::publish(const std::string& recipient) const {
    try {
        execute(recipient, PublishCmd());
    }
    catch(daq::rc::InErrorState& ex) {
        // This should never happen: in case just re-throw it
        throw daq::rc::ExecutionFailed(ERS_HERE, RunControlCommands::PUBLISH_CMD, ex.message(), ex);
    }
}

void CommandSender::changePublishInterval(const std::string& recipient, unsigned int newInterval) const {
    try {
        execute(recipient, ChangePublishIntervalCmd(newInterval));
    }
    catch(daq::rc::InErrorState& ex) {
        // This should never happen: in case just re-throw it
        throw daq::rc::ExecutionFailed(ERS_HERE, RunControlCommands::CHANGE_PROBE_INTERVAL_CMD, ex.message(), ex);
    }
}

void CommandSender::publishStats(const std::string& recipient) const {
    try {
        execute(recipient, PublishStatsCmd());
    }
    catch(daq::rc::InErrorState& ex) {
        // This should never happen: in case just re-throw it
        throw daq::rc::ExecutionFailed(ERS_HERE, RunControlCommands::PUBLISHSTATS_CMD, ex.message(), ex);
    }
}

void CommandSender::changeFullStatsInterval(const std::string& recipient, unsigned int newInterval) const {
    try {
        execute(recipient, ChangeFullStatIntervalCmd(newInterval));
    }
    catch(daq::rc::InErrorState& ex) {
        // This should never happen: in case just re-throw it
        throw daq::rc::ExecutionFailed(ERS_HERE, RunControlCommands::CHANGE_FULLSTATS_INTERVAL_CMD, ex.message(), ex);
    }
}

void CommandSender::changeStatus(const std::string& recipient, const ChangeStatusCmd& cmd) const {
    try {
        execute(recipient, cmd);
    }
    catch(daq::rc::InErrorState& ex) {
        // This should never happen: in case just re-throw it
        throw daq::rc::ExecutionFailed(ERS_HERE, cmd.name(), ex.message(), ex);
    }
    catch(daq::rc::Busy& ex) {
        // This should never happen: in case just re-throw it
        throw daq::rc::ExecutionFailed(ERS_HERE, cmd.name(), ex.message(), ex);
    }
}

void CommandSender::changeStatus(const std::string& recipient, bool status, const std::vector<std::string>& components) const {
    try {
        execute(recipient, ChangeStatusCmd(status, components));
    }
    catch(daq::rc::InErrorState& ex) {
        // This should never happen: in case just re-throw it
        throw daq::rc::ExecutionFailed(ERS_HERE,
                                       status == true ? RunControlCommands::ENABLE_CMD : RunControlCommands::DISABLE_CMD,
                                       ex.message(),
                                       ex);
    }
    catch(daq::rc::Busy& ex) {
        // This should never happen: in case just re-throw it
        throw daq::rc::ExecutionFailed(ERS_HERE,
                                       status == true ? RunControlCommands::ENABLE_CMD : RunControlCommands::DISABLE_CMD,
                                       ex.message(),
                                       ex);
    }
}

void CommandSender::startStopApplications(const std::string& recipient, const StartStopAppCmd& cmd) const {
    try {
        execute(recipient, cmd);
    }
    catch(daq::rc::InErrorState& ex) {
        // This should never happen: in case just re-throw it
        throw daq::rc::ExecutionFailed(ERS_HERE, cmd.name(), ex.message(), ex);
    }
}

void CommandSender::dynamicRestart(const std::string& recipient, const DynRestartCmd& cmd) const {
    execute(recipient, cmd);
}

void CommandSender::testApp(const std::string& recipient, const TestAppCmd& cmd) const {
    try {
        execute(recipient, cmd);
    }
    catch(daq::rc::InErrorState& ex) {
        // This should never happen: in case just re-throw it
        throw daq::rc::ExecutionFailed(ERS_HERE, RunControlCommands::TESTAPP_CMD, ex.message(), ex);
    }
}

void CommandSender::startApplications(const std::string& recipient, const std::vector<std::string>& applications) const {
    try {
        execute(recipient, StartStopAppCmd(StartStopAppCmd::is_start_app_cmd, applications));
    }
    catch(daq::rc::InErrorState& ex) {
        // This should never happen: in case just re-throw it
        throw daq::rc::ExecutionFailed(ERS_HERE, RunControlCommands::STARTAPP_CMD, ex.message(), ex);
    }
}

void CommandSender::stopApplications(const std::string& recipient, const std::vector<std::string>& applications) const {
    try {
        execute(recipient, StartStopAppCmd(StartStopAppCmd::is_stop_app_cmd, applications));
    }
    catch(daq::rc::InErrorState& ex) {
        // This should never happen: in case just re-throw it
        throw daq::rc::ExecutionFailed(ERS_HERE, RunControlCommands::STOPAPP_CMD, ex.message(), ex);
    }
}

void CommandSender::restartApplications(const std::string& recipient, const std::vector<std::string>& applications) const {
    try {
        execute(recipient, StartStopAppCmd(StartStopAppCmd::is_restart_app_cmd, applications));
    }
    catch(daq::rc::InErrorState& ex) {
        // This should never happen: in case just re-throw it
        throw daq::rc::ExecutionFailed(ERS_HERE, RunControlCommands::RESTARTAPP_CMD, ex.message(), ex);
    }
}

void CommandSender::dynamicRestart(const std::string& recipient, const std::vector<std::string>& segments) const {
    execute(recipient, DynRestartCmd(segments));
}

void CommandSender::testApp(const std::string& recipient, const std::vector<std::string>& applications) const {
    try {
        execute(recipient, TestAppCmd(applications));
    }
    catch(daq::rc::InErrorState& ex) {
        // This should never happen: in case just re-throw it
        throw daq::rc::ExecutionFailed(ERS_HERE, RunControlCommands::TESTAPP_CMD, ex.message(), ex);
    }
}

void CommandSender::exit(const std::string& recipient) const {
    try {
        execute(recipient, ExitCmd());
    }
    catch(daq::rc::InErrorState& ex) {
        // This should never happen: in case just re-throw it
        throw daq::rc::ExecutionFailed(ERS_HERE, RunControlCommands::EXIT_CMD, ex.message(), ex);
    }
}

void CommandSender::ignoreError(const std::string& recipient) const {
    try {
        execute(recipient, IgnoreErrorCmd());
    }
    catch(daq::rc::InErrorState& ex) {
        // This should never happen: in case just re-throw it
        throw daq::rc::ExecutionFailed(ERS_HERE, RunControlCommands::IGNORE_ERROR_CMD, ex.message(), ex);
    }
    catch(daq::rc::Busy& ex) {
        // This should never happen: in case just re-throw it
        throw daq::rc::ExecutionFailed(ERS_HERE, RunControlCommands::IGNORE_ERROR_CMD, ex.message(), ex);
    }
}

void CommandSender::updateChild(const std::string& recipient, const ApplicationStatusCmd& statusUpdateCmd) const {
    try {
        ::rc::commander_var server = m_partition.lookup< ::rc::commander>(recipient);
        server->updateChild(statusUpdateCmd.serialize().c_str());
    }
    catch(daq::ipc::Exception& ex) {
        throw daq::rc::IPCLookup(ERS_HERE, recipient, ex);
    }
    catch(daq::rc::BadCommand& ex) {
        throw;
    }
    catch(CORBA::Exception& ex) {
        throw daq::rc::CorbaException(ERS_HERE, ex._name(), recipient);
    }
}

std::unique_ptr<ApplicationStatusCmd> CommandSender::status(const std::string& recipient) const {
    try {
        ::rc::commander_var server = m_partition.lookup< ::rc::commander>(recipient);

        ::CORBA::String_var applicationStatus;
        server->status(applicationStatus.out());

        return std::unique_ptr<ApplicationStatusCmd>(new ApplicationStatusCmd(std::string((const char*) applicationStatus)));
    }
    catch(daq::ipc::Exception& ex) {
        throw daq::rc::IPCLookup(ERS_HERE, recipient, ex);
    }
    catch(daq::rc::BadCommand& ex) {
        throw;
    }
    catch(CORBA::Exception& ex) {
        throw daq::rc::CorbaException(ERS_HERE, ex._name(), recipient);
    }
}

std::unique_ptr<ApplicationStatusCmd> CommandSender::childStatus(const std::string& recipient, const std::string& childName) const {
    try {
        ::rc::commander_var server = m_partition.lookup< ::rc::commander>(recipient);

        ::CORBA::String_var chdStatus;
        server->childStatus(childName.c_str(), chdStatus.out());

        return std::unique_ptr<ApplicationStatusCmd>(new ApplicationStatusCmd(std::string((const char*) chdStatus)));
    }
    catch(daq::ipc::Exception& ex) {
        throw daq::rc::IPCLookup(ERS_HERE, recipient, ex);
    }
    catch(daq::rc::BadCommand& ex) {
        throw;
    }
    catch(::rc::UnknownChild&) {
        throw daq::rc::UnknownChild(ERS_HERE, childName);
    }
    catch(CORBA::Exception& ex) {
        throw daq::rc::CorbaException(ERS_HERE, ex._name(), recipient);
    }
}

void CommandSender::execute(const std::string& recipient, const RunControlBasicCommand& cmd) const {
    try {
        ::rc::commander_var server = m_partition.lookup< ::rc::commander>(recipient);

        ::rc::SenderContext sc;
        sc.userName = ::CORBA::string_dup(credentials(m_user_name, recipient).c_str());
        sc.hostName = ::CORBA::string_dup(m_host_name.c_str());
        sc.applicationId = ::CORBA::string_dup(m_app_id.c_str());

        server->executeCommand(sc, cmd.serialize().c_str());
    }
    catch(daq::ipc::Exception& ex) {
        throw daq::rc::IPCLookup(ERS_HERE, recipient, ex);
    }
    catch(::rc::CannotExecute& ex) {
        ::rc::FailureReason fr = ex.reason;
        const std::string errMsg(ex.error_message);
        switch(fr) {
            case(::rc::BUSY): {
                throw daq::rc::Busy(ERS_HERE, cmd.name());
            }
            case(::rc::NOT_ALLOWED): {
                throw daq::rc::NotAllowed(ERS_HERE, cmd.name(), errMsg);
            }
            case(::rc::IN_ERROR): {
                throw daq::rc::InErrorState(ERS_HERE, cmd.name());
            }
            case(::rc::UNEXPECTED): {
                throw daq::rc::ExecutionFailed(ERS_HERE, cmd.name(), errMsg);
            }
            default: {
                throw daq::rc::ExecutionFailed(ERS_HERE, cmd.name(), errMsg);
            }
        }
    }
    catch(daq::rc::BadCommand& ex) {
        throw;
    }
    catch(CORBA::Exception& ex) {
        throw daq::rc::CorbaException(ERS_HERE, ex._name(), recipient);
    }
}

int CommandSender::debugLevel(const std::string& recipient) const {
    try {
        ::rc::commander_var server = m_partition.lookup< ::rc::commander>(recipient);
        return server->debug_level();
    }
    catch(daq::ipc::Exception& ex) {
        throw daq::rc::IPCLookup(ERS_HERE, recipient, ex);
    }
    catch(CORBA::Exception& ex) {
        throw daq::rc::CorbaException(ERS_HERE, ex._name(), recipient);
    }
}

void CommandSender::debugLevel(const std::string& recipient, int level) const {
    try {
        ::rc::commander_var server = m_partition.lookup< ::rc::commander>(recipient);
        return server->debug_level(level);
    }
    catch(daq::ipc::Exception& ex) {
        throw daq::rc::IPCLookup(ERS_HERE, recipient, ex);
    }
    catch(CORBA::Exception& ex) {
        throw daq::rc::CorbaException(ERS_HERE, ex._name(), recipient);
    }
}

void CommandSender::sendCommand(const std::string& recipient, const RunControlBasicCommand& cmd) const {
    try {
        const TransitionCmd& trCmd = dynamic_cast<const TransitionCmd&>(cmd);
        makeTransition(recipient, trCmd);
    }
    catch(std::bad_cast&) {
        execute(recipient, cmd);
    }
}

void CommandSender::sendCommand(const std::string& recipient, RunControlBasicCommand& cmd, const CommandNotifier& cmdNot) const {
    if(cmd.notifySupported() == false) {
        throw daq::rc::BadCommand(ERS_HERE, cmd.name(), "asynchronous notification on completion not supported");
    }

    cmd.senderReference(cmdNot.reference());
    sendCommand(recipient, cmd);
}

bool CommandSender::sendCommand(const std::string& recipient, RunControlBasicCommand& cmd, unsigned long timeout) const {
    if(cmd.notifySupported() == false) {
        throw daq::rc::BadCommand(ERS_HERE, cmd.name(), "asynchronous notification on completion not supported");
    }

    InternalNotifier notifier;

    cmd.senderReference(notifier.reference());
    sendCommand(recipient, cmd);

    bool toReturn = true;

    if(timeout == 0) {
        notifier.wait();
    } else {
        toReturn = notifier.wait(timeout);
    }

    // In principle only needed when the 'wait' returns for the timeout being elapsed
    notifier.deactivate();

    return toReturn;
}

std::string CommandSender::credentials(std::string_view userName, std::string_view recipient) const {
    static bool s_tokens_enabled = daq::tokens::enabled();
    return s_tokens_enabled ?
        daq::tokens::acquire(daq::tokens::Mode::Reuse, "rc://" + m_partition.name() + "/" + std::string(recipient)) : std::string(userName);
}

}}
