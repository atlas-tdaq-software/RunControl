/*
 * RunControlCommands.cxx
 *
 *  Created on: Oct 2, 2012
 *      Author: avolio
 */

#include "RunControl/FSM/FSMCommands.h"
#include "RunControl/FSM/FSMStates.h"
#include "RunControl/Common/Exceptions.h"
#include "RunControl/Common/RunControlCommands.h"

#include <ers/ers.h>
#include <ers/LocalContext.h>
#include <TestManager/Test.h>

#include <boost/assign/list_of.hpp>
#include <boost/algorithm/string.hpp>

#include <algorithm>
#include <typeinfo>
#include <sstream>
#include <type_traits>

namespace daq { namespace rc {

namespace {
    template<typename T>
    std::unique_ptr<RunControlBasicCommand> build_command(const std::string& commandRepresentation) {
        return std::unique_ptr<T>(new T(commandRepresentation));
    }

    template<>
    std::unique_ptr<RunControlBasicCommand> build_command<TransitionCmd>(const std::string& commandRepresentation) {
        return TransitionCmd::create(commandRepresentation);
    }
}

struct CmdAccessor {
    void setBroadcastBitOnUserCommand(UserCmd& cmd, bool value) {
        cmd.isBroadcast(value);
    }
};

/**
 * RunControlCommands implementation
 */
const std::string RunControlCommands::MAKE_TRANSITION_CMD = "MAKE_TRANSITION";
const std::string RunControlCommands::ENABLE_CMD = "ENABLE";
const std::string RunControlCommands::DISABLE_CMD = "DISABLE";
const std::string RunControlCommands::IGNORE_ERROR_CMD = "IGNORE_ERROR";
const std::string RunControlCommands::STOPAPP_CMD = "STOPAPP";
const std::string RunControlCommands::STARTAPP_CMD = "STARTAPP";
const std::string RunControlCommands::RESTARTAPP_CMD = "RESTARTAPP";
const std::string RunControlCommands::PUBLISH_CMD = "PUBLISH";
const std::string RunControlCommands::PUBLISHSTATS_CMD = "PUBLISHSTATS";
const std::string RunControlCommands::EXIT_CMD = "EXIT";
const std::string RunControlCommands::USER_CMD = "USER";
const std::string RunControlCommands::DYN_RESTART_CMD = "DYN_RESTART";
const std::string RunControlCommands::CHANGE_PROBE_INTERVAL_CMD = "CHANGE_PROBE_INTERVAL";
const std::string RunControlCommands::CHANGE_FULLSTATS_INTERVAL_CMD = "CHANGE_FULLSTATS_INTERVAL";
const std::string RunControlCommands::UPDATE_CHILD_STATUS_CMD = "UPDATE_CHILD_STATUS";
const std::string RunControlCommands::TESTAPP_CMD = "TESTAPP";
const std::string RunControlCommands::REGEX_TOKEN = "//r";

const RunControlCommands::bm_type RunControlCommands::CMD_MAP =
		boost::assign::list_of<RunControlCommands::bm_type::relation>(RC_COMMAND::MAKE_TRANSITION, MAKE_TRANSITION_CMD, &build_command<TransitionCmd>)
																	 (RC_COMMAND::ENABLE, ENABLE_CMD, &build_command<ChangeStatusCmd>)
																	 (RC_COMMAND::DISABLE, DISABLE_CMD, &build_command<ChangeStatusCmd>)
																	 (RC_COMMAND::IGNORE_ERROR, IGNORE_ERROR_CMD, &build_command<IgnoreErrorCmd>)
																	 (RC_COMMAND::STOPAPP, STOPAPP_CMD, &build_command<StartStopAppCmd>)
																	 (RC_COMMAND::STARTAPP, STARTAPP_CMD, &build_command<StartStopAppCmd>)
																	 (RC_COMMAND::RESTARTAPP, RESTARTAPP_CMD, &build_command<StartStopAppCmd>)
																	 (RC_COMMAND::PUBLISH, PUBLISH_CMD, &build_command<PublishCmd>)
																	 (RC_COMMAND::PUBLISHSTATS, PUBLISHSTATS_CMD, &build_command<PublishStatsCmd>)
																	 (RC_COMMAND::EXIT, EXIT_CMD, &build_command<ExitCmd>)
																	 (RC_COMMAND::USER, USER_CMD, &build_command<UserCmd>)
																	 (RC_COMMAND::DYN_RESTART, DYN_RESTART_CMD, &build_command<DynRestartCmd>)
																	 (RC_COMMAND::CHANGE_PROBE_INTERVAL, CHANGE_PROBE_INTERVAL_CMD, &build_command<ChangePublishIntervalCmd>)
																	 (RC_COMMAND::CHANGE_FULLSTATS_INTERVAL, CHANGE_FULLSTATS_INTERVAL_CMD, &build_command<ChangeFullStatIntervalCmd>)
                                                                     (RC_COMMAND::TESTAPP, TESTAPP_CMD, &build_command<TestAppCmd>)
																	 (RC_COMMAND::UPDATE_CHILD_STATUS, UPDATE_CHILD_STATUS_CMD, &build_command<ApplicationStatusCmd>);

std::string RunControlCommands::commandToString(RC_COMMAND cmd) {
	const RunControlCommands::bm_type::left_map::const_iterator& it = RunControlCommands::CMD_MAP.left.find(cmd);
	return it->second;
}

RC_COMMAND RunControlCommands::stringToCommand(const std::string& cmd) {
	const RunControlCommands::bm_type::right_map::const_iterator& it = RunControlCommands::CMD_MAP.right.find(boost::algorithm::to_upper_copy(cmd));
	if(it != RunControlCommands::CMD_MAP.right.end()) {
		return it->second;
	} else {
	    const std::string& errMsg = std::string("the command \"") + cmd + "\" is not valid" ;
	    throw daq::rc::BadCommand(ERS_HERE, cmd, errMsg);
	}
}

std::unique_ptr<RunControlBasicCommand> RunControlCommands::factory(const std::string& stringRepresentation) {
    // this throws
    const RunControlBasicCommand rcCmd(stringRepresentation);
    const std::string& cmdName = rcCmd.name();

    const RunControlCommands::bm_type::right_map::const_iterator& it = RunControlCommands::CMD_MAP.right.find(boost::algorithm::to_upper_copy(cmdName));
    if(it != RunControlCommands::CMD_MAP.right.end()) {
        return (it->info)(stringRepresentation);
    } else {
        const std::string& errMsg = std::string("the command \"") + cmdName + "\" is not valid" ;
        throw daq::rc::BadCommand(ERS_HERE, cmdName, errMsg);
    }
}

/**
 * ExitCmd implementation
 */
ExitCmd::ExitCmd(const std::string& commandDescription) :
        RunControlBasicCommand(commandDescription)
{
    if(notifySupported() == true) {
        throw daq::rc::BadCommand(ERS_HERE,
                                  daq::rc::RunControlCommands::EXIT_CMD,
                                  "command configured to support asynchronous notification but it does not");
    }
}

ExitCmd::ExitCmd() :
        RunControlBasicCommand(RC_COMMAND::EXIT)
{
    notifySupported(false);
}

std::unique_ptr<ExitCmd> ExitCmd::clone() const {
    return std::unique_ptr<ExitCmd>(doClone());
}

ExitCmd* ExitCmd::doClone() const {
    return new ExitCmd(*this);
}

std::set<RC_COMMAND> ExitCmd::notConcurrentWith() const {
    return std::set<RC_COMMAND>{RC_COMMAND::EXIT};
}

/**
 * DynRestartCmd implementation
 */

const std::string DynRestartCmd::DYN_RESTART_CTRL_NAMES = "_ctrl_names_";

DynRestartCmd::DynRestartCmd(const std::string& commandDescription) :
        RunControlBasicCommand(commandDescription)
{
    if(notifySupported() == true) {
        throw daq::rc::BadCommand(ERS_HERE,
                                  daq::rc::RunControlCommands::DYN_RESTART_CMD,
                                  "command configured to support asynchronous notification but it does not");
    }
}

DynRestartCmd::DynRestartCmd(const std::vector<std::string>& ctrls) :
        RunControlBasicCommand(RC_COMMAND::DYN_RESTART)
{
    notifySupported(false);
    controllers(ctrls);
}

void DynRestartCmd::controllers(const std::vector<std::string>& ctrls) {
    parameters<std::string>(DynRestartCmd::DYN_RESTART_CTRL_NAMES, ctrls);
}

std::vector<std::string> DynRestartCmd::controllers() const {
    return parameters<std::string>(DynRestartCmd::DYN_RESTART_CTRL_NAMES);
}

bool DynRestartCmd::equals(const RunControlBasicCommand& other) const {
    bool is_the_same = RunControlBasicCommand::equals(other);

    if(is_the_same == true) {
        try {
            try {
                const DynRestartCmd& drCmd = dynamic_cast<const DynRestartCmd&>(other);
                is_the_same = (drCmd.controllers() == controllers());
            }
            catch(std::bad_cast&) {
                throw;
            }
        }
        catch(...) {
            is_the_same = false;
        }
    }

    return is_the_same;
}

std::unique_ptr<DynRestartCmd> DynRestartCmd::clone() const {
    return std::unique_ptr<DynRestartCmd>(doClone());
}

DynRestartCmd* DynRestartCmd::doClone() const {
    return new DynRestartCmd(*this);
}

std::string DynRestartCmd::toString() const {
    std::ostringstream hrStr;

    hrStr << RunControlBasicCommand::toString();

    for(const auto& c : controllers()) {
        hrStr << " " + c;
    }

    return hrStr.str();
}

std::vector<std::string> DynRestartCmd::arguments() const {
    return controllers();
}

/**
 * TestAppCmd implementation
 */

const std::string TestAppCmd::TESTAPP_APP_NAMES = "_test_app_names_";
const std::string TestAppCmd::TEST_APP_SCOPE = "_test_scope_";
const std::string TestAppCmd::TEST_APP_LEVEL = "_test_level_";

TestAppCmd::TestAppCmd(const std::string& commandDescription) :
        RunControlBasicCommand(commandDescription)
{
    try {
        level();
    }
    catch(daq::rc::Exception& ex) {
        throw daq::rc::BadCommand(ERS_HERE, RunControlCommands::TESTAPP_CMD,
                                  "the test\'s level is not defined", ex);
    }

    try {
        scope();
    }
    catch(daq::rc::Exception& ex) {
        throw daq::rc::BadCommand(ERS_HERE, RunControlCommands::TESTAPP_CMD,
                                  "the test\'s scope is not defined", ex);
    }
}

TestAppCmd::TestAppCmd(const std::vector<std::string>& apps) :
        RunControlBasicCommand(RC_COMMAND::TESTAPP)
{
    applications(apps);
    level(1);
    scope(daq::tm::Test::Scope::Any);
}

void TestAppCmd::applications(const std::vector<std::string>& apps) {
    parameters<std::string>(TestAppCmd::TESTAPP_APP_NAMES, apps);
}

std::vector<std::string> TestAppCmd::applications() const {
    return parameters<std::string>(TestAppCmd::TESTAPP_APP_NAMES);
}

int TestAppCmd::level() const {
    return parameter<int>(TestAppCmd::TEST_APP_LEVEL);
}

void TestAppCmd::level(int testLevel) {
    parameter<int>(TestAppCmd::TEST_APP_LEVEL, testLevel);
}

std::string TestAppCmd::scope() const {
    return parameter<std::string>(TestAppCmd::TEST_APP_SCOPE);
}

void TestAppCmd::scope(const std::string& testScope) {
    parameter<std::string>(TestAppCmd::TEST_APP_SCOPE, testScope);
}

std::set<RC_COMMAND> TestAppCmd::notConcurrentWith() const {
    return std::set<RC_COMMAND>{ RC_COMMAND::EXIT };
}

bool TestAppCmd::equals(const RunControlBasicCommand& other) const {
    bool is_the_same = RunControlBasicCommand::equals(other);

    if(is_the_same == true) {
        try {
            try {
                const TestAppCmd& taCmd = dynamic_cast<const TestAppCmd&>(other);
                is_the_same = (taCmd.applications() == applications());
            }
            catch(std::bad_cast&) {
                throw;
            }
        }
        catch(...) {
            is_the_same = false;
        }
    }

    return is_the_same;
}

std::unique_ptr<TestAppCmd> TestAppCmd::clone() const {
    return std::unique_ptr<TestAppCmd>(doClone());
}

TestAppCmd* TestAppCmd::doClone() const {
    return new TestAppCmd(*this);
}

std::string TestAppCmd::toString() const {
    std::ostringstream hrStr;

    hrStr << RunControlBasicCommand::toString();

    for(const auto& c : applications()) {
        hrStr << " " + c;
    }

    return hrStr.str();
}

std::vector<std::string> TestAppCmd::arguments() const {
    return applications();
}

/**
 * IgnoreErrorCmd implementation
 */
IgnoreErrorCmd::IgnoreErrorCmd(const std::string& commandDescription) :
        RunControlBasicCommand(commandDescription)
{
    if(notifySupported() == true) {
        throw daq::rc::BadCommand(ERS_HERE,
                                  daq::rc::RunControlCommands::IGNORE_ERROR_CMD,
                                  "command configured to support asynchronous notification but it does not");
    }
}

IgnoreErrorCmd::IgnoreErrorCmd() :
        RunControlBasicCommand(RC_COMMAND::IGNORE_ERROR)
{
    notifySupported(false);
}

std::unique_ptr<IgnoreErrorCmd> IgnoreErrorCmd::clone() const {
    return std::unique_ptr<IgnoreErrorCmd>(doClone());
}

IgnoreErrorCmd* IgnoreErrorCmd::doClone() const {
    return new IgnoreErrorCmd(*this);
}

/**
 * PublishCmd command implementation
 */
PublishCmd::PublishCmd(const std::string& commandDescription)
    : RunControlBasicCommand(commandDescription)
{
}

PublishCmd::PublishCmd()
    : RunControlBasicCommand(RC_COMMAND::PUBLISH)
{
}

std::set<RC_COMMAND> PublishCmd::notConcurrentWith() const {
    return std::set<RC_COMMAND>{ RC_COMMAND::EXIT };
}

std::unique_ptr<PublishCmd> PublishCmd::clone() const {
    return std::unique_ptr<PublishCmd>(doClone());
}

PublishCmd* PublishCmd::doClone() const {
    return new PublishCmd(*this);
}

/**
 * ChangePublishInterval command implementation
 */
const std::string ChangePublishIntervalCmd::CHANGE_PUBLISH_INTERVAL_TAG = "_probe_interval_";

ChangePublishIntervalCmd::ChangePublishIntervalCmd(const std::string& commandDescription) :
        RunControlBasicCommand(commandDescription)
{
    // Verify that the new interval is defined
    try {
        publishInterval();
    }
    catch(daq::rc::Exception& ex) {
        throw daq::rc::BadCommand(ERS_HERE, RunControlCommands::CHANGE_PROBE_INTERVAL_CMD,
                                  "the new publish interval is not defined", ex);
    }
}

ChangePublishIntervalCmd::ChangePublishIntervalCmd(unsigned int newInterval) :
        RunControlBasicCommand(RC_COMMAND::CHANGE_PROBE_INTERVAL)
{
    publishInterval(newInterval);
}

unsigned int ChangePublishIntervalCmd::publishInterval() const {
    return parameter<unsigned int>(ChangePublishIntervalCmd::CHANGE_PUBLISH_INTERVAL_TAG);
}

void ChangePublishIntervalCmd::publishInterval(unsigned int newInterval) {
    parameter<unsigned int>(ChangePublishIntervalCmd::CHANGE_PUBLISH_INTERVAL_TAG, newInterval);
}

std::unique_ptr<ChangePublishIntervalCmd> ChangePublishIntervalCmd::clone() const {
    return std::unique_ptr<ChangePublishIntervalCmd>(doClone());
}

ChangePublishIntervalCmd* ChangePublishIntervalCmd::doClone() const {
    return new ChangePublishIntervalCmd(*this);
}

std::set<RC_COMMAND> ChangePublishIntervalCmd::notConcurrentWith() const {
    return std::set<RC_COMMAND>{ RC_COMMAND::MAKE_TRANSITION, RC_COMMAND::CHANGE_PROBE_INTERVAL, RC_COMMAND::EXIT };
}

std::string ChangePublishIntervalCmd::toString() const {
    std::ostringstream hrStr;

    hrStr << RunControlBasicCommand::toString();
    hrStr << " ";
    hrStr << publishInterval();

    return hrStr.str();
}

std::vector<std::string> ChangePublishIntervalCmd::arguments() const {
    return { std::to_string(publishInterval()) };
}

/**
 * PublishStatsCmd command implementation
 */
PublishStatsCmd::PublishStatsCmd(const std::string& commandDescription)
    : RunControlBasicCommand(commandDescription)
{
}

PublishStatsCmd::PublishStatsCmd()
    : RunControlBasicCommand(RC_COMMAND::PUBLISHSTATS)
{
}

std::set<RC_COMMAND> PublishStatsCmd::notConcurrentWith() const {
    return std::set<RC_COMMAND>{ RC_COMMAND::EXIT };
}

std::unique_ptr<PublishStatsCmd> PublishStatsCmd::clone() const {
    return std::unique_ptr<PublishStatsCmd>(doClone());
}

PublishStatsCmd* PublishStatsCmd::doClone() const {
    return new PublishStatsCmd(*this);
}

/**
 * ChangeFullStatInterval command implementation
 */
const std::string ChangeFullStatIntervalCmd::CHANGE_FULLSTAT_INTERVAL_TAG = "_fullstat_interval_";

ChangeFullStatIntervalCmd::ChangeFullStatIntervalCmd(const std::string& commandDescription) :
        RunControlBasicCommand(commandDescription)
{
    // Verify that the new interval is defined
    try {
        fullStatInterval();
    }
    catch(daq::rc::Exception& ex) {
        throw daq::rc::BadCommand(ERS_HERE, RunControlCommands::CHANGE_FULLSTATS_INTERVAL_CMD,
                                  "the new full statistics interval is not defined", ex);
    }
}

ChangeFullStatIntervalCmd::ChangeFullStatIntervalCmd(unsigned int newInterval) :
        RunControlBasicCommand(RC_COMMAND::CHANGE_FULLSTATS_INTERVAL)
{
    fullStatInterval(newInterval);
}

unsigned int ChangeFullStatIntervalCmd::fullStatInterval() const {
    return parameter<unsigned int>(ChangeFullStatIntervalCmd::CHANGE_FULLSTAT_INTERVAL_TAG);
}

void ChangeFullStatIntervalCmd::fullStatInterval(unsigned int newInterval) {
    parameter<unsigned int>(ChangeFullStatIntervalCmd::CHANGE_FULLSTAT_INTERVAL_TAG, newInterval);
}

std::unique_ptr<ChangeFullStatIntervalCmd> ChangeFullStatIntervalCmd::clone() const {
    return std::unique_ptr<ChangeFullStatIntervalCmd>(doClone());
}

ChangeFullStatIntervalCmd* ChangeFullStatIntervalCmd::doClone() const {
    return new ChangeFullStatIntervalCmd(*this);
}

std::set<RC_COMMAND> ChangeFullStatIntervalCmd::notConcurrentWith() const {
    return std::set<RC_COMMAND>{ RC_COMMAND::MAKE_TRANSITION, RC_COMMAND::CHANGE_FULLSTATS_INTERVAL, RC_COMMAND::EXIT };
}

std::string ChangeFullStatIntervalCmd::toString() const {
    std::ostringstream hrStr;

    hrStr << RunControlBasicCommand::toString();
    hrStr << " ";
    hrStr << fullStatInterval();

    return hrStr.str();
}

std::vector<std::string> ChangeFullStatIntervalCmd::arguments() const {
    return { std::to_string(fullStatInterval()) };
}

/**
 * ChangeStatusCmd implementation
 */
const std::string ChangeStatusCmd::CHANGESTATUS_CMD_CMPNTS_TAG = "_components_";
const std::string ChangeStatusCmd::CHANGESTATUS_CMD_ENABLE_DISABLE_TAG = "_is_enable_";

ChangeStatusCmd::ChangeStatusCmd(const std::string& commandDescription) :
        RunControlBasicCommand(commandDescription)
{
    try {
        isEnable();
    }
    catch(daq::rc::Exception& ex) {
        throw daq::rc::BadCommand(ERS_HERE, RunControlCommands::ENABLE_CMD + " " + RunControlCommands::DISABLE_CMD,
                                  "the command is ambiguous (enable/disabled not defined), probably corrupted", ex);
    }
}

ChangeStatusCmd::ChangeStatusCmd(bool isEnable, const std::string& componentName) :
        ChangeStatusCmd(isEnable, std::vector<std::string>({componentName}))
{
}

ChangeStatusCmd::ChangeStatusCmd(bool isEnable, const std::vector<std::string>& cmpnts) :
           RunControlBasicCommand(((isEnable == true) ? RC_COMMAND::ENABLE : RC_COMMAND::DISABLE))
{
    enable(isEnable);
    components(cmpnts);
}

void ChangeStatusCmd::enable(bool isEnable) {
    parameter<bool>(ChangeStatusCmd::CHANGESTATUS_CMD_ENABLE_DISABLE_TAG, isEnable);
}

bool ChangeStatusCmd::isEnable() const {
    return parameter<bool>(ChangeStatusCmd::CHANGESTATUS_CMD_ENABLE_DISABLE_TAG);
}

void ChangeStatusCmd::components(const std::vector<std::string>& cmpnts) {
    parameters<std::string>(ChangeStatusCmd::CHANGESTATUS_CMD_CMPNTS_TAG, cmpnts);
}

std::vector<std::string> ChangeStatusCmd::components() const {
    return parameters<std::string>(ChangeStatusCmd::CHANGESTATUS_CMD_CMPNTS_TAG);
}

std::unique_ptr<ChangeStatusCmd> ChangeStatusCmd::clone() const {
    return std::unique_ptr<ChangeStatusCmd>(doClone());
}

std::string ChangeStatusCmd::toString() const {
    std::ostringstream hrStr;
    hrStr << RunControlBasicCommand::toString();

    for(const auto& c : components()) {
        hrStr << " " << c;
    }

    return hrStr.str();
}

std::vector<std::string> ChangeStatusCmd::arguments() const {
    return components();
}

bool ChangeStatusCmd::equals(const RunControlBasicCommand& other) const {
    bool is_the_same = RunControlBasicCommand::equals(other);

    if(is_the_same == true) {
        try {
            try {
                const ChangeStatusCmd& chgCmd = dynamic_cast<const ChangeStatusCmd&>(other);
                is_the_same = ((isEnable() == chgCmd.isEnable()) && (components() == chgCmd.components()));
            }
            catch(std::bad_cast&) {
                throw;
            }
        }
        catch(...) {
            is_the_same = false;
        }
    }

    return is_the_same;
}

ChangeStatusCmd* ChangeStatusCmd::doClone() const {
    return new ChangeStatusCmd(*this);
}

/**
 * UserCmd implementation
 */
const std::string UserCmd::USR_CMD_NAME_TAG = "_user_cmd_name_";
const std::string UserCmd::USR_CMD_PARAMS_TAG = "_user_cmd_parameters_";
const std::string UserCmd::USR_CMD_FSM_STATE_TAG = "_user_cmd_state_";
const std::string UserCmd::USR_CMD_IS_BROADCAST = "_user_cmd_brdct_";

UserCmd::UserCmd(const std::string& commandDescription) :
        RunControlBasicCommand(commandDescription)
{
    // Verify that the user command has a name
    try {
        commandName();
    }
    catch(daq::rc::Exception& ex) {
        throw daq::rc::BadCommand(ERS_HERE, RunControlCommands::USER_CMD,
                                  "no name has been defined for the user command", ex);
    }

    try {
        // Check this in order to be sure that the conversion to 'bool' does not fail
        // in case the command description is corrupted
        isBroadcast();
    }
    catch(daq::rc::Exception& ex) {
        throw daq::rc::BadCommand(ERS_HERE, RunControlCommands::USER_CMD,
                                  "the command is corrupted", ex);
    }
}

UserCmd::UserCmd(const std::string& cmdName, const std::vector<std::string>& params) :
        RunControlBasicCommand(RC_COMMAND::USER)
{
    commandName(cmdName);
    commandParameters(params);
    isBroadcast(false);
}

bool UserCmd::isBroadcast() const {
    return parameter<bool>(UserCmd::USR_CMD_IS_BROADCAST);
}

void UserCmd::isBroadcast(bool value) {
    parameter<bool>(UserCmd::USR_CMD_IS_BROADCAST, value);
}

std::string UserCmd::currentFSMState() const {
    return parameter<std::string>(UserCmd::USR_CMD_FSM_STATE_TAG);
}

void UserCmd::currentFSMState(FSM_STATE state) {
    parameter<std::string>(UserCmd::USR_CMD_FSM_STATE_TAG, FSMStates::stateToString(state));
}

std::string UserCmd::commandName() const {
    return parameter<std::string>(UserCmd::USR_CMD_NAME_TAG);
}

void UserCmd::commandName(const std::string& userCommandName) {
    parameter<std::string>(UserCmd::USR_CMD_NAME_TAG, userCommandName);
}

std::vector<std::string> UserCmd::commandParameters() const {
    return parameters<std::string>(UserCmd::USR_CMD_PARAMS_TAG);
}

void UserCmd::commandParameters(const std::vector<std::string>& params) {
    parameters<std::string>(UserCmd::USR_CMD_PARAMS_TAG, params);
}

bool UserCmd::equals(const RunControlBasicCommand& other) const {
    bool is_the_same = RunControlBasicCommand::equals(other);

    if(is_the_same == true) {
        try {
            try {
                const UserCmd& userCmd = dynamic_cast<const UserCmd&>(other);
                is_the_same = ((userCmd.commandName() == commandName()) && (userCmd.commandParameters() == commandParameters()));
            }
            catch(std::bad_cast&) {
                throw;
            }
        }
        catch(...) {
            is_the_same = false;
        }
    }

    return is_the_same;
}

std::unique_ptr<UserCmd> UserCmd::clone() const {
    return std::unique_ptr<UserCmd>(doClone());
}

UserCmd* UserCmd::doClone() const {
    return new UserCmd(*this);
}

std::string UserCmd::toString() const {
    std::ostringstream hrStr;
    hrStr << RunControlBasicCommand::toString();
    hrStr << " ";
    hrStr << commandName();
    for(const auto& p : commandParameters()) {
        hrStr << " " + p;
    }

    return hrStr.str();
}

std::vector<std::string> UserCmd::arguments() const {
    std::vector<std::string> args;
    args.push_back(commandName());

    const auto& params = commandParameters();
    args.insert(args.end(), params.begin(), params.end());
    return args;
}

/**
 * ApplicationStatusCmd implementation
 */
const std::string ApplicationStatusCmd::APP_NAME_TAG = "_app_name_";
const std::string ApplicationStatusCmd::APP_STATE_TAG = "_app_state_";
const std::string ApplicationStatusCmd::APP_IS_TR_TAG = "_app_is_transitioning_";
const std::string ApplicationStatusCmd::APP_ERROR_TAG = "_app_error_reasons_";
const std::string ApplicationStatusCmd::APP_BAD_CHLD_TAG = "_app_bad_children_";
const std::string ApplicationStatusCmd::APP_IS_BUSY_TAG = "_app_is_busy_";
const std::string ApplicationStatusCmd::APP_IS_FULLINIT_TAG = "_app_is_fullinit_";
const std::string ApplicationStatusCmd::APP_LAST_TR_CMD_TAG = "_app_last_transition_cmd_";
const std::string ApplicationStatusCmd::APP_CURR_TR_CMD_TAG = "_app_current_transition_cmd";
const std::string ApplicationStatusCmd::APP_LAST_TR_TIME_TAG = "_app_last_transition_time_";
const std::string ApplicationStatusCmd::APP_LAST_CMD_TAG = "_app_last_cmd_";
const std::string ApplicationStatusCmd::APP_INFO_TIME_TAG_TAG = "_app_info_time_tag_";
const std::string ApplicationStatusCmd::APP_INFO_WALL_TIME_TAG = "_app_info_wall_time_tag_";


// throws daq::rc::BadCommand
ApplicationStatusCmd::ApplicationStatusCmd(const std::string& commandDescription) :
        RunControlBasicCommand(commandDescription)
{
    try {
        // Check the presence and validity of the FSM state
        FSMStates::stringToState(fsmState());
    }
    catch(daq::rc::Exception& ex) {
         throw daq::rc::BadCommand(ERS_HERE, RunControlCommands::UPDATE_CHILD_STATUS_CMD,
                                   "the application's current FSM state is not defined", ex);
    }

    try {
        applicationName();
    }
    catch(daq::rc::Exception& ex) {
        throw daq::rc::BadCommand(ERS_HERE, RunControlCommands::UPDATE_CHILD_STATUS_CMD,
                                  "no application has been associated to the status update command", ex);
    }

    try {
        // Check this in order to be sure that the conversion to 'bool' does not fail
        // in case the command description is corrupted
        isTransitioning();
        isBusy();
        isFullyInitialized();

        // And this to be sure that it is a number
        lastTransitionDuration();
        infoTimeTag();
        infoWallTime();
    }
    catch(daq::rc::Exception& ex) {
        throw daq::rc::BadCommand(ERS_HERE, RunControlCommands::UPDATE_CHILD_STATUS_CMD,
                                  "the command is corrupted", ex);
    }
}

ApplicationStatusCmd::ApplicationStatusCmd(const std::string& appName,
                                           FSM_STATE appState,
                                           bool transitioning,
                                           bool busy,
                                           bool fullyInitialized,
                                           infotime_t timeTag,
                                           long long wallTime) :
        RunControlBasicCommand(RC_COMMAND::UPDATE_CHILD_STATUS)
{
    applicationName(appName);
    fsmState(appState);
    infoTimeTag(timeTag);
    infoWallTime(wallTime);
    isTransitioning(transitioning);
    isBusy(busy);
    isFullyInitialized(fullyInitialized);
    lastTransitionDuration(0);
}

std::string ApplicationStatusCmd::applicationName() const {
    return parameter<std::string>(ApplicationStatusCmd::APP_NAME_TAG);
}

void ApplicationStatusCmd::applicationName(const std::string& appName) {
    parameter<std::string>(ApplicationStatusCmd::APP_NAME_TAG, appName);
}

std::string ApplicationStatusCmd::fsmState() const {
    return parameter<std::string>(ApplicationStatusCmd::APP_STATE_TAG);
}

void ApplicationStatusCmd::fsmState(FSM_STATE state) {
    parameter<std::string>(ApplicationStatusCmd::APP_STATE_TAG, FSMStates::stateToString(state));
}

bool ApplicationStatusCmd::isTransitioning() const {
    return parameter<bool>(ApplicationStatusCmd::APP_IS_TR_TAG);
}

void ApplicationStatusCmd::isTransitioning(bool isTransitioning) {
    parameter<bool>(ApplicationStatusCmd::APP_IS_TR_TAG, isTransitioning);
}

std::vector<std::string> ApplicationStatusCmd::errorReasons() const {
    return parameters<std::string>(ApplicationStatusCmd::APP_ERROR_TAG);
}

void ApplicationStatusCmd::errorReasons(const std::vector<std::string>& errReasons) {
    parameters<std::string>(ApplicationStatusCmd::APP_ERROR_TAG, errReasons);
}

std::vector<std::string> ApplicationStatusCmd::badChildren() const {
    return parameters<std::string>(ApplicationStatusCmd::APP_BAD_CHLD_TAG);
}

void ApplicationStatusCmd::badChildren(const std::vector<std::string>& badCh) {
    parameters<std::string>(ApplicationStatusCmd::APP_BAD_CHLD_TAG, badCh);
}

bool ApplicationStatusCmd::isBusy() const {
    return parameter<bool>(ApplicationStatusCmd::APP_IS_BUSY_TAG);
}

void ApplicationStatusCmd::isBusy(bool isBusy) {
    parameter<bool>(ApplicationStatusCmd::APP_IS_BUSY_TAG, isBusy);
}

bool ApplicationStatusCmd::isFullyInitialized() const {
    return parameter<bool>(ApplicationStatusCmd::APP_IS_FULLINIT_TAG);
}

void ApplicationStatusCmd::isFullyInitialized(bool done) {
    parameter<bool>(ApplicationStatusCmd::APP_IS_FULLINIT_TAG, done);
}

infotime_t ApplicationStatusCmd::infoTimeTag() const {
    return parameter<infotime_t>(ApplicationStatusCmd::APP_INFO_TIME_TAG_TAG);
}

void ApplicationStatusCmd::infoTimeTag(infotime_t time) {
    parameter<infotime_t>(ApplicationStatusCmd::APP_INFO_TIME_TAG_TAG, time);
}

// Wall time needed by the Expert System
long long ApplicationStatusCmd::infoWallTime() const {
    return parameter<long long>(ApplicationStatusCmd::APP_INFO_WALL_TIME_TAG);
}

// Wall time needed by the Expert System
void ApplicationStatusCmd::infoWallTime(long long time) {
    parameter<long long>(ApplicationStatusCmd::APP_INFO_WALL_TIME_TAG, time);
}

std::unique_ptr<TransitionCmd> ApplicationStatusCmd::lastTransitionCommand() const {
    std::unique_ptr<TransitionCmd> trCmd;

    try {
        // Here we want to return the same original command and with the right runtime type
        const std::string& stringCommand = commandRepresentation(ApplicationStatusCmd::APP_LAST_TR_CMD_TAG);
        trCmd = TransitionCmd::create(stringCommand);
    }
    catch(daq::rc::CommandParameterNotFound& ex) {
        ERS_DEBUG(5, ex);
    }
    catch(daq::rc::CommandParameterInvalid& ex) {
        ers::error(ex);
    }
    catch(daq::rc::BadCommand& ex) {
        ers::error(ex);
    }

    return trCmd;
}

void ApplicationStatusCmd::lastTransitionCommand(const TransitionCmd& cmd) {
    command(ApplicationStatusCmd::APP_LAST_TR_CMD_TAG, cmd);
}

std::unique_ptr<TransitionCmd> ApplicationStatusCmd::currentTransitionCommand() const {
    std::unique_ptr<TransitionCmd> trCmd;

    try {
        // Here we want to return the same original command and with the right runtime type
        const std::string& stringCommand = commandRepresentation(ApplicationStatusCmd::APP_CURR_TR_CMD_TAG);
        trCmd = TransitionCmd::create(stringCommand);
    }
    catch(daq::rc::CommandParameterNotFound& ex) {
        ERS_DEBUG(5, ex);
    }
    catch(daq::rc::CommandParameterInvalid& ex) {
        ers::error(ex);
    }
    catch(daq::rc::BadCommand& ex) {
        ers::error(ex);
    }

    return trCmd;
}

void ApplicationStatusCmd::currentTransitionCommand(const TransitionCmd& cmd) {
    command(ApplicationStatusCmd::APP_CURR_TR_CMD_TAG, cmd);
}

std::unique_ptr<RunControlBasicCommand> ApplicationStatusCmd::lastCommand() const {
    std::unique_ptr<RunControlBasicCommand> cmd;

    try {
        const std::string& stringCommand = commandRepresentation(ApplicationStatusCmd::APP_LAST_CMD_TAG);
        cmd = RunControlCommands::factory(stringCommand);
    }
    catch(daq::rc::CommandParameterNotFound& ex) {
        ERS_DEBUG(5, ex);
    }
    catch(daq::rc::CommandParameterInvalid& ex) {
        ers::error(ex);
    }
    catch(daq::rc::BadCommand& ex) {
        ers::error(ex);
    }

    return cmd;
}

void ApplicationStatusCmd::lastCommand(const RunControlBasicCommand& cmd) {
    command(ApplicationStatusCmd::APP_LAST_CMD_TAG, cmd);
}

infotime_t ApplicationStatusCmd::lastTransitionDuration() const {
    return parameter<infotime_t>(ApplicationStatusCmd::APP_LAST_TR_TIME_TAG);
}

void ApplicationStatusCmd::lastTransitionDuration(infotime_t duration) {
    parameter<infotime_t>(ApplicationStatusCmd::APP_LAST_TR_TIME_TAG, duration);
}

std::unique_ptr<ApplicationStatusCmd> ApplicationStatusCmd::clone() const {
    return std::unique_ptr<ApplicationStatusCmd>(doClone());
}

ApplicationStatusCmd* ApplicationStatusCmd::doClone() const {
    return new ApplicationStatusCmd(*this);
}

std::string ApplicationStatusCmd::toString() const {
    std::ostringstream hrStr;

    hrStr << RunControlBasicCommand::toString() <<
            " applicationName=" << applicationName() <<
            " fsmState=" << fsmState() <<
            " isTransitioning=" << isTransitioning() <<
            " isInternalError=" << !errorReasons().empty() <<
            " isBusy=" << isBusy() << " " <<
            " transitionTime=" << lastTransitionDuration();

    std::unique_ptr<TransitionCmd>&& lt = lastTransitionCommand();
    if(lt.get() != nullptr) {
        hrStr << " lastTransition=\"" << lt->toString() << "\"";
    }

    std::unique_ptr<TransitionCmd>&& ct = currentTransitionCommand();
    if(ct.get() != nullptr) {
        hrStr << " currentTransition=\"" << ct->toString() << "\"";
    }

    return hrStr.str();
}

/**
 * StartStopApp command implementation
 */
const std::string StartStopAppCmd::APP_TAG = "_applications_";
const std::string StartStopAppCmd::CURRENT_STATE_TAG = "_current_state_";
const std::string StartStopAppCmd::DESTINATION_STATE_TAG = "_destination_state_";
const std::string StartStopAppCmd::COMMAND_TAG = "_transition_cmd_";
const std::string StartStopAppCmd::IS_START_TAG = "_is_start_";
const std::string StartStopAppCmd::IS_RESTART_TAG = "_is_restart_";

const StartStopAppCmd::is_start_t StartStopAppCmd::is_start_app_cmd;
const StartStopAppCmd::is_stop_t StartStopAppCmd::is_stop_app_cmd;
const StartStopAppCmd::is_restart_t StartStopAppCmd::is_restart_app_cmd;

StartStopAppCmd::StartStopAppCmd(const std::string& commandDescription) :
		RunControlBasicCommand(commandDescription)
{
    try {
        isStart();
        isRestart();
    }
    catch(daq::rc::Exception& ex) {
        throw daq::rc::BadCommand(ERS_HERE,
                                  RunControlCommands::STOPAPP_CMD + " " +
                                  RunControlCommands::RESTARTAPP_CMD + " " + RunControlCommands::STARTAPP_CMD,
                                  "the command is ambiguous: start/stop/restart is not defined",
                                  ex);
    }
}

StartStopAppCmd::StartStopAppCmd(is_start_t, const std::vector<std::string>& apps) :
		RunControlBasicCommand(RC_COMMAND::STARTAPP)
{
    parameter<bool>(StartStopAppCmd::IS_START_TAG, true);
    parameter<bool>(StartStopAppCmd::IS_RESTART_TAG, false);
    applications(apps);
}

StartStopAppCmd::StartStopAppCmd(is_stop_t, const std::vector<std::string>& apps) :
        RunControlBasicCommand(RC_COMMAND::STOPAPP)
{
    parameter<bool>(StartStopAppCmd::IS_START_TAG, false);
    parameter<bool>(StartStopAppCmd::IS_RESTART_TAG, false);
    applications(apps);
}

StartStopAppCmd::StartStopAppCmd(is_restart_t, const std::vector<std::string>& apps) :
        RunControlBasicCommand(RC_COMMAND::RESTARTAPP)
{
    parameter<bool>(StartStopAppCmd::IS_START_TAG, false);
    parameter<bool>(StartStopAppCmd::IS_RESTART_TAG, true);
    applications(apps);
}

std::set<std::string> StartStopAppCmd::applications() const {
	const std::vector<std::string>& apps = parameters<std::string>(StartStopAppCmd::APP_TAG);
	std::set<std::string> uniqueApps(apps.begin(), apps.end());
	return uniqueApps;
}

void StartStopAppCmd::applications(const std::vector<std::string>& applications) {
	parameters<std::string>(StartStopAppCmd::APP_TAG, applications);
}

std::string StartStopAppCmd::currentState() const {
    return parameter<std::string>(StartStopAppCmd::CURRENT_STATE_TAG);
}

void StartStopAppCmd::currentState(FSM_STATE state) {
    parameter<std::string>(StartStopAppCmd::CURRENT_STATE_TAG, FSMStates::stateToString(state));
}

std::string StartStopAppCmd::destinationState() const {
    return parameter<std::string>(StartStopAppCmd::DESTINATION_STATE_TAG);
}

void StartStopAppCmd::destinationState(FSM_STATE state) {
    parameter<std::string>(StartStopAppCmd::DESTINATION_STATE_TAG, FSMStates::stateToString(state));
}

std::string StartStopAppCmd::transitionCommand() const {
    return parameter<std::string>(StartStopAppCmd::COMMAND_TAG);
}

void StartStopAppCmd::transitionCommand(FSM_COMMAND cmd) {
    parameter<std::string>(StartStopAppCmd::COMMAND_TAG, FSMCommands::commandToString(cmd));
}

bool StartStopAppCmd::isStart() const {
    return parameter<bool>(StartStopAppCmd::IS_START_TAG);
}

bool StartStopAppCmd::isRestart() const {
    return parameter<bool>(StartStopAppCmd::IS_RESTART_TAG);
}

bool StartStopAppCmd::equals(const RunControlBasicCommand& other) const {
    bool is_the_same = RunControlBasicCommand::equals(other);

    if(is_the_same == true) {
        try {
            try {
                const StartStopAppCmd& cmd = dynamic_cast<const StartStopAppCmd&>(other);
                is_the_same = ((cmd.isRestart() == isRestart()) && (cmd.isStart() == isStart()) && (cmd.applications() == applications()));
            }
            catch(std::bad_cast&) {
                throw;
            }
            catch(daq::rc::CommandParameterNotFound& ex) {
                ers::warning(ex);
                throw;
            }
        }
        catch(...) {
            is_the_same = false;
        }
    }

    return is_the_same;
}

std::set<RC_COMMAND> StartStopAppCmd::notConcurrentWith() const {
    if((isRestart() == true) || (isStart() == true)) {
        return std::set<RC_COMMAND>{RC_COMMAND::EXIT};
    } else {
        return std::set<RC_COMMAND>();
    }
}

std::unique_ptr<StartStopAppCmd> StartStopAppCmd::clone() const {
    return std::unique_ptr<StartStopAppCmd>(doClone());
}

StartStopAppCmd* StartStopAppCmd::doClone() const {
    return new StartStopAppCmd(*this);
}

std::string StartStopAppCmd::toString() const {
    std::ostringstream hrStr;

    hrStr << RunControlBasicCommand::toString();

    for(const auto& apps : applications()) {
        hrStr << " " + apps;
    }

    hrStr << " isStart=" << isStart() <<
             " isRestart=" << isRestart() <<
             " isStop=" << !(isStart() || isRestart()) <<
             " transitionCommand=" << transitionCommand() <<
             " currentState=" << currentState() <<
             " destinationState=" << destinationState();

    return hrStr.str();
}

std::vector<std::string> StartStopAppCmd::arguments() const {
    const auto& apps = applications();
    return std::vector<std::string>(apps.begin(), apps.end());
}

/**
 * TransitionCommand implementation
 */
const std::string TransitionCmd::FSM_HEADER_TAG = "_fsm_";
const std::string TransitionCmd::FSM_COMMAND_TAG = TransitionCmd::FSM_HEADER_TAG + "." + "event";
const std::string TransitionCmd::FSM_SRC_STATE_TAG = TransitionCmd::FSM_HEADER_TAG + "." + "source";
const std::string TransitionCmd::FSM_DST_STATE_TAG = TransitionCmd::FSM_HEADER_TAG + "." + "destination";
const std::string TransitionCmd::FSM_SUBTR_DONE_TAG = TransitionCmd::FSM_HEADER_TAG + "." + "done_subtransitions";
const std::string TransitionCmd::FSM_SUBTR_SKIP_TAG = TransitionCmd::FSM_HEADER_TAG + "." + "skip_subtransitions";
const std::string TransitionCmd::FSM_IS_RESTART_TAG = TransitionCmd::FSM_HEADER_TAG + "." + "is_restart";
const std::string TransitionCmd::FSM_SUBTR_TAG = TransitionCmd::FSM_HEADER_TAG + "." + "subtransitions";

TransitionCmd::TransitionCmd(const std::string& commandDescription) :
        RunControlBasicCommand(commandDescription)
{
    try {
        // Check the correctness of the FSM command
        FSMCommands::stringToCommand(fsmCommand());
    }
    catch(daq::rc::Exception& ex) {
        throw daq::rc::BadCommand(ERS_HERE, RunControlCommands::MAKE_TRANSITION_CMD, "the FSM command is not defined", ex);
    }

    try {
        FSMStates::stringToState(fsmSourceState());
    }
    catch(daq::rc::InvalidState& ex) {
        throw daq::rc::BadCommand(ERS_HERE, RunControlCommands::MAKE_TRANSITION_CMD, "the FSM source state is not valid", ex);
    }
    catch(daq::rc::CommandParameterNotFound& ex) {
        // This is fine here, the command is filled by the FSM
    }

    try {
        FSMStates::stringToState(fsmDestinationState());
    }
    catch(daq::rc::InvalidState& ex) {
        throw daq::rc::BadCommand(ERS_HERE, RunControlCommands::MAKE_TRANSITION_CMD, "the FSM destination state is not valid", ex);
    }
    catch(daq::rc::CommandParameterNotFound& ex) {
        // This is fine here, the command is filled by the FSM
    }

    try {
        isRestart();
        subTransitionsDone();
        skipSubTransitions();
    }
    catch(daq::rc::Exception& ex) {
       throw daq::rc::BadCommand(ERS_HERE, RunControlCommands::MAKE_TRANSITION_CMD + " " + fsmCommand(), "the command is corrupted: " + ex.message(), ex);
    }
}

TransitionCmd::TransitionCmd(FSM_COMMAND cmd, bool restart) :
        RunControlBasicCommand(RC_COMMAND::MAKE_TRANSITION)
{
    fsmCommand(cmd);
    isRestart(restart);
    subTransitionsDone(false);
    skipSubTransitions(false);
}

std::unique_ptr<TransitionCmd> TransitionCmd::create(const std::string& commandDescription) {
    std::unique_ptr<TransitionCmd> trCmd(new TransitionCmd(commandDescription));

    // TODO: Update here if additional transition commands are defined
    const FSM_COMMAND fsmCmd = FSMCommands::stringToCommand(trCmd->fsmCommand());
    switch(fsmCmd) {
        case FSM_COMMAND::USERBROADCAST:
        {
            trCmd.reset(new UserBroadcastCmd(commandDescription));
            break;
        }
        case FSM_COMMAND::SUB_TRANSITION:
        {
            trCmd.reset(new SubTransitionCmd(commandDescription));
            break;
        }
        case FSM_COMMAND::RESYNCH:
        {
            trCmd.reset(new ResynchCmd(commandDescription));
            break;
        }
        default:
            break;
    }

    return trCmd;
}

std::string TransitionCmd::fsmCommand() const {
    return parameter<std::string>(TransitionCmd::FSM_COMMAND_TAG);
}

void TransitionCmd::fsmCommand(FSM_COMMAND cmd) {
    parameter<std::string>(TransitionCmd::FSM_COMMAND_TAG, FSMCommands::commandToString(cmd));
}

std::string TransitionCmd::fsmSourceState() const {
    return parameter<std::string>(TransitionCmd::FSM_SRC_STATE_TAG);
}

void TransitionCmd::fsmSourceState(FSM_STATE state) {
    parameter<std::string>(TransitionCmd::FSM_SRC_STATE_TAG, FSMStates::stateToString(state));
}

std::string TransitionCmd::fsmDestinationState() const {
    return parameter<std::string>(TransitionCmd::FSM_DST_STATE_TAG);
}

void TransitionCmd::fsmDestinationState(FSM_STATE state) {
    parameter<std::string>(TransitionCmd::FSM_DST_STATE_TAG, FSMStates::stateToString(state));
}

bool TransitionCmd::isRestart() const {
    return parameter<bool>(TransitionCmd::FSM_IS_RESTART_TAG);
}

void TransitionCmd::isRestart(bool is_controller_restart) {
    parameter<bool>(TransitionCmd::FSM_IS_RESTART_TAG, is_controller_restart);
}

std::vector<std::string> TransitionCmd::subTransitions() const {
    return parameters<std::string>(TransitionCmd::FSM_SUBTR_TAG + "." + fsmCommand());
}

void TransitionCmd::subTransitions(const std::map<FSM_COMMAND, std::vector<std::string>>& sts) {
    for(const auto& p : sts) {
        parameters<std::string>(TransitionCmd::FSM_SUBTR_TAG + "." + FSMCommands::commandToString(p.first), p.second);
    }
}

bool TransitionCmd::subTransitionsDone() const {
    return parameter<bool>(TransitionCmd::FSM_SUBTR_DONE_TAG);
}

void TransitionCmd::subTransitionsDone(bool done) {
    parameter<bool>(TransitionCmd::FSM_SUBTR_DONE_TAG, done);
}

bool TransitionCmd::skipSubTransitions() const {
    return parameter<bool>(TransitionCmd::FSM_SUBTR_SKIP_TAG);
}

void TransitionCmd::skipSubTransitions(bool skip) {
    parameter<bool>(TransitionCmd::FSM_SUBTR_SKIP_TAG, skip);
}

bool TransitionCmd::equals(const RunControlBasicCommand& other) const {
    bool is_the_same = RunControlBasicCommand::equals(other);

    if(is_the_same == true) {
        try {
            try {
                const TransitionCmd& trCmd = dynamic_cast<const TransitionCmd&>(other);
                is_the_same = (trCmd.fsmCommand() == fsmCommand());
            }
            catch(std::bad_cast&) {
                throw;
            }
        }
        catch(...) {
            is_the_same = false;
        }
    }

    return is_the_same;
}

std::unique_ptr<TransitionCmd> TransitionCmd::clone() const {
    return std::unique_ptr<TransitionCmd>(doClone());
}

TransitionCmd* TransitionCmd::doClone() const {
    return new TransitionCmd(*this);
}

std::set<RC_COMMAND> TransitionCmd::notConcurrentWith() const {
    if(fsmCommand() != FSMCommands::SHUTDOWN_CMD) {
        return std::set<RC_COMMAND>{RC_COMMAND::EXIT, RC_COMMAND::MAKE_TRANSITION, RC_COMMAND::RESTARTAPP, RC_COMMAND::STARTAPP, RC_COMMAND::STOPAPP};
    } else {
        // It should always be possible to execute SHUTDOWN
        return std::set<RC_COMMAND>{RC_COMMAND::EXIT};
    }
}

std::string TransitionCmd::toString() const {
    return RunControlBasicCommand::toString() + " " + fsmCommand();
}

std::vector<std::string> TransitionCmd::arguments() const {
    return { fsmCommand() };
}

/**
 * UserBroadcast implementation
 */
const std::string UserBroadcastCmd::USR_BRDCST_USR_CMD_TAG = "_user_cmd_";

UserBroadcastCmd::UserBroadcastCmd(const std::string& commandDescription) :
        TransitionCmd(commandDescription)
{
    try {
        userCommand();
    }
    catch(daq::rc::Exception& ex) {
        throw daq::rc::BadCommand(ERS_HERE, RunControlCommands::MAKE_TRANSITION_CMD + " " + fsmCommand(),
                                  "no user command is defined", ex);
    }
}

UserBroadcastCmd::UserBroadcastCmd(const UserCmd& userCmd) :
        TransitionCmd(FSM_COMMAND::USERBROADCAST)
{
    UserCmd cmd(userCmd);

    CmdAccessor ca;
    ca.setBroadcastBitOnUserCommand(cmd, true);

    userCommand(cmd);
}

std::unique_ptr<UserCmd> UserBroadcastCmd::userCommand() const {
    return command<UserCmd>(UserBroadcastCmd::USR_BRDCST_USR_CMD_TAG);
}

void UserBroadcastCmd::userCommand(const UserCmd& userCmd) {
    UserCmd cmd(userCmd);

    CmdAccessor ca;
    ca.setBroadcastBitOnUserCommand(cmd, true);

    command(UserBroadcastCmd::USR_BRDCST_USR_CMD_TAG, cmd);
}

bool UserBroadcastCmd::equals(const RunControlBasicCommand& other) const {
    bool is_the_same = TransitionCmd::equals(other);

    if(is_the_same == true) {
        try {
            try {
                const UserBroadcastCmd& ubCmd = dynamic_cast<const UserBroadcastCmd&>(other);
                is_the_same = ((*ubCmd.userCommand()) == (*userCommand()));
            }
            catch(std::bad_cast&) {
                throw;
            }
        }
        catch(...) {
            is_the_same = false;
        }
    }

    return is_the_same;
}

std::unique_ptr<UserBroadcastCmd> UserBroadcastCmd::clone() const {
    return std::unique_ptr<UserBroadcastCmd>(doClone());
}

UserBroadcastCmd* UserBroadcastCmd::doClone() const {
    return new UserBroadcastCmd(*this);
}

std::string UserBroadcastCmd::toString() const {
    return TransitionCmd::toString() + " " + userCommand()->toString();
}

std::vector<std::string> UserBroadcastCmd::arguments() const {
    std::vector<std::string>&& args = TransitionCmd::arguments();

    const auto& others = userCommand()->arguments();
    args.insert(args.end(), others.begin(), others.end());

    return std::move(args);
}

/**
 * ResynchCmd implementation
 */

const std::string ResynchCmd::RESYNCH_ECR_TAG = "_resynch_ecr_";
const std::string ResynchCmd::RESYNCH_EL1ID_TAG = "_resynch_el1id_";
const std::string ResynchCmd::RESYNCH_MODULES_TAG = "_resync_modules_";

ResynchCmd::ResynchCmd(const std::string& commandDescription)
    : TransitionCmd(commandDescription)
{
    // Check that the ECR counts are properly defined
    try {
        ecrCounts();
    }
    catch(daq::rc::Exception& ex) {
        throw daq::rc::BadCommand(ERS_HERE, RunControlCommands::MAKE_TRANSITION_CMD + " " + fsmCommand(),
                                  "the ECR number is not defined", ex);
    }

    // Check that the extended L1ID is properly defined
    try {
        extendedL1ID();
    }
    catch(daq::rc::Exception& ex) {
        throw daq::rc::BadCommand(ERS_HERE, RunControlCommands::MAKE_TRANSITION_CMD + " " + fsmCommand(),
                                  "the extended L1ID is not defined", ex);
    }
}

ResynchCmd::ResynchCmd(std::uint32_t ecrCount, std::uint32_t extendedL1id, const std::vector<std::string>& mods)
    : TransitionCmd(FSM_COMMAND::RESYNCH)
{
    ecrCounts(ecrCount);
    extendedL1ID(extendedL1id);
    modules(mods);
}

std::uint32_t ResynchCmd::ecrCounts() const {
    return parameter<std::uint32_t>(ResynchCmd::RESYNCH_ECR_TAG);
}

void ResynchCmd::ecrCounts(const std::uint32_t ecr) {
    parameter<std::uint32_t>(ResynchCmd::RESYNCH_ECR_TAG, ecr);
}

std::uint32_t ResynchCmd::extendedL1ID() const {
    return parameter<std::uint32_t>(ResynchCmd::RESYNCH_EL1ID_TAG);
}

void ResynchCmd::extendedL1ID(const std::uint32_t extendedL1ID) {
    parameter<std::uint32_t>(ResynchCmd::RESYNCH_EL1ID_TAG, extendedL1ID);
}

std::vector<std::string> ResynchCmd::modules() const {
    return parameters<std::string>(ResynchCmd::RESYNCH_MODULES_TAG);
}

void ResynchCmd::modules(const std::vector<std::string>& mods) {
    parameters<std::string>(ResynchCmd::RESYNCH_MODULES_TAG, mods);
}

std::unique_ptr<ResynchCmd> ResynchCmd::clone() const {
    return std::unique_ptr<ResynchCmd>(doClone());
}

ResynchCmd* ResynchCmd::doClone() const {
    return new ResynchCmd(*this);
}

std::string ResynchCmd::toString() const {
    std::string str = TransitionCmd::toString() + " " + std::to_string(ecrCounts());
    for(const auto& m : modules()) {
        str += " " + m;
    }

    return str;
}

std::vector<std::string> ResynchCmd::arguments() const {
    std::vector<std::string>&& args = TransitionCmd::arguments();

    args.push_back(std::to_string(ecrCounts()));
    for(const auto& m : modules()) {
        args.push_back(m);
    }

    return std::move(args);
}

bool ResynchCmd::equals(const RunControlBasicCommand& other) const {
    bool is_the_same = TransitionCmd::equals(other);

    if(is_the_same == true) {
        try {
            try {
                const ResynchCmd& resCmd = dynamic_cast<const ResynchCmd&>(other);
                is_the_same = ((resCmd.ecrCounts() == ecrCounts()) && (resCmd.extendedL1ID() == extendedL1ID()) && (resCmd.arguments() == arguments()));
            }
            catch(std::bad_cast&) {
                throw;
            }
        }
        catch(...) {
            is_the_same = false;
        }
    }

    return is_the_same;
}

/**
 * SubTransitionCmd implementation
 */
const std::string SubTransitionCmd::SUB_TRANSITION_NAME_TAG = "sub_transition_name";
const std::string SubTransitionCmd::SUB_TRANSITION_MAIN_TR_TAG = "main_transition";
const std::string SubTransitionCmd::SUB_TRANSITION_ALL_SUBTR_TAG = "full_sub_list";

SubTransitionCmd::SubTransitionCmd(const std::string& commandDescription)
    : TransitionCmd(commandDescription)
{
    try {
        subTransition();
    }
    catch(daq::rc::Exception& ex) {
        throw daq::rc::BadCommand(ERS_HERE, RunControlCommands::MAKE_TRANSITION_CMD + " " + fsmCommand(),
                                  "the sub-transition is not defined", ex);
    }

    try {
        // Check the presence and validity of the main transition command
        FSMCommands::stringToCommand(mainTransitionCmd());
    }
    catch(daq::rc::Exception& ex) {
        throw daq::rc::BadCommand(ERS_HERE, RunControlCommands::MAKE_TRANSITION_CMD + " " + fsmCommand() + " " + subTransition(),
                                  "the main transition is not defined", ex);
    }
}

SubTransitionCmd::SubTransitionCmd(const std::string& subName, FSM_COMMAND mainTr)
    : TransitionCmd(FSM_COMMAND::SUB_TRANSITION)
{
    subTransition(subName);
    mainTransitionCmd(mainTr);
}

std::string SubTransitionCmd::subTransition() const {
    return parameter<std::string>(SubTransitionCmd::SUB_TRANSITION_NAME_TAG);
}

void SubTransitionCmd::subTransition(const std::string& subTr) {
    parameter<std::string>(SubTransitionCmd::SUB_TRANSITION_NAME_TAG, subTr);
}

std::string SubTransitionCmd::mainTransitionCmd() const {
    return parameter<std::string>(SubTransitionCmd::SUB_TRANSITION_MAIN_TR_TAG);
}

void SubTransitionCmd::mainTransitionCmd(FSM_COMMAND mainTrCmd) {
    parameter<std::string>(SubTransitionCmd::SUB_TRANSITION_MAIN_TR_TAG, FSMCommands::commandToString(mainTrCmd));
}

std::vector<std::string> SubTransitionCmd::subTransitions() const {
    return parameters<std::string>(TransitionCmd::FSM_SUBTR_TAG + "." + mainTransitionCmd());
}

std::map<FSM_COMMAND, std::vector<std::string>> SubTransitionCmd::allSubTransitions() {
    std::map<FSM_COMMAND, std::vector<std::string>> m;

    const auto& its = FSMCommands::commands();
    for(auto it = its.first; it != its.second; ++it) {
        auto&& sts = parameters<std::string>(SubTransitionCmd::SUB_TRANSITION_ALL_SUBTR_TAG + "." + FSMCommands::commandToString(it->first));
        if(sts.empty() == false) {
            m.insert(std::make_pair(it->first, std::move(sts)));
        }
    }

    return m;
}

void SubTransitionCmd::allSubTransitions(const std::map<FSM_COMMAND, std::vector<std::string>>& subTrMap) {
    for(const auto& p : subTrMap) {
        parameters<std::string>(SubTransitionCmd::SUB_TRANSITION_ALL_SUBTR_TAG + "." + FSMCommands::commandToString(p.first), p.second);
    }
}

std::unique_ptr<SubTransitionCmd> SubTransitionCmd::clone() const {
    return std::unique_ptr<SubTransitionCmd>(doClone());
}

SubTransitionCmd* SubTransitionCmd::doClone() const {
    return new SubTransitionCmd(*this);
}

std::string SubTransitionCmd::toString() const {
    return TransitionCmd::toString() + " " + subTransition();
}

std::vector<std::string> SubTransitionCmd::arguments() const {
    std::vector<std::string>&& args = TransitionCmd::arguments();
    args.push_back(subTransition());

    return std::move(args);
}

bool SubTransitionCmd::equals(const RunControlBasicCommand& other) const {
    bool is_the_same = TransitionCmd::equals(other);

    if(is_the_same == true) {
        try {
            try {
                const SubTransitionCmd& stCmd = dynamic_cast<const SubTransitionCmd&>(other);
                is_the_same = ((stCmd.subTransition() == subTransition()) && (stCmd.mainTransitionCmd() == mainTransitionCmd()));
            }
            catch(std::bad_cast&) {
                throw;
            }
        }
        catch(...) {
            is_the_same = false;
        }
    }

    return is_the_same;
}

}}
