/*
 * OnlineServices.cxx
 *
 *  Created on: Dec 7, 2012
 *      Author: avolio
 */

#include "RunControl/Common/OnlineServices.h"
#include "RunControl/Common/Exceptions.h"

#include <config/Configuration.h>
#include <config/Errors.h>
#include <config/map.h>
#include <ers/LocalContext.h>
#include <dal/Partition.h>
#include <dal/RunControlApplicationBase.h>
#include <dal/Segment.h>
#include <dal/util.h>
#include <system/Host.h>

#include <system_error>
#include <cstdlib>
#include <mutex>

namespace daq { namespace rc {

class OSDeleter {
    public:
        void operator()(OnlineServices* onl_svc) const {
            delete onl_svc;
        }
};

const std::string OnlineServices::TDAQ_APPLICATION_NAME = "TDAQ_APPLICATION_NAME";
const std::string OnlineServices::TDAQ_APPLICATION_OBJECT_ID = "TDAQ_APPLICATION_OBJECT_ID";

std::atomic<bool> OnlineServices::m_global_init_done(false);
std::once_flag OnlineServices::m_global_init_flag;
std::once_flag OnlineServices::m_config_init_flag;
std::once_flag OnlineServices::m_host_name_init_flag;

std::unique_ptr<OnlineServices, OSDeleter> OnlineServices::m_self(nullptr, OSDeleter());


OnlineServices::OnlineServices(const std::string& partName, const std::string& segName) :
        m_application_name((std::getenv(OnlineServices::TDAQ_APPLICATION_NAME.c_str()) != nullptr) ? std::getenv(OnlineServices::TDAQ_APPLICATION_NAME.c_str()) : ""),
        m_partition_name(partName), m_segment_name(segName), m_ipc_partition(partName), m_converter(0)
{
    if(m_application_name.empty() == true) {
        throw daq::rc::OnlineServicesFailure(ERS_HERE, std::string("The mandatory environment variable \"") + OnlineServices::TDAQ_APPLICATION_NAME + "\" is not set");
    }
}

OnlineServices::~OnlineServices() {
}

void OnlineServices::initialize(const std::string& partName,
                                const std::string& segName)
{
    // Here we do in such a way globalInit(...) is called only once
    std::call_once(OnlineServices::m_global_init_flag, OnlineServices::globalInit, partName, segName);
}

void OnlineServices::globalInit(const std::string& partName, const std::string& segName) {
    // Here we call the constructor and synchronize with instance()
    OnlineServices::m_self.reset(new OnlineServices(partName, segName));
    OnlineServices::m_global_init_done.store(true, std::memory_order_release);
}

void OnlineServices::configurationInit() {
    std::call_once(OnlineServices::m_config_init_flag, &OnlineServices::buildConfiguration, this);
}

void OnlineServices::buildConfiguration() {
    try {
        m_configuration.reset(new Configuration(""));

        const daq::core::Partition* p = daq::core::get_partition(*(m_configuration.get()), m_partition_name);
        if(p == nullptr) {
            throw daq::rc::ConfigurationIssue(ERS_HERE, std::string("the partition ") +
                                                        m_partition_name + " is not valid (got a null pointer)");
        }

        m_converter = new daq::core::SubstituteVariables(*p);
        m_configuration->register_converter(m_converter);
    }
    catch(daq::config::Exception& ex) {
        const std::string& errMsg = std::string("cannot build the partition configuration (") + ex.message() + ")";
        throw daq::rc::ConfigurationIssue(ERS_HERE, errMsg, ex);
    }
}

OnlineServices& OnlineServices::instance() {
    // Here we synchronize with globalInit(...)
    if(OnlineServices::m_global_init_done.load(std::memory_order_acquire) == false) {
        throw daq::rc::NotInitialized(ERS_HERE);
    }

    return *(OnlineServices::m_self.get());
}

const std::string& OnlineServices::applicationName() const {
    return m_application_name;
}

const std::string& OnlineServices::segmentName() const {
    return m_segment_name;
}

const IPCPartition& OnlineServices::getIPCPartition() const {
    return m_ipc_partition;
}

Configuration& OnlineServices::getConfiguration() {
    configurationInit();

    std::shared_lock<std::shared_mutex> lk(m_mutex);
    return *(m_configuration.get());
}

const daq::core::Partition& OnlineServices::getPartition() {
    configurationInit();

    std::shared_lock<std::shared_mutex> lk(m_mutex);

    const daq::core::Partition* p = nullptr;

    try {
        if((p = daq::core::get_partition(*(m_configuration.get()), m_partition_name)) == nullptr) {
            const std::string& errMsg = std::string("the partition \"") + m_partition_name + "\" does not exists or is invalid";
            throw daq::rc::ConfigurationIssue(ERS_HERE, errMsg);
        }
    }
    catch(daq::config::Exception& ex) {
        const std::string& errMsg = std::string("cannot retrieve the partition object from the database (") + ex.message() + ")";
        throw daq::rc::ConfigurationIssue(ERS_HERE, errMsg, ex);
    }

    return *p;
}

const daq::core::RunControlApplicationBase& OnlineServices::getApplication() {
    configurationInit();

    static const char* appObjectId = std::getenv(OnlineServices::TDAQ_APPLICATION_OBJECT_ID.c_str()) != nullptr
                                     ? std::getenv(OnlineServices::TDAQ_APPLICATION_OBJECT_ID.c_str())
                                     : m_application_name.c_str();

    std::shared_lock<std::shared_mutex> lk(m_mutex);

    const daq::core::RunControlApplicationBase* app = nullptr;

    try {
        if((app = m_configuration->get<daq::core::RunControlApplicationBase>(std::string(appObjectId))) == nullptr) {
            const std::string& errMsg = std::string("the application \"") + m_application_name + "\" is not described in the database or is invalid";
            throw daq::rc::ConfigurationIssue(ERS_HERE, errMsg);
        }
    }
    catch(daq::config::Exception& ex) {
        const std::string& errMsg = std::string("cannot retrieve the application object from the database (") + ex.message() + ")";
        throw daq::rc::ConfigurationIssue(ERS_HERE, errMsg, ex);
    }

    return *app;
}

const daq::core::Segment& OnlineServices::getSegment() {
    configurationInit();

    std::shared_lock<std::shared_mutex> lk(m_mutex);

    try {
        const daq::core::Partition* p = daq::core::get_partition(*(m_configuration.get()), m_partition_name);
        if(p != nullptr) {
            return *(p->get_segment(m_segment_name));
        } else {
            const std::string msg = "cannot retrieve the Segment object from the database because received a null pointer to the Partition object";
            throw daq::rc::ConfigurationIssue(ERS_HERE, msg);
        }
    }
    catch(daq::core::AlgorithmError& ex) {
        throw daq::rc::BadSegment(ERS_HERE, "cannot retrieve the Segment object from the database", m_segment_name, ex);
    }
    catch(daq::config::Exception& ex) {
        const std::string& errMsg = std::string("cannot retrieve the Segment object from the database (") + ex.message() + ")";
        throw daq::rc::ConfigurationIssue(ERS_HERE, errMsg, ex);
    }
}

void OnlineServices::reloadConfiguration(bool unreadObjects) {
    configurationInit();

    const daq::core::Partition& p = getPartition();

    {
        std::lock_guard<std::shared_mutex> lk(m_mutex);

        m_configuration->unread_all_objects(unreadObjects);
        m_converter->reset(p);
    }

    // Check that the segment is still valid
    getSegment();
}

const std::string& OnlineServices::getHostName() const {
    // Lazy instantiation of the full host name
    // Avoid to resolve the host name if not needed, it may just cause unwanted load on the DNS
    // Remember that System::LocalHost::full_local_name is not thread safe
    std::call_once(OnlineServices::m_host_name_init_flag, [this]() { m_host_name = System::LocalHost::full_local_name(); });

    return m_host_name;
}

}}
