/*
 * CommandNotifier.cxx
 *
 *  Created on: May 12, 2014
 *      Author: avolio
 */

#include "RunControl/Common/CommandNotifier.h"
#include "RunControl/Common/CommandNotifierAdapter.h"
#include "RunControl/Common/Exceptions.h"

#include <ers/LocalContext.h>

#include <functional>


namespace daq { namespace rc {

CommandNotifier::CommandNotifier()
    : m_deactivated(false), m_not_adapter(new CommandNotifierAdapter(std::bind(&CommandNotifier::commandExecuted, this, std::placeholders::_1)))
{
    // Passing "this" to the CommandNotifierAdapter technically means to let
    // "this" escape the constructor. This is usually a problem in multi-threaded
    // programs. It is not in this case because there is no way for the CommandNotifierAdapter
    // to execute the call-back function before the object is fully built.
    // Indeed the call-back can be called only after a reference of this class
    // is sent to the CommandSender.
}

CommandNotifier::~CommandNotifier() {
    std::lock_guard<std::mutex> lk(m_mutex);

    if(m_deactivated == false) {
        m_not_adapter->_destroy();
    }
}

std::string CommandNotifier::reference() const {
    std::lock_guard<std::mutex> lk(m_mutex);

    if(m_deactivated == false) {
        return m_not_adapter->reference();
    }

    throw daq::rc::OnlineServicesFailure(ERS_HERE, "The CORBA reference could not be built because the CORBA object is not active");
}

void CommandNotifier::deactivate() {
    std::lock_guard<std::mutex> lk(m_mutex);

    if(m_deactivated == false) {
        m_not_adapter->_destroy(true);
        m_deactivated = true;
    }
}

}}
