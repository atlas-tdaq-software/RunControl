#ifndef DAQ_RC_ITEMCTRL_PYTOCTOOLKIT_
#define DAQ_RC_ITEMCTRL_PYTOCTOOLKIT_

/**
 * @file RunControl/Python/RCPyToolkit.h
 */

// Include this first in order to avoid compilation
// warning on slc6 about "_XOPEN_SOURCE redefined"
#include <boost/scoped_ptr.hpp>

#include <string>
#include <memory>


namespace daq { namespace rc {

class Controllable;

class PyToolkit {
public:
    /**
    * Give access to singleton PyToolkit object
    */
    static PyToolkit& Instance();
    /**
    * Return Controllable object defined in Python script with name <modName>.py
    * described by class <className> name <params>
    * \param modName string containing the name of module without ".py"
    * \param className string contain the name of class from <modName>.py
    * \param name string contain indentifier of object will be created
    */
    std::unique_ptr<daq::rc::Controllable> createWrap(const std::string& modName,
                                                      const std::string& className);

    ~PyToolkit();

private:
    PyToolkit();

    static boost::scoped_ptr<PyToolkit> _inst;
};

}} // daq::rc

#endif
