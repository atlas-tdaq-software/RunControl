/*
 * ScheduledThreadPool.h
 *
 *  Created on: Apr 23, 2013
 *      Author: avolio
 */

/**
 * @file RunControl/Common/ScheduledThreadPool.h
 */

#ifndef DAQ_RC_SCHEDULED_THREAD_POOL_H_
#define DAQ_RC_SCHEDULED_THREAD_POOL_H_

#include "RunControl/Common/Exceptions.h"

#include <boost/thread/thread.hpp>
#include <boost/thread/condition_variable.hpp>
#include <boost/thread/exceptions.hpp>
#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/locks.hpp>
#include <boost/thread/shared_mutex.hpp>
#include <boost/thread/barrier.hpp>
#include <boost/chrono.hpp>
#include <boost/ratio.hpp>

#include <tbb/concurrent_priority_queue.h>

#include <memory>
#include <functional>
#include <atomic>
#include <string>
#include <vector>
#include <array>
#include <map>

// Completely inspired by java.util.concurrent.ScheduledThreadPoolExecutor

namespace daq { namespace rc {

namespace chr = boost::chrono;

/**
 * @brief Class representing a task that is regularly executed
 *
 * A ScheduledTask is designed to be used by the ScheduledThreadPool.
 */
class ScheduledTask {
    public:
        /**
         * @brief Typedef for nanoseconds
         */
        typedef chr::nanoseconds unit;

        /**
         * @brief Typedef for the function that is regularly executed: it returns a boolean and takes no arguments
         */
        typedef std::function<bool ()> Callback;

        /**
         * @brief "Less than" operator used to order different ScheduledTask instances
         */
        bool operator<(const ScheduledTask& rhs) const {
            return (m_end_delay < rhs.m_end_delay);
        }

        /**
         * @brief It returns a constant reference to the call-back function
         *
         * @return A constant reference to the call-back function
         */
        const Callback& callback() const {
            return m_cb;
        }

        /**
         * @brief It returns the amount of time between two executions of the call-back functions
         *
         * @return The amount of time between two executions of the call-back functions
         */
        const unit& period() const {
            return m_period;
        }

        /**
         * @brief It returns the amount of time to wait for the first execution of the call-back function
         *
         * @return The amount of time to wait for the first execution of the call-back function
         */
        const unit& initialDelay() const {
            return m_initial_delay;
        }

        /**
         * @brief It returns the identifier of this task
         *
         * @return The identifier of this task
         */
        const std::string& id() const {
            return m_id;
        }

    protected:
        typedef chr::high_resolution_clock clock;
        typedef clock::rep Representation;

        static_assert(clock::is_steady == true, "A steady clock is required");

        friend class ScheduledThreadPool;

        /**
         * @brief Constructor
         *
         * @tparam Period A boost::ratio representing the tick period (i.e., the number of seconds per tick)
         * @param cb The call-back function to be regularly executed
         * @param initialDelay The initial delay (after submission) before the task is executed the first time (in units of @em Period)
         * @param period The time between two executions of the call-back function
         * @param id The task's identifier (required to be unique)
         */
        template<typename Period>
        ScheduledTask(const Callback& cb, unsigned int initialDelay, unsigned int period, const std::string& id, const Period&)
            : m_cb(cb),
              m_initial_delay(chr::duration_cast<unit>(chr::duration<Representation, Period>(initialDelay))),
              m_period(chr::duration_cast<unit>(chr::duration<Representation, Period>(period))),
              m_end_delay(clock::now() + m_initial_delay),
              m_id(id)
        {
        }

        /**
         * @brief Copy constructor
         */
        ScheduledTask(const ScheduledTask& other)
            : m_cb(other.m_cb),
              m_initial_delay(other.m_initial_delay),
              m_period(other.m_period),
              m_end_delay(clock::now() + m_period),
              m_id(other.m_id)
        {
        }

        /**
         * @brief Move constructor
         */
        ScheduledTask(ScheduledTask&& other)
            : m_cb(std::move(other.m_cb)),
              m_initial_delay(other.m_initial_delay),
              m_period(other.m_period),
              m_end_delay(clock::now() + m_period),
              m_id(other.m_id)
        {
        }

        /**
         * @brief It returns @em true if the delay is elapsed and the call-back should be called
         *
         * @return @em true if the delay is elapsed and the call-back should be called
         */
        bool delayElapsed() const {
            return (clock::now() >= m_end_delay);
        }

        /**
         * @brief It returns the point in time when the call-back function should be called for the next time
         *
         * @return The point in time when the call-back function should be called for the next time
         */
        chr::time_point<clock, unit>
        delayTimePoint() const {
            return m_end_delay;
        }

        /**
         * @brief It executes the call-back function
         *
         * @return It forwards the return value of the call-back function. It return @em false when the call-back
         *         function throws an exception
         *
         * @throws boost::thread_interrupted It notifies that the thread has been interrupted
         */
        bool run() {
            try {
                return m_cb();
            }
            catch(boost::thread_interrupted&) {
                // Let the interruption go
                throw;
            }
            catch(std::exception& ex) {
                ers::error(daq::rc::ThreadPoolError(ERS_HERE, ex.what(), ex));
                return false;
            }
        }

    private:
        Callback m_cb;
        const unit m_initial_delay;
        const unit m_period;
        const chr::time_point<clock, unit> m_end_delay;
        const std::string m_id;
};

/**
 * @brief A thread pool that can be used to regularly execute tasks
 *
 * Tasks are executed with a fixed delay between the termination of one execution and the commencement of the next one.
 *
 * @note The regular execution of tasks is stopped when the registered call-back function returns @em false
 * @note Tasks are interrupted (for instance when the thread pool is destroyed) using the boost interruption mechanism
 *       (http://www.boost.org/doc/libs/1_53_0/doc/html/thread/thread_management.html#interruption_points). Users can check
 *       the interruption status using the boost::this_thread::interruption_point() function and propagating the eventual
 *       boost::thread_interrupted exception.
 */
class ScheduledThreadPool {
    public:
        /**
         * @brief Typedef for the call-back function to be regularly executed
         */
        typedef std::function<bool ()> Callback;

        /**
         * @brief Constructor
         *
         * @param size The number of threads in the pool
         * @throws daq::rc::ThreadPoolError Some thread could not be started
         */
        explicit ScheduledThreadPool(unsigned int size)
            : m_terminating(false), m_paused(false), m_waiting_pause(false),
              m_barrier(size + 1), m_queue(), m_pushed_items(0), m_threads()
        {
            try {
                for(unsigned int i = 0; i < size; ++i) {
                    m_threads.create_thread(std::bind(&ScheduledThreadPool::consumeTask, this));
                }
            }
            catch(boost::thread_exception& ex) {
                throw daq::rc::ThreadPoolError(ERS_HERE,
                                               std::string("Some thread in the pool cannot be started: ") + ex.what(),
                                               ex);
            }
        }

        /**
         * @brief Copy constructor: deleted
         */
        ScheduledThreadPool(const ScheduledThreadPool&) = delete;

        /**
         * @brief Move constructor: deleted
         */
        ScheduledThreadPool(ScheduledThreadPool&&) = delete;

        /**
         * @brief Copy assignment operator: deleted
         */
        ScheduledThreadPool& operator=(const ScheduledThreadPool&) = delete;

        /**
         * @brief Move assignment operator: deleted
         */
        ScheduledThreadPool& operator=(ScheduledThreadPool&&) = delete;

        /**
         * @brief Destructor
         */
        ~ScheduledThreadPool() {
            m_terminating = true;
            m_threads.interrupt_all();
            m_threads.join_all();
        }

        /**
         * @brief It submits a task to the thread pool
         *
         * @tparam Period A boost::ratio representing the tick period (i.e., the number of seconds per tick). Default is one second
         * @param task The task to be regularly executed
         * @param id An unique identifier for the task
         * @param initialDelay The initial delay (after the submission) to wait for the first execution of the task
         * @param delay The time between the termination of one execution and the commencement of the next one
         */
        template<typename Period = boost::ratio<1, 1>>
        void submit(const Callback& task, const std::string& id, unsigned int initialDelay, unsigned int delay) {
            std::shared_ptr<ScheduledTask> st(new ScheduledTask(task, initialDelay, delay, id, Period()));
            m_queue.push(st);

            {
                boost::lock_guard<boost::shared_mutex> lk(m_queue_mutex);
                ++m_pushed_items;
            }

            m_queue_cond.notify_one();
        }

        /**
         * @brief It pauses the execution of all the tasks
         *
         * Tasks are interrupted
         *
         * @param wait If @em true, then this method returns only after the tasks are actually stopped
         */
        void pauseExecution(bool wait) {
            // This mutex is needed in order to synchronize the pause and resume
            boost::lock_guard<boost::recursive_mutex> lk_u(m_pause_resume_mutex);

            {
                // ...and this one is needed to properly access 'm_paused' and 'm_waiting_pause'
                boost::upgrade_lock<boost::upgrade_mutex> lk_up(m_pause_mutex);
                if(m_paused == true) {
                    return;
                 }

                boost::upgrade_to_unique_lock<boost::upgrade_mutex> lk_utu(lk_up);

                m_paused = true;
                m_waiting_pause = wait;

                m_threads.interrupt_all();
            }

            // Mandatory for this to be out of the lock on 'm_pause_mutex'
            // This does not allow to invoke a resume if the pause is not yet completed
            if(wait == true) {
                m_barrier.wait();
            }
        }

        /**
         * @brief It resumes the execution of all the tasks
         */
        void resumeExecution() {
            boost::lock_guard<boost::recursive_mutex> lk_u(m_pause_resume_mutex);

            {
                boost::upgrade_lock<boost::upgrade_mutex> lk_up(m_pause_mutex);
                if(m_paused == false) {
                    return;
                 }

                boost::upgrade_to_unique_lock<boost::upgrade_mutex> lk_utu(lk_up);

                m_paused = false;
                m_waiting_pause = false;
            }

            m_pause_resume_cond.notify_all();
        }

        /**
         * @brief It cancels the execution of all the tasks
         *
         * Running tasks are interrupted, while queued tasks are never executed.
         *
         * @return All the tasks that were submitted
         */
        std::vector<std::shared_ptr<ScheduledTask>>
        cancelExecution() {
            boost::lock_guard<boost::recursive_mutex> lk_u(m_pause_resume_mutex);

            bool wasPaused = false;
            {
                boost::shared_lock<boost::upgrade_mutex> lk_s(m_pause_mutex);
                wasPaused = m_paused;
            }

            pauseExecution(true);

            // Clean the queue
            std::vector<std::shared_ptr<ScheduledTask>> tasks;
            std::shared_ptr<ScheduledTask> st;
            while(m_queue.try_pop(st) == true) {
                tasks.push_back(st);
            }

            if(wasPaused == false) {
                resumeExecution();
            }

            return tasks;
        }

        /**
         * @brief It reschedules the execution of tasks
         *
         * @tparam Period A boost::ratio representing the tick period (i.e., the number of seconds per tick). Default is one second
         * @param newDelays Map from task's identifier to new delays (first element of the array is the initial delay, second element is the delay)
         */
        template<typename Period = boost::ratio<1, 1>>
        void rescheduleExecution(const std::map<std::string, std::array<unsigned int, 2>>& newDelays) {
            boost::lock_guard<boost::recursive_mutex> lk(m_pause_resume_mutex);

            const auto& tasks = cancelExecution();

            for(const auto& t : tasks) {
                const auto& it = newDelays.find(t->id());
                if(it != newDelays.end()) {
                    submit<Period>(t->callback(),
                                   t->id(),
                                   (it->second)[0],
                                   (it->second)[1]);
                } else {
                    submit(t);
                }
            }

        }

    protected:
        /**
         * @brief It submits a task to the thread pool
         *
         * @param task The task to be submitted
         */
        void submit(const std::shared_ptr<ScheduledTask>& task) {
            m_queue.push(task);

            {
                boost::lock_guard<boost::shared_mutex> lk(m_queue_mutex);
                ++m_pushed_items;
            }

            m_queue_cond.notify_one();
        }

        /**
         * @brief It re-queues a task after its execution
         *
         * @param task The task to be re-queued
         */
        void re_queue(const std::shared_ptr<ScheduledTask>& task) {
            // If the task has to be re-queued, then do not notify the condition
            // variable otherwise threads will start to oscillate
            // The notification is not needed because the same thread that performs
            // the re-queuing will then ask the queue about the next task to execute
            m_queue.push(task);
        }

        /**
         * @brief Main loop
         */
        void consumeTask() {
            while(m_terminating == false) {
                std::shared_ptr<ScheduledTask> st;
                bool re_submit = true;
                try {
                    boost::this_thread::interruption_point();

                    // Keep track of the number of pushed items in the queue so far
                    // If the queue is empty it will help in knowing the right moment to
                    // look at the queue again
                    boost::shared_lock<boost::shared_mutex> lk_1(m_queue_mutex);
                    unsigned long num = m_pushed_items;
                    lk_1.unlock();

                    bool execute = false;
                    const bool popped = m_queue.try_pop(st);
                    if(popped == true) {
                        if(st->delayElapsed() == false) {
                            // It is still to early to execute the task
                            // Two cases may happen here:
                            // - Something new has been added to the queue
                            // - Or we just wait for the right time to come (unless a new item is added to the queue)
                            boost::shared_lock<boost::shared_mutex> lk_2(m_queue_mutex);
                            while(num == m_pushed_items) {
                                const boost::cv_status waitStatus = m_queue_cond.wait_until(lk_2, st->delayTimePoint());
                                if(waitStatus == boost::cv_status::timeout) {
                                    // The condition variable has expired, it is time to execute the task
                                    execute = true;
                                    break;
                                }

                                // If we are here, then the condition variable was signaled
                            }
                        } else {
                            // It is already the right time to execute the task
                            execute = true;
                        }

                        if(execute == true) {
                            // Run the task and then submit it again (the delay will be updated)
                            if((re_submit = st->run()) == true) {
                                re_queue(std::shared_ptr<ScheduledTask>(new ScheduledTask(std::move(*st))));
                            }
                        } else {
                            // The task has not been executed, reschedule it as it is
                            re_queue(st);
                        }
                    } else {
                        // The queue was empty, wait for something to be added to it
                        boost::shared_lock<boost::shared_mutex> lk_3(m_queue_mutex);
                        while(num == m_pushed_items) {
                            m_queue_cond.wait(lk_3);
                        }
                    }
                }
                catch(boost::thread_interrupted&) {
                    // We can be here in two cases: termination or pause
                    // If we are terminating, then do nothing, the loop will stop at the next iteration
                    if((re_submit == true) && (m_terminating == false)) {
                        // Here it means that we are pausing
                        if(st.get() != nullptr) {
                            // Reschedule the task if it was popped from the queue
                            re_queue(st);
                        }

                        // This mutex has to be locked in order to not break the consistency with pausing and resuming
                        // Not locking this mutex would allow calls to 'pause' to be mixed with calls to 'resume'
                        // with dead-locks very easy to appear
                        boost::shared_lock<boost::upgrade_mutex> lk(m_pause_mutex);
                        if(m_waiting_pause == true) {
                            // The 'pause' caller is waiting on the barrier, we do the same
                            m_barrier.wait();
                         }

                        // We are paused, now wait for a signal to be resumed
                        while((m_paused == true) && (m_terminating == false)) {
                            try {
                                m_pause_resume_cond.wait(lk);
                            }
                            catch(boost::thread_interrupted&) {
                                // With a fast resume/pause, it may be that the interrupt is received before
                                // the signal on the condition variable. That's why we check here.
                                if((m_terminating == false) && (m_waiting_pause == true)) {
                                    m_barrier.wait();
                                }
                            }
                        }
                    }
                }
            }
        }

    private:
        struct Comparator {
            bool operator()(const std::shared_ptr<ScheduledTask>& lhs, const std::shared_ptr<ScheduledTask>& rhs) const {
                // The operator is defined in this way so that the tasks to be executed later will have
                // lower priority with respect to the one to be executed sooner.
                return ((*rhs) < (*lhs));
            }
        };

        std::atomic<bool> m_terminating;
        bool m_paused;
        bool m_waiting_pause;
        boost::recursive_mutex m_pause_resume_mutex;
        boost::upgrade_mutex m_pause_mutex;
        boost::condition_variable_any m_pause_resume_cond;
        boost::barrier m_barrier;
        tbb::concurrent_priority_queue<std::shared_ptr<ScheduledTask>, ScheduledThreadPool::Comparator> m_queue;
        boost::shared_mutex m_queue_mutex;
        boost::condition_variable_any m_queue_cond;
        unsigned long m_pushed_items;
        boost::thread_group m_threads;
};

}}

#endif /* DAQ_RC_SCHEDULED_THREAD_POOL_H_ */
