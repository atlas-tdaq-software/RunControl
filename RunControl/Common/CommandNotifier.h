/*
 * CommadNotifier.h
 *
 *  Created on: May 12, 2014
 *      Author: avolio
 */

/**
 * @file RunControl/Common/CommandNotifier.h
 */

#ifndef DAQ_RC_COMMANDNOTIFIER_H_
#define DAQ_RC_COMMANDNOTIFIER_H_

#include <string>
#include <mutex>


namespace daq { namespace rc {

class CommandNotifierAdapter;
class RunControlBasicCommand;

/**
 * @brief Using this class it is possible to receive notifications from Run Control applications
 *        as soon as a command has been executed.
 *
 * The execution of a command is notified via the execution of the CommandNotifier::commandExecuted
 * methods that has to be properly implemented by derived classes.
 *
 * @remarks Of course an instance of this class must have such a lifetime that it can be properly
 *          reached when the command is done. In case it is needed to destroy this object while
 *          some call-backs are still pending, the daq::rc::CommandNotifier::deactivate method
 *          @b must be called in order to avoid call-back processing to be interleaved with the
 *          object destruction.
 *
 * @see daq::rc::CommandSender::sendCommand(const std::string& recipient, RunControlBasicCommand& cmd, const CommandNotifier& cmdNot) const
 */
class CommandNotifier {
    public:
        /**
         * @brief Constructor
         */
        CommandNotifier();

        /**
         * @brief Destructor
         */
        virtual ~CommandNotifier();

        /**
         * @brief Copy constructor: deleted
         */
        CommandNotifier(const CommandNotifier&) = delete;

        /**
         * @brief Move constructor: deleted
         */
        CommandNotifier(CommandNotifier&&) = delete;

        /**
         * @brief Copy assignment operator: deleted
         */
        CommandNotifier& operator=(const CommandNotifier&) = delete;

        /**
         * @brief Move assignment operator: deleted
         */
        CommandNotifier& operator=(CommandNotifier&&) = delete;

        /**
         * @brief It returns the CORBA reference to be used in order to receive
         *        the remote notification
         *
         * @return The CORBA reference to be used in order to receive the remote notification
         *
         * @throws daq::rc::OnlineServicesFailure Some error occurred building the CORBA reference string
         */
        std::string reference() const;

        /**
         * @brief It deactivates this object
         *
         * After this method is called this object will not be able to accept any call-back and
         * cannot be used anymore for that purpose.
         *
         * @note It is needed to call this method when the object has to be destroyed but
         *       some call-back has still to come.
         */
        void deactivate();

    protected:
        /**
         * @brief Called when a command has been executed
         *
         * @warning Note that in some circumstances (e.g., @em cmd represents a transition
         *          not allowed given the current FSM state) the command object may not contain
         *          all the attributes that it usually has when accessed through the daq::rc::Controllable
         *          and daq::rc::UserRoutines interfaces. Please, look at the command object's documentation
         *          for additional information.
         *
         * @param cmd The command that has been executed
         */
        virtual void commandExecuted(const RunControlBasicCommand& cmd) noexcept = 0;

    private:
        mutable std::mutex m_mutex;
        bool m_deactivated;
        CommandNotifierAdapter* m_not_adapter;
};

}}

#endif /* DAQ_RC_COMMANDNOTIFIER_H_ */
