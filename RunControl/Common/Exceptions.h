/*
 * Exceptions.h
 *
 *  Created on: Aug 24, 2012
 *      Author: avolio
 */

/**
 * @file RunControl/Common/Exceptions.h
 */

#ifndef DAQ_RC_EXCEPTIONS_H_
#define DAQ_RC_EXCEPTIONS_H_

#include <ers/ers.h>

#include <string>

namespace daq {
    /**
     * @class daq::rc::Exception
     * @brief Base class for all the Run Control exceptions
     */
    ERS_DECLARE_ISSUE(rc, Exception, ERS_EMPTY, ERS_EMPTY)

    /**
     * @class daq::rc::BackupHostsDisabled
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     BackupHostsDisabled,
     rc::Exception,
     "Backup hosts are defined for application \"" << appName << "\" but all of them are disabled",
     ERS_EMPTY,
     ((std::string) appName)        // Name of the application
    )

    /**
     * @class daq::rc::CmdLineError
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     CmdLineError,
     rc::Exception,
     "Error parsing the command line options: " << reason,
     ERS_EMPTY,
     ((std::string) reason)
    )

    /**
     * @class daq::rc::CmdLineHelp
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     CmdLineHelp,
     rc::Exception,
     ERS_EMPTY,
     ERS_EMPTY,
     ERS_EMPTY
    )

    /**
     * @class daq::rc::PMGError
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     PMGError,
     rc::Exception,
     "The PMG server running on host \"" << host_name << "\" could not be properly contacted: " << message,
     ERS_EMPTY,
     ((std::string) host_name)   // Host where the PMG is running on
     ((std::string) message)     // Failure reason
     ((bool) pmg_exists)
    )

    /**
     * @class daq::rc::Busy
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     Busy,
     rc::Exception,
     "Command \"" << command << "\" cannot not be executed because the application is busy",
     ERS_EMPTY,
     ((std::string) command)   // Command name
    )

    /**
     * @class daq::rc::InErrorState
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     InErrorState,
     rc::Exception,
     "Command \"" << command << "\" cannot not be executed because the application is in an error state",
     ERS_EMPTY,
     ((std::string) command)   // Command name
    )

    /**
     * @class daq::rc::NotAllowed
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     NotAllowed,
     rc::Exception,
     "Command \"" << command << " cannot be executed because is not allowed. Reason: " << reason,
     ERS_EMPTY,
     ((std::string) command)   // Command name
     ((std::string) reason)
    )

    /**
     * @class daq::rc::ExecutionFailed
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     ExecutionFailed,
     rc::Exception,
     "The execution of command \"" << command << "\" failed" << (reason.empty() == false ? std::string(". Reason: ") + reason : ""),
     ERS_EMPTY,
     ((std::string) command)   // Command name
     ((std::string) reason)
    )

    //
    // Issues related to the FSM
    //

    /**
     * @class daq::rc::FSMBusy
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     FSMBusy,
     rc::Exception,
     "Transition \"" << name << "\" cannot be performed because the FSM is already busy with a different transition",
     ERS_EMPTY,
     ((std::string) name)        // Name of the transition (usually the name of the command)
    )

    /**
     * @class daq::rc::TransitionFailed
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     TransitionFailed,
     rc::Exception,
     "The transition \"" << name << "\" has not been properly completed" << (reason.empty() == false ? std::string(". Reason: ") + reason : ""),
     ERS_EMPTY,
     ((std::string) name)        // Name of the transition (usually the name of the command)
     ((std::string) reason)
    )

    /**
     * @class daq::rc::UserRoutineFailed
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     UserRoutineFailed,
     rc::Exception,
     "Failed to execute the user action \"" << transitionName << "\". Reason: " << reason,
     ERS_EMPTY,
     ((std::string) transitionName)
     ((std::string) reason)
    )

    /**
     * @class daq::rc::TransitionNotAllowed
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     TransitionNotAllowed,
     rc::Exception,
     "The FSM does not allow the execution of transition \"" << name << "\". Reason: " << reason,
     ERS_EMPTY,
     ((std::string) name)        // Name of the transition
     ((std::string) reason)
    )

    /**
     * @class daq::rc::EventNotProcessed
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     EventNotProcessed,
     rc::Exception,
     "The FSM cannot handle the event corresponding to transition \"" << transitionName << "\": the command is not valid given the current FSM state",
     ERS_EMPTY,
     ((std::string) transitionName)        // Name of the transition
    )

    /**
     * @class daq::rc::InvalidState
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     InvalidState,
     rc::Exception,
     "The state \"" << state << "\" is not an FSM state",
     ERS_EMPTY,
     ((std::string) state)        // Name of the state
    )

    /**
     * @class daq::rc::TransitionInterrupted
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     TransitionInterrupted,
     rc::Exception,
     "Transition \"" << name << "\" has been interrupted",
     ERS_EMPTY,
     ((std::string) name)        // Name of the transition (usually the name of the command)
    )

    /**
     * @class daq::rc::TransitionPropagationFailure
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     TransitionPropagationFailure,
     rc::Exception,
     "The transition \"" << transition << "\" could not be propagated to application \"" << application << "\"",
     ERS_EMPTY,
     ((std::string) transition)
     ((std::string) application)
    )

    /**
     * @class daq::rc::TransitionTimeout
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     TransitionTimeout,
     rc::Exception,
     "Timeout elapsed for transition \"" << name << "\"",
     ERS_EMPTY,
     ((std::string) name)        // Name of the transition (usually the name of the command)
    )

    /**
     * @class daq::rc::FSMPathError
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     FSMPathError,
     rc::Exception,
     "Cannot find the path between states " << src << " and " << dst << ": " << reason,
     ERS_EMPTY,
     ((std::string) src)
     ((std::string) dst)
     ((std::string) reason)
    )

    //
    // Specific exception to notify a problem with the reload of the database
    //
    /**
     * @class daq::rc::DBReloadFailure
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     DBReloadFailure,
     rc::Exception,
     "The reload of the database could not be successfully completed. Reason: " << reason,
     ERS_EMPTY,
     ((std::string) reason)
    )

    //
    // Issues linked to problems with the controller initialization
    //

    /**
     * @class daq::rc::ControllerInitializationFailed
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     ControllerInitializationFailed,
     rc::Exception,
     "The controller cannot be initialized. Reason: " << reason,
     ERS_EMPTY,
     ((std::string) reason)    // Failure reason.
    )

    /**
     * @class daq::rc::ControllerCannotStart
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     ControllerCannotStart,
     rc::Exception,
     "The controller failed to start. Reason: " << reason,
     ERS_EMPTY,
     ((std::string) reason)    // Failure reason.
    )

    //
    // Issues with tests
    //
    /**
     * @class daq::rc::TestIssue
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     TestIssue,
     rc::Exception,
     "Problems occurred while trying to test application \"" << appName << "\": " << reason,
     ERS_EMPTY,
     ((std::string) appName)
     ((std::string) reason)
    )

    //
    // Issues with online services (i.e., config, IS, IPC)
    //

    /**
     * @class daq::rc::NotInitialized
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     NotInitialized,
     rc::Exception,
     "The service cannot be used because it has not been initialized yet",
     ERS_EMPTY,
     ERS_EMPTY
    )

    /**
     * @class daq::rc::OnlineServicesFailure
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     OnlineServicesFailure,
     rc::Exception,
     ERS_EMPTY,
     ERS_EMPTY,
     ERS_EMPTY
    )

    /**
     * @class daq::rc::IPCLookup
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     IPCLookup,
     rc::OnlineServicesFailure,
     "Failed to lookup application \"" << server << "\" in IPC",
     ERS_EMPTY,
     ((std::string) server)
    )

    /**
     * @class daq::rc::CorbaException
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     CorbaException,
     rc::OnlineServicesFailure,
     "Received CORBA exception \"" << excp << "\" when interacting with " << src,
     ERS_EMPTY,
     ((std::string) excp)
     ((std::string) src)
    )

    /**
     * @class daq::rc::ConfigurationIssue
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     ConfigurationIssue,
     rc::OnlineServicesFailure,
     "Problems accessing the online system configuration. Reason: " << reason,
     ERS_EMPTY,
     ((std::string) reason)
    )

    /**
     * @class daq::rc::BadSegment
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     BadSegment,
     rc::ConfigurationIssue,
     "The segment \"" << segment << "\" is disabled or does not exist - ",
     ((std::string) reason),
     ((std::string) segment)
    )

    /**
     * @class daq::rc::BadSubtransition
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     BadSubtransition,
     rc::ConfigurationIssue,
     "Issues getting the sub-transitions from the database for the main transition \"" << main_transition << "\" - ",
     ((std::string) reason),
     ((std::string) main_transition)
    )

    //
    // Process management issues
    //

    /**
     * @class daq::rc::ApplicationGenericIssue
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     ApplicationGenericIssue,
     rc::Exception,
     "Issue with application \"" << app_name << "\": " << message,
     ERS_EMPTY,
     ((std::string) app_name)   // Application name
     ((std::string) message)    // Failure reason.
    )

    /**
     * @class daq::rc::ApplicationPingFailed
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     ApplicationPingFailed,
     rc::Exception,
     "Application \"" << app_name << "\" is not responding to control signal",
     ERS_EMPTY,
     ((std::string) app_name) // Application name
    )

    /**
     * @class daq::rc::ApplicationRestartingFailure
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     ApplicationRestartingFailure,
     rc::Exception,
     "Application \"" << app_name << "\" cannot be re-started. Reason: " << message,
     ERS_EMPTY,
     ((std::string) app_name)   // Application name.
     ((std::string) message)    // Failure reason.
    )

    /**
     * @class daq::rc::ApplicationStartingFailure
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     ApplicationStartingFailure,
     rc::Exception,
     "Application \"" << app_name << "\" cannot be started. Reason: " << message,
     ERS_EMPTY,
     ((std::string) app_name)   // Application name.
     ((std::string) message)    // Failure reason.
    )

    /**
     * @class daq::rc::ApplicationStopFailure
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     ApplicationStopFailure,
     rc::Exception,
     "Application \"" << app_name << "\" cannot not be stopped. Reason: " << message,
     ERS_EMPTY,
     ((std::string) app_name)   // Application name.
     ((std::string) message)    // Failure reason.
    )

    /**
     * @class daq::rc::ApplicationLookupFailure
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     ApplicationLookupFailure,
     rc::Exception,
     "Unable to lookup application \"" << app_name << "\". Reason: " << message,
     ERS_EMPTY,
     ((std::string) app_name)   // Application name.
     ((std::string) message)    // Failure reason.
    )

    /**
     * @class daq::rc::ApplicationReconnectionFailure
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     ApplicationReconnectionFailure,
     rc::Exception,
     "Failed trying to find and eventually reconnect to an already running instance of application \"" << app_name << "\". Reason: " << message,
     ERS_EMPTY,
     ((std::string) app_name)   // Application name.
     ((std::string) message)    // Failure reason.
    )

    /**
     * @class daq::rc::LeftoverKillingFailure
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     LeftoverKillingFailure,
     rc::Exception,
     "Unable to kill left-over instances of application \"" << app_name << "\". Reason: " << message,
     ERS_EMPTY,
     ((std::string) app_name)   // Application name.
     ((std::string) message)    // Failure reason.
    )

    /**
     * @class daq::rc::DependenciesFailure
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     DependenciesFailure,
     rc::Exception,
     "Dependencies not fulfilled trying to " << ((isStart == true) ? "start " : "stop ") << "application \"" << app_name << "\": " << message,
     ERS_EMPTY,
     ((bool) isStart)           // Whether the failure is related to the start or stop of the application
     ((std::string) app_name)   // Application name.
     ((std::string) message)    // Failure reason.
    )

    /**
     * @class daq::rc::DependencyCycle
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     DependencyCycle,
     rc::Exception,
     "A cycle in dependencies has been detected trying to " << ((isStart == true) ? "start" : "stop") << " the following applications: " << apps,
     ERS_EMPTY,
     ((bool) isStart)           // Whether the failure is related to the start or stop of the application
     ((std::string) apps)       // Applications
    )

    /**
     * @class daq::rc::ApplicationLaunchFailure
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     ApplicationLaunchFailure,
     rc::Exception,
     "The application \"" << app_name << "\" cannot not be launched. Reason: " << message,
     ERS_EMPTY,
     ((std::string) app_name)   // Application name.
     ((std::string) message)    // Failure reason.
    )

    /**
     * @class daq::rc::ApplicationSignaled
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     ApplicationSignaled,
     rc::Exception,
     "Application \"" << application << "\" on host \"" << hostName << "\" died" << (onExit ? " while exiting " : " ") << "on signal " << signal << ". Logs are \"" << outStream << "/err\".",
     ERS_EMPTY,
     ((std::string)application)
     ((std::string)hostName)
     ((std::string)outStream)
     ((std::string)errStream)
     ((long)signal)
     ((bool)onExit)
    )

    /**
     * @class daq::rc::ApplicationExited
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     ApplicationExited,
     rc::Exception,
     "Application \"" << application << "\" on host \"" << hostName << "\" exited with exit code \"" << exitCode << "\". Logs are \"" << outStream << "/err\".",
     ERS_EMPTY,
     ((std::string)application)
     ((std::string)hostName)
     ((std::string)outStream)
     ((std::string)errStream)
     ((long)exitCode)
    )

    /**
     * @class daq::rc::BadPMGCallback
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     BadPMGCallback,
     rc::Exception,
     "Received a bad call-back from the PMG for application \"" << app_name << "\" with state \"" << app_state << "\"",
     ERS_EMPTY,
     ((std::string) app_name)   // Application name.
     ((std::string) app_state)    // Failure reason.
    )

    /**
     * @class daq::rc::PMGSystemError
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     PMGSystemError,
     rc::Exception,
     "The PMG system cannot be initialized",
     ERS_EMPTY,
     ERS_EMPTY
    )

    /**
     * @class daq::rc::HostDisabled
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     HostDisabled,
     rc::Exception,
     "The host \"" << host_name << "\" is disabled in the database",
     ERS_EMPTY,
     ((std::string) host_name)
    )

    /**
     * @class daq::rc::InvalidStatus
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     InvalidStatus,
     rc::Exception,
     "The application status \"" << status << "\" is not valid",
     ERS_EMPTY,
     ((std::string) status)        // Name of the status
    )

    /**
     * @class daq::rc::BadChildren
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     BadChildren,
     rc::Exception,
     ERS_EMPTY,
     ERS_EMPTY,
     ERS_EMPTY
    )

    /**
     * @class daq::rc::UnknownChild
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     UnknownChild,
     rc::Exception,
     "The application \"" << app_name << "\" is not in the list of known children and no information about it can be updated or retrieved",
     ERS_EMPTY,
     ((std::string) app_name)
    )

    //
    // Issues related to commands
    //
    /**
     * @class daq::rc::BadCommand
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     BadCommand,
     rc::Exception,
     "Reason: " << message,
     ERS_EMPTY,
     ((std::string) cmd_name)
     ((std::string) message)
    )

    /**
     * @class daq::rc::CommandParameterNotFound
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     CommandParameterNotFound,
     rc::Exception,
     "The parameter \"" << par_name << "\" is not a property of command \"" << cmd_name << "\"",
     ERS_EMPTY,
     ((std::string) par_name)
     ((std::string) cmd_name)
    )

    /**
     * @class daq::rc::CommandParameterInvalid
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     CommandParameterInvalid,
     rc::Exception,
     "The parameter \"" << par_name << "\" cannot be " << ((is_get == true) ? "retrieved" : "set")  << " for command \"" << cmd_name << "\". Reason: " << reason,
     ERS_EMPTY,
     ((std::string) par_name)
     ((std::string) cmd_name)
     ((std::string) reason)
     ((bool) is_get)
    )

    //
    // Run parameter issues
    //
    /**
     * @class daq::rc::RunNumber
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     RunNumber,
     rc::Exception,
     reason,
     ERS_EMPTY,
     ((std::string) reason)
    )

    /**
     * @class daq::rc::RunParams
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     RunParams,
     rc::Exception,
     reason,
     ERS_EMPTY,
     ((std::string) reason)
    )

    //
    // Master Trigger
    //
    /**
     * @class daq::rc::MasterTriggerNotDefined
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     MasterTriggerNotDefined,
     rc::Exception,
     "Master Trigger not defined",
     ERS_EMPTY,
     ERS_EMPTY
    )

    //
    // On-going transition issue
    //
    /**
     * @class daq::rc::OngoingTransition
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     OngoingTransition,
     rc::Exception,
     "Transition " << transition << " is on-going",
     ERS_EMPTY,
     ((std::string) transition)
    )

    //
    // Transition completed issue
    //
    /**
     * @class daq::rc::CompletedTransition
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     CompletedTransition,
     rc::Exception,
     "Transition " << transition << " has been completed",
     ERS_EMPTY,
     ((std::string) transition)
    )

    //
    // Start of run issue
    //
    /**
     * @class daq::rc::StartOfRun
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     StartOfRun,
     rc::Exception,
     message,
     ERS_EMPTY,
     ((std::string) message)
    )

    //
    // End of run issue
    //
    /**
     * @class daq::rc::EndOfRun
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     EndOfRun,
     rc::Exception,
     message,
     ERS_EMPTY,
     ((std::string) message)
    )

    //
    // ItemCtrl issue
    //
    /**
     * @class daq::rc::ItemCtrlInitializationFailed
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     ItemCtrlInitializationFailed,
     rc::Exception,
     "The run control framework could not be properly initialized: " << reason,
     ERS_EMPTY,
     ((std::string) reason)
    )

    //
    // Issue with the "child update"
    //
    /**
     * @class daq::rc::ParentUpdateFailure
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     ParentUpdateFailure,
     rc::Exception,
     "Failed to notify the parent controller \"" << controller_name << "\" about changes in my status",
     ERS_EMPTY,
     ((std::string) controller_name)
    )

    /**
     * @class daq::rc::ChildUpdateFailure
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     ChildUpdateFailure,
     rc::Exception,
     "Failed to update the information about the child process \"" << child_name << "\"",
     ERS_EMPTY,
     ((std::string) child_name)
    )

    //
    // Thread pool related issues
    //

    /**
     * @class daq::rc::ThreadPoolPaused
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     ThreadPoolPaused,
     rc::Exception,
     "The thread pool has been paused",
     ERS_EMPTY,
     ERS_EMPTY
    )

    /**
     * @class daq::rc::ThreadPoolError
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     ThreadPoolError,
     rc::Exception,
     "The thread pool could not be created",
     ERS_EMPTY,
     ERS_EMPTY
    )

    /**
     * @class daq::rc::TimeoutException
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     TimeoutException,
     rc::Exception,
     "The task could not be completed within the defined timeout",
     ERS_EMPTY,
     ERS_EMPTY
    )

    /**
     * @class daq::rc::CancellationException
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     CancellationException,
     rc::Exception,
     "The task has been cancelled",
     ERS_EMPTY,
     ERS_EMPTY
    )

    /**
     * @class daq::rc::InterruptedException
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     InterruptedException,
     rc::Exception,
     "The thread has been interrupted",
     ERS_EMPTY,
     ERS_EMPTY
    )

    //
    // Exceptions for sender notifications
    //
    /**
     * @class daq::rc::CannotNotifyCommandSender
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     CannotNotifyCommandSender,
     rc::Exception,
     "Cannot notify command sender (CORBA reference is \"" << reference << "\") about execution of command \"" << cmd << "\": " << reason,
     ERS_EMPTY,
     ((std::string) reference)
     ((std::string) cmd)
     ((std::string) reason)
    )

    //
    // Exceptions from the Python layer
    //
    /**
     * @class daq::rc::PythonInnerException
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     PythonInnerException,
     rc::Exception,
     "Exception while executing the python code; here is the traceback: \r\n" << traceback ,
     ERS_EMPTY,
     ((const char*) traceback)
    )

    /**
     * @class daq::rc::PythonModuleLoadException
     */
    ERS_DECLARE_ISSUE_BASE
    (rc,
     PythonModuleLoadException,
     rc::Exception,
     "Some python modules could not be loaded: \r\n" << traceback ,
     ERS_EMPTY,
     ((const char*) traceback)
    )
}

#endif /* DAQ_RC_EXCEPTIONS_H_ */
