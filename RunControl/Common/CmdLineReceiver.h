/*
 * CmdLineReceiver.h
 *
 *  Created on: Sep 4, 2013
 *      Author: avolio
 */

/**
 * @file RunControl/Common/CmdLineReceiver.h
 */

#ifndef DAQ_RC_CMDLINERECEIVER_H_
#define DAQ_RC_CMDLINERECEIVER_H_

#include "RunControl/Common/CommandReceiver.h"

#include <memory>
#include <string>

namespace daq { namespace rc {

class CommandedApplication;
struct CmdLineReceiverDeleter;

/**
 * @brief Class for a command receiver whose input is the command line
 *
 * @note Instances of this class should be created via the daq::rc::CommandReceiverFactory class.
 */
class CmdLineReceiver : public CommandReceiver {
    public:
        /**
         * @brief Copy constructor: deleted.
         */
        CmdLineReceiver(const CmdLineReceiver&) = delete;

        /**
         * @brief Move constructor: deleted
         */
        CmdLineReceiver(CmdLineReceiver&&) = delete;

        /**
         * @brief Copy assignment operator: deleted.
         */
        CmdLineReceiver& operator=(const CmdLineReceiver&) = delete;

        /**
         * @brief Move assignment operator: deleted
         */
        CmdLineReceiver& operator=(CmdLineReceiver&&) = delete;

        /**
         * @brief It does nothing
         */
        void init() override;

        /**
         * @brief It waits on the standard input for commands
         *
         * This blocks and returns when daq::rc::CmdLineReceiver::shutdown is called or the
         * @em quit string is received.
         */
        void start() override;

        /**
         * @brief It causes daq::rc::CmdLineReceiver::start to return
         */
        void shutdown() override;

        /**
         * @brief The same as daq::rc::CmdLineReceiver::shutdown
         */
        void deactivate() override;

    private:
        friend struct CmdLineReceiverDeleter;
        friend class CommandReceiverFactory;

        /**
         * @brief It creates a new instance of this class
         *
         * @param ca A reference to the application executing the received commands
         * @return A pointer to a new instance of this class
         */
        static std::shared_ptr<CmdLineReceiver> create(CommandedApplication& ca);

        /**
         * @brief Constructor
         *
         * @param ca A reference to the application executing the received commands
         */
        CmdLineReceiver(CommandedApplication& ca);

        /**
         * @brief Destructor
         */
        ~CmdLineReceiver() noexcept;

        const std::string m_user_name;
        const std::string m_host_name;
        const std::string m_sender_id;
        CommandedApplication& m_commanded_application; //!< The application executing the received commands
};

}}

#endif /* DAQ_RC_CMDLINERECEIVER_H_ */
