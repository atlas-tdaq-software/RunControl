/*
 * ThreadPool.h
 *
 *  Created on: Dec 18, 2012
 *      Author: avolio
 */

/**
 * @file RunControl/Common/ThreadPool.h
 */

#ifndef DAQ_RC_THREADPOOL_H_
#define DAQ_RC_THREADPOOL_H_

#include "RunControl/Common/Exceptions.h"

#include <tbb/concurrent_queue.h>

#include <boost/scope_exit.hpp>

#include <vector>
#include <memory>
#include <functional>
#include <atomic>
#include <chrono>
#include <thread>
#include <future>
#include <utility>
#include <tuple>
#include <mutex>

#include <signal.h>

namespace daq { namespace rc {

/**
 * @brief Simple class to be used in order to add an interruptible state to any class
 */
class Interruptible {
    public:
        /**
         * @brief Constructor
         */
        Interruptible() : m_interrupted(false) {}

        /**
         * @brief Destructor
         */
        virtual ~Interruptible() {}

        /**
         * @brief It activates the interrupted state
         */
        void interrupt() {
            m_interrupted = true;
        }

        /**
         * @brief Method to be used in order to check the interrupted state
         * @throws daq::rc::InterruptedException The interrupted state has been activated
         */
        void interrupted() const {
            if(m_interrupted == true) {
                throw daq::rc::InterruptedException(ERS_HERE);
            }
        }

    private:
        std::atomic<bool> m_interrupted;
};

/**
 * @brief An interruptible task whose result can be queried
 *
 * Derived class have to override FutureTask::run and may want to
 * override FutureTask::done.
 *
 * @note Tasks are usually submitted to a thread pool but, for convenience, they
 *       can also be executed by using the FutureTask::execute method (in this case,
 *       of course, the task must not be submitted for future execution to any thread
 *       pool).
 *
 * @tparam ResultType Type of the task's result
 */
template<typename ResultType>
class FutureTask : public Interruptible {
    public:
        /**
         * @brief Default constructor
         */
        FutureTask() : Interruptible(), m_promise(), m_future(m_promise.get_future()), m_cancel(false) {}

        /**
         * @brief Default move constructor
         */
        FutureTask(FutureTask<ResultType>&&) = default;

        /**
         * @brief Default move assignment operator
         */
        FutureTask<ResultType>& operator=(FutureTask<ResultType>&&) = default;

        /**
         * @brief Destructor
         */
        virtual ~FutureTask() {}

        /**
         * @brief It cancel this task's execution
         *
         * Cancellation cannot be reverted.
         * @note Cancellation avoids the execution of the task if not yet started; if the task is
         *       already being executed, then its execution can only be terminated with the
         *       co-operation of the user code that could periodically check the task's status
         *       via FutureTask::cancelled or, eventually, Interruptible::interrupted.
         *
         * @param shouldInterrupt If @em true the interrupted state will be activated as well
         */
        void cancel(bool shouldInterrupt) {
            m_cancel = true;
            if(shouldInterrupt == true) {
                interrupt();
            }
        }

        /**
         * @brief It returns @em true if this task has been cancelled
         * @return @em true if this task has been cancelled
         */
        bool cancelled() const {
            return m_cancel;
        }

        /**
         * @brief It gets the result of the task's execution
         *
         * It waits until the task has completed its execution.
         *
         * @warning It cannot be called more than once
         *
         * @return The result of the task's execution
         *
         * @throws daq::rc::CancellationException The task has been cancelled
         * @throws Anything thrown by FutureTask::run
         */
        ResultType get() {
            if(m_cancel == true) {
                throw daq::rc::CancellationException(ERS_HERE);
            }

            return m_future.get();
        }

        /**
         * @brief It gets the result of the task's execution
         *
         * It waits until the task has completed its execution or the defined timeout has elapsed.
         *
         * @warning It cannot be called more than once unless the timeout has expired
         *
         * @tparam Rep An arithmetic type representing the number of ticks
         * @tparam Period A std::ratio representing the tick period (i.e., the number of seconds per tick)
         * @param timeout_duration The timeout's duration
         * @return The result of the task's execution
         *
         * @throws daq::rc::TimeoutException Timeout elapsed
         * @throws daq::rc::CancellationException The task has been cancelled
         * @throws Anything thrown by FutureTask::run
         */
        template<typename Rep, typename Period>
        ResultType get(const std::chrono::duration<Rep, Period>& timeout_duration) {
            if(m_cancel == true) {
                throw daq::rc::CancellationException(ERS_HERE);
            }

            std::future_status status = m_future.wait_for(timeout_duration);
            if(status == std::future_status::ready) {
                return m_future.get();
            }

            throw daq::rc::TimeoutException(ERS_HERE);
        }

        /**
         * @brief It executes this task.
         *
         * @warning To be used only when a task is not submitted to a thread pool
         */
        void execute() {
            BOOST_SCOPE_EXIT_TPL(this_)
            {
                this_->done();
            }
            BOOST_SCOPE_EXIT_END

            if(m_cancel == false) {
                try {
                    m_promise.set_value(run());
                }
                catch(...) {
                    m_promise.set_exception(std::current_exception());
                }
            } else {
                m_promise.set_exception(std::make_exception_ptr(daq::rc::CancellationException(ERS_HERE)));
            }
        }

    protected:
        /**
         * @brief This method is called when the task is executed
         *
         * @return The result of the task's execution
         */
        virtual ResultType run() = 0;

        /**
         * @brief This is <b>always</b> called when the task's execution is completed
         *        (either normally or because of an exception)
         */
        virtual void done() noexcept {}

    private:
        std::promise<ResultType> m_promise;
        std::future<ResultType> m_future;
        std::atomic<bool> m_cancel;
};

// Specialization for 'void' return type
// Note the "inline" (compilation will fail without it)
template<>
inline void FutureTask<void>::execute() {
    BOOST_SCOPE_EXIT_TPL(this_)
    {
        this_->done();
    }
    BOOST_SCOPE_EXIT_END

    if(m_cancel == false) {
        try {
            run();
            m_promise.set_value();
        }
        catch(...) {
            m_promise.set_exception(std::current_exception());
        }
    } else {
        m_promise.set_exception(std::make_exception_ptr(daq::rc::CancellationException(ERS_HERE)));
    }
}

/**
 * @brief Simple utility class used to keep trace of started tasks
 */
class TaskBookkeeper {
    public:
        /**
         * @brief Default constructor
         */
        TaskBookkeeper() {}

        /**
         * @brief Destructor
         */
        ~TaskBookkeeper() {}

        /**
         * @brief It adds a task
         *
         * @note Only a weak reference to the task is kept
         *
         * @param task The task to be added
         */
        void addTask(const std::shared_ptr<Interruptible>& task) {
            std::lock_guard<std::mutex> lk(m_mutex);

            purge();
            m_tasks.push_back(std::weak_ptr<Interruptible>(task));
        }

        /**
         * @brief It sets the interrupted state of all the tasks
         */
        void interruptTasks() {
            std::lock_guard<std::mutex> lk(m_mutex);

            for(auto it = m_tasks.begin(); it != m_tasks.end(); ) {
                const auto& shared = it->lock();
                if(shared.get() != nullptr) {
                    shared->interrupt();
                    ++it;
                } else {
                    m_tasks.erase(it++);
                }
            }
        }

        /**
         * @brief It removes all the tasks
         */
        void clear() {
            std::lock_guard<std::mutex> lk(m_mutex);

            m_tasks.clear();
        }

    protected:
        /**
         * @brief It removes all the expired tasks
         */
        void purge() {
            m_tasks.remove_if([](const std::weak_ptr<Interruptible>& element) -> bool { return element.expired(); });
        }

    private:
        std::mutex m_mutex;
        std::list<std::weak_ptr<Interruptible>> m_tasks;
};

/**
 * @brief A pool of threads
 */
class ThreadPool {
    public:
        /**
         * @brief Constructor
         *
         * @note By default queued and not executed yet tasks are just cancelled when a thread pool
         *       is destroyed (see ThreadPool::waitOnExit).
         *
         * @param size The number of threads the pool is made up of
         * @throws daq::rc::ThreadPoolError Some thread could not be started
         */
        explicit ThreadPool(unsigned int size) :
                m_num_threads(size), m_wait(false), m_paused(false), m_joiner(m_threads)
        {
            try {
                for(unsigned int i = 0; i < size; ++i) {
                    m_threads.push_back(std::thread(&ThreadPool::consumeTasks, this));
                }
            }
            catch(std::system_error& ex) {
                throw daq::rc::ThreadPoolError(ERS_HERE,
                                               std::string("Some thread in the pool cannot be started: ") + ex.what(),
                                               ex);
            }
        }

        /**
         * @brief Copy constructor: deleted
         */
        ThreadPool(const ThreadPool&) = delete;

        /**
         * @brief Move constructor: deleted
         */
        ThreadPool(ThreadPool&&) = delete;

        /**
         * @brief Copy assignment operator: deleted
         */
        ThreadPool& operator=(const ThreadPool&) = delete;

        /**
         * @brief Move assignment operator: deleted
         */
        ThreadPool& operator=(ThreadPool&&) = delete;

        /**
         * @brief Destructor
         *
         * If ThreadPool::waitOnExit has been set, then it waits for all the tasks
         * submitted but not executed yet to be completed; otherwise it only waits
         * for tasks being executed to be completed.
         */
        ~ThreadPool() {
            if(m_wait == false) {
                flush();
            }

            for(unsigned int i = 0; i < m_num_threads; ++i) {
                m_task_queue.push([]() { throw StopWorker(); });
            }
        }

        /**
         * @brief It submits a task for execution
         *
         * @param task The task to be executed
         * @tparam ResultType The type of the task's result
         * @throws daq::rc::ThreadPoolPaused The thread pool is paused
         *
         * @see ThreadPool::pause
         */
        template<typename ResultType>
        void execute(const std::shared_ptr<FutureTask<ResultType>>& task) {
            if(m_paused == false) {
                m_task_queue.push(std::bind(&FutureTask<ResultType>::execute, task));
           } else {
               throw daq::rc::ThreadPoolPaused(ERS_HERE);
           }
        }

        /**
         * @brief It submits a task for execution
         *
         * @param task The task to be executed
         * @tparam ResultType The type of the task's result
         *
         * @return A std::future in order to retrieve the result of the task's execution
         *
         * @throws daq::rc::ThreadPoolPaused The thread pool is paused
         *
         * @see ThreadPool::pause
         */
        template<typename ResultType>
        std::future<ResultType> submit(const std::function<ResultType ()>& task) {
            if(m_paused == false) {
                std::shared_ptr<std::packaged_task<ResultType ()>> pt(new std::packaged_task<ResultType ()>(task));
                std::future<ResultType> fut = pt->get_future();
                m_task_queue.push(std::bind(&std::packaged_task<ResultType ()>::operator(), std::move(pt)));

                return fut;
            } else {
                throw daq::rc::ThreadPoolPaused(ERS_HERE);
            }
        }

        /**
         * @brief It removes all the tasks submitted but not executed yet
         */
        void flush() {
            std::function<void ()> f;
            while(m_task_queue.try_pop(f)) {};
        }

        /**
         * @brief It paused this thread pool: task's submission is not allowed
         *
         * Any attempt to submit a task to this thread pool will result in a daq::rc::ThreadPoolPaused exception
         */
        void pause() {
            m_paused = true;
        }

        /**
         * @brief It resumes this thread pool: task's submission is allowed again
         */
        void resume() {
            m_paused = false;
        }

        /**
         * @brief It sets whether submitted but not executed tasks should be waited when this pool is destroyed
         * @param wait If @em true the destructor will wait until all the tasks in the queue are executed
         */
        void waitOnExit(bool wait) {
            m_wait = wait;
        }

    protected:
        /**
         * @brief Main loop for threads
         */
        void consumeTasks() {
            // This avoids the threads to be interrupted by a signal
            sigset_t sig_set;
            ::sigemptyset(&sig_set);
            ::sigaddset(&sig_set, SIGINT);
            ::sigaddset(&sig_set, SIGTERM);
            ::pthread_sigmask(SIG_SETMASK, &sig_set, 0);

            while(true) {
                std::function<void ()> f;
                m_task_queue.pop(f);

                try {
                    f();
                }
                catch(StopWorker& ex) {
                    // We are asked to stop our job...
                    break;
                }
                catch(std::exception& ex) {
                    ers::error(daq::rc::ThreadPoolError(ERS_HERE, ex.what(), ex));
                }
            }
       }

    private:
        class ThreadJoiner {
            public:
                explicit ThreadJoiner(std::vector<std::thread>& threads) :
                        m_threads(threads)
                {
                }

                ThreadJoiner(const ThreadJoiner&) = delete;
                ThreadJoiner& operator=(const ThreadJoiner&) = delete;

                ~ThreadJoiner() {
                    for(std::thread& thrd : m_threads) {
                        if(thrd.joinable()) {
                            thrd.join();
                        }
                    }
                }

            private:
                std::vector<std::thread>& m_threads;
        };

        struct StopWorker {
        };

        const unsigned int m_num_threads;
        std::atomic<bool> m_wait;
        std::atomic<bool> m_paused;
        tbb::concurrent_bounded_queue<std::function<void ()>> m_task_queue;
        std::vector<std::thread> m_threads;
        ThreadJoiner m_joiner;
};

}}

#endif /* DAQ_RC_THREADPOOL_H_ */
