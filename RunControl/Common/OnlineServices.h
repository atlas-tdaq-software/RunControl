/*
 * OnlineServices.h
 *
 *  Created on: Nov 12, 2012
 *      Author: avolio
 */

/**
 * @file RunControl/Common/OnlineServices.h
 */

#ifndef DAQ_RC_ONLINESERVICES_H_
#define DAQ_RC_ONLINESERVICES_H_

#include <ipc/partition.h>

#include <string>
#include <memory>
#include <mutex>
#include <atomic>
#include <shared_mutex>

class Configuration;
namespace daq { namespace core {
    class Partition;
    class RunControlApplicationBase;
    class Segment;
    class SubstituteVariables;
}}

namespace daq { namespace rc {

class OSDeleter;

/**
 * @brief Utility class giving access to some online services (mainly Configuration) used by the Run Control.
 *
 * A valid instance of this class can be obtained calling OnlineServices::instance() but not before a proper initialization
 * is performed via OnlineServices::initialize(const std::string& partName, const std::string& segName).
 *
 * An instance of this class can be safely accessed concurrently from multiple threads.
 *
 * @note The initialization is normally performed when a daq::rc::ItemCtrl or daq::rc::Controller object is created. If early access
 *       is needed, then the initialization can be safely performed in advance.
 *       \n
 * @note The Configuration is lazy loaded (<em>i.e.</em>, it is not created or accessed until one of OnlineServices::getConfiguration(),
 *       OnlineServices::getPartition(), OnlineServices::getApplication() or OnlineServices::getSegment() methods is called).
 *
 * @remark Do not cache locally the Configuration object returned by OnlineServices::getConfiguration().
 * @remark Do not cache locally the Configuration objects returned by OnlineServices::getPartition(), OnlineServices::getApplication()
 *         or OnlineServices::getSegment().
 *
 * @attention Developers are strongly encouraged to not manually create Configuration objects but use OnlineServices::getConfiguration().
 *            This will greatly reduced the resources used by the application.
 */
class OnlineServices {
    public:
        /**
         * Default constructor: deleted.
         */
        OnlineServices() = delete;

        /**
         * Copy constructor: deleted.
         */
        OnlineServices(const OnlineServices&) = delete;

        /**
         * Move constructor: deleted.
         */
        OnlineServices(OnlineServices&&) = delete;

        /**
         * Copy assignment operator: deleted.
         */
        OnlineServices& operator=(const OnlineServices&) = delete;

        /**
         * Move assignment operator: deleted.
         */
        OnlineServices& operator=(OnlineServices&&) = delete;

        /**
         * @brief It performs the proper initialization.
         *
         * @note Multiple calls to this method have no effect and the initialization is performed only once.
         *
         * @post daq::rc::OnlineServices::instance can be safely called
         *
         * @param partName The name of the IPC partition
         * @param segName The name od the segment the application belongs to
         *
         * @throws daq::rc::OnlineServicesFailure The initialization procedure failed
         *
         * @attention Any failure here should be considered as a fatal error.
         */
        static void initialize(const std::string& partName, const std::string& segName);

        /**
         * @brief It returns a reference to a valid daq::rc::OnlineServices instance.
         *
         * @pre daq::rc::OnlineServices::initialize has to be successfully executed
         *
         * @return A reference to a valid daq::rc::OnlineServices instance
         *
         * @throws daq::rc::NotInitialized The initialization has not been performed yet
         */
        static OnlineServices& instance();

        /**
         * @brief It returns the Configuration object holding the partition configuration.
         *
         * @return A reference to a valid Configuration object holding the partition configuration
         *
         * @throws daq::rc::ConfigurationIssue The Configuration cannot be created or the Partition is not found
         *                                     in the configuration
         */
        Configuration& getConfiguration();

        /**
         * @brief It returns the daq::core::Partition object.
         *
         * @return A reference to a valid daq::core::Partition object
         *
         * @throws daq::rc::ConfigurationIssue The Configuration cannot be accessed or the Partition is not found
         *                                     in the configuration
         */
        const daq::core::Partition& getPartition();

        /**
         * @brief It returns the daq::core::RunControlApplicationBase object representing this Run Control application.
         *
         * @return A reference to a valid daq::core::RunControlApplicationBase object representing this application
         *
         * @throws daq::rc::ConfigurationIssue The Configuration cannot be accessed or the RunControlApplicationBase
         *                                     object cannot be found in the configuration
         *
         * @note The returned reference can be properly casted using methods provided by the Configuration layer.
         * @see Configuration::cast
         */
        const daq::core::RunControlApplicationBase& getApplication();

        /**
         * @brief It returns the name of this Run Control application.
         *
         * That is the name as it is published in IPC.
         *
         * @return The name of this Run Control application
         *
         * @attention Always use this method to retrieve the application's name: the UID of the daq::core::RunControlApplicationBase object
         *            returned by OnlineServices::getApplication() does not represent the application name if the application is a
         *            template one.
         */
        const std::string& applicationName() const;

        /**
         * @brief It returns the daq::core::Segment object representing the segment this Run Control application belongs to.
         *
         * @return A reference to a valid daq::core::Segment object representing the segment this Run Control application belongs to
         *
         * @throws daq::rc::ConfigurationIssue The Configuration or the daq::core::Partition cannot be accessed
         * @throws daq::rc::BadSegment The daq::core::Segment object is not found in the Configuration or is not valid (<em>e.g.</em>, it
         *                             may be disabled)
         */
        const daq::core::Segment& getSegment();

        /**
         * @brief It returns the name of the segment this Run Control application belongs to.
         *
         * @return The name of the segment this Run Control application belongs to
         *
         * @attention Always use this method to retrieve the name of the application's segment: the UID of the daq::core::Segment object
         *            returned by OnlineServices::getApplication() does not represent the segment name if the segment is a
         *            template one.
         *
         */
        const std::string& segmentName() const;

        /**
         * @brief It returns the IPCPartition object representing the current partition.
         *
         * @return A reference to a valid IPCPartition object representing the current partition
         */
        const IPCPartition& getIPCPartition() const;

        /**
         * @brief It returns the host full qualified name
         *
         * @return The host full qualified name
         */
        const std::string& getHostName() const;

    protected:
        /**
         * @brief It reloads the configuration (for instance, after a database reload).
         *
         * @param unreadObjects If @em true all the objects in the Configuration cache will be cleared
         *
         * @throws daq::rc::ConfigurationIssue The Configuration or the daq::core::Partition object cannot be accessed
         * @throws daq::rc::BadSegment After the reload of the configuration, the segment the applications belongs to cannot
         *                             be found
         *
         * @see Configuration::unread_all_objects(bool)
         */
        void reloadConfiguration(bool unreadObjects);

        /**
         * @brief It initializes the Configuration.
         *
         * @throws daq::rc::ConfigurationIssue Errors occurred creating the Configuration object or accessing the daq::core::Partition
         *                                     object representing the current partition
         *
         * @note This is a wrapper around OnlineServices::buildConfiguration() in order to assure that the Configuration is initialized
         *       only once.
         */
        void configurationInit();

    private:
        friend class OSDeleter;
        friend class RunControllerFSMOperations; // for the reload of the configuration

        /**
         * @brief Constructor.
         *
         * @param partName The name of the partition this Run Control application belongs to
         * @param segName The name of the segment this Run Control application belongs to
         *
         * @throws daq::rc::OnlineServicesFailure Errors instantiating the OnlineServices
         *
         * @note This constructor should not be called directly but always through
         *       OnlineServices::globalInit(const std::string& partName, const std::string& segName). That is
         *       to be sure that only one instance of this class is ever created.
         */
        OnlineServices(const std::string& partName, const std::string& segName);

        /**
         * @brief Destructor.
         */
        ~OnlineServices();

        /**
         * @brief It builds the Configuration object.
         *
         * @throws daq::rc::ConfigurationIssue Errors occurred creating the Configuration object or accessing the daq::core::Partition
         *                                     object representing the current partition
         *
         * @attention It should never be called directly, but always through OnlineServices::configurationInit()
         */
        void buildConfiguration();

        /**
         * @brief Global initialization function for OnlineServices.
         *
         * @param partName The name of the partition this Run Control application belongs to
         * @param segName The name of the segment this Run Control application belongs to
         *
         * @throws daq::rc::OnlineServicesFailure Errors instantiating the OnlineServices
         */
        static void globalInit(const std::string& partName, const std::string& segName);

        const std::string m_application_name;
        const std::string m_partition_name;
        const std::string m_segment_name;
        mutable std::string m_host_name;

        IPCPartition m_ipc_partition;

        std::unique_ptr<Configuration> m_configuration;
        daq::core::SubstituteVariables* m_converter;

        mutable std::shared_mutex m_mutex;

        static const std::string TDAQ_APPLICATION_NAME;
        static const std::string TDAQ_APPLICATION_OBJECT_ID;
        static std::atomic<bool> m_global_init_done;
        static std::once_flag m_global_init_flag;
        static std::once_flag m_config_init_flag;
        static std::once_flag m_host_name_init_flag;
        static std::unique_ptr<OnlineServices, OSDeleter> m_self;
};

}}

#endif /* DAQ_RC_ONLINESERVICES_H_ */
