/**
 * \file RunControl/Common/UserExceptions.h
 *
 * \author John Erik Sloper <john.erik.sloper@cern.ch>
 *
 *	This file defines all the public ERS issues to be used by sub-systems.
 *	All the issues are on the following form:
 *
 *      ERS_DECLARE_ISSUE(rc,                                   // namespace name
 *                        ExampleIssue,                         // issue name
 *                        "Example message " << example << ".", // message to be sent with issue
 *                        ((const char *) example)              // arguments for the message
 *                         ...                                  //any number of additional arguments
 *                        )
 *
 * 	Example use (in the case an application is not responding):
 *
 * 	#include "RunControl/Common/UserExceptions.h"
 * 	...
 *	daq::rc::ApplicationNotResponding issueName(ERS_HERE,"idOfTheApp@Application");
 *	ers::error(issueName);
 **/

#ifndef DAQ_RC_USER_EXCEPTIONS_H
#define DAQ_RC_USER_EXCEPTIONS_H

#include <ers/ers.h>

namespace daq {
/**
 * @class daq::rc::UserException
 * @brief This is the base issue for all RunControl user exceptions
 **/
ERS_DECLARE_ISSUE(rc, UserException, ERS_EMPTY, ERS_EMPTY)

/**
 * @class daq::rc::Recovery
 * @brief This is the base issue for all recovery related issues.
 **/
ERS_DECLARE_ISSUE_BASE(rc,
                       Recovery,
                       UserException,
                       ERS_EMPTY,
                       ERS_EMPTY,
                       )

/**
 * @class daq::rc::HardwareError
 * @brief This is the baseclass for all recovery related issues concerning hardware
 */
ERS_DECLARE_ISSUE_BASE(rc,
                       HardwareError,
                       Recovery,
                       appId << " " << sender,
                       ERS_EMPTY,
                       ((const char*) appId)
                       ((const char*) sender))

/**
 * @class daq::rc::HardwareSynchronization
 * @brief This is the class to trigger a recovery attempt on part of the system
 */
ERS_DECLARE_ISSUE_BASE(rc,
                       HardwareSynchronization,
                       Recovery,
                       branchController << " would like to resynchronize its hardware.",
                       ERS_EMPTY,
                       ((const char*) branchController))

/**
 * @class daq::rc::HardwareRecoverd
 * @brief This is the class to trigger the re-enabling of disabled RODs
 */
ERS_DECLARE_ISSUE_BASE(rc,
                       HardwareRecovered,
                       Recovery,
                       appId << " " << sender,
                       ERS_EMPTY,
                       ((const char*) appId)
                       ((const char*) sender))

/**
 * @class daq::rc::ReadyForHardwareRecovery
 * @brief This is the class for notification that we can start the recovery of disabled RODs
 */
ERS_DECLARE_ISSUE_BASE(rc,
                       ReadyForHardwareRecovery,
                       Recovery,
                       appId << " " << sender,
                       ERS_EMPTY,
                       ((const char*) appId)
                       ((const char*) sender))

/**
 * @class daq::rc::ReadyForModuleRecovery
 * @brief This is the class for notification that we can start the recovery of disabled Modules
 */
ERS_DECLARE_ISSUE_BASE(rc,
                       ReadyForModuleRecovery,
                       Recovery,
                       appId << " " << modules,
                       ERS_EMPTY,
                       ((std::string) appId)
                       ((std::string) modules))

/**
 * @class daq::rc::PixelUp
 * @brief This is the class for notification that Pixel pre-amplifiers can be turned on
 */
ERS_DECLARE_ISSUE_BASE(rc,
                       PixelUp,
                       Recovery,
                       branchController << " is ready to enable pre-amplifiers.",
                       ERS_EMPTY,
                       ((const char*) branchController))
/**
 * @class daq::rc::PixelDown
 * @brief This is the class for notification that Pixel pre-amplifiers must be turned off
 */
ERS_DECLARE_ISSUE_BASE(rc,
                       PixelDown,
                       Recovery,
                       branchController << " must turn off pre-amplifiers.",
                       ERS_EMPTY,
                       ((const char*) branchController))

/**
 * @class daq::rc::ModulesDisabled
 * @brief This is the class to triggers the notification to the ResourceInfoProvider that some modules have been disabled
 */
ERS_DECLARE_ISSUE_BASE(rc,
                       ModulesDisabled,
                       Recovery,
                       modules << " have been disabled.",
                       ERS_EMPTY,
                       ((const char*) modules)
                       ((bool) changeLB))

/**
 * @class daq::rc::ModulesEnabled
 * @brief This is the class to triggers the notification to the ResourceInfoProvider that some modules have been enabled
 */
ERS_DECLARE_ISSUE_BASE(rc,
                       ModulesEnabled,
                       Recovery,
                       modules << " have been enabled.",
                       ERS_EMPTY,
                       ((const char*) modules)
                       ((bool) changeLB))

/**
 * @class daq::rc::ApplicationError
 * @brief This is the baseclass of all recovery related issues concerning an application
 */
ERS_DECLARE_ISSUE_BASE(rc,
                       ApplicationError,
                       Recovery,
                       appId,
                       ERS_EMPTY,
                       ((const char*)appId))

/**
 * @class daq::rc::ApplicationNotResponding
 * @brief This issues represents a non-responding application
 **/
ERS_DECLARE_ISSUE_BASE(rc,
                       ApplicationNotResponding,
                       ApplicationError,
                       ERS_EMPTY,
                       ((const char*) appId),)

/**
 * @class daq::rc::ApplicationDead
 * @brief This issue should be raised in the case of a (unexpectedly) dead application
 **/
ERS_DECLARE_ISSUE_BASE(rc,
                       ApplicationDead,
                       ApplicationError,
                       ERS_EMPTY,
                       ((const char*) appId),
                       ERS_EMPTY)

/**
 * @class daq::rc::ApplicationFatalError
 * @brief This issues represents a fatal error for the application
 **/
ERS_DECLARE_ISSUE_BASE(rc,
                       ApplicationFatalError,
                       ApplicationError,
                       ERS_EMPTY,
                       ((const char*) appId),
                       ERS_EMPTY)

/**
 * @class daq::rc::ProcessingTimeout
 * @brief This issues represents a timeout for an application
 **/
ERS_DECLARE_ISSUE_BASE(rc,
                       ProcessingTimeout,
                       ApplicationError,
                       ERS_EMPTY,
                       ((const char*) appId),
                       ERS_EMPTY)

/**
 * @class daq::rc::MAX_EVT_DONE
 * @brief Raised when the max event number has been reached
 **/
ERS_DECLARE_ISSUE_BASE(rc,
                       MAX_EVT_DONE,
                       UserException,
                       ERS_EMPTY,
                       ERS_EMPTY,
                       ERS_EMPTY)
/**
 * @class daq::rc::LatencyMismatch
 * @brief This issue represents a problem in some detector's trigger latency settings
 **/
ERS_DECLARE_ISSUE_BASE(rc,
		       LatencyMismatch,
		       UserException,
		       "Latency mismatch for detector " << detector << (reason.empty() == false ? std::string(". Reason: ") + reason : ""),
		       ERS_EMPTY,
		       ((std::string) detector)
		       ((std::string) reason)
		       )

} //end namespace daq

/**
 * @namespace l1calo::rc
 * @brief Namespace for L1Calo issues used in recovery actions
 */
namespace l1calo {

/** @class l1calo::rc::L1CaloZeroPpmLut
 *  @brief This issue is a request to the expert system to zero the PPM LUT for a tower.
 */
ERS_DECLARE_ISSUE(rc,
                  L1CaloZeroPpmLut,
                  "0x" << std::hex << towerID << std::dec,
                  ((unsigned int) towerID))

/** @class l1calo::rc::L1CaloRestorePpmLut
 *  @brief This issue is a request to the expert system to restore the PPM LUT for a tower.
 */
ERS_DECLARE_ISSUE(rc,
                  L1CaloRestorePpmLut,
                  "0x" << std::hex << towerID << std::dec,
                  ((unsigned int) towerID))

/** @class l1calo::rc::L1CaloSetPpmNoiseCut
 *  @brief This issue is a request to the expert system to adjust the PPM noise cut for a tower.
 */
ERS_DECLARE_ISSUE(rc,
                  L1CaloSetPpmNoiseCut,
                  "0x" << std::hex << towerID << " 0x" << noiseCut << std::dec,
                  ((unsigned int) towerID)
                  ((unsigned int) noiseCut))
} //end namespace l1calo

#endif //RC_USER_EXCEPTIONS_H
