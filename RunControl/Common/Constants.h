/*
 * Constants.h
 *
 *  Created on: Oct 11, 2013
 *      Author: avolio
 */

/**
 * @file RunControl/Common/Constants.h
 */

#ifndef DAQ_RC_CONSTANTS_H_
#define DAQ_RC_CONSTANTS_H_

#include <string>

namespace daq { namespace rc {
namespace {

/**
 * @brief Class holding simple constants
 */
class Constants {
    public:
        /**
         * @brief The name of the @em RunCtrl IS server
         */
        static const std::string RC_IS_SERVER_NAME;

        /**
         * @brief The @em Supervision string for IS information
         */
        static const std::string RC_SV_INFO;

        /**
         * @brief The name of the @em Setup IS server
         */
        static const std::string SETUP_IS_SERVER_NAME;

        /**
         * @brief The name of the @em RunParams IS server
         */
        static const std::string RUN_PARAMS_IS_SERVER;

    private:
        Constants() = delete;
        ~Constants() = delete;
};

const std::string Constants::RC_IS_SERVER_NAME = "RunCtrl";
const std::string Constants::RC_SV_INFO = "Supervision";
const std::string Constants::SETUP_IS_SERVER_NAME = "Setup";
const std::string Constants::RUN_PARAMS_IS_SERVER = "RunParams";

}
}}


#endif /* DAQ_RC_CONSTANTS_H_ */
