/*
 * CommandReceiver.h
 *
 *  Created on: Oct 1, 2012
 *      Author: avolio
 */

/**
 * @file RunControl/Common/CommandReceiver.h
 */

#ifndef DAQ_RC_COMMANDRECEIVER_H_
#define DAQ_RC_COMMANDRECEIVER_H_

namespace daq { namespace rc {

// In the current schema CommandReceiver instances are never created directly
// but only through the CommandReceiverFactory. Please, keep using this scheme
// when new receivers are implemented

/**
 * @brief Interface representing an entity able to receive external commands
 *        and re-route them to a daq::rc::CommandedApplication
 */
class CommandReceiver {
	public:
        /**
         * @brief Destructor
         */
		virtual ~CommandReceiver() noexcept {;};

		/**
		 * @brief Implementing classes should perform initialization here so that the
		 *        daq::rc::CommandReceiver::start method can be safely called
		 */
		virtual void init() = 0;

		/**
		 * @brief After this method is called, implementing classes should be ready to receive commands
		 *
		 * @note This method should block and return when daq::rc::CommandReceiver::shutdown is called.
		 */
		virtual void start() = 0;

		/**
		 * @brief This method should just let daq::rc::CommandReceiver::start to return
		 */
		virtual void shutdown() = 0;

		/**
		 * @brief Implementing classes may use this method in order to stop the receiver
		 *
		 * @note With respect to daq::rc::CommandReceiver::shutdown, this method does not require
		 *       daq::rc::CommandReceiver::start to return.
		 */
		virtual void deactivate() = 0;
};

}}

#endif /* DAQ_RC_COMMANDRECEIVER_H_ */
