/*
 * Clocks.h
 *
 *  Created on: Apr 29, 2013
 *      Author: avolio
 */

/**
 * @file RunControl/Common/Clocks.h
 */

#ifndef DAQ_RC_CLOCKS_H_
#define DAQ_RC_CLOCKS_H_

#include <boost/chrono.hpp>
#include <boost/chrono/system_clocks.hpp>


namespace daq { namespace rc {
    /**
     * @namespace daq::rc::chrono
     *
     * The reported typedefs are meant to define a monotonic, steady clock (and all the related types).
     * Currently the boost chrono implementation is used (the std implementation available in gcc 8.2.0 from LCG does
     * not contain any real steady clock).
     *
     * @todo The boost chrono implementation may be replaced by the std one, once a real monotonic clock is available.
     */
    namespace chrono = boost::chrono;

    /**
     * High resolution steady clock.
     */
    typedef chrono::high_resolution_clock clock_t;

    /**
     * Arithmetic type representing the number of ticks in the clock's duration.
     */
    typedef clock_t::rep infotime_t;

    /**
     * A ratio type representing the tick period of the clock, in seconds duration.
     */
    typedef clock_t::period tick_t;

    /**
     * A time duration type.
     */
    typedef clock_t::duration duration_t;

    /**
     * A time point type.
     */
    typedef clock_t::time_point timepoint_t;

    static_assert(clock_t::is_steady == true, "A steady clock is required");
}}

#endif /* DAQ_RC_CLOCKS_H_ */
