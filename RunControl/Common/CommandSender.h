/*
 * CommandSender.h
 *
 *  Created on: Dec 12, 2012
 *      Author: avolio
 */

/**
 * @file RunControl/Common/CommandSender.h
 */

#ifndef DAQ_RC_COMMANDSENDER_H_
#define DAQ_RC_COMMANDSENDER_H_

#include <ipc/partition.h>

#include <string>
#include <memory>
#include <vector>
#include <string_view>

namespace daq { namespace rc {

enum class FSM_COMMAND : unsigned int;
class RunControlBasicCommand;
class TransitionCmd;
class ApplicationStatusCmd;
class ChangeStatusCmd;
class StartStopAppCmd;
class UserCmd;
class DynRestartCmd;
class TestAppCmd;
class CommandNotifier;

/**
 * @brief Utility class to be used in order to send commands to remote Run Control applications.
 *
 * Three different types of "sender" methods are available, so that users can chose the one that fits
 * his/her needs:
 * @li <em>Generic command sender</em>: the method CommandSender::sendCommand(const std::string&, const RunControlBasicCommand&)
 *                                      can be used to send any kind of command;
 * @li <em>Specialized senders</em>: Different methods for different commands that are described as full objects;
 * @li <em>Basic senders</em>: Different methods for different commands with those methods taking basic types as arguments.
 *
 * @note Some commands may not be executed by the remote application being it a segment's controller or a leaf application.
 * @note When a method of this class is declared to throw a daq::rc::Busy exception, it does not mean that the corresponding command
 *       will fail every time the recipient application is busy with some other command. It just means that there may be conditions
 *       in which a command may not be executed because some <b>specific</b> other commands are in execution.
 * @note An object of this class can be safely used in multiple threads concurrently.
 *       \n
 * @remark All the sending-command methods (unless specified) are asynchronous (<em>i.e.</em>, they return before the remote application
 *         has completed the execution of the command). If the caller desires to be notified when a command has been executed by
 *         the remote applications, then one of the two following methods can be used:
 *         @li void sendCommand(const std::string& recipient, RunControlBasicCommand& cmd, const CommandNotifier& cmdNot) const;
 *         @li bool sendCommand(const std::string& recipient, RunControlBasicCommand& cmd, unsigned long timeout) const.
 *         Note that this mechanism is not supported by the daq::rc::RC_COMMAND::IGNORE_ERROR,  daq::rc::RC_COMMAND::DYN_RESTART and
 *         daq::rc::RC_COMMAND::EXIT.
 *
 * @warning With the <em>specialized senders</em> do not use the same daq::rc::RunControlBasicCommand object multiple times:
 *          any command object has an unique identifier associated to it and applications may have issues dealing concurrently
 *          with commands having the same UID (this is especially valid for FSM transitions not causing a change in the
 *          FSM state).
 *
 * @see daq::rc::RunControlBasicCommand
 * @see daq::rc::RunControlCommands
 * @see daq::rc::Controllable to have access to actions associated to command execution in leaf applications
 * @see daq::rc::UserRoutines to have access to actions associated to command execution in segment controllers
 */
class CommandSender {
    public:
        /**
         * @brief Class representing an error token.
         *
         * It encapsulates the reason why an application (mainly a segment's controller) is in an error state: it is identified
         * by the name of the child application being the source of the issue, the human readable description of the issue and
         * its type.
         *
         * @note Used by the Expert System to notify error conditions to controllers and applications.
         */
        class ErrorElement {
            public:
                /**
                 * @brief Constructor.
                 *
                 * @param applicationName The name of the application being the source of the error condition
                 * @param errorDescription A human readable description of the error condition
                 * @param errorType The "type" of the error condition
                 *
                 * @note An application can be in error because of its "internal" state: in this case <em>applicationName</em>
                 *       will be the name of the application itself.
                 */
                ErrorElement(const std::string& applicationName,
                             const std::string& errorDescription,
                             const std::string& errorType);

                /**
                 * It returns the name of the application being the source of the error condition.
                 *
                 * @return The name of the application being the source of the error condition
                 */
                const std::string& applicationName() const;

                /**
                 * It returns a human readable description of the error condition.
                 *
                 * @return A string with a description of the error condition
                 */
                const std::string& errorDescription() const;

                /**
                 * It returns the "type" of the error condition.
                 *
                 * @return The "type" of the error condition
                 */
                const std::string& errorType() const;

            private:
                const std::string m_app_name;
                const std::string m_err_descr;
                const std::string m_err_type;
        };

    public:
        /**
         * @brief Constructor.
         *
         * @note A single daq::rc::CommandSender instance can be used to send multiple commands to
         *       the same application.
         *
         * @param partition The name of the IPC partition the applications belongs to
         * @param senderId String identifying the application sending the command
         */
        CommandSender(const std::string& partition, const std::string& senderId);

        /**
         * @overload CommandSender::CommandSender(const IPCPartition&, const std::string&)
         */
        CommandSender(const IPCPartition& partition, const std::string& senderId);

        /**
         * @brief Copy constructor.
         */
        CommandSender(const CommandSender&) = default;

        /**
         * @brief The destructor.
         */
        ~CommandSender();

        /**
         * @brief It sends a command to a specific application
         *
         * This method can be used to send any kind of command: it detects if the command is a transition command or not and it dispatches it properly.
         *
         * @param recipient The name of the application receiving the command
         * @param cmd The command to be executed by the receiving application
         *
         * @throws daq::rc::IPCLookup The @em recipient cannot be found (<em>i.e.</em>, it is not published in IPC)
         * @throws daq::rc::BadCommand The @em cmd is not valid or ill-formed
         * @throws daq::rc::CorbaException Some communication issue occurred trying to reach @em recipient
         * @throws daq::rc::Busy The command could not be executed because the @em recipient was busy
         * @throws daq::rc::NotAllowed The command could not be executed but its execution was not allowed (<em>e.g.</em>, by the Access Management system)
         * @throws daq::rc::ExecutionFailed Some error occurred during the execution of the command
         * @throws daq::rc::InErrorState The command could not be executed because the application was in an error state
         */
        void sendCommand(const std::string& recipient, const RunControlBasicCommand& cmd) const;

        /**
         * @brief It sends a command to a specific application with the possibility to receive a notification when
         *        the command has been executed
         *
         * This method can be used to send any kind of command: it detects if the command is a transition command or not and it dispatches it properly.
         *
         * @param recipient The name of the application receiving the command
         * @param cmd The command to be executed by the receiving application
         * @param cmdNot A daq::rc::CommandNotifier instance that will be invoked when the command has been executed
         *
         * @throws daq::rc::IPCLookup The @em recipient cannot be found (<em>i.e.</em>, it is not published in IPC)
         * @throws daq::rc::BadCommand The @em cmd is not valid, ill-formed or does not support the asynchronous notification mechanism
         * @throws daq::rc::CorbaException Some communication issue occurred trying to reach @em recipient
         * @throws daq::rc::Busy The command could not be executed because the @em recipient was busy
         * @throws daq::rc::NotAllowed The command could not be executed but its execution was not allowed (<em>e.g.</em>, by the Access Management system)
         * @throws daq::rc::ExecutionFailed Some error occurred during the execution of the command
         * @throws daq::rc::InErrorState The command could not be executed because the application was in an error state
         * @throws daq::rc::OnlineServicesFailure The @em cmdNot object cannot be properly configured to receive notifications
         */
        void sendCommand(const std::string& recipient, RunControlBasicCommand& cmd, const CommandNotifier& cmdNot) const;

        /**
         * @brief It sends a command to a specific application waiting for the command to be executed
         *
         * This method can be used to send any kind of command: it detects if the command is a transition command or not and it dispatches it properly.
         *
         * @param recipient The name of the application receiving the command
         * @param cmd The command to be executed by the receiving application
         * @param timeout The timeout (in milliseconds) to wait for the command to be completed
         * @return @em true if the command has been completed within the defined timeout, @em false otherwise
         *
         * @throws daq::rc::IPCLookup The @em recipient cannot be found (<em>i.e.</em>, it is not published in IPC)
         * @throws daq::rc::BadCommand The @em cmd is not valid, ill-formed or does not support the asynchronous notification mechanism
         * @throws daq::rc::CorbaException Some communication issue occurred trying to reach @em recipient
         * @throws daq::rc::Busy The command could not be executed because the @em recipient was busy
         * @throws daq::rc::NotAllowed The command could not be executed but its execution was not allowed (<em>e.g.</em>, by the Access Management system)
         * @throws daq::rc::ExecutionFailed Some error occurred during the execution of the command
         * @throws daq::rc::InErrorState The command could not be executed because the application was in an error state
         * @throws daq::rc::OnlineServicesFailure The @em cmdNot object cannot be properly configured to receive notifications
         */
        bool sendCommand(const std::string& recipient, RunControlBasicCommand& cmd, unsigned long timeout) const;

        /**
         * @brief It asks the remote application to perform an FSM transition.
         *
         * When a leaf application receives a transition command, the corresponding daq::rc::Controllable method will be called.
         * When a segment's controller receives a transition command, first the corresponding daq::rc::UserRoutines method will be called,
         * and then the command will be dispatched to all of its children.
         *
         * @param recipient The name of the application receiving the command
         * @param trCommand The transition command
         *
         * @throws daq::rc::IPCLookup The @em recipient cannot be found (<em>i.e.</em>, it is not published in IPC)
         * @throws daq::rc::BadCommand The @em trCommand is not valid or ill-formed
         * @throws daq::rc::CorbaException Some communication issue occurred trying to reach @em recipient
         * @throws daq::rc::Busy The command could not be executed because the @em recipient was busy
         * @throws daq::rc::NotAllowed The command could not be executed but its execution was not allowed (<em>e.g.</em>, by the Access Management system)
         * @throws daq::rc::ExecutionFailed Some error occurred during the execution of the command
         * @throws daq::rc::InErrorState The command could not be executed because the application was in an error state
         *
         * @see CommandSender::makeTransition(const std::string&, FSM_COMMAND) const
         */
        void makeTransition(const std::string& recipient, const TransitionCmd& trCommand) const;

        /**
         * @overload void makeTransition(const std::string&, FSM_COMMAND) const
         *
         * @param recipient The name of the application receiving the command
         * @param cmd The transition command as defined in the daq::rc::FSM_COMMAND enumeration
         */
        void makeTransition(const std::string& recipient, FSM_COMMAND cmd) const;

        /**
         * @brief It asks the remote application to execute an user-defined command.
         *
         * When this command is sent to a leaf application, the Controllable::user(const UserCmd& usrCmd) method will be called.
         * This command has no effect if sent to a segment's controller.
         *
         * @param recipient The name of the application receiving the command
         * @param userCmd The user-defined command
         *
         * @throws daq::rc::IPCLookup The @em recipient cannot be found (<em>i.e.</em>, it is not published in IPC)
         * @throws daq::rc::BadCommand The sent command is not valid or ill-formed
         * @throws daq::rc::CorbaException Some communication issue occurred trying to reach @em recipient
         * @throws daq::rc::NotAllowed The command could not be executed but its execution was not allowed (<em>e.g.</em>, by the Access Management system)
         * @throws daq::rc::ExecutionFailed Some error occurred during the execution of the command
         *
         * @see CommandSender::userCommand(const std::string&, const std::string&, const std::vector<std::string>&) const
         */
        void userCommand(const std::string& recipient, const UserCmd& userCmd) const;

        /**
         * @overload void userCommand(const std::string&, const std::string&, const std::vector<std::string>&) const
         *
         * @param recipient The name of the application receiving the command
         * @param cmdName Name of the user-defined command
         * @param args The arguments of the user-defined command
         */
        void userCommand(const std::string& recipient, const std::string& cmdName, const std::vector<std::string>& args) const;

        /**
         * @brief It asks the remote application to change the status (<em>i.e.</em>, enabled/disabled) of some components.
         *
         * If this command is sent to a segment's controller, then the controller will put in/out of membership the defined child applications.
         * If this command is sent to a leaf application, then the application will call daq::rc::Controllable::enable(const std::vector<std::string>&) or
         * daq::rc::Controllable::enable(const std::vector<std::string>&) accordingly.
         *
         * @note This method is synchronous when the command is sent to a segment's controller.
         *
         * @param recipient The name of the application receiving the command
         * @param cmd The command defining the components to be enabled or disabled
         *
         * @throws daq::rc::IPCLookup The @em recipient cannot be found (<em>i.e.</em>, it is not published in IPC)
         * @throws daq::rc::BadCommand The @em cmd is not valid or ill-formed
         * @throws daq::rc::CorbaException Some communication issue occurred trying to reach @em recipient
         * @throws daq::rc::NotAllowed The command could not be executed but its execution was not allowed (<em>e.g.</em>, by the Access Management system)
         * @throws daq::rc::ExecutionFailed Some error occurred during the execution of the command
         *
         * @see CommandSender::changeStatus(const std::string&, bool, const std::vector<std::string>&) const
         */
        void changeStatus(const std::string& recipient, const ChangeStatusCmd& cmd) const;

        /**
         * @overload void changeStatus(const std::string&, bool, const std::vector<std::string>&) const
         *
         * @param recipient The name of the application receiving the command
         * @param enabled If @em true the @em components will enabled
         * @param components The components to enable or disable
         */
        void changeStatus(const std::string& recipient, bool enabled, const std::vector<std::string>& components) const;

        /**
         * @brief It asks a segment's controller to start, stop o restart some child applications.
         *
         * @param recipient The name of the controller receiving the command
         * @param cmd The command defining the applications to be started, stopper or restarted
         *
         * @note Sending this command to a leaf application has no effect.
         *
         * @throws daq::rc::IPCLookup The @em recipient cannot be found (<em>i.e.</em>, it is not published in IPC)
         * @throws daq::rc::BadCommand The @em cmd is not valid or ill-formed
         * @throws daq::rc::CorbaException Some communication issue occurred trying to reach @em recipient
         * @throws daq::rc::Busy The command could not be executed because the @em recipient was busy
         * @throws daq::rc::NotAllowed The command could not be executed but its execution was not allowed (<em>e.g.</em>, by the Access Management system)
         * @throws daq::rc::ExecutionFailed Some error occurred during the execution of the command
         *
         * @see CommandSender::startApplications
         * @see CommandSender::stopApplications
         * @see CommandSender::restartApplications
         */
        void startStopApplications(const std::string& recipient, const StartStopAppCmd& cmd) const;

        /**
         * @brief It asks a segment's controller to perform a dynamic restart (as known as "TTC Restart") of
         *        some of its child controllers.
         *
         * @param recipient The name of the controller receiving the command
         * @param cmd The "dynamic restart" command
         *
         * @note Sending this command to a leaf application has no effect.
         *
         * @throws daq::rc::IPCLookup The @em recipient cannot be found (<em>i.e.</em>, it is not published in IPC)
         * @throws daq::rc::BadCommand The @em cmd is not valid or ill-formed
         * @throws daq::rc::CorbaException Some communication issue occurred trying to reach @em recipient
         * @throws daq::rc::Busy The command could not be executed because the @em recipient was busy
         * @throws daq::rc::NotAllowed The command could not be executed but its execution was not allowed (<em>e.g.</em>, by the Access Management system)
         * @throws daq::rc::ExecutionFailed Some error occurred during the execution of the command
         * @throws daq::rc::InErrorState The command could not be executed because the application was in an error state
         */
        void dynamicRestart(const std::string& recipient, const DynRestartCmd& cmd) const;

        /**
         * @overload void dynamicRestart(const std::string&, const std::vector<std::string>&) const
         *
         * @param recipient The name of the controller receiving the command
         * @param segments The child segments to be dynamically restarted
         */
        void dynamicRestart(const std::string& recipient, const std::vector<std::string>& segments) const;

        /**
         * @brief It asks a segment's controller to test the functionality of some of its child applications.
         *
         * @param recipient The name of the controller receiving the command
         * @param cmd The "test" command
         *
         * @note Sending this command to a leaf application has no effect.
         *
         * @throws daq::rc::IPCLookup The @em recipient cannot be found (<em>i.e.</em>, it is not published in IPC)
         * @throws daq::rc::BadCommand The @em cmd is not valid or ill-formed
         * @throws daq::rc::CorbaException Some communication issue occurred trying to reach @em recipient
         * @throws daq::rc::Busy The command could not be executed because the @em recipient was busy
         * @throws daq::rc::NotAllowed The command could not be executed but its execution was not allowed (<em>e.g.</em>, by the Access Management system)
         * @throws daq::rc::ExecutionFailed Some error occurred during the execution of the command
         */
        void testApp(const std::string& recipient, const TestAppCmd& cmd) const;

        /**
         * @overload void testApp(const std::string&, const std::vector<std::string>&) const
         *
         * @note Test's level is set to 1 and scope is set to daq::tm::Test::Scope::Any
         *
         * @param recipient The name of the controller receiving the command
         * @param applications The child applications to be tested
         */
        void testApp(const std::string& recipient, const std::vector<std::string>& applications) const;

        /**
         * @brief It asks a segment's controller to start some child applications.
         *
         * @note This command has no effect if sent to a leaf application.
         *
         * @param recipient The controller asked to execute the command
         * @param applications The child applications to be started
         *
         * @throws daq::rc::IPCLookup The @em recipient cannot be found (<em>i.e.</em>, it is not published in IPC)
         * @throws daq::rc::BadCommand The sent command is not valid or ill-formed
         * @throws daq::rc::CorbaException Some communication issue occurred trying to reach @em recipient
         * @throws daq::rc::Busy The command could not be executed because the @em recipient was busy
         * @throws daq::rc::NotAllowed The command could not be executed but its execution was not allowed (<em>e.g.</em>, by the Access Management system)
         * @throws daq::rc::ExecutionFailed Some error occurred during the execution of the command
         *
         * @see CommandSender::startStopApplications
         */
        void startApplications(const std::string& recipient, const std::vector<std::string>& applications) const;

        /**
         * @brief It asks a segment's controller to stop some child applications.
         *
         * @note This command has no effect if sent to a leaf application.
         *
         * @param recipient The controller asked to execute the command
         * @param applications The child applications to be stopped
         *
         * @throws daq::rc::IPCLookup The @em recipient cannot be found (<em>i.e.</em>, it is not published in IPC)
         * @throws daq::rc::BadCommand The sent command is not valid or ill-formed
         * @throws daq::rc::CorbaException Some communication issue occurred trying to reach @em recipient
         * @throws daq::rc::Busy The command could not be executed because the @em recipient was busy
         * @throws daq::rc::NotAllowed The command could not be executed but its execution was not allowed (<em>e.g.</em>, by the Access Management system)
         * @throws daq::rc::ExecutionFailed Some error occurred during the execution of the command
         *
         * @see CommandSender::startStopApplications
         */
        void stopApplications(const std::string& recipient, const std::vector<std::string>& applications) const;

        /**
         * @brief It asks a segment's controller to restart some child applications.
         *
         * @note This command has no effect if sent to a leaf application.
         *
         * @param recipient The controller asked to execute the command
         * @param applications The child applications to be restarted
         *
         * @throws daq::rc::IPCLookup The @em recipient cannot be found (<em>i.e.</em>, it is not published in IPC)
         * @throws daq::rc::BadCommand The sent command is not valid or ill-formed
         * @throws daq::rc::CorbaException Some communication issue occurred trying to reach @em recipient
         * @throws daq::rc::Busy The command could not be executed because the @em recipient was busy
         * @throws daq::rc::NotAllowed The command could not be executed but its execution was not allowed (<em>e.g.</em>, by the Access Management system)
         * @throws daq::rc::ExecutionFailed Some error occurred during the execution of the command
         *
         * @see CommandSender::startStopApplications
         */
        void restartApplications(const std::string& recipient, const std::vector<std::string>& applications) const;

        /**
         * @brief It asks the remote application to publish its information.
         *
         * If it is sent to a segment's controller, then the controller will execute UserRoutines::publishAction() on the user code.
         * If it is sent to a leaf application, then the application will execute Controllable::publish() on all the defined Controllable.
         *
         * @param recipient The application asked to execute the command
         *
         * @throws daq::rc::IPCLookup The @em recipient cannot be found (<em>i.e.</em>, it is not published in IPC)
         * @throws daq::rc::BadCommand The sent command is not valid or ill-formed
         * @throws daq::rc::CorbaException Some communication issue occurred trying to reach @em recipient
         * @throws daq::rc::Busy The command could not be executed because the @em recipient was busy
         * @throws daq::rc::NotAllowed The command could not be executed but its execution was not allowed (<em>e.g.</em>, by the Access Management system)
         * @throws daq::rc::ExecutionFailed Some error occurred during the execution of the command
         */
        void publish(const std::string& recipient) const;

        /**
         * @brief It asks the remote application to change its "publish" interval.
         *
         * For segment's controllers that means to change the frequency of calls to UserRoutines::publishAction().
         * For leaf applications that means to change the frequency of calls to Controllable::publish() on all the defined Controllable.
         *
         * @note Any change in the frequency is volatile and the default value (as defined in the configuration) will be restored
         *       at the next CONFIGURE transition.
         *
         * @param recipient The application asked to execute the command
         * @param newInterval The new interval (in seconds). A value of 0 disables the timer.
         *
         * @throws daq::rc::IPCLookup The @em recipient cannot be found (<em>i.e.</em>, it is not published in IPC)
         * @throws daq::rc::BadCommand The sent command is not valid or ill-formed
         * @throws daq::rc::CorbaException Some communication issue occurred trying to reach @em recipient
         * @throws daq::rc::Busy The command could not be executed because the @em recipient was busy
         * @throws daq::rc::NotAllowed The command could not be executed but its execution was not allowed (<em>e.g.</em>, by the Access Management system)
         * @throws daq::rc::ExecutionFailed Some error occurred during the execution of the command
         */
        void changePublishInterval(const std::string& recipient, unsigned int newInterval) const;

        /**
         * @brief It asks the remote application to publish its statistics.
         *
         * If it is sent to a segment's controller, then the controller will execute UserRoutines::publishStatisticsAction() on the user code.
         * If it is sent to a leaf application, then the application will execute Controllable::publishFullStats() on all the defined Controllable.
         *
         * @param recipient The application asked to execute the command
         *
         * @throws daq::rc::IPCLookup The @em recipient cannot be found (<em>i.e.</em>, it is not published in IPC)
         * @throws daq::rc::BadCommand The sent command is not valid or ill-formed
         * @throws daq::rc::CorbaException Some communication issue occurred trying to reach @em recipient
         * @throws daq::rc::Busy The command could not be executed because the @em recipient was busy
         * @throws daq::rc::NotAllowed The command could not be executed but its execution was not allowed (<em>e.g.</em>, by the Access Management system)
         * @throws daq::rc::ExecutionFailed Some error occurred during the execution of the command
         */
        void publishStats(const std::string& recipient) const;

        /**
         * @brief It asks the remote application to change its "publish statistics" interval.
         *
         * For segment's controllers that means to change the frequency of calls to UserRoutines::publishStatisticsAction().
         * For leaf applications that means to change the frequency of calls to Controllable::publishFullStats() on all the defined Controllable.
         *
         * @note Any change in the frequency is volatile and the default value (as defined in the configuration) will be restored
         *       at the next CONFIGURE transition.
         *
         * @param recipient The application asked to execute the command
         * @param newInterval The new interval (in seconds). A value of 0 disables the timer.
         *
         * @throws daq::rc::IPCLookup The @em recipient cannot be found (<em>i.e.</em>, it is not published in IPC)
         * @throws daq::rc::BadCommand The sent command is not valid or ill-formed
         * @throws daq::rc::CorbaException Some communication issue occurred trying to reach @em recipient
         * @throws daq::rc::Busy The command could not be executed because the @em recipient was busy
         * @throws daq::rc::NotAllowed The command could not be executed but its execution was not allowed (<em>e.g.</em>, by the Access Management system)
         * @throws daq::rc::ExecutionFailed Some error occurred during the execution of the command
         */
        void changeFullStatsInterval(const std::string& recipient, unsigned int newInterval) const;

        /**
         * @brief It asks a segment's controller to ignore any error cause.
         *
         * @note This command has no effect if sent to a leaf application.
         *
         * @param recipient The controller asked to ignore any error cause
         *
         * @throws daq::rc::IPCLookup The @em recipient cannot be found (<em>i.e.</em>, it is not published in IPC)
         * @throws daq::rc::BadCommand The sent command cmd is not valid or ill-formed
         * @throws daq::rc::CorbaException Some communication issue occurred trying to reach @em recipient
         * @throws daq::rc::NotAllowed The command could not be executed but its execution was not allowed (<em>e.g.</em>, by the Access Management system)
         * @throws daq::rc::ExecutionFailed Some error occurred during the execution of the command
         */
        void ignoreError(const std::string& recipient) const;

        /**
         * @brief It asks a segment's controller to gracefully exit.
         *
         * @note This command has no effect if sent to a leaf application.
         *
         * @param recipient The controller asked to exit
         *
         * @throws daq::rc::IPCLookup The @em recipient cannot be found (<em>i.e.</em>, it is not published in IPC)
         * @throws daq::rc::BadCommand The sent command is not valid or ill-formed
         * @throws daq::rc::CorbaException Some communication issue occurred trying to reach @em recipient
         * @throws daq::rc::Busy The command could not be executed because the @em recipient was busy
         * @throws daq::rc::NotAllowed The command could not be executed but its execution was not allowed (<em>e.g.</em>, by the Access Management system)
         * @throws daq::rc::ExecutionFailed Some error occurred during the execution of the command
         */
        void exit(const std::string& recipient) const;

        /**
         * @brief It sets the ERS debug level of a remote application.
         *
         * @param recipient The name of the remote application
         * @param level The new debug level
         *
         * @throws daq::rc::IPCLookup The @em recipient cannot be found (<em>i.e.</em>, it is not published in IPC)
         * @throws daq::rc::CorbaException Some communication issue occurred trying to reach @em recipient
         */
        void debugLevel(const std::string& recipient, int level) const;

        /**
         * @brief It asks a segment's controller to set the error state.
         *
         * @note This command has no effect if sent to a leaf application.
         * @note This command is synchronous.
         *
         * @warning Reserved to the Expert System in order to set/clear the error state of a controller.
         *
         * @param recipient The name of the segment's controller
         * @param errorReasons List of causes for the controller to be in error (if empty the error state is cleared)
         *
         * @throws daq::rc::IPCLookup The @em recipient cannot be found (<em>i.e.</em>, it is not published in IPC)
         * @throws daq::rc::CorbaException Some communication issue occurred trying to reach @em recipient
         */
        void setError(const std::string& recipient, const std::vector<CommandSender::ErrorElement>& errorReasons) const;

        /**
         * @brief It asks a remote application to give back information about its status.
         *
         * @note This method is synchronous.
         *
         * @param recipient The name of the remote application
         * @return Object containing the application's information
         *
         * @throws daq::rc::IPCLookup The @em recipient cannot be found (<em>i.e.</em>, it is not published in IPC)
         * @throws daq::rc::CorbaException Some communication issue occurred trying to reach @em recipient
         * @throws daq::rc::BadCommand The sent command is not valid or ill-formed
         */
        std::unique_ptr<ApplicationStatusCmd> status(const std::string& recipient) const;

        /**
         * @brief It asks a segment's controller information about one of its child applications
         *
         * @warning If sent to a leaf application, this method always throws daq::rc::UnknowkChild
         *
         * @param recipient The name of the segment controller
         * @param childName The name of the controlle's child application
         *
         * @return Object containing the child application's information
         *
         * @throws daq::rc::IPCLookup The @em recipient cannot be found (<em>i.e.</em>, it is not published in IPC)
         * @throws daq::rc::CorbaException Some communication issue occurred trying to reach @em recipient
         * @throws daq::rc::BadCommand The sent command is not valid or ill-formed
         * @throws daq::rc::UnknowkChild @em childName is not a child of the controller
         */
        std::unique_ptr<ApplicationStatusCmd> childStatus(const std::string& recipient, const std::string& childName) const;

        /**
         * @brief It asks a remote application its ERS debug level.
         *
         * @param recipient The name of the remote application
         * @return The application's ERS debug level
         *
         * @throws daq::rc::IPCLookup The @em recipient cannot be found (<em>i.e.</em>, it is not published in IPC)
         * @throws daq::rc::CorbaException Some communication issue occurred trying to reach @em recipient
         */
        int debugLevel(const std::string& recipient) const;

    protected:
        // throws daq::rc::IPCLookup, daq::rc::BadCommand, daq::rc::CorbaException, daq::rc::Busy, daq::rc::NotAllowed, daq::rc::ExecutionFailed, daq::rc::InErrorState
        void execute(const std::string& recipient, const RunControlBasicCommand& cmd) const;

        /**
         * @brief It asks a segment's controller to update the status of one of its child.
         *
         * This method is usually called by child applications to notify their parent about changes in their status.
         *
         * @param recipient The name of the controller receiving the command
         * @param statusUpdateCmd Command containing all the details about the status of the child application
         *
         * @throws daq::rc::IPCLookup The @em recipient cannot be found (<em>i.e.</em>, it is not published in IPC)
         * @throws daq::rc::BadCommand The @em statusUpdateCmd is not valid or ill-formed
         * @throws daq::rc::CorbaException Some communication issue occurred trying to reach @em recipient
        */
        void updateChild(const std::string& recipient, const ApplicationStatusCmd& statusUpdateCmd) const;

        /**
         * @brief Get credentials in form of token or user name.
         *
         * @param userName Use if tokens are not enabled.
         * @return The credential for the operation.
         *
         * @throws daq::tokens::CannotAcquire() if tokens are enabled, but no token can be acquired.
         */
    std::string credentials(std::string_view userName, std::string_view recipient) const;

    private:
        friend class ItemCtrlImpl;
        friend class RunController;

        const IPCPartition m_partition;
        const std::string m_user_name;
        const std::string m_host_name;
        const std::string m_app_id;
};

}}

#endif /* DAQ_RC_COMMANDSENDER_H_ */
