/*
 * FSMBase.h
 *
 *  Created on: Sep 7, 2012
 *      Author: avolio
 */

/**
 * @file RunControl/FSM/Details/FSMBase.h
 */

#ifndef DAQ_RC_FSMBASE_H_
#define DAQ_RC_FSMBASE_H_

#include "RunControl/FSM/FSMDefs.h"

#include <memory>

namespace std {
    class exception;
}

namespace daq { namespace rc {

class TransitionCmd;
class RunControlTransitionActions;
class UserRoutines;

/**
 * @brief Class implementing FSM's functionality offered to FSM's internals (call-back functions,
 *        states, events, etc)
 */
class FSMBase {
    public:
        /**
         * @brief Constructor
         */
        FSMBase();

        /**
         * @brief Destructor
         */
        virtual ~FSMBase();

        /**
         * @brief It sets actions to be executed during the state transitions
         *
         * @param ctrl Reference to the object implementing actions to be executed during the state transitions
         */
        void controller(const std::shared_ptr<RunControlTransitionActions>& ctrl);

        /**
         * @brief It sets actions to be executed before a state transition is initiated
         *
         * @param userFunctions Reference to the object implementing actions to be executed before a state transition is initiated
         */
        void userRoutines(const std::shared_ptr<UserRoutines>& userFunctions);

        /**
         * @brief It returns a reference to the object implementing actions to be executed during the state transitions
         *
         * @return A reference to the object implementing actions to be executed during the state transitions
         */
        RunControlTransitionActions& controller() const;

        /**
         * @brief It returns a reference to the object implementing actions to be executed before a state transition is initiated
         *
         * @return A reference to the object implementing actions to be executed before a state transition is initiated
         */
        UserRoutines& userRoutines() const;

        /**
         * @brief It returns @em true when performing a transition moving away from the FSM initial state
         *
         * @return @em true when performing a transition moving away from the FSM initial state
         */
        bool goingUp() const;

        /**
         * @brief It sets whether the current transition is moving away from the FSM initial state
         *
         * @param up @em true when performing a transition moving away from the FSM initial state
         */
        void goingUp(bool up);

        /**
         * @brief It notifies all subscribers that a transition is initiated
         *
         * @param trCmd The command representing the transition
         */
        void notifyTransitioning(const TransitionCmd& trCmd);

        /**
         * @brief It registers a call-back function to be executed when a transition is initiated
         *
         * @param cb The call-back function
         */
        void registerTransitioningCallback(const FSMDefs::OnTransitioningSlotType& cb);

        /**
         * @brief It notifies all subscribers that a transition has been completed normally
         *
         * @param trCmd The command representing the transition
         */
        void notifyTransitionDone(const TransitionCmd& trCmd);

        /**
         * @brief It registers a call-back function to be executed when a transition has been completed normally
         *
         * @param cb The call-back function
         */
        void registerTransitionDoneCallback(const FSMDefs::OnTransitionDoneSlotType& cb);

        /**
         * @brief It notifies all subscribers that no transition will be executed for the received command
         *
         * @param trCmd The command representing the transition
         */
        void notifyNoTransition(const TransitionCmd& trCmd);

        /**
         * @brief It registers a call-back function to be executed when there is no transition corresponding to a given FSM command
         *
         * @param cb The call-back function
         */
        void registerNoTransitionCallback(const FSMDefs::OnNoTransitionSlotType& cb);

        /**
         * @brief It notifies all subscribers that a transition has been stopped because of an exception
         *
         * @param trCmd The command representing the transition
         * @param ex Reference to the exception
         */
        void notifyTransitionException(const TransitionCmd& trCmd, std::exception& ex);

        /**
         * @brief It registers a call-back function to be executed when a transition has been stopped because of an exception
         *
         * @param cb The call-back function
         */
        void registerTransitionExceptionCallback(const FSMDefs::OnTransitionExceptionSlotType& cb);

    private:
        std::shared_ptr<RunControlTransitionActions> m_ctrl;
        std::shared_ptr<UserRoutines> m_user;

        bool m_going_up;

        FSMDefs::OnTransitioning m_transitioning_sig;
        FSMDefs::OnTransitionDone m_transition_done_sig;
        FSMDefs::OnNoTransition m_no_transition_sig;
        FSMDefs::OnTransitionException m_transition_ex_sig;
};

}}

#endif /* DAQ_RC_FSMBASE_H_ */
