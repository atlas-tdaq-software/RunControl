/*
 * FSMActions.h
 *
 *  Created on: Sep 7, 2012
 *      Author: avolio
 */

/**
 * @file RunControl/FSM/FSMActions.h
 */

#ifndef DAQ_RC_FSMACTIONS_H_
#define DAQ_RC_FSMACTIONS_H_

#include "RunControl/FSM/FSMDefs.h"

#include <tuple>
#include <memory>
#include <vector>

namespace daq { namespace rc {

class TransitionCmd;
enum class FSM_STATE : unsigned int;
enum class FSM_COMMAND : unsigned int;

/**
 * @brief Pure virtual interface defining the FSM's functionality offered to controllers and leaf
 *        applications via the daq::rc::FSM class
 */
class FSMActions {
	public:
        /**
         * @brief Destructor
         */
		virtual ~FSMActions() {}

		/**
		 * @brief This method shall start the FSM moving it to its initial state
		 *
		 * @param isControllerRestarting @em true if the application is restarted after a crash
		 */
		virtual void start(bool isControllerRestarting) = 0;

		/**
		 * @brief This method shall interrupt any on-going transition
		 *
		 * @note The interrupted state shall be persistent and removed only on request
		 *
		 * @param shouldInterrupt If @em true the interrupted state is set
		 */
		virtual void interruptTransition(bool shouldInterrupt) = 0;

		/**
		 * @brief It shall return @em true if the FSM is already performing a state transition
		 *
		 * @return @em true if the FSM is already performing a state transition
		 */
		virtual bool busy() const = 0;

		/**
		 * @brief It shall return @em true if @em cmd is a valid command for the FSM
		 *
		 * @param cmd The command to check
		 * @return @em true if @em cmd is a valid command for the FSM
		 */
		virtual bool isCommandValid(FSM_COMMAND cmd) const = 0;

		/**
		 * @brief It shall return @em true if @em state is a valid state for the FSM
		 *
		 * @param state The state to check
		 * @return @em true if @em state is a valid state for the FSM
		 */
        virtual bool isStateValid(FSM_STATE state) const = 0;

        /**
         * @brief It shall return the current status of the FSM
         *
         * @return A tuple with the following information (in order): current state, destination state, last command
         */
		virtual std::tuple<FSM_STATE, FSM_STATE, FSM_COMMAND> status() const = 0;

		/**
		 * @brief It shall initiate an FSM transition
		 *
		 * @param trCommand The command representing the transition
		 * @return @em true if the transition is done, @em false if the FSM is already busy with a different transition
		 */
		virtual bool makeTransition(const std::shared_ptr<TransitionCmd>& trCommand) = 0;

		/**
		 * @brief It shall return all the commands that should be sent to the FSM in order to move from the @em from
		 *        state to the @em to state
		 *
		 * @param from The starting state
		 * @param to The ending state
		 * @return The set of commands
		 */
		virtual std::vector<FSM_COMMAND> transitions(FSM_STATE from, FSM_STATE to) const = 0;

		/**
		 * @brief It shall register a call-back to be executed when an FSM transition is started
		 *
		 * @param cb The call-back function
		 */
		virtual void registerOnTransitioningCallback(const FSMDefs::OnTransitioningSlotType& cb) = 0;

        /**
         * @brief It shall register a call-back to be executed when an FSM transition is completed normally
         *
         * @param cb The call-back function
         */
		virtual void registerOnTransitionDoneCallback(const FSMDefs::OnTransitionDoneSlotType& cb) = 0;

        /**
         * @brief It shall register a call-back to be executed when the FSM busy state changes
         *
         * @param cb The call-back function
         */
		virtual void registerOnFSMBusyCallback(const FSMDefs::OnFSMBusyType& cb) = 0;

        /**
         * @brief It shall register a call-back to be executed when there is no transition for the received
         *        FSM command
         *
         * @param cb The call-back function
         */
		virtual void registerOnNoTransitionCallback(const FSMDefs::OnNoTransitionSlotType& cb) = 0;

        /**
         * @brief It shall register a call-back to be executed when an FSM transition is stopped because of an exception
         *
         * @param cb The call-back function
         */
		virtual void registerOnTransitionExceptionCallback(const FSMDefs::OnTransitionExceptionSlotType& cb) = 0;
};

}}

#endif /* DAQ_RC_FSMACTIONS_H_ */
