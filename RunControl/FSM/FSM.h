/*
 * FSM.h
 *
 *  Created on: Aug 9, 2012
 *      Author: avolio
 */

/**
 * @file RunControl/FSM/FSM.h
 */

#ifndef DAQ_RC_FSM_H_
#define DAQ_RC_FSM_H_

#include "RunControl/FSM/FSMActions.h"
#include "RunControl/FSM/FSMDefs.h"

#include <boost/preprocessor/cat.hpp>

#include <tbb/concurrent_unordered_map.h>

#include <memory>
#include <mutex>
#include <atomic>
#include <tuple>
#include <set>
#include <shared_mutex>

// Small macros to avoid code repetition

/**
 * @def DECLARE_SEND_FSM_EVENT(FSM_EVENT)
 *
 * Declaration of a function whose signature is <b>void send_FSM_EVENT(const std::shared_ptr<TransitionCmd>& trCmd)</b>
 */
#define DECLARE_SEND_FSM_EVENT(FSM_EVENT) \
void BOOST_PP_CAT(send_, FSM_EVENT)(const std::shared_ptr<TransitionCmd>& trCmd); \

/**
 * @def IMPLEMENT_SEND_FSM_EVENT(FSM_EVENT)
 *
 * Implementation of a function whose signature is <b>void send_FSM_EVENT(const std::shared_ptr<TransitionCmd>& trCmd)</b>
 */
#define IMPLEMENT_SEND_FSM_EVENT(FSM_EVENT) \
template<FSM_STATE InitialState> \
void BOOST_PP_CAT(FSM<InitialState>::send_, FSM_EVENT)(const std::shared_ptr<TransitionCmd>& trCmd) { \
    std::shared_ptr<typename FSM_Initial<InitialState>::FSM> pFSM = std::static_pointer_cast<typename FSM_Initial<InitialState>::FSM>(m_fsm); \
    if(pFSM->process_event(FSMEvent<FSM_COMMAND::FSM_EVENT>(trCmd)) == back::HANDLED_GUARD_REJECT) { \
        throw daq::rc::EventNotProcessed(ERS_HERE, trCmd->fsmCommand()); \
    } \
} \

/**
 * @def MAP_FSM_EVENT(FSM_EVENT)
 *
 * It maps an <b>FSM_COMMAND::FSM_EVENT</b> to the corresponding <b>std::bind(&FSM<InitialState>::send_FSM_EVENT), this, std::placeholders::_1)</b>
 * function.
 */
#define MAP_FSM_EVENT(FSM_EVENT) \
m_fsm_cmd_map.insert(std::make_pair(FSM_COMMAND::FSM_EVENT, std::bind(&FSM<InitialState>::BOOST_PP_CAT(send_, FSM_EVENT), this, std::placeholders::_1))); \

// Forward declarations
namespace std {
    class exception;
}

namespace daq { namespace rc {

class RunControlTransitionActions;
class UserRoutines;
class TransitionCmd;
enum class FSM_STATE : unsigned int;
enum class FSM_COMMAND : unsigned int;

/**
 * @brief Main class describing the FSM
 * @tparam InitialState The FSM initial state (it can be daq::rc::FSM_STATE::NONE or daq::rc::FSM_STATE::INITIAL)
 *
 * @see daq::rc::FSM_Initial
 * @see daq::rc::FSM_Active
 *
 * @image html FSM_UML.jpg "The FSM: states and main transitions are shown"
 * @image latex FSM_UML.eps "The FSM: states and main transitions are shown" width=7cm
 */
template<FSM_STATE InitialState>
class FSM : public FSMActions {
	public:
        /**
         * @brief Constructor
         *
         * @param ctrl Run Control actions to be executed during transitions
         * @param ur User actions to be executed before a transition is propagated to children (user actions have a meaning only
         *           for controllers and not for leaf applications)
         */
		FSM(const std::shared_ptr<RunControlTransitionActions>& ctrl, const std::shared_ptr<UserRoutines>& ur);

		/**
		 * @brief Constructor
		 *
		 * @param ctrl Run Control actions to be executed during transitions
		 */
        explicit FSM(const std::shared_ptr<RunControlTransitionActions>& ctrl);

        /**
         * @brief Destructor
         */
		~FSM();

		/**
		 * @brief Copy constructor: deleted
		 */
		FSM(const FSM&) = delete;

        /**
         * @brief Move constructor: deleted
         */
        FSM(FSM&&) = delete;

        /**
         * @brief Copy assignment operator: deleted
         */
        FSM& operator=(const FSM&) = delete;

        /**
         * @brief Move assignment operator: deleted
         */
        FSM& operator=(FSM&&) = delete;

        /**
         * @brief It starts the FSM operations (the FSM enters its initial state)
         *
         * @param isRestart @em true if the controller is restarting after a crash
         */
		void start(bool isRestart) override;

		/**
		 * @brief It interrupts the current transition
		 *
		 * The interrupted state will persist until it is not explicitly disabled
		 *
		 * @param shouldInterrupt @em true if the interrupted state should be enabled
		 */
		void interruptTransition(bool shouldInterrupt) override;

		/**
		 * @brief It performs the transition described in the @em cmd command
		 *
		 * @note This method returns only once the transition has been completed (unless the
		 *       FSM is already busy; in that case this method returns immediately)
		 *
		 * @note If the @em cmd command is valid but it does not correspond to any transition
		 *       given the current FSM state, then no exceptions are thrown but the caller can
		 *       receive a notification via the call-back eventually registered with
		 *       FSM::registerOnNoTransitionCallback
		 *
		 * @attention @em cmd must at least define the corresponding daq::rc::FSM_COMMAND
		 *
		 * @param cmd The command describing the transition to be done
		 * @return @em false if the transition cannot be executed because the FSM is already busy
		 *
		 * @throws daq::rc::TransitionNotAllowed The corresponding FSM command is not valid (<em>i.e.</em>, it is daq::rc::FSM_COMMAND::INIT_FSM
		 *                                       or daq::rc::FSM_COMMAND::STOP_FSM)
		 * @throws daq::rc::EventNotProcessed The command is rejected by the FSM
		 * @throws daq::rc::BadCommand The transition command described by @em cmd is not a valid daq::rc::FSM_COMMAND
		 *
		 * @see FSM::isCommandValid
		 */
		bool makeTransition(const std::shared_ptr<TransitionCmd>& cmd) override;

        /**
         * @brief It returns @em true if the FSM is already performing a state transition
         *
         * @return @em true if the FSM is already performing a state transition
         */
        bool busy() const override;

        /**
         * @brief It checks if @em cmd is a valid command for the FSM
         *
         * Here valid means that the command is part of the set of allowed FSM events:
         * @li daq::rc::FSM_COMMAND::INIT_FSM and daq::rc::FSM_COMMAND::STOP_FSM are not valid because reserved for initialization
         *     and termination of the FSM;
         * @li Other commands may not be valid because referring to transitions that do not exist given an initial state
         *     (<em>e.g.</em>, daq::rc::FSM_COMMAND::SHUTDOWN is not valid for an FSM whose initial state is daq::rc::FSM_STATE::INITIAL)
         *
         * @note This command may be used before calling FSM::makeTransition when an early detection of a bad
         *       command is needed.
         *
         * @note If the command is part of the set of allowed FSM events but the corresponding transition cannot be performed
         *       given the current state (<em>i.e.</em>, trying to perform the daq::rc::FSM_COMMAND::START in the daq::rc:FSM_STATE::INITIAL
         *       state), then this method will return @em true anyway.
         *
         * @param cmd The FSM command
         * @return @em true if the FSM command is valid
         */
        bool isCommandValid(FSM_COMMAND cmd) const override;

        /**
         * @brief It returns the set of valid commands for this FSM (it may depend on the FSM initial state)
         *
         * @return The set of valid commands for this FSM
         */
        const std::set<FSM_COMMAND>& commands() const;

        /**
         * @brief It checks if @em state is a valid state for this FSM
         *
         * @param state The FSM state to verify
         * @return @em true if the state is valid
         */
        bool isStateValid(FSM_STATE state) const override;

        /**
         * @brief It returns the set of valid states for this FSM (it may depend on the FSM initial state)
         *
         * @return The set of valid states for this FSM
         */
        const std::set<FSM_STATE>& states() const;

        /**
         * @brief It returns the current status of the FSM
         *
         * @return A tuple representing the FSM status; in order: current state, destination state, last command
         */
        std::tuple<FSM_STATE, FSM_STATE, FSM_COMMAND> status() const override;

        /**
         * @brief It returns the ordered list of transition that have to be performed in order to move from @em from to @em to
         *
         * @param from The starting state
         * @param to The ending state
         * @return The ordered list of transition that have to be performed in order to move from @em from to @em to
         *
         * @throws daq::rc::FSMPathError The two states cannot be linked
         */
        std::vector<FSM_COMMAND> transitions(FSM_STATE from, FSM_STATE to) const override;

        /**
         * @brief It registers a call-back to be executed any time the busy state of the FSM changes
         *
         * @param cb The call-back function
         */
        void registerOnFSMBusyCallback(const FSMDefs::OnFSMBusyType& cb) override;

        /**
         * @brief It registers a call-back to be called when a state transition begins
         *
         * Even if the "busy" call-back is received, there is no guarantee that this call-back will be executed
         * as well; indeed the event may be rejected by the FSM and then the "no transition" call-back will be
         * executed followed by the "no more busy" one.
         *
         * @param cb The call-back
         */
        void registerOnTransitioningCallback(const FSMDefs::OnTransitioningSlotType& cb) override;

        /**
         * @brief It registers a call-back to be called when a transition is successfully done
         *
         * @attention This call-back is not called when the transition is aborted because some
         *            exception is thrown (in this case the "transition exception" call-back is called)
         *
         * @param cb The call-back
         */
		void registerOnTransitionDoneCallback(const FSMDefs::OnTransitionDoneSlotType& cb) override;

        /**
         * @brief It registers a call-back to be called when there is no transition corresponding to
         *        a defined event given the current FSM state.
         *
         * @param cb The call-back
         */
		void registerOnNoTransitionCallback(const FSMDefs::OnNoTransitionSlotType& cb) override;

        /**
         * @brief It registers a call-back to be called when some exception is thrown during the execution
         *        of a transition's action.
         *
         * @param cb The call-back
         */
		void registerOnTransitionExceptionCallback(const FSMDefs::OnTransitionExceptionSlotType& cb) override;

	protected:
		/**
		 * @brief It indicates that a transition is started
		 * @param trCmd The transition command
		 */
        void transitioning(const TransitionCmd& trCmd);

        /**
         * @brief It indicates that a transition has been completed normally
         * @param trCmd The transition command
         */
        void transitionDone(const TransitionCmd& trCmd);

        /**
         * @brief It indicates that a transition has not been executed
         * @param trCmd The transition command
         */
        void noTransition(const TransitionCmd& trCmd);

        /**
         * @brief It indicates that a transition has been stopped because of an exception
         * @param trCmd The transition command
         * @param ex The exception
         */
        void transitionException(const TransitionCmd& trCmd, std::exception& ex);

	private:
        /**
         * @brief It fills the map associating a daq::rc::FSM_COMMAND to the corresponding function to be called in order
         *        to send the proper corresponding event to the FSM
         */
        void fillCommandMap();

        DECLARE_SEND_FSM_EVENT(INITIALIZE)
        DECLARE_SEND_FSM_EVENT(CONFIGURE)
        DECLARE_SEND_FSM_EVENT(CONFIG)
        DECLARE_SEND_FSM_EVENT(CONNECT)
        DECLARE_SEND_FSM_EVENT(START)
        DECLARE_SEND_FSM_EVENT(STOP)
        DECLARE_SEND_FSM_EVENT(STOPROIB)
        DECLARE_SEND_FSM_EVENT(STOPDC)
        DECLARE_SEND_FSM_EVENT(STOPHLT)
        DECLARE_SEND_FSM_EVENT(STOPRECORDING)
        DECLARE_SEND_FSM_EVENT(STOPGATHERING)
        DECLARE_SEND_FSM_EVENT(STOPARCHIVING)
        DECLARE_SEND_FSM_EVENT(DISCONNECT)
        DECLARE_SEND_FSM_EVENT(UNCONFIG)
        DECLARE_SEND_FSM_EVENT(UNCONFIGURE)
        DECLARE_SEND_FSM_EVENT(SHUTDOWN)
        DECLARE_SEND_FSM_EVENT(USERBROADCAST)
        DECLARE_SEND_FSM_EVENT(RELOAD_DB)
        DECLARE_SEND_FSM_EVENT(RESYNCH)
        DECLARE_SEND_FSM_EVENT(NEXT_TRANSITION)
        DECLARE_SEND_FSM_EVENT(SUB_TRANSITION)

	private:
        mutable std::mutex m_fsm_mutex;
        mutable std::shared_mutex m_state_mutex;

        tbb::concurrent_unordered_map<FSM_COMMAND, std::function<void (const std::shared_ptr<TransitionCmd>&)>> m_fsm_cmd_map;

        bool m_caught_exception;
		std::atomic<bool> m_busy;
		FSMDefs::OnFSMBusy m_busy_sig;

		FSM_STATE m_current_state;
		FSM_STATE m_destination_state;
		FSM_COMMAND m_last_command;
		FSMDefs::OnTransitioning m_tr_sig;
		FSMDefs::OnTransitionDone m_tr_done_sig;
		FSMDefs::OnNoTransition m_no_tr_sig;
		FSMDefs::OnTransitionException m_tr_ex_sig;

		const std::shared_ptr<RunControlTransitionActions> m_rc_actions;
        const std::shared_ptr<void> m_fsm;
};

}}

#endif /* DAQ_RC_FSM_H_ */
