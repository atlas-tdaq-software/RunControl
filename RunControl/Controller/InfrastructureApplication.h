/*
 * InfrastructureApplication.h
 *
 *  Created on: Nov 1, 2012
 *      Author: avolio
 */

/**
 * @file RunControl/Controller/InfrastructureApplication.h
 */

#ifndef DAQ_RC_INFRASTRUCTUREAPPLICATION_H_
#define DAQ_RC_INFRASTRUCTUREAPPLICATION_H_

#include "RunControl/Controller/Application.h"

class Configuration;
namespace daq { namespace core {
	class Partition;
	class BaseApplication;
}}

namespace daq { namespace rc {

/**
 * @brief Class representing a child infrastructure application
 */
class InfrastructureApplication : public Application {
	public:
        /**
         * @brief Constructor
         *
         * Back-up hosts are properly computed.
         *
         * @param db The ::Configuration object
         * @param p The daq::core::Partition object
         * @param appConfig The daq::core::BaseApplication object describing this application
         * @param isParentRoot @em true if the parent of this application is the RootControler
         *
         * @throws daq::rc::ConfigurationIssue Error accessing information from the configuration database
         */
		InfrastructureApplication(Configuration& db,
		                          const daq::core::Partition& p,
		                          const daq::core::BaseApplication& appConfig,
		                          bool isParentRoot = false);


        /**
         * @brief Copy constructor: deleted
         */
		InfrastructureApplication(const InfrastructureApplication&) = delete;

        /**
         * @brief Move constructor: deleted
         */
		InfrastructureApplication(InfrastructureApplication&&) = delete;

        /**
         * @brief Copy assignment operator: deleted
         */
		InfrastructureApplication& operator=(const InfrastructureApplication&) = delete;

        /**
         * @brief Move assignment operator: deleted
         */
		InfrastructureApplication& operator=(InfrastructureApplication&&) = delete;

        /**
         * @brief Destructor
         */
		virtual ~InfrastructureApplication() noexcept;

		/**
		 * @brief It returns daq::rc::ApplicationType::INFRASTRUCTURE
		 *
		 * @return daq::rc::ApplicationType::INFRASTRUCTURE
		 */
		virtual ApplicationType type() const noexcept;
};

}}

#endif /* DAQ_RC_INFRASTRUCTUREAPPLICATION_H_ */
