/*
 * InformationPublisher.h
 *
 *  Created on: Jan 31, 2013
 *      Author: avolio
 */

/**
 * @file RunControl/Controller/InformationPublisher.h
 */

#ifndef DAQ_RC_INFORMATIONPUBLISHER_H_
#define DAQ_RC_INFORMATIONPUBLISHER_H_

#include "RunControl/Controller/ApplicationSelfInfo.h"

#include <rc/TestInfo.h>

#include <string>
#include <map>
#include <memory>
#include <functional>
#include <utility>
#include <vector>

namespace daq { namespace es { namespace rc {
    class ExpertSystemInterface;
    struct StateInfo;
    struct ProcessInfo;
    struct TestInfo;
}}}

namespace daq { namespace rc {

class ThreadPool;
class ApplicationSVInfo;
enum class ApplicationType : unsigned int;

/**
 * @brief Component used to publish information to the IS servers and notify the Expert System
 *
 * Methods of this class publish information in an asynchronous way unless differently mentioned.
 */
class InformationPublisher {
    public:
        /**
         * @brief Enumeration used to identify the destination of the information to be published
         */
        typedef enum Destinations {
            IS_SERVER = 1,    //!< IS_SERVER - Information is published to IS servers only
            EXPERT_SYSTEM = 2,//!< EXPERT_SYSTEM - Information is passed to the Expert System only
            ALL = 3           //!< ALL - The information is published to both IS servers and the Expert System
        } Destinations_t;

        /**
         * @brief Typedef for a pair describing the status of the controller
         *
         * The first boolean element of the pair indicates whether the controller is
         * exiting or not.
         */
        typedef std::pair<bool, ApplicationSelfInfo> ControllerInfo;
        typedef std::function<ControllerInfo ()> ControllerInfoFn;

    public:
        /**
         * @brief Constructor
         * @param fn Function returning information about the status of the controller
         *
         * @throws daq::rc::ThreadPoolError Failure in creating the internal thread pools
         */
        InformationPublisher(ControllerInfoFn fn);

        /**
         * @brief Destructor
         */
        ~InformationPublisher();

        /**
         * @brief Copy constructor: deleted
         */
        InformationPublisher(const InformationPublisher&) = delete;

        /**
         * @brief Move constructor: deleted
         */
        InformationPublisher(InformationPublisher&&) = delete;

        /**
         * @brief Copy assignment operator: deleted
         */
        InformationPublisher& operator=(const InformationPublisher&) = delete;

        /**
         * @brief Move assignment operator: deleted
         */
        InformationPublisher& operator=(InformationPublisher&&) = delete;

        /**
         * @brief It publishes the application information described by the daq::rc::ApplicationSelfInfo class
         *
         * @note This is used by the controller to publish information about its internal status and the
         *       status of child applications (run control applications and child controllers).
         *
         * @remark Each controller updates the IS information independently, while the ES is always notified
         *         via the parent controller.
         *
         * @param appName The application's name
         * @param actionTimeout The application's actions timeout
         * @param exitTimeout The application's exit timeout
         * @param info The application's information
         * @param dest It specifies where the information should be published to
         */
        void publish(const std::string& appName,
                     int actionTimeout,
                     int exitTimeout,
                     const ApplicationSelfInfo& info,
                     InformationPublisher::Destinations_t dest = InformationPublisher::ALL);

        /**
         * @brief It updates the ES about the status of several applications (bulk notification)
         *
         * @param appsInfo Map containing the application's information (the maps'key represents the
         *                 name of the application)
         */
        void publish(const std::map<std::string, ApplicationSelfInfo>& appsInfo);

        /**
         * @brief It updates the ES about the status of several applications (bulk notification)
         *
         * @param appsInfo Map containing the application's information (the maps'key represents the
         *                 name of the application)
         */
        void publish(const std::map<std::string, ApplicationSVInfo>& appsInfo);

        /**
         * @brief It publishes the application information described by the daq::rc::ApplicationSVInfo class
         *
         * @param appName The application's name
         * @param info The application's information
         * @param dest It specifies where the information should be published to
         */
        void publish(const std::string& appName,
                     const ApplicationSVInfo& info,
                     InformationPublisher::Destinations_t dest = InformationPublisher::ALL);

        /**
         * @brief It publishes the information about tests executed for an application
         *
         * @param appName The application's name
         * @param testInfo Information about the test global result and the failure actions
         *                 (needed when the information should be sent to the Expert System)
         * @param globalResult Global result of the tests
         *                     (needed when the information should be sent to the IS server)
         * @param components Results of single tests
         *                   (needed when the information should be sent to the IS server)
         * @param dest It Specifies where the information should be published to
         */
        void publish(const std::string& appName,
                     const daq::es::rc::TestInfo& testInfo,
                     const ::TestInfo& globalResult,
                     const std::vector< ::TestInfo>& components,
                     InformationPublisher::Destinations_t dest = InformationPublisher::ALL);

        /**
         * @brief It removes from the IS server the application's information of the daq::rc::ApplicationSVInfo kind
         *
         * @param appName The name of the application
         */
        void removeProcessInfo(const std::string& appName);

        /**
         * @brief It removes from the IS server the application's information of the daq::rc::ApplicationSelfInfo kind
         *
         * @param appName the name of the application
         */
        void removeRCInfo(const std::string& appName);

        /**
         * @brief It removes from the IS server any information about tests executed for an application
         *
         * @param appName The name of the application
         */
        void removeTestInfo(const std::string& appName);

        /**
         * @brief It notifies the Expert System that the configuration database is going to be reloaded
         *
         * @note This method is synchronous
         *
         * @throws daq::es::ESInterface::CannotNotify
         */
        void dbReloading();

        /**
         * @brief It notifies the Expert System that the configuration database has been reloaded
         *
         * @note This method is synchronous
         *
         * @throws daq::es::ESInterface::CannotNotify
         */
        void dbReloaded();

        /**
         * @brief It notifies the Expert System that the daq::rc::FSM_COMMAND::SHUTDOWN transition has been done
         *
         * @note This method is synchronous
         *
         * @throws daq::es::ESInterface::CannotNotify
         */
        void shutdownExecuted();

        /**
         * @brief It notifies the Expert System that a state transition has been completed
         *
         * @note This method is synchronous
         *
         * @param trCommand The name of the transition command
         * @param fsmState The name of the reached FSM state
         *
         * @throws daq::es::ESInterface::CannotNotify
         */
        void transitionDone(const std::string& trCommand, const std::string& fsmState);

    protected:
        /**
         * @brief It properly fills data structures to be sent to the ES
         *
         * @param from Object representing the application's status
         * @param to Data structure to be filled
         * @param cInfo Information about the controller's status
         */
        static void fillInfo(const ApplicationSelfInfo& from, daq::es::rc::StateInfo& to, const ControllerInfo& cInfo);

        /**
         * @brief It properly fills data structures to be sent to the ES
         *
         * @param from Object representing the application's status
         * @param to Data structure to be filled
         * @param cInfo Information about the controller's status
         */
        static void fillInfo(const ApplicationSVInfo& from, daq::es::rc::ProcessInfo& to, const ControllerInfo& cInfo);

    private:
        long m_self_info_time;
        const ControllerInfoFn m_info_fn;
        const std::shared_ptr<daq::es::rc::ExpertSystemInterface> m_es_interface;
        const std::unique_ptr<ThreadPool> m_esnotifier_tp;
        const std::unique_ptr<ThreadPool> m_rc_isupdater_tp;
        const std::unique_ptr<ThreadPool> m_setup_isupdater_tp;
};

}}

#endif /* DAQ_RC_INFORMATIONPUBLISHER_H_ */
