#ifndef DAQ_RC_ACTIVETIME_H_
#define	DAQ_RC_ACTIVETIME_H_

/**
 * @file RunControl/Controller/Root/ActiveTime.h
 */

#include "RunControl/Common/Clocks.h"

#include <rc/RunInfoNamed.h>

#include <string>
#include <memory>
#include <shared_mutex>


namespace daq { namespace rc {

class ScheduledThreadPool;

/**
 * @brief Simple class used to calculate the time the Root Controller is in the daq::rc::FSM_STATE::RUNNING state
 *
 * The time is regularly published in the @em RunParams.RunInfo IS server with the @em RunInfo type.
 */
class ActiveTime {
    public:
        /**
         * @brief Constructor
         *
         * @throws daq::rc::ThreadPoolError The thread taking care of the periodic update could not be started
         */
        ActiveTime();

        /**
         * @brief Copy constructor: deleted
         */
        ActiveTime(const ActiveTime&) = delete;

        /**
         * @brief Copy assignment operator: deleted
         */
        ActiveTime& operator=(const ActiveTime&) = delete;

        /**
         * @brief Move constructor: deleted
         */
        ActiveTime(ActiveTime&&) = delete;

        /**
         * @brief Move assignment operator: deleted
         */
        ActiveTime& operator=(ActiveTime&&) = delete;

        /**
         * @brief Destructor
         */
        ~ActiveTime();

        /**
         * @brief It perform some basic initialization
         *
         * @warning This method has to be called only once and before ActiveTime::start
         */
        void init();

        /**
         * @brief It starts the time counting and the regular publication of information to the IS server
         *
         * @warning The ActiveTime::init method must be called before this one
         *
         * @param continuing @em true if the time counting should continue from the last value or not (useful in case
         *                   the Root Controller is restarted)
         */
        void start(bool continuing);

        /**
         * @brief It stops the time counting and the regular publication of information to the IS server
         */
        void stop();

        /**
         * @brief It returns the time in seconds
         * @return The time in seconds
         */
        unsigned long get_activeTime() const;

    protected:
        /**
         * @brief Call-back function used to regularly update the information to the IS server
         *
         * @return Always @em true
         */
        bool update();

    private:
        mutable std::shared_mutex m_mutex;
        timepoint_t m_start_time;
        RunInfoNamed m_runinfo;
        std::unique_ptr<ScheduledThreadPool> m_timer;
        const static std::string RUN_INFO_SERVER;
};

}}

#endif /* DAQ_RC_ACTIVETIME_H_ */
