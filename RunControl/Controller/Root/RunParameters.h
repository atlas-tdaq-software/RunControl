#ifndef DAQ_RC_RUNPARAMETERS_H_
#define	DAQ_RC_RUNPARAMETERS_H_

/**
 * @file RunControl/Controller/Root/RunParameters.h
 */

#include <rn/rn.h>
#include <ers/ers.h>

#include <string>
#include <memory>
#include <atomic>

namespace daq { namespace rc {

class ActiveTime;

/**
 * @brief Utility class dealing with run parameters and the run number service
 */
class RunParameters {
    public:
        /**
         * @brief Constructor
         *
         * @param at The daq::rc::ActiveTime object used to count the running time
         */
        RunParameters(const std::shared_ptr<ActiveTime>& at);

        /**
         * @brief Copy constructor: removed
         */
        RunParameters(const RunParameters&) = delete;

        /**
         * @brief Copy assignment operator: removed
         */
        RunParameters& operator=(const RunParameters&) = delete;

        /**
         * @brief Move constructor: removed
         */
        RunParameters(RunParameters&&) = delete;

        /**
         * @brief Move assignment operator: removed
         */
        RunParameters& operator=(RunParameters&&) = delete;

        /**
         * @brief Destructor
         */
        ~RunParameters();

        /**
         * @brief It publishes initial values in the @em RunParams IS server
         *
         * If the IS server already contains the information, then that is checked-out
         *
         * @warning This method must be called before RunParameters::start
         */
        void init();

        /**
         * @brief It publishes Start Of Run information to IS and retrieves the run number from the RN service
         *
         * It additionally publishes the @em zero Luminosity Block
         *
         * @throws daq::rc::RunNumber Some error occurred retrieving the run number
         */
        void start();

        /**
         * @brief It updates the Run Parameters and writes the End Of Run parameters to IS
         */
        void stop();

        /**
         * @brief It returns the current run number
         *
         * @return The current run number
         */
        unsigned long getRunNumber() const;

        /**
         * @brief It re-calculates the detector mask (e.g., after a reload of the database)
         *
         * The information in the @em RunParams IS server is updated
         */
        void reloadDetectorMask();

        /**
         * @brief It sets an error state flag in case a run is not stopped properly
         *
         * @param error If @em true is sets the error state
         */
        void setErrorState(bool error);

    protected:
        /**
         * @brief It returns the default run type
         *
         * The default run type is "Physics" or the first one of the list defined in the Partition
         *
         * @return The default run type
         */
        std::string getDefaultRunType() const;

        /**
         * @brief It calculates the detector mask
         *
         * @return The detector mask (in its string representation)
         */
        std::string getDetectorMask() const;

    private:
        struct RnDeleter {
            void operator()(::tdaq::RunNumber* rn) const {
                try {
                    // It is important to call 'close()' before the destructor
                    // because the destructor may throw...
                    if(rn != nullptr) {
                        rn->close();
                    }
                }
                catch(tdaq::rn::Exception& ex) {
                    ers::warning(ex);
                }

                delete rn;
            }
        };

        std::atomic<bool> m_error_state;
        const std::shared_ptr<ActiveTime> m_active_time;
        std::unique_ptr< ::tdaq::RunNumber, RnDeleter> m_rn_srvc;
        unsigned long m_rn;

        static const std::string RN_ENV_VAR;
        static const std::string FIXED_RN;
        static const std::string RUN_PARAMS;
        static const std::string RUN_PARAMS_SOR;
        static const std::string RUN_PARAMS_EOR;
        static const std::string RUN_PARAMS_DQ;
        static const std::string LB_INFO_NAME;
};

}}

#endif /* DAQ_RC_RUNPARAMETERS_H_ */
