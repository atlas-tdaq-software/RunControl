/*
 * RootController.h
 *
 *  Created on: Dec 10, 2012
 *      Author: avolio
 */

/**
 * @file RunControl/Controller/Root/RootController.h
 */

#ifndef DAQ_RC_ROOTCONTROLLER_H_
#define DAQ_RC_ROOTCONTROLLER_H_

#include "RunControl/Controller/RunController.h"

#include <ers/IssueReceiver.h>
#include <config/Configuration.h>
#include <ResourceManager/RM_Client.h>
#include <is/infodictionary.h>

#include <vector>
#include <string>
#include <memory>
#include <shared_mutex>

class ConfigurationChange;

namespace ers {
    class Issue;
}

namespace daq { namespace rc {

class Controller;
class UserRoutines;
class FSMController;
class ApplicationSelfInfo;
class ApplicationStatusCmd;
enum class FSM_STATE : unsigned int;

/**
 * @brief Class implementing the functionality of the Root Controller
 *
 * The Root Controller is a controller augmented with specific UserRoutines and actions
 * when some transitions are completed.
 */
class RootController : public RunController, public ers::IssueReceiver {
    public:
        /**
         * @brief Copy constructor: deleted
         */
        RootController(const RootController&) = delete;

        /**
         * @brief Move constructor: deleted
         */
        RootController(RootController&&) = delete;

        /**
         * @brief Copy assignment operator: deleted
         */
        RootController& operator=(const RootController&) = delete;

        /**
         * @brief Move assignment operator: deleted
         */
        RootController& operator=(RootController&&) = delete;

        /**
         * @brief Destructor
         */
        ~RootController() noexcept;

        /**
         * @brief It creates and starts the Root Controller
         *
         * This method blocks and returns only when the Root Controller is asked to exit
         *
         * @param name Name of the Root Controller
         * @param partition Name of the partition
         * @param segment Name of the segment
         *
         * @throws daq::rc::ControllerInitializationFailed The Root Controller could not be created or initialized
         */
        static void createAndRun(const std::string& name,
                                 const std::string& partition,
                                 const std::string& segment);

    protected:
        /**
         * @brief Called on reception of ERS messages
         *
         * @param issue The ERS issue
         */
        void receive(const ers::Issue& issue) override;

        /**
         * @brief It returns the name of the Master Trigger
         *
         * @return The name of the Master Trigger (it can be empty if the Master Trigger is not defined)
         */
        std::string getMasterTrigger() const;

        /**
         * @brief Executed when the database is reloaded (call-back from the configuration database)
         */
        void static configCallback(const std::vector< ::ConfigurationChange*>&, void*);

        /**
         * @brief Constructor
         *
         * @param name Name of the Root Controller
         * @param partition Name of the partition
         * @param segment Name of the segment
         * @param ur UserRoutines to be executed
         *
         * @throws daq::rc::ControllerInitializationFailed The Root Controller could not be created or initialized
         */
        RootController(const std::string& name,
                       const std::string& partition,
                       const std::string& segment,
                       const std::shared_ptr<UserRoutines>& ur);

        /**
         * @brief Overridden to implement actions for transitions not having UserRoutines
         */
        void fsmTransitioning(const TransitionCmd& trCmd) override;

        /**
         * @brief Overridden to implement actions at the end of a transition that only the Root Controller
         *        should do (e.g., the start of triggers)
         */
        void fsmTransitionDone(const TransitionCmd& trCmd) override;

        /**
         * @brief Overridden to implement specific actions when a transition is stopped because of an
         *        exception
         */
        void fsmTransitionException(const TransitionCmd& trCmd, std::exception& ex) override;

        /**
         * @brief It performs initialization
         *
         * The FSM is initiated and the subscription on configuration changes is performed
         */
        void onInit() override;

        /**
         * @brief Overridden to only publish status information (the Root Controller has no parent)
         *
         * @note The RootController not only updates IS information, but it also informs the ES
         */
        void notifyStatusChange(const ApplicationSelfInfo& myInfo, int actionTimeout, int exitTimeout) override;

        /**
         * @brief Overridden to add cleanup actions specific to the Root Controller (e.g., remove ERS
         *        handler and subscription to configuration changes)
         */
        void onExit() override;

        /**
         * @brief Overridden to return always @em nullptr (the Root Controller has no parent)
         *
         * @return Always @em nullptr
         */
        std::unique_ptr<const ApplicationStatusCmd> getParentStatus() const override;

        /**
         * @brief It sets the name of the Master Trigger
         *
         * @param name The name of the Master Trigger
         */
        void setMasterTrigger(const std::string& name);

        /**
         * @brief It retrieves the name of the Master Trigger from the configuration
         *
         * @return The name of the Master Trigger (can be empty when no Master Trigger is defined)
         */
        std::string findMasterTrigger() const;

        /**
         * @brief It performs proper clean-up given the current FSM state
         *
         * @param currState The current FSM state
         */
        void cleanUp(FSM_STATE currState);

    private:
        static const std::string TRG_IS_SERVER;
        static const std::string ERS_STREAM_SUBSCRIPTION;
        static const std::string MAX_EV_MSG_TYPE;
        static const std::string START_TIME_INFO_IS_SERVER;
        static const std::string CONFIG_VERSION_ENV_NAME;

        mutable std::shared_mutex m_mutex;
        ::Configuration::CallbackId m_config_cbk;
        daq::rmgr::RM_Client m_rm_client;
        std::string m_mt_name;
        ::ISInfoDictionary m_is_dict;
};

}}

#endif /* DAQ_RC_ROOTCONTROLLER_H_ */
