/*
 * DVSController.h
 *
 *  Created on: Nov 28, 2013
 *      Author: avolio
 */

/**
 * @file RunControl/Controller/DVSController.h
 */

#ifndef DAQ_RC_DVSCONTROLLER_H_
#define DAQ_RC_DVSCONTROLLER_H_

#include "RunControl/Common/ObjectPool.h"
#include "RunControl/Controller/Application.h"

#include <tmgr/tmresult.h>
#include <dvs/manager.h>

#include <string>
#include <memory>
#include <vector>
#include <mutex>
#include <future>
#include <shared_mutex>


class Configuration;
namespace daq { namespace dvs {
    class Component;
    struct Result;
}}

namespace daq { namespace tm {
    struct ComponentResult;
}}

namespace daq { namespace rc {

class ApplicationList;
class InformationPublisher;

/**
 * @brief Class taking care of verifying the functionality of the applications controlled by the controller
 *
 * It uses the DVS library to execute tests for the applications.
 */
class DVSController {
    public:
        /**
         * @brief Constructor
         *
         * @param appList The list of applications managed by the controller
         * @param infoPub Object used to publish information about the executed tests
         */
        DVSController(const std::shared_ptr<ApplicationList>& appList,
                      const std::shared_ptr<InformationPublisher>& infoPub);

        /**
         * @brief Destructor
         *
         * It asks DVS to stop all the tests and information is removed from IS
         */
        ~DVSController();

        /**
         * @brief Copy constructor: deleted
         */
        DVSController(const DVSController&) = delete;

        /**
         * @brief Copy assignment operator: deleted
         */
        DVSController& operator=(const DVSController&) = delete;

        /**
         * @brief Move constructor: deleted
         */
        DVSController(DVSController&&) = delete;

        /**
         * @brief Move assignment operator: deleted
         */
        DVSController& operator=(DVSController&&) = delete;

        /**
         * @brief It lets the DVS sub-system load the configuration
         *
         * Default information is published in IS so that it is available for all the clients
         *
         * @param config The ::Configuration object
         * @param partitionName The name of the partition
         * @param segmentName The name of the segment
         *
         * @throws daq::rc::ConfigurationIssue Some error occurred accessing the configuration data
         */
        void load(::Configuration& config, const std::string& partitionName, const std::string& segmentName);

        /**
         * @brief It lets the DVS sub-system to reload the configuration
         *
         * Method to be called after the configuration database has been reloaded.
         * IS information is initiated for new added applications and removed for no more existing applications
         *
         * @param addedApps Applications that have been added to the configuration
         * @param removedApps Applications that have been removed from the configuration
         *
         * @throws daq::rc::ConfigurationIssue Some error occurred accessing the configuration data
         */
        void reload(const Application::ApplicationSet& addedApps, const Application::ApplicationSet& removedApps);

        /**
         * @brief It asks the DVS sub-system to execute tests for an application
         *
         * Before asking DVS to start the tests, the local value of the last executed tests is reset to the default value.
         *
         * @param app The application to be tested
         * @param scope The scope the tests should be executed
         * @param level The level of the tests to be executed
         * @param flushCache If @em true the DVS sub-system is asked to reset the current test status before
         *                   starting the new tests (this is to by-pass cached values)
         *
         * @return A future representing the asynchronous execution of tests
         */
        std::future<daq::dvs::Result> testApplication(const std::shared_ptr<Application>& app,
                                                      const daq::dvs::TestScope& scope,
                                                      const daq::dvs::TestLevel& level,
                                                      bool flushCache = false);

        /**
         * @brief It asks the DVS sub-system to stop tests for an application
         *
         * @param appName The name of the application tests should be stopped
         */
        void stopTest(const std::string& appName);

        /**
         * @brief It resets the status of tests for an application to its default value
         *
         * @param app The application whose test status is reset
         *
         * @see Application::setTestStatus
         */
        void resetTestStatus(const std::shared_ptr<Application>& app);

    protected:
        /**
         * @brief It initializes the IS information for a set applications
         *
         * @param apps The set of applications
         */
        void initISInfo(const Application::ApplicationSet& apps);

        /**
         * @brief Call-back executed by the DVS sub-system any time tests for an application have been done
         *
         * @param result Structure holding the results of the tests executed for an application
         */
        void testCallback(std::vector<daq::dvs::Result> result);

        /**
         * @brief It collects the logs of a test executed for an application
         *
         * @param result Structure holding the result of a test
         * @return String representing the test's log
         */
        std::string getLog(const daq::tm::ComponentResult& result);

        /**
         * @brief It asks the DVS sub-system to stop all the tests being executed
         */
        void stopAllTests();

        /**
         * @brief Utility method to translate a daq::tmgr::TestResult to its string representation
         *
         * @param rc The result of a test
         * @return The string representation of @em rc
         */
        static std::string toString(daq::tmgr::TestResult rc);

    private:
        // The position of this is important: its deleter will
        // take care of removing the information about tests from
        // the IS server and has to be destroyed after the DVS manager
        // (in this way call-backs should not be received anymore)
        std::shared_ptr<void> m_is_cleaner;

        std::shared_mutex m_mutex;
        ObjectPool<std::mutex> m_app_sync;
        const std::shared_ptr<ApplicationList> m_app_list;
        const std::shared_ptr<InformationPublisher> m_info_pub;
        const std::unique_ptr<daq::dvs::Manager> m_dvs_manager;
        std::shared_ptr<daq::dvs::Component> m_top_component;
};

}}


#endif /* DAQ_RC_DVSCONTROLLER_H_ */
