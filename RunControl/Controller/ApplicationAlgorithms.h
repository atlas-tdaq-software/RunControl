/*
 * ApplicationAlgorithms.h
 *
 *  Created on: Dec 4, 2012
 *      Author: avolio
 */

/**
 * @file RunControl/Controller/ApplicationAlgorithms.h
 */

#ifndef DAQ_RC_APPLICATIONALGORITHMS_H_
#define DAQ_RC_APPLICATIONALGORITHMS_H_

#include "RunControl/Controller/Application.h"

#include <vector>
#include <string>
#include <map>
#include <memory>

namespace daq { namespace pmg {
    class Handle;
    class Process;
}}

namespace daq { namespace rc {

/**
 * @brief Algorithms used mainly by the daq::rc::ApplicationController class
 *
 * No instances of this class can be created; it just contains static methods.
 */
class ApplicationAlgorithms {
    public:
        /**
         * @brief Constructor: deleted
         */
        ApplicationAlgorithms() = delete;

        /**
         * @brief Destructor: deleted
         */
        ~ApplicationAlgorithms() = delete;

        /**
         * @brief It kills some left-over processes
         *
         * @param foundHandles Handles of the left-over processes
         *
         * @throws daq::rc::LeftoverKillingFailure Some error occurred killing processes
         */
        static void killLeftOver(const std::vector<std::shared_ptr<daq::pmg::Handle>>& foundHandles);

        /**
         * @brief It asks the PMG for the handle of process @em appName
         *
         * @note It is a caller's duty to properly delete the returned handle.
         *
         * @param appName The name of the application
         * @param hostName The name of the where the application should be running on
         * @param partName The name of the partition the application runs in
         *
         * @return A pointer to the application's handle or @em nullptr if the process has not been found
         *
         * @throws daq::rc::PMGError Some error occurred trying to contact the PMG
         */
        static std::unique_ptr<daq::pmg::Handle> getPmgHandle(const std::string& appName, const std::string& hostName, const std::string& partName);

        /**
         * @brief It returns a daq::pmg::Process for the process whose PMG handle is @em handle
         *
         * @note Never delete the returned daq::pmg::Process pointer.
         *
         * @param handle The process's handle
         *
         * @return A pointer to the daq::pmg::Process process or @em nullptr if the process has not been found
         *
         * @throws daq::rc::PMGError Some error occurred trying to contact the PMG
         */
        static std::shared_ptr<daq::pmg::Process> getPmgProcess(const daq::pmg::Handle& handle);

        /**
         * @brief Typedef for map where group of applications are organized in independent layers.
         */
        typedef std::map<unsigned int, std::vector<std::shared_ptr<Application>>> OrderedApplicationMap;

        /**
         * @brief It resolves start/shutdown dependencies for a set of applications
         *
         * Applications are organized in layers: all the applications belonging to the same layer are independent,
         * while layer <em>N - 1</em> depends on layer <em>N</em>. That means higher order layers have to be done first.
         *
         * @param appList The list of applications
         * @param isStart @em true if start dependencies have to be resolved, @em false for shutdown dependencies
         * @return An OrderedApplicationMap where applications are organized in dependent layers.
         *
         * @throws daq::rc::DependencyCycle Some dependency cycle exists and dependencies cannot be properly resolved. This usually
         *                                  indicates a configuration error.
         */
        static OrderedApplicationMap resolveDependencies(const Application::ApplicationSet& appList,
                                                         bool isStart);

        /**
         * @brief It returns @em true if the @em app application should be up in the @em state FSM state
         *
         * @param app The application to be checked
         * @param state The FSM state
         * @return @em true if the @em app application can be up in the @em state FSM state
         */
        static bool shouldRunInState(const Application& app, const std::string& state);

        /**
         * @brief It returns @em true if the @em app application can be up in the @em state FSM state
         *
         * @param app The application to be checked
         * @param state The FSM state
         * @return @em true if the @em app application can be up in the @em state FSM state
         */
        static bool canRunInState(const Application& app, const std::string& state);

        /**
         * @brief It returns @em true if the @em app application can be started given the defined context
         *
         * @param app The application to be checked
         * @param currentState The current FSM state
         * @param destinationState The FSM destination state (equal to @em current state if no FSM transitions are on-going)
         * @param isRestart @em true if @em app is currently being restarted
         * @return @em true if the @em app application can be started given the defined context
         */
        static bool canBeStarted(const Application& app,
                                 const std::string& currentState,
                                 const std::string& destinationState,
                                 bool isRestart);

        /**
         * @brief It returns @em true if the start of the @em app application has been completed.
         *
         * Pre-condition is that a start procedure has been initiated (it does not make any sense to call
         * this method if the application is steady in the daq::rc::APPLICATION_STATUS::UP or daq::rc::APPLICATION_STATUS::EXITED
         * status, for instance).
         *
         * The start procedure is considered completed when the application reaches the daq::rc::APPLICATION_STATUS::UP
         * or daq::rc::APPLICATION_STATUS::EXITED.
         *
         * If @em checkMembership is set to @em true, then this method returns @em true as soon as the
         * application is put out of membership.
         *
         * If @em isTransitioning is set to @em false, then the start is considered as done even if the
         * application is in the daq::rc::APPLIACTION_STATUS::FAILED status.
         *
         * @param app The application to be checked
         * @param checkMembership @em true if the application's membership should be taken into account
         * @param isTransitioning @em true if the application is being started in the context of an FSM transition
         *
         * @return @em true if the start of the @em app application has been completed
         */
        static bool isStartCompleted(const Application& app, bool checkMembership, bool isTransitioning);

        /**
         * @brief It returns @em true if the stop of the @em app application has been completed.
         *
         * Pre-condition is that a stop procedure has been initiated (it does not make any sense to call
         * this method if the application is steady in the daq::rc::APPLICATION_STATUS::FAILED, daq::rc::APPLICATION_STATUS::EXITED
         * or daq::rc::APPLICATION_STATUS::ABSENT status, for instance).
         *
         * The stop procedure is considered completed when the application reaches the daq::rc::APPLICATION_STATUS::EXITED,
         * daq::rc::APPLICATION_STATUS::FAILED or daq::rc::APPLICATION_STATUS::ABSENT status.
         *
         * If @em checkMembership is set to @em true, then this method returns @em true as soon as the
         * application is put out of membership.
         *
         * @param app The application to be checked
         * @param checkMembership @em true if the application's membership should be taken into account
         *
         * @return @em true if the stop of the @em app application has been completed
         */
        static bool isStopCompleted(const Application& app, bool checkMembership);
};

}}

#endif /* DAQ_RC_APPLICATIONALGORITHMS_H_ */
