/*
 * ItemCtrlImpl.h
 *
 *  Created on: Jun 10, 2013
 *      Author: avolio
 */

/**
 * @file RunControl/ItemCtrl/ItemCtrlImpl.h
 */

#ifndef DAQ_RC_ITEMCTRLIMPL_H_
#define DAQ_RC_ITEMCTRLIMPL_H_

#include "RunControl/ItemCtrl/ControllableDispatcher.h"
#include "RunControl/Common/CommandedApplication.h"
#include "RunControl/Common/Clocks.h"
#include "RunControl/Common/Algorithms.h"
#include "RunControl/FSM/FSMStates.h"

#include <owl/semaphore.h>

#include <string>
#include <memory>
#include <mutex>
#include <shared_mutex>
#include <tuple>
#include <vector>


namespace std {
    class exception;
}

namespace ers {
    class Issue;
    class IssueCatcherHandler;
}

namespace daq { namespace rc {

class CommandReceiver;
class CommandSender;
class ThreadPool;
class ScheduledThreadPool;
class TransitionCmd;
class ApplicationStatusCmd;
class ItemCtrlStatus;
class AMBridge;
class RunControlBasicCommand;
template<FSM_STATE State> class FSM;

/**
 * @brief Class implementing all the functionality of the daq::rc::ItemCtrl
 */
class ItemCtrlImpl : public CommandedApplication {
    public:
        /**
         * @brief Destructor
         */
        ~ItemCtrlImpl() noexcept;

        /**
         * @brief Copy constructor: deleted
         */
        ItemCtrlImpl(const ItemCtrlImpl&) = delete;

        /**
         * @brief Copy assignment operator: deleted
         */
        ItemCtrlImpl& operator=(const ItemCtrlImpl&) = delete;

        /**
         * @brief It starts executing an FSM transition
         *
         * @note This method waits for the transition to be completed only when the ItemCtrl
         *       is started in interactive mode
         *       \n
         * @note Called directly by the defined daq::rc::CommandSender
         *
         * @param ctx Context of the command sender
         * @param commandDescription String representation of the transition command (@see daq::rc::TransitionCmd)
         *
         * @throws daq::rc::BadCommand @em commandDescription does not represent a valid command
         * @throws daq::rc::InErrorState The transition could not be executed because the error status flag is set
         * @throws daq::rc::NotAllowed The AccessManager denied the execution of the transition
         */
        void makeTransition(const CommandedApplication::SenderContext& ctx,
                            const std::string& commandDescription) override;

        /**
         * @brief It starts the execution of a command (not a transition)
         *
         * @brief This method waits for the command to be completed only when the ItemCtrl
         *        is started in interactive mode
         *        \n
         * @note  Called directly by the defined daq::rc::CommandSender
         *
         * @param ctx Context of the command sender
         * @param commandDescription String representation of the transition command (@see daq::rc::RunControlBasicCommand)
         *
         * @throws daq::rc::BadCommand @em commandDescription does not represent a valid command
         * @throws daq::rc::NotAllowed The AccessManager denied the execution of the transition
         */
        void executeCommand(const CommandedApplication::SenderContext& ctx,
                            const std::string& commandDescription) override;

        /**
         * @brief Not currently supported
         *
         * To be properly implemented if the error state should be set externally (i.e., by the ES)
         *
         * @note Called directly by the defined daq::rc::CommandSender
         */
        void setError(const CommandedApplication::SenderContext& ctx,
                      const ErrorItems& errorItems) override;

        /**
         * @brief It returns an encoded string containing information about the status of the application
         * @return An encoded string containing information about the status of the application
         *
         * @note Called directly by the defined daq::rc::CommandSender
         *
         * @see daq::rc::ApplicationStatusCmd
         */
        std::string status() override;

        /**
         * @brief It always throws daq::rc::UnknownChild (leaf applications have no children)
         * @note Called directly by the defined daq::rc::CommandSender
         */
        std::string childStatus(const std::string&) override;

        /**
         * @brief Not supported: leaf applications have no children
         * @note Called directly by the defined daq::rc::CommandSender
         */
        void updateChild(const std::string&) override;

        /**
         * @brief It returns the name of the application
         * @return The name of the application
         */
        std::string id() override;

        /**
         * @brief It returns the name of the partition
         * @return The name of the partition
         */
        std::string partition() override;

        /**
         * @brief It returns the name of the parent controller
         * @return The name of the parent controller
         */
        const std::string& getParent() const;

        /**
         * @brief It returns the name of the segment
         * @return The name of the segment
         */
        const std::string& getSegment() const;

    protected:
        friend class ItemCtrl;
        friend class TransitionTask;
        friend class ExecuteCmdTask;

        /**
         * @brief Constructor
         *
         * @param name The name of the application
         * @param parent The name of the parent controller
         * @param segment The name of the segment
         * @param partition The name of the partition
         * @param ctrlList The list of daq::rc::Controllable items
         * @param isInteractive @em true if the interactive mode should be used
         * @param dispatcher The daq::rc::ControllableDispatcher to be used
         *
         * @throws daq::rc::ItemCtrlInitializationFailed Failed initializing internal data structures and/or services
         */
        ItemCtrlImpl(const std::string& name,
                     const std::string& parent,
                     const std::string& segment,
                     const std::string& partition,
                     const ControllableDispatcher::ControllableList& ctrlList,
                     bool isInteractive,
                     const std::shared_ptr<ControllableDispatcher>& dispatcher);

        /**
         * @brief Initialization method to be called before ItemCtrlImpl::run
         *
         * List of performed actions:
         * @li IPC is initialized;
         * @li The ERS handler is registered (used to trap FATAL issues);
         * @li FSM call-backs are registered;
         * @li IPC publication is done and the application is ready to receive commands (in case
         *     of interactive mode a command line receiver is started).
         *
         * @note The PMG synch is not created here but in daq::rc::ItemCtrlImpl::run
         *
         * @throws daq::rc::ItemCtrlInitializationFailed Initialization failed
         */
        void init();

        /**
         * @brief To be called when the application is ready to be started
         *
         * List of performed actions:
         * @li The PMG sync is created;
         * @li If not in interactive mode, then the parent FSM state is reached.
         *
         * This method blocks until daq::rc::ItemCtrlImpl::shutdown is called
         *
         * @pre ItemCtrlImpl::init has been called
         */
        void run();

        /**
         * @brief To be called when the application should be stopped
         *
         * The daq::rc::ItemCtrlImpl::run method returns and some additional clean-up
         * is performed.
         */
        void shutdown();

        /**
         * @brief It executes daq::rc::ItemCtrlImpl::publishInformation_cmdln in interactive mode,
         *        otherwise daq::rc::ItemCtrlImpl::publishInformation_corba
         */
        void publishInformation() const;

        /**
         * @brief It publishes information to IS and notifies the parent controller
         */
        void publishInformation_corba() const;

        /**
         * @brief It does nothing
         */
        void publishInformation_cmdln() const;

        // Call-backs from the FSM
        /**
         * @brief Executed when the FSM initiates a transition
         * @param trCmd The object representing the current transition
         *
         * @see daq::rc::FSM::registerOnTransitioningCallback
         */
        void fsmTransitioning(const TransitionCmd& trCmd);

        /**
         * @brief Executed when an FSM transition is completed normally
         * @param trCmd The object representing the current transition
         *
         * @see daq::rc::FSM::registerOnTransitionDoneCallback
         */
        void fsmTransitionDone(const TransitionCmd& trCmd);

        /**
         * @brief Executed when there is no transition corresponding to the FSM event
         * @param trCmd The object representing the current transition
         *
         * @see daq::rc::FSM::registerOnNoTransitionCallback
         */
        void fsmNoTransition(const TransitionCmd& trCmd);

        /**
         * @brief Executed when an FSM transition is terminated because of an exception
         * @param trCmd The object representing the current transition
         * @param ex The exception
         *
         * @see daq::rc::FSM::registerOnTransitionExceptionCallback
         */
        void fsmTransitionException(const TransitionCmd& trCmd, std::exception& ex);

        /**
         * @brief It implements regular "publish" actions
         * @return Always @em true
         *
         * @throws boost::thread_interrupted Thread interrupted
         */
        bool periodicProbe();

        /**
         * @brief It implements regular "publish stats" actions
         * @return Always @em true
         *
         * @throws boost::thread_interrupted Thread interrupted
         */
        bool periodicFullStats();

        /**
         * @brief ERS issue handler
         *
         * When a FATAL issue is received, the error state is set.
         *
         * @param ex The ERS issue
         */
        void issueCatcher(const ers::Issue& ex);

        /**
         * @brief It performs clean-up before exiting
         */
        void cleanUp();

        /**
         * @brief It returns all the sub-transitions defined for the application
         *
         * Sub-transition set is created taking into account all the parent segments, up to
         * the Root Controller.
         *
         * @return All the sub-transitions defined for the application
         *
         * @throws daq::rc::ConfigurationIssue
         */
        const Algorithms::SubtransitionMap& subTransitions() const;

        /**
         * @brief It notifies a command sender about the execution a given command
         *
         * @param cmd The completed command
         */
        void notifyCommandSender(const std::shared_ptr<RunControlBasicCommand>& cmd) const;

        /**
         * @brief It returns the probe interval (in seconds)
         *
         * @return The probe interval (in seconds)
         */
        static unsigned int probeInterval();

        /**
         * @brief It returns the full stats interval (in seconds)
         *
         * @return The full stats interval (in seconds)
         */
        static unsigned int fullStatsInterval();

        /**
         * @brief It returns the child-update command object to be sent to the parent controller
         *
         * @param appName The name of the application
         * @param status Reference to the object holding the application's status description
         *
         * @return The child-update command object to be sent to the parent controller
         */
        static std::unique_ptr<ApplicationStatusCmd> parentUpdateCmd(const std::string& appName,
                                                                     const ItemCtrlStatus& status);

    private:
        OWLSemaphore m_exiting_sem;
        const std::string m_name;
        const std::string m_parent;
        const std::string m_segment;
        const std::string m_partition;
        const std::unique_ptr<AMBridge> m_am_bridge;
        const bool m_is_interactive;
        const unsigned int m_probe_interval;
        const unsigned int m_full_stat_interval;
        mutable Algorithms::SubtransitionMap m_subtr_map;
        mutable infotime_t m_last_is_update;
        chrono::time_point<clock_t> m_transition_start;
        std::unique_ptr<ers::IssueCatcherHandler> m_ers_handler;
        const std::unique_ptr<CommandSender> m_cmd_sender;
        mutable std::shared_mutex m_status_mutex;
        const std::unique_ptr<ItemCtrlStatus> m_status;
        const ControllableDispatcher::ControllableList m_controllables;
        const std::shared_ptr<ControllableDispatcher> m_dispatcher;
        const std::unique_ptr<FSM<FSM_STATE::INITIAL>> m_fsm;
        mutable std::once_flag m_notifier_tp_flag;
        mutable std::unique_ptr<ThreadPool> m_notifier_tp;
        const std::unique_ptr<ThreadPool> m_publisher_tp;
        const std::unique_ptr<ScheduledThreadPool> m_timers_tp;
        const std::unique_ptr<ThreadPool> m_cmd_tp;
        std::shared_ptr<CommandReceiver> m_cmd_rec;

        static const unsigned int DEFAULT_PROBE_INT;
        static const unsigned int DEFAULT_FULL_STATS_INT;
        static std::once_flag m_subtr_init_flag;
};

}}


#endif /* DAQ_RC_ITEMCTRLIMPL_H_ */
