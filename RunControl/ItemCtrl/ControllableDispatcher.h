/*
 * ControllableDispatcher.h
 *
 *  Created on: Jul 14, 2011
 *      Author: avolio
 */

/**
 * @file RunControl/ItemCtrl/ControllableDispatcher.h
 */

#ifndef DAQ_RC_CONTROLLABLEDISPATCHER_H_
#define DAQ_RC_CONTROLLABLEDISPATCHER_H_

#include <list>
#include <functional>
#include <memory>

namespace daq { namespace rc {

class Controllable;

/**
 * @brief Base class for controllable dispatchers.
 *
 * An instance of this class is passed to the daq::rc::ItemCtrl in order to execute actions on multiple daq::rc::Controllable
 * instances following a well defined policy.
 *
 * Derived classes need to implement the daq::rc::ControllableDispatcher::dispatch method which is called any time an action
 * should be executed by the various daq::rc::Controllable instances.
 *
 * The requirements on the daq::rc::ControllableDispatcher::dispatch method are the following:
 * @li It has to return only when all the daq::rc::Controllable instances have done their job;
 * @li Any exception thrown by any action of any daq::rc::Controllable instance should be propagated to the caller.
 *
 * Three default implementations are provided: daq::rc::DefaultDispatcher, daq::rc::InverseDispatcher and daq::rc::ParallelDispatcher.
 */
class ControllableDispatcher {
    public:
        /**
         * @brief Type for a list of daq::rc::Controllable instances.
         */
        typedef std::list<std::shared_ptr<Controllable>> ControllableList;

        /**
         * @brief Type identifying the function to be called on each daq::rc::Controllable instance.
         */
        typedef std::function<void (const std::shared_ptr<Controllable>&)> ControllableCall;

    public:
        /**
         * @brief Destructor.
         */
        virtual ~ControllableDispatcher() {}

        /**
         * @brief Method called any time an action has to be executed by each daq::rc::Controllable.
         *
         *  A possible implementation of this method may be as simple as:
         *  @code{.cpp} std::for_each(ctrlList.begin(), ctrlList.end(), f); @endcode
         *
         * @param ctrlList List of daq::rc::Controllable instances that have to execute the action
         * @param f Action (function) to be executed by each daq::rc::Controllable instance in the @em ctrlList list
         * @param isUpTransition @em true if the command goes towards the daq::rc::FSM_STATE::RUNNING state
         *                       (or does not cause any change in state), otherwise @em false.
         *
         * @throws Any  Any exception thrown by a daq::rc::Controllable instance executing the @em f action
         */
        virtual void dispatch(const ControllableList& ctrlList, const ControllableCall& f, bool isUpTransition) = 0;
};

/**
 * @brief Dispatcher executing commands in a serial way.
 * @see DefaultDispatcher::dispatch for a description of the used policy.
 */
class DefaultDispatcher: public ControllableDispatcher {
    public:
        /**
         * @brief Constructor.
         */
        DefaultDispatcher();

        /**
         * @brief Destructor.
         */
        ~DefaultDispatcher();

        /**
         * @brief Actions on the daq::rc::Controllable instances are called following the same order as they are passed
         *        to the daq::rc::ItemCtrl constructor.
         *
         * @param ctrlList List of daq::rc::Controllable instances that have to execute the action
         * @param f Action (function) to be executed by each daq::rc::Controllable instance in the @em ctrlList list
         * @param isUpTransition @em true if the command goes towards the daq::rc::FSM_STATE::RUNNING state
         *                       (or does not cause any change in state), otherwise @em false.
         *
         * @throws Any  Any exception thrown by a daq::rc::Controllable instance executing the @em f action
         */
        void dispatch(const ControllableList& ctrlList, const ControllableCall& f, bool isUpTransition) override;
};

/**
 * @brief Dispatcher executing commands in a serial way.
 * @see InverseDispatcher::dispatch for a description of the used policy.
 */
class InverseDispatcher: public ControllableDispatcher {
    public:
        /**
         * @brief Constructor.
         */
        InverseDispatcher();

        /**
         * @brief Destructor.
         */
        ~InverseDispatcher();

        /**
         * @brief Actions are executed by the daq::rc::Controllable instances in a serial way.
         *
         * Here is the applied policy:
         * @li @em isUpTransition is @em true, then actions on the daq::rc::Controllable instances are called following the same
         *     order as they are passed to the daq::rc::ItemCtrl constructor;
         * @li @em isUpTransition is @em false, then actions on the daq::rc::Controllable instances are called following the @b reverse
         *     order with respect to the order they are passed to the daq::rc::ItemCtrl constructor.
         *
         * @param ctrlList List of daq::rc::Controllable instances that have to execute the action
         * @param f Action (function) to be executed by each daq::rc::Controllable instance in the @em ctrlList list
         * @param isUpTransition @em true if the command goes towards the daq::rc::FSM_STATE::RUNNING state
         *                       (or does not cause any change in state), otherwise @em false.
         *
         * @throws Any  Any exception thrown by a daq::rc::Controllable instance executing the @em f action
         */
        void dispatch(const ControllableList& ctrlList, const ControllableCall& f, bool isUpTransition) override;
};

/**
 * @brief Dispatcher executing commands in a concurrent way.
 * @see ParallelDispatcher::dispatch for a description of the used policy.
 *
 * @warning This dispatcher uses the @em tbb::parallel_for_each algorithm from the Intel Threading Building Blocks
 *          library. If you already use the @em tbb library in your application (and in particular the @em tbb task scheduler)
 *          then, please, carefully evaluate any possible interference.
 */
class ParallelDispatcher: public ControllableDispatcher {
    public:
        /**
         * @brief Constructor.
         */
        ParallelDispatcher();

        /**
         * @brief Destructor.
         */
        ~ParallelDispatcher();

        /**
         * @brief Actions on the daq::rc::Controllable instances are called concurrently.
         *
         * There is no guarantee on the order the daq::rc::Controllable instances will be called and on
         * the actual level of concurrency (it may change depending on the host HW).
         *
         * @param ctrlList List of daq::rc::Controllable instances that have to execute the action
         * @param f Action (function) to be executed by each daq::rc::Controllable instance in the @em ctrlList list
         * @param isUpTransition @em true if the command goes towards the daq::rc::FSM_STATE::RUNNING state
         *                       (or does not cause any change in state), otherwise @em false.
         *
         * @throws Any  Any exception thrown by a daq::rc::Controllable instance executing the @em f action
         */
        void dispatch(const ControllableList& ctrlList, const ControllableCall& f, bool isUpTransition) override;
};

}}

#endif /* DAQ_RC_CONTROLLABLEDISPATCHER_H_ */
