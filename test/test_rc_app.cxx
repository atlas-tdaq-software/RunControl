// ATLAS Run Controller
//
// Author: Raul Murillo Garcia
// rmg 22.08.07 Only send an IPC command as a test for Run Control Applications.
// Attempting to subscribe to IS and catch the change of the command field 
// to PUBLISH results in random FATALS of the type: 
// FATAL 2007-Aug-22 12:32:07 [int OWLSemaphore::post(...) at owl/src/semaphore.cc:92] Assertion (sem_p) failed because of bad internal state

#include "RunControl/Common/CommandSender.h"
#include "RunControl/Common/Exceptions.h"

#include <ipc/core.h>
#include <tmgr/tmresult.h>
#include <ers/ers.h>
#include <ers/Issue.h>
#include <ers/LocalContext.h>
#include <ipc/exceptions.h>

#include <boost/program_options.hpp>

#include <cstdlib>
#include <iostream>
#include <string>


namespace po = boost::program_options;

int main(int argc, char ** argv) {
    // Initialize IPC
    try {
        IPCCore::init(argc, argv);
    }
    catch(daq::ipc::CannotInitialize& e) {
        ERS_DEBUG(0, "Can not initialize IPC: " << e);
        return daq::tmgr::TmUnresolved;
    }
    catch(daq::ipc::AlreadyInitialized& e) {
        ERS_DEBUG(0, "IPC already initialized: " << e);
    }

    // Parse the command line
    std::string partition = (std::getenv("TDAQ_PARTITION") == nullptr) ? "" : std::getenv("TDAQ_PARTITION");
    std::string name;
    std::string verbosity;

    try {
        po::options_description desc("Send a PUBLISH command to a controller to see whether it responds over IPC.");

        desc.add_options()("partition,p", po::value<std::string>(&partition)->default_value(partition), "Name of the partition (TDAQ_PARTITION)")
                          ("name,n", po::value<std::string>(&name), "Name of the application")
                          ("verbose,v", po::value<std::string>(&verbosity), "Verbosity. Needed by DVS, actually ignored here.")
                          ("help,h", "Print help message");

        po::variables_map vm;
        po::store(po::parse_command_line(argc, argv, desc), vm);
        po::notify(vm);

        if(vm.count("help")) {
            std::cout << desc << std::endl;
            return daq::tmgr::TmUntested;
        }

        if(!vm.count("name")) {
            ers::error(daq::rc::CmdLineError(ERS_HERE, "Missing application name"));
            std::cout << desc << std::endl;
            return EXIT_FAILURE;
        }
    }
    catch(std::exception& ex) {
        ers::error(daq::rc::CmdLineError(ERS_HERE, ex.what(), ex));
        return EXIT_FAILURE;
    }

    daq::rc::CommandSender commander(partition, "cmdln");

    ERS_DEBUG(0, "Sending a ping to RC application " << name << ".");
    try {
        commander.debugLevel(name);
    }
    catch(ers::Issue& ex) {
        ERS_LOG("Can not send command to RC application: " << ex);
        return daq::tmgr::TmFail;
    }

    return daq::tmgr::TmPass;
}
