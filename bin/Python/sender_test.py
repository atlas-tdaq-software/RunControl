from __future__ import print_function

from rcpy import *

command_init = FSM_COMMAND.INITIALIZE
command_stop = FSM_COMMAND.STOP

print("*********************************************************************")
print("Transition Tests")
print("*********************************************************************")
transCmd = TransitionCmd(command_init)
print(transCmd.serialize())
#transCmd.fsmCommand(FSM_COMMAND.START)
#transCmd.fsmSourceState(FSM_STATE.ABSENT)
#transCmd.fsmDestinationState(FSM_STATE.RUNNING)
#transCmd.isRestart(False)
#transCmd.subTransitionsDone(True)
#transCmd.skipSubTransitions(True)

#C = FSM_COMMAND
#command_dict = {C.INIT_FSM:['One','Two','Three'], C.STOPDC:['Four','Five','Six'], C.RESYNCH:['Seven','Eight','Nine']}
#transCmd.subTransitions(command_dict)
#print transCmd.serialize()

print("*********************************************************************")
print("SubTransition Tests")
print("*********************************************************************")
subTransCmd = SubTransitionCmd('SuperPuperSubTransition', command_stop)
print(subTransCmd.serialize())
#subTransCmd.subTransition("subTransition")
#subTransCmd.mainTransitionCmd(C.RELOAD_DB)
#print subTransCmd.serialize()

print("*********************************************************************")
print("Resynch Tests")
print("*********************************************************************")
resynchCmd = ResynchCmd(42, 0, ['1','2','3'])
print(resynchCmd.serialize())
#resynchCmd.ecrCounts(24)
#resynchCmd.modules(['Ein','Zwei','Drei'])
#print resynchCmd.serialize()


print("*********************************************************************")
print("UserCmd Tests")
print("*********************************************************************")
userCmd = UserCmd('UserCmd',['asd','fgh','jkl'])
print(userCmd.serialize())
#userCmd.currentFSMState(FSM_STATE.CONNECTED)
#userCmd.commandName("AnotherCommandName")
#userCmd.commandParameters(['Vier','Funf','Sechs'])
#print userCmd.serialize()

