from __future__ import print_function

from rcpy import Controllable
import ers

def tCmt_print(header, opts):
	D = {'toString()':opts.toString(),
			 'name()':opts.name(),
			 'serialize()':opts.serialize(),
			 'uid':opts.uid(),
			 'isRestart':opts.isRestart(),
			 'fsmCommand':opts.fsmCommand(),
			 'fsmSourceState':opts.fsmSourceState(),
			 'fsmDestinationState':opts.fsmDestinationState(),
			 'subTransitions':opts.subTransitions(),
			 'arguments':opts.arguments()}
	print("--------------------------------------")
	print("======================================")
	print(header)
	print("TRANSITION")
	for i in D:
		print(" -- ", i, " - ", D[i])
	print("======================================")
	print("--------------------------------------")

def stCmt_print(header, opts):
	D = {'toString()':opts.toString(),
			 'name()':opts.name(),
			 'serialize()':opts.serialize(),
			 'uid':opts.uid(),
			 'subTransition':opts.subTransition(),
			 'mainTransitionCmd':opts.mainTransitionCmd()
		}
	print("--------------------------------------")
	print("======================================")
	print(header)
	print("SUB_TRANSITION")
	for i in D:
		print(" -- ", i, " - ", D[i])
	print("======================================")
	print("--------------------------------------")

def uCmt_print(header, opts):
	D = {'toString()':opts.toString(),
			 'name()':opts.name(),
			 'serialize()':opts.serialize(),
			 'uid':opts.uid(),
			 'currentFSMState':opts.currentFSMState(),
			 'commandName':opts.commandName(),
			 'commandParameters':opts.commandParameters()
		}
	print("--------------------------------------")
	print("======================================")
	print(header)
	print("USER_Command")
	for i in D:
		print(" -- ", i, " - ", D[i])
	print("======================================")
	print("--------------------------------------")

def rCmt_print(header, opts):
	D = {'toString()':opts.toString(),
			 'name()':opts.name(),
			 'serialize()':opts.serialize(),
			 'uid':opts.uid(),
			 'ecrCounts':opts.ecrCounts(),
			 'extendedL1ID':opts.extendedL1ID(),
			 'modules':opts.modules()
		}
	print("--------------------------------------")
	print("======================================")
	print(header)
	print("RESYNCH")
	for i in D:
		print(" -- ", i, " - ", D[i])
	print("======================================")
	print("--------------------------------------")

class testClass(Controllable):
	def __init__(self):
		super(testClass, self).__init__()
	#---------------------------------------------------------------------------
	def configure(self, opts):
		tCmt_print("PYTHON - Configure", opts)
	#---------------------------------------------------------------------------
	def prepareForRun(self, opts):
		tCmt_print("PYTHON - PrepareForRun", opts)

	#---------------------------------------------------------------------------
	def stopROIB(self, opts):
		tCmt_print("PYTHON - stopROIB", opts)

	#---------------------------------------------------------------------------
	def unconfigure(self, opts):
		tCmt_print("PYTHON - unconfigure", opts)

	#---------------------------------------------------------------------------
	def subTransition(self, opts):
		stCmt_print("PYTHON - subTransition", opts)
	
	#---------------------------------------------------------------------------
	def resynch(self, opts):
		rCmt_print("PYTHON - resynch", opts)
	
	#---------------------------------------------------------------------------
	def user(self, opts):
		uCmt_print("PYTHON - user", opts)
	
	#---------------------------------------------------------------------------
	def publishFullStats(self):
		print("PYTHON - publishFullStatistics")

	#---------------------------------------------------------------------------
	def publish(self):
		print("PYTHON - publish")
	#---------------------------------------------------------------------------
	def disable(self, opts):
		print("--------------------------------------")
		print("======================================")
		print("PYTHON - disable")
		print(opts)
		print("======================================")
		print("--------------------------------------")
	#---------------------------------------------------------------------------
	def enable(self, opts):
		print("--------------------------------------")
		print("======================================")
		print("PYTHON - enable")
		print(opts)
		print("======================================")
		print("--------------------------------------")
	#---------------------------------------------------------------------------
	def onExit(self, opts):
		print("PYTHON - onExit")
		print(str(opts))
		print(repr(opts))

