/*
 * rc_simple_application.cxx
 *
 *  Created on: Jun 10, 2013
 *      Author: avolio
 */

#include "RunControl/ItemCtrl/ItemCtrl.h"
#include "RunControl/ItemCtrl/ControllableDispatcher.h"
#include "RunControl/Common/RunControlCommands.h"
#include "RunControl/Common/CmdLineParser.h"
#include "RunControl/Common/Controllable.h"
#include "RunControl/Common/OnlineServices.h"
#include "RunControl/Common/Exceptions.h"
#include "RunControl/FSM/FSMStates.h"

#include <ers/ers.h>
#include <ers/Issue.h>
#include <ers/LocalContext.h>
#include <dal/Segment.h>

#include <boost/program_options.hpp>

#include <iostream>
#include <unistd.h>
#include <stdlib.h>
#include <string>
#include <vector>
#include <random>
#include <thread>
#include <chrono>
#include <memory>

namespace {
    ERS_DECLARE_ISSUE(rc, RandomFailure, ERS_EMPTY, ERS_EMPTY)
}

// Simple implementation of the daq::rc::Controllable interface
// At every transition the application just sleeps or, eventually, throws an exception
class MyControllable : public daq::rc::Controllable {
    public:
        MyControllable(unsigned int sleep = 10, bool failingMode = false)
            : daq::rc::Controllable(), m_rndm_gen(std::random_device()()), m_rndm_dist(0, sleep * 1000),
              m_fail(failingMode)
        {
            doIt();
        }

        void configure(const daq::rc::TransitionCmd& cmd) override {
            ERS_LOG("Executing command: " << cmd.toString());

            // Usage of the daq::rc::OnlineServices class
            // Here in the same way the Configuration can be accessed
            ERS_LOG("I am a controllable running for application \"" << daq::rc::OnlineServices::instance().applicationName() <<
                    "\" in segment \"" << daq::rc::OnlineServices::instance().segmentName() << "\"");

            doIt();
        }

        void connect(const daq::rc::TransitionCmd& cmd) override {
            ERS_LOG("Executing command: " << cmd.toString());
            doIt();
        }

        void prepareForRun(const daq::rc::TransitionCmd& cmd) override {
            ERS_LOG("Executing command: " << cmd.toString());
            doIt();
        }

        void stopROIB(const daq::rc::TransitionCmd& cmd) override {
            ERS_LOG("Executing command: " << cmd.toString());
            doIt();
        }

        void stopDC(const daq::rc::TransitionCmd& cmd) override {
            ERS_LOG("Executing command: " << cmd.toString());
            doIt();
        }

        void stopHLT(const daq::rc::TransitionCmd& cmd) override {
            ERS_LOG("Executing command: " << cmd.toString());
            doIt();
        }

        void stopRecording(const daq::rc::TransitionCmd& cmd) override {
            ERS_LOG("Executing command: " << cmd.toString());
            doIt();
        }

        void stopGathering(const daq::rc::TransitionCmd& cmd) override {
            ERS_LOG("Executing command: " << cmd.toString());
            doIt();
        }

        void stopArchiving(const daq::rc::TransitionCmd& cmd) override {
            ERS_LOG("Executing command: " << cmd.toString());
            doIt();
        }

        void disconnect(const daq::rc::TransitionCmd& cmd) override {
            ERS_LOG("Executing command: " << cmd.toString());
            doIt();
        }

        void unconfigure(const daq::rc::TransitionCmd& cmd) override {
            ERS_LOG("Executing command: " << cmd.toString());
            doIt();
        }

        void subTransition(const daq::rc::SubTransitionCmd& cmd) override {
            ERS_LOG("Executing command: " << cmd.toString());
            doIt();
        }

        void resynch(const daq::rc::ResynchCmd& cmd) override {
            ERS_LOG("Executing command: " << cmd.toString());
            doIt();
        }

        void user(const daq::rc::UserCmd& cmd) override {
            ERS_LOG("Executing command " << cmd.toString() << " in state " << cmd.currentFSMState());

            // User command name and arguments
            const std::string& name = cmd.commandName();
            const auto& params = cmd.commandParameters();

            ERS_LOG("Command name is: " << name);
            ERS_LOG("Here are the command parameters:");
            for(const auto& p : params) {
                ERS_LOG(p);
            }

            doIt();
        }

        void publish() override {
        }

        void publishFullStats() override {
        }

        void enable(const std::vector<std::string>&) override {
        }

        void disable(const std::vector<std::string>&) override {
        }

        void onExit(daq::rc::FSM_STATE state) noexcept override {
            ERS_LOG("Exiting while in state \"" << daq::rc::FSMStates::stateToString(state) << "\"");
        }

    protected:
        void doIt() {
            const int ms = m_rndm_dist(m_rndm_gen);

            if((m_fail == true) && (ms < ((m_rndm_dist.b() - m_rndm_dist.a()) / 2))) {
                throw rc::RandomFailure(ERS_HERE);
            } else {
                std::this_thread::sleep_for(std::chrono::milliseconds(ms));
            }
        }

    private:
        std::mt19937 m_rndm_gen;
        std::uniform_int_distribution<> m_rndm_dist;
        const bool m_fail;
};

namespace po = boost::program_options;

int main(int argc, char** argv) {
    po::options_description desc("Simple run-control application sleeping for a random amount of time during state transitions");

    try {
        // Parser for this application's specific options: note that the "help" is not managed here
        unsigned int sleepFor = 10;
        bool failingMode = false;
        desc.add_options()("sleep,S", po::value<unsigned int>(&sleepFor)->default_value(sleepFor), "Maximum sleep time (in seconds)")
                          ("failure,F", "Flag for activating random failures");
        po::variables_map vm;
        po::store(po::command_line_parser(argc, argv).options(desc).allow_unregistered().run(), vm);
        po::notify(vm);

        if(vm.count("failure")) {
            failingMode = true;
        }

        // Parser for the command line options required by the Run Control framework
        // Note that the "help" functionality is enabled
        daq::rc::CmdLineParser cmdParser(argc, argv, true);

        // Here the ItemCtrl is created: the same Controllable will be executed twice in a parallel way
        // Several different Controllable instances may be passed
        // The ParallelDispatcher is just used as an example
        daq::rc::ItemCtrl itemCtrl(cmdParser,
                                   {
                                    std::shared_ptr<daq::rc::Controllable>(new MyControllable(sleepFor, failingMode)),
                                    std::shared_ptr<daq::rc::Controllable>(new MyControllable(sleepFor, failingMode))
                                   },
                                   std::shared_ptr<daq::rc::ControllableDispatcher>(new daq::rc::ParallelDispatcher()));

        // The ItemCtrl is initialized
        itemCtrl.init();

        // Run the ItemCtrl: the application is ready to accept and execute commands
        // The PMG sync is done and the ProcessManagement system will report the process as up
        // This blocks
        itemCtrl.run();
    }
    catch(daq::rc::CmdLineHelp& ex) {
        // Show the help message: note that both messages from the CmdLineParser and the specific parser are reported
        std::cout << desc << std::endl;
        std::cout << ex.message() << std::endl;
    }
    catch(ers::Issue& ex) {
        // Any exception thrown during the ItemCtrl construction or initialization is a fatal error
        ers::fatal(ex);
        return EXIT_FAILURE;
    }
    catch(boost::program_options::error& ex) {
        // This may come from the library used to parse this application's specific options
        ers::fatal(daq::rc::CmdLineError(ERS_HERE, ex.what(), ex));
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

