#include "RunControl/Common/Exceptions.h"
#include "RunControl/Common/OnlineServices.h"
#include "RunControl/FSM/FSMCommands.h"
#include "RunControl/Controller/Application.h"
#include "RunControl/Controller/ApplicationList.h"
#include "RunControl/Controller/ApplicationAlgorithms.h"

#include <ers/ers.h>
#include <ers/Issue.h>
#include <ers/LocalContext.h>

#include <boost/program_options.hpp>

#include <string>
#include <memory>
#include <vector>
#include <iostream>
#include <cstdlib>
#include <iterator>
#include <map>
#include <utility>


namespace po = boost::program_options;
using namespace daq::rc;

int main(int argc, char** argv) {
    std::string partitionName = ((std::getenv("TDAQ_PARTITION") == nullptr) ? "" : std::getenv("TDAQ_PARTITION"));
    std::string dbName = ((std::getenv("TDAQ_DB") == nullptr) ? "" : std::getenv("TDAQ_DB"));
    std::string transitionName = FSMCommands::INIT_FSM_CMD;
    std::string segmentName;

    try {
        po::options_description desc("Helper application unfolding the start dependencies for a given transition");

        desc.add_options()("partition,p",  po::value<std::string>(&partitionName)->default_value(partitionName)    , "Name of the partition (TDAQ_PARTITION)")
                          ("database,d",   po::value<std::string>(&dbName)->default_value(dbName)                  , "Name of the database (TDAQ_DB)")
                          ("segment,s",    po::value<std::string>(&segmentName)                                    , "Name of the segment")
                          ("transition,t", po::value<std::string>(&transitionName)->default_value(transitionName)  , "Name of the transition")
                          ("help,h"                                                                                , "Print help message");

        po::variables_map vm;
        po::store(po::parse_command_line(argc, argv, desc), vm);
        po::notify(vm);

        if(vm.count("help")) {
            std::cout << desc << std::endl;
            return EXIT_SUCCESS;
        }

        if(!vm.count("segment")) {
            throw daq::rc::CmdLineError(ERS_HERE, "The segment name has to be defined as command line parameters!");
        }

        if(dbName.empty() == true) {
            throw daq::rc::CmdLineError(ERS_HERE, "The database name is not defined!");
        } else {
            ::setenv("TDAQ_DB", dbName.c_str(), 1);
        }

        if(partitionName.empty() == true) {
            throw daq::rc::CmdLineError(ERS_HERE, "The partition name is not defined!");
        } else {
            ::setenv("TDAQ_PARTITION", partitionName.c_str(), 1);
        }
    }
    catch(daq::rc::CmdLineError& ex) {
        ers::fatal(ex);
        return EXIT_FAILURE;
    }
    catch(boost::program_options::error& ex) {
        ers::fatal(daq::rc::CmdLineError(ERS_HERE, ex.what(), ex));
        return EXIT_FAILURE;
    }

    try {
        // This is to verify that the passed transition actually exists
        FSMCommands::stringToCommand(transitionName);

        // Use a fake application name to let OnlineServices work
        ::setenv("TDAQ_APPLICATION_NAME", segmentName.c_str(), 0);

        OnlineServices::initialize(partitionName, segmentName);
        OnlineServices& onlSvc = OnlineServices::instance();

        std::shared_ptr<ApplicationList> appList(new ApplicationList());
        appList->load(onlSvc.getConfiguration(),
                      onlSvc.getPartition(),
                      onlSvc.segmentName());

        const std::vector<std::vector<ApplicationType>> appTypes = {{ApplicationType::ONLINE_SEGMENT_INFRASTRUCTURE}, {ApplicationType::INFRASTRUCTURE},
                                                                    {ApplicationType::CHILD_CONTROLLER, ApplicationType::RUN_CONTROL, ApplicationType::STANDARD}};

        for(const auto& types : appTypes) {
            Application::ApplicationSet apps;

            for(ApplicationType type : types) {
                const auto& tmp = appList->getApplications(type, transitionName, true);
                apps.insert(tmp.begin(), tmp.end());
            }

            auto&& ordered_apps = ApplicationAlgorithms::resolveDependencies(apps, true); // this may throw

            // Remember the reverse order!
            const auto& b = ordered_apps.rbegin();
            const auto& e = ordered_apps.rend();
            unsigned int count = 0;
            for(auto it = b; it != e; ++it) {
                std::cout << "--- Application group " << ++count << std::endl;

                std::cout << "\t";
                for(auto itt = it->second.begin(); itt != it->second.end(); ++itt) {
                    std::cout << (*itt)->getName() << " ";
                }

                std::cout << std::endl;
            }
        }
    }
    catch(ers::Issue& ex) {
        ers::fatal(ex);

        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

