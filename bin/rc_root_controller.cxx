/*
 * rc_controller.cxx
 *
 *  Created on: Dec 12, 2012
 *      Author: avolio
 */

#include "RunControl/Controller/Root/RootController.h"
#include "RunControl/Common/CmdLineParser.h"
#include "RunControl/Common/Exceptions.h"

#include <ipc/core.h>
#include <ipc/exceptions.h>

#include <string>
#include <list>
#include <utility>
#include <iostream>

int main(int argc, char** argv) {
    try {
        const daq::rc::CmdLineParser cmdParser(argc, argv, true);
        if(cmdParser.interactiveMode() == true) {
            ERS_LOG("Interactive mode not supported by the RootController");
        } else {
            std::list<std::pair<std::string, std::string>> opt = IPCCore::extractOptions(argc, argv);
            opt.push_front(std::make_pair(std::string("threadPerConnectionPolicy"), std::string("0")));
            opt.push_front(std::make_pair(std::string("maxServerThreadPoolSize"),   std::string("20")));
            opt.push_front(std::make_pair(std::string("threadPoolWatchConnection"), std::string("0")));

            try {
                IPCCore::init(opt);
            }
            catch(daq::ipc::AlreadyInitialized& ex) {
                ers::info(ex);
            }

            daq::rc::RootController::createAndRun(cmdParser.applicationName(),
                                                  cmdParser.partitionName(),
                                                  cmdParser.segmentName());
        }
    }
    catch(daq::rc::CmdLineHelp& ex) {
        std::cout << ex.message() << std::endl;
    }
    catch(ers::Issue& ex) {
        ers::fatal(daq::rc::ControllerCannotStart(ERS_HERE, ex.message(), ex));
        return EXIT_FAILURE;
    }
    catch(std::exception& ex) {
        ers::fatal(daq::rc::ControllerCannotStart(ERS_HERE, ex.what(), ex));
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
