==========
Commands
==========

CommandSender class uses broad range of commands. Here is the list:

    .. class:: RunControlBasicCommand
    
        This is the base class all the other commands derive from
    
        .. method:: toString()
        .. method:: name()
        .. method:: serialize()
        .. method:: uid()
        .. method:: arguments()

    .. class:: TransitionCmd(transition)
    
        :param transition: the transition to be executed
        :type transition: FSM_COMMAND
    
        .. method:: fsmCommand()
        .. method:: fsmSourceState()
        .. method:: fsmDestinationState()
        .. method:: isRestart()
        .. method:: subTransitionsDone()
        .. method:: skipSubTransitions()
        .. method:: subTransitions()


    .. class:: SubTransitionCmd(sub-transition, main-transition) 
    
        :param sub-transition: name of the sub-transition
        :type sub-transition: string
        :param main-transition: the main transition
        :type main-transition: FSM_COMMAND

        .. method:: subTransition()
        .. method:: mainTransitionCmd()
        
    .. class:: ResynchCmd(ecr_counts, modules)
        
        :param integer ecr_counts: number of ECR counts
        :param modules: modules to be re-synchronized
        :type modules: array of strings
        
    .. class:: UserCmd(command_name, command_arguments)
    
       :param string command_name: name of the user-defined command
       :param command_arguments: arguments of the user-defined command
       :type command_arguments: array of strings
       
       .. method:: currentFSMState()
       .. method:: commandName()
       .. method:: commandParameters()
       
    .. class:: ChangeStatusCmd(enable, components)
    
        :param boolean enable: whether the components should be enabled or disabled
        :param components: components to enable or disable
        :type components: array of strings
        
        .. method:: isEnable()
        .. method:: components()
        
    .. class:: StartStopAppCmd(is_start, applications)

       :param is_start: whether applications should be started, stopped or restarted
       :type is_start: is_start_t, is_stop_t or is_restart_t
       :param applications: list of applications
       :type applications: array of strings

       .. method:: applications()
       .. method:: currentState()
       .. method:: destinationState()
       .. method:: transitionCommand()
       .. method:: isRestart()
       .. method:: isStart()
       
    .. class:: TestAppCmd(applications)

       :param applications: list of applications to be tested
       :type applications: array of strings

       .. method:: level()
       .. method:: level(test_level)
       .. method:: scope()
       .. method:: scope(test_scope)
       
All the command classes are wrapper of the corresponding C++ classes. Please, give a look at the C++ documentation
`here <https://atlas-tdaq-sw.web.cern.ch/atlas-tdaq-sw/doxygen/nightly/html/d5/dda/classdaq_1_1rc_1_1RunControlCommands.html>`_
in order to have additional details.


